-- Defines the elements to put in the HUD
-- and their position on the game screen.
-- Negative x or y coordinates mean to measure from the right or bottom
-- of the screen, respectively.

local tlx, tly = 10, 8
local item_x, item_y = 6, 172
local item_radius = 14

local hud_config = {

  -- Health
  {
    menu_script = "scripts/hud/bar_health",
    x = tlx,
    y = tly,
  },

  -- Bullets
  {
    menu_script = "scripts/hud/bullets",
    x = tlx - 5,
    y = tly + 5,
  },

  --Madness
  {
    menu_script = "scripts/hud/bar_madness",
    x = tlx,
    y = tly + 22,
  },

  -- Money counter.
  {
    menu_script = "scripts/hud/money",
    x = 378,
    y = 220,
  },

  -- Equipped Items:
  {
    menu_script = "scripts/hud/item",
    x = item_x + item_radius * 2,
    y = item_y + item_radius,
    slot = "melee",  -- Item slot
  },
  {
    menu_script = "scripts/hud/item",
    x = item_x,
    y = item_y + item_radius,
    slot = "gun",  -- Item slot
  },
  --Item 2
  {
    menu_script = "scripts/hud/item",
    x = item_x + item_radius,
    y = item_y,
    slot = 2,
  },
 -- Item 1
  {
    menu_script = "scripts/hud/item",
    x = item_x + item_radius,
    y = item_y + item_radius * 2,
    slot = 1,  -- Item slot
  },

 -- Items picked-up (used when buying item in shops as well)
  {
    menu_script = "scripts/hud/item_panels",
    id = "item_panels",
    x = -48,
    y = 8,
    duration = 2500,
  },

  --Interaction Icon
  {
    menu_script = "scripts/hud/interact_icon",
    --x = 4, y = 222,
    x = 4, y = 156,
  },

  --Status effects
  {
    menu_script = "scripts/hud/status_effects",
    x = 2, y = 20,
  },

  --Message Queue
  {
    menu_script = "scripts/hud/message_queue",
  },
}

return hud_config
