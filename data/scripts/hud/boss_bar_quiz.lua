--[[
REQUIREMENTS / HOW TO TEST:

put some enemy (preferably either the deputy or deputy_beast boss) on a map, give it a name
Then, in the map's script (probably the on_started event, or maybe on the on_activated event of a sensor), call:

your_test_enemy:start_boss_bar()

The boss bar should appear, and it should go away if you kill the enemy or leave the map
I'm not worried about if the bar should go away if you end up too far from the boss, we're not an open world game. We can add that if it ends up we need it.
--]]

local enemy_meta = sol.main.get_metatable("enemy")
local font, font_size = require("scripts/language_manager"):get_menu_font() --we have a script to get the correct font and size based on the chosen language. Otherwise, we'd have to check language every time we set a font anywhere

local menu = {} --a menu is just a table that we TREAT as a menu, which means the engine will call special callbacks (events) on it, and you can define them. Such as menu:on_draw() (called every frame to let you draw the menu on the screen)

--[[
First we need to set up several drawables that we'll need.
The health bar is like a sandwich, there's the background, then then actual red "how much health" part, then the overlay to make it look nice. So we create sprites for each of those
We also create a "text surface" to display the boss's name. These are special surfaces that display text
Finally, we create a blank surface as an intermediary to draw all of these onto
The big thing with drawing, is you want to draw as little as you need to. Since the bar only changes when the boss's health does, we don't need to re-draw everything every frame.
So we do our draws onto the intermediary surface when we need to, and just draw the intermediary surface every frame. This saves processing.
ALSO, we get the width and height. We could hard code these if we wanted to, but I didn't in case we change the sprites or something. The width is important later.
--]]
menu.bar_background = sol.sprite.create"hud/stat_bars/boss_bar"
menu.bar_background:set_animation("background")
menu.health_bar = sol.sprite.create"hud/stat_bars/red"
menu.bar_overlay = sol.sprite.create"hud/stat_bars/boss_bar"
menu.bar_overlay:set_animation("overlay")
menu.name_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
}
menu.width, menu.height = menu.bar_overlay:get_size()
menu.bar_surface = sol.surface.create(menu.width, menu.height)


--To give any enemy the method to start a boss bar, we'll add a function to the enemy metatable
--NOTE: in order for the progam to know we're writing to the enemy metatable, we need to run this script. We'll do that by requiring it from scripts/features. I've already done that.
function enemy_meta:start_boss_bar()
  --Set this enemy as the menu's enemy, then start the menu
end


function menu:set_enemy(enemy)
  --This function takes in an enemy, and sets it as the menu's actively-measuring-health-of enemy.
  --We'll need a way to reference this enemy whenever we decide how much health to draw

  --Let's also set the name on the boss bar
  --I've put boss name strings under the string key "enemies.tombwater_enemies.bosses.boss_name" - you'll notice this is "enemies." .. [enemy breed]
  --But ALSO, notice the string keys use "." for folders, instead of "/". So you'll need to use the lua string function that replaces characters.
  --Getting the breed and subbing the characters can be done pretty simply in one line.

  local name_string key -- = what?
  menu.name_surface:set_text(sol.language.get_string(name_string_key))
end


function menu:update_life()
  --You'll want to update how much of the life bar is drawn in some function like this.
  --Since the total HP of a boss will vary, we clearly can't do 1 hp = 1 pixel of width. So we need to do some ratio to find current life / total life
  --There is no actual concept of "total life" in the lua API, so you'll need to have saved that somehow
  --So the main thing to calculate is the enemy's percentage of life from it's total

  --This would also be a good place to STOP the menu if the enemy's life goes to 0 or less

  --Here is a good place to draw the elements of the bar, since they only change when the enemy's life is updated.
  --Draw the menu elements to the menu.bar_surface surface - this is that intermediary surface I mentioned earlier
  --First draw the background, then the health_bar (using draw_region), then the bar_overlay.
  --Also draw the name_surface
end


function menu:on_draw(dst)
  --Only draw the menu.bar_surface here, since any changes would have been done to it in menu:update_life()
end

--Technically, if this menu is only started from the enemy:start_boss_bar() function, we don't need to return the menu. However, if we want to access the menu manually, we'll require this script, which will return the menu
return menu
