--HUD Script Steps

-- Usage:
-- require("scripts/hud/hud")

--Get HUD config:
local hud_elements_config = require("scripts/hud/hud_config")

-- Creates and runs a HUD for the specified game.
local function init_hud(game)

  --Create HUD table:
  local hud = {
    elements = {},
    enabled = false,
  }


  --Build HUD Menus:
  for _, config in pairs(hud_elements_config) do
    local element_builder = require(config.menu_script)
    local element_menu = element_builder:new(game, config)
    table.insert(hud.elements, element_menu)
  end


  local function do_to_each_menu(callback)
    for _, element_menu in pairs(hud.elements) do
      callback(element_menu)
    end
  end


  function hud:is_enabled()
    return hud.enabled
  end

  --Return HUD
  function game:get_hud() return hud end


  function hud:set_enabled(enabled_bool)
    if enabled_bool == hud:is_enabled() then return end --return if the hud is already as desired
    hud.enabled = enabled_bool
    if enabled_bool == true then
      do_to_each_menu(function(menu)
        sol.menu.start(game, menu)
      end)
    else
      do_to_each_menu(function(menu)
        sol.menu.stop(menu)
      end)
    end
  end


  function game:set_hud_enabled(enabled_bool)
    hud:set_enabled(enabled_bool)
  end


  local function on_hud_paused(game)
    do_to_each_menu(function(menu)
      if menu.on_paused then menu:on_paused() end
    end)
  end


  local function on_hud_unpaused(game)
    do_to_each_menu(function(menu)
      if menu.on_unpaused then menu:on_unpaused() end
    end)
  end

  game:register_event("on_paused", function() on_hud_paused(game) end)
  game:register_event("on_unpaused", function() on_hud_unpaused(game) end)

  -- Start the HUD.
  hud:set_enabled(true)
end

-- Init HUD when game starts:
local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", init_hud)
