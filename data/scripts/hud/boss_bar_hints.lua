local enemy_meta = sol.main.get_metatable("enemy")
local font, font_size = require("scripts/language_manager"):get_menu_font() --we have a script to get the correct font and size based on the chosen language. Otherwise, we'd have to check language every time we set a font anywhere

local menu = {}

menu.bar_background = sol.sprite.create"hud/stat_bars/boss_bar"
menu.bar_background:set_animation("background")
menu.health_bar = sol.sprite.create"hud/stat_bars/red"
menu.bar_overlay = sol.sprite.create"hud/stat_bars/boss_bar"
menu.bar_overlay:set_animation("overlay")
menu.name_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
}
menu.width, menu.height = menu.bar_overlay:get_size()
menu.bar_surface = sol.surface.create(menu.width, menu.height)


--To give any enemy the method to start a boss bar, we'll add a function to the enemy metatable
--NOTE: in order for the progam to know we're writing to the enemy metatable, we need to run this script. We'll do that by requiring it from scripts/features. I've already done that.
function enemy_meta:start_boss_bar()

end


function menu:set_enemy(enemy)

  local name_string key = sol.language.get_string( "enemies." .. enemy:get_breed())
  print(name_string_key) --did you sub out the . for / 
  menu.name_surface:set_text(sol.language.get_string(name_string_key))

  --I left a couple things out. One is maybe obvious, when you set the enemy you'll need to measure it's life at that point. Then save it to something like menu.max_life
  --The other thing, is you need to start calling menu:update_life() at some point, probably in this function
  --Maybe put it on a timer every 100ms or so that repeats
  --Or you could get fancy, and register an event to that enemy's enemy:on_hurt() event, using the multievents enemy:register_event("on_hurt", function() end) syntax
end


function menu:update_life()
  --You'll calculate the width of the health bar like:
  --local health_width = menu.width * current_life / menu.max_life
  --Then use that as the width to draw the health bar

  --ALSO, you'll need to clear the surface you're drawing on before you draw it. If you draw to a surface twice, it just draws OVER that surface. For the health bar, you won't be able to see it going down if you aren't clearing the menu.bar_surface before drawing to it
end


function menu:on_draw(dst)
  --The hint here is just the answer:
  menu.bar_surface:draw(dst, 8, 208)
end

return menu
