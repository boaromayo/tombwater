-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled or disabled independently by commenting
-- or uncommenting lines below.

local features = {}
--Scripts that need to be called first (other scripts use them):
require"scripts/multi_events"
--
require"scripts/utility/item_name_retriever"
require"scripts/utility/colors"

--Scripts that can be called later:
require("items/charms/charm_manager")
require("items/guns/gun_manager")
require("items/item_helper")
require("entities/particles/helper_functions")
require("entities/events/mad_stagecoach_helper_functions")
require("scripts/controls/controls_manager")
require("scripts/controls/rumble")
require"scripts/action/dash_manager"
require"scripts/action/explosives"
require"scripts/action/fall_manager"
require"scripts/action/hole_drop_landing"
require"scripts/action/jogging_manager"
require"scripts/action/quickswap_manager"
require"scripts/action/swim_manager"
require"scripts/action/unstable_ground_manager"
require"scripts/checkpoint/autosave"
require"scripts/checkpoint/checkpoint_actions"
require"scripts/checkpoint/enemy_respawn_manager"
--require"scripts/coroutine_helper" --removed, GPL
require"scripts/elements/enemy_elemental_meta"
require"scripts/elements/hero_elemental_meta"
require"scripts/elements/map_elemental_meta"
require"scripts/fx/blood_splatter"
require"scripts/fx/cutscene_manager"
require"scripts/fx/fog"
require"scripts/fx/footstep_manager"
require"scripts/fx/lighting/lighting_manager"
require"scripts/fx/lighting/map_lighting"
require"scripts/fx/visual_fx_processing_manager"
require"scripts/fx/white_flash"
require"scripts/gameover/gameover"
require"scripts/gameover/blood_ghost"
require"scripts/hud/boss_bar"
require"scripts/hud/hud"
require"scripts/hud/title_slam"
require"scripts/menus/pause/pause_submenu_manager"
require"scripts/menus/shop/shop_manager"
require"scripts/menus/tutorial/tutorial_popup"
require"scripts/meta/all_entities"
require"scripts/meta/bush"
require"scripts/meta/camera"
require"scripts/meta/chest"
require"scripts/meta/custom_entity"
require"scripts/meta/door"
require"scripts/meta/enemy"
require"scripts/meta/game"
require"scripts/meta/hero"
require"scripts/meta/item"
require"scripts/meta/map"
require"scripts/meta/sensor"
require"scripts/meta/switch"
require"scripts/misc/area_level_manager"
require"scripts/misc/damage_calculations"
require"scripts/misc/level_up_manager"
require"scripts/misc/on_healed_checker"
require"scripts/status_effects/enemy_status_manager"
require"scripts/status_effects/status_manager"
require"scripts/trillium_dialog/dialog_manager"

require("scripts/utility/ammo_tracker").add_to_item_api()
require"scripts/utility/angle_utility"
require"scripts/utility/draw_bounding_boxes"
require"scripts/utility/savegame_tables"
require"scripts/utility/table_duplication"
require"scripts/utility/text_stamper"
--]]

--Some scripts need a game passed to initialize. features.init(game) is called by the game manager, so those scripts are initialized here:
function features.init(game)
  require("scripts/button_inputs"):initialize(game)
  require("scripts/debug_keys"):initialize(game)
  --require("scripts/fx/lighting_effects"):initialize()
  require("scripts/misc/magic_regeneration_manager"):init(game)
end

return features
