--[[
return {
  up = "w",
  down = "s",
  left = "a",
  right = "d",
  action = "f",
  attack = "return",
  item_1 = "q",
  item_2 = "e",
  pause = "escape",
--custom_commands
  dodge = "space",
  special = "left shift",
  aim = "right shift",
  quickswap_melee = "1",
  quickswap_gun = "2",
  quickswap_item_1 = "3",
  quickswap_item_2 = "4",

  --menu_select = "return",
  --menu_cancel = "escape",
}
--]]

--
return {
  up = "up",
  down = "down",
  left = "left",
  right = "right",
  action = "return",
  attack = "c",
  pause = "d",
  item_1 = "x",
  item_2 = "v",

  --custom commands
  dodge = "space",
  fire = "z",
  aim = "left shift",
  quickswap_melee = "1",
  quickswap_gun = "2",
  quickswap_item_1 = "3",
  quickswap_item_2 = "4",

  --menu commands
  --menu_select = "return",
  --menu_cancel = "escape",
} --]]
