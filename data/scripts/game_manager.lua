--Creates a game object:
require("scripts/multi_events")
local game_initializer = require("scripts/initial_game")

local manager = {}


function manager:create(filename)
  -- Create the game
  local exists = sol.game.exists(filename)
  local game = sol.game.load(filename)
  --If the game wasn't loaded from a save file, init the new game:
  if not exists then
    game_initializer:initialize_new_savegame(game)
  end
  --Initialize all scripts that need it
  require("scripts/features").init(game)

  return game
end


return manager

