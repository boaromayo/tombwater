--[[
Created by Max Mraz, licensed MIT
Adapter to interface with whatever button/command binding system is in place.
This manager must contain a function called manager:get_command_sprite_id(command)
The dialog will call manager:get_command_sprite_id(command), which much return a string of dialog sprite information to the dialog system
The sprite information string can be formatted (without animation) like: "path/to/sprite" -OR- (with animation) "path/to/sprite?animation=some_animation"

This implementation is designed to work with the controls_manager script I've written
--]]

local manager = {}
local controls_manager = require("scripts/controls/controls_manager")

function manager:get_command_sprite_id(command)
  --Define this function such that it returns a sprite id (string)(with optional animation argument) based on the command string passed as an argument

  --solarus <= 1.6 compatibility:
  if sol.main.old_controls_version then
    return manager:get_command_sprite_compatibility(command)
  end

  local game = sol.main.get_game()
  local controls_ob = sol.controls.get_main_controls()
  local sprite_id
  local command_display_style = controls_ob.joypad and "joypad" or "keyboard_and_mouse"

  if command_display_style == "joypad" then
    local button_name = controls_ob:get_joypad_binding(command):gsub(" %+", ""):gsub(" %-", "")
    local joypad_type = controls_manager.get_joypad_type() or "generic"
    sprite_id = "hud/button_icons/" .. joypad_type .. "?animation=" .. button_name

  elseif command_display_style == "keyboard" then
    local key = controls_ob:get_keyboard_binding(command):gsub(" ", "_")
    sprite_id = "hud/button_icons/keyboard?animation=" .. key

  else --default to keyboard/mouse display:
    local key = controls_ob:get_keyboard_binding(command):gsub(" ", "_")
    if command == "attack" then key = "mouse_left"
    elseif command == "aim" then key = "mouse_right"
    end
    sprite_id = "hud/button_icons/keyboard?animation=" .. key

  end

  return sprite_id
end










---The below (other than "return manager") is all for compatibility with Solarus <= 1.6

function manager:get_command_sprite_compatibility(command)
  manager.command_binding_manager = require("scripts/misc/command_binding_manager")
  local game = sol.main.get_game()
  local command_display_style = game and game:get_value("option_display_controls_as") or "keyboard_and_mouse"
  local sprite_id

  if command_display_style == "controller" then
    --return menu:get_sprite_id_for_button(command_adapter:get_button_binding(command))
    sprite_id = manager:get_sprite_id_for_button(manager:get_button_binding(command))

  elseif command_display_style == "keyboard" then
    sprite_id = manager:get_sprite_id_for_keyboard(manager:get_key_binding(command))

  else --default to keyboard/mouse display:
    local key = manager:get_kbm_binding(command)
    if command == "attack" then key = "mouse_left"
    elseif command == "aim" then key = "mouse_right"
    end
    sprite_id = manager:get_sprite_id_for_keyboard(key)

  end

  return sprite_id
end


local button_name_conversions = {
  ["button 0"] = "a",
  ["button 1"] = "b",
  ["button 2"] = "x",
  ["button 3"] = "y",
  ["button 4"] = "left_shoulder",
  ["button 5"] = "right_shoulder",
  ["button 6"] = "back", --"select", but it's "share" on Xbox
  ["button 7"] = "start",
  ["button 8"] = "left_stick",
  ["button 9"] = "right_stick",
  ["axis 0 -"] = "axis_left",
  ["axis 0 +"] = "axis_left",
  ["axis 1 -"] = "axis_left",
  ["axis 1 +"] = "axis_left",

  ["axis 3 -"] = "axis_right",
  ["axis 3 +"] = "axis_right",
  ["axis 4 -"] = "axis_right",
  ["axis 4 +"] = "axis_right",

  ["axis 2 +"] = "trigger_left",
  ["axis 2 -"] = "trigger_left",
  ["axis 5 +"] = "trigger_right",
  ["axis 5 -"] = "trigger_right",

  ["hat 0 right"] = "dpad_right",
  ["hat 0 left"] = "dpad_left",
  ["hat 0 up"] = "dpad_up",
  ["hat 0 down"] = "dpad_down",
}


--Controller buttons:
function manager:get_button_binding(command)
  local button = manager.command_binding_manager:get_command_joypad_binding(command)
  --Convert to normal button names (prep for Solarus 1.7):
  local button_name = button_name_conversions[button]
  return button_name
end

function manager:get_sprite_id_for_button(button_name)
  --For now, hardcoded to use "generic" button icons, rather than checking Xbox, PS, or Switch
  --TODO: create some setting to reference, which will determine Xbox, PS, or Switch buttons
  local sprite_id = "hud/button_icons/generic?animation=" .. button_name
  return sprite_id
end


--Keyboard Keys:
function manager:get_key_binding(command)
  local key = manager.command_binding_manager:get_command_keyboard_binding(command):gsub(" ", "_")
  return key
end

function manager:get_kbm_binding(command) --Same as keyboard binding, but has specific overrides for hardcoded mouse commands:
  local key = manager.command_binding_manager:get_command_keyboard_binding(command):gsub(" ", "_")
  if command == "attack" then key = "mouse_left"
  elseif command == "aim" then key = "mouse_right"
  end
  return key
end

function manager:get_sprite_id_for_keyboard(key_name)
  local sprite_id = "hud/button_icons/keyboard?animation=" .. key_name
  return sprite_id
end




return manager
