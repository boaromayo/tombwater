local item_list = {
  "bullet_magnet",
  "bulletbiter",
  "deflect",
  "final_bullet",
  "heavyhook",
  "heavyhook_plus",
  "warrior_health",
  "warrior_bullet",
  "quickstep",
  "flamestep",
  "chillstep",
  "dash_deflect",
  "sharp_dash",
  "wounded_evasion",
  "gradual_heal",
  "gradual_bullet",
  "damage_up_bullet",
  "damage_up_fire",
  "damage_up_ice",
  "damage_up_magic",
  "damage_up_physical",
  "inflict_poison",
  "inflict_burn",
  "inflict_cold",
  "status_damager",
  "treasure_chime",
  "damage_wager",
  "grave_coin",
  "lifecut",
  "madness_cut",
  "madness_butcher",
  "painful_sanity",
  "solid_spell",
  "exp_booster",
  "exp_booster_plus",
  "boom_charm",
  "wealth",
  "deep_draught",
  "revenge_poison",
  "revenge_burn",
  "revenge_cold",
  "unnerving_presence",
  "wounded_restoration",
  "gunsmoke_sharpener",
  "third_eye_blindfold",
  "sacrificial_eye",
  "spawn_ally",
  "spiderwounds",
  "needle_skin",
  "wisp_lantern",
}

local allowed_objects = {}

local i = 1
for _, item in pairs(item_list) do
  local object = {}
  object.name = "charms/" .. item
  object.sprite = "_item"
  object.description_name_string = "items.charms." .. item
  object.description_dialog = "item_descriptions.charms." .. item
  object.description_sprite_animation = "charms/" .. item
  object.display_function = "_item"
  object.text_offset = {x = 6, y = 12}
  object.text_function = function()
    local txt = ""
    if sol.main.get_game():is_charm_equipped("charms/" .. item) then txt = "*" end
    return txt
  end,
  table.insert(allowed_objects, object)
end

local description_panel_config = table.duplicate(require("scripts/menus/inventory/item_description_panel_config"))
description_panel_config.name_surface_offset = {x = 8, y = 65}
description_panel_config.text_surface_y_offset = 76
local description_panel = require("scripts/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local slot_menu = require("scripts/menus/checkpoint/charm_slots")
slot_menu.x, slot_menu.y = 196, 44
local cost_menu = require("scripts/menus/checkpoint/charm_cost")
cost_menu.x, cost_menu.y = 176, 196
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = allowed_objects,
  aux_menus = { description_panel, slot_menu, cost_menu },
}

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

function menu:on_finished()
  local game = sol.main.get_game()
  if menu.parent_menu then
    sol.menu.stop(self)
    sol.menu.start(game, menu.parent_menu)
  else
    game:set_suspended(false)
  end
end


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "confirm" or command == "action" then
    handled = true
    local selected_object = menu:get_selected_object() 
    if not selected_object then return handled end
    local game= sol.main.get_game()
    assert(game.equipped_charms, "game.equipped_charms table is nil - check that items/charms/charm_manager.lua is in place to use the charms menu")

    local item_name = selected_object.name

    if not game.equipped_charms[item_name]then --charm is not equipped
      game.charm_manager:equip_charm(item_name)
    else --charm is already equipped, unequip it
      game.charm_manager:unequip_charm(item_name)
    end
    menu:update_objects()
    menu:notify_aux_menus()
  elseif command == "cancel" or command == "attack" then
    sol.menu.stop(menu)
    handled = true
  end
  return handled
end)

return menu