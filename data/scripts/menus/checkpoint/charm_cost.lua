--[[
Created by Max Mraz, licensed MIT
Shows the cost of a selected charm
--]]
local language_manager = require("scripts/language_manager")
local font, font_size, font_spacing = language_manager:get_menu_font()

local menu = {x=0, y=0}

local surface = sol.surface.create(256, 16)
local cost_label = sol.text_surface.create({
  text_key = "menu.charms.cost",
  font = font, font_size = font_size,
})
cost_label.width, _ = cost_label:get_size()
local slot_sprite = sol.sprite.create("menus/inventory/charm_slot")
slot_sprite:set_animation("used")

function menu:update(cost)
  surface:clear()
  if cost < 1 then return end
  cost_label:draw(surface, 4, 8)
  for i = 1, cost do
    slot_sprite:draw(surface, 12 * i + cost_label.width, 14)
  end
end


function menu:notify(grid_menu)
  local game = sol.main.get_game()
  local sel_ob = grid_menu:get_selected_object()
  if not sel_ob then
    menu:update(0)
    return
  end
  local cost = game:get_item(sel_ob.name).charm_cost
  menu:update(cost)
end


function menu:on_draw(dst)
  surface:draw(dst, menu.x, menu.y)
end

return menu
