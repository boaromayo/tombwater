--[[
Transocculurgy
--]]

local item_list = sol.main.get_items_in_directory("items/spell_eyes")

local allowed_objects = {}

for _, item_id in pairs(item_list) do
  local object = {}
  object.name = item_id
  object.sprite = "_item"
  object.display_function = "_item"
  table.insert(allowed_objects, object)
end

local name_box = require("scripts/menus/collection/name_box").create()
name_box.x, name_box.y = 176, 130

local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = allowed_objects,
  aux_menus = { name_box },
}

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

--Eye illustration:
local eye_tool_surface = sol.surface.load("sprites/menus/checkpoint/transocculurgy_tools.png")


menu:register_event("on_command_pressed", function(self, command)
  local game = sol.main.get_game()
  local handled = false
  if command == "confirm" or command == "action" then
    local sel_ob = menu:get_selected_object()
    if not sel_ob then return end
    local item_suffix = sel_ob.name:match("spell_eyes/([%w_]+)") --TODO, get this
    local spell_item = game:get_item("spells/" .. item_suffix)
    local eye_item = game:get_item(sel_ob.name)
    spell_item:set_variant(1)
    spell_item:show_popup()
    eye_item:set_variant(0)
    menu:update_objects()
    menu:flash_eye()
    handled = true

  elseif command == "cancel" or command == "pause" or command == "attack" or command == "dodge" then
    sol.menu.stop(menu)
    sol.menu.start(game, menu.parent_menu)
    handled = true
  end
  return handled
end)


menu:register_event("on_draw", function(menu, dst)
  if menu.eye_flash then
    menu.fx_surface:clear()
    menu.eye_flash:draw(menu.fx_surface, menu.eye_flash.cx, menu.eye_flash.cy)
  end

  eye_tool_surface:draw(dst, 176, 32)
end)


function menu:flash_eye()
  local eye_flash = sol.sprite.create("menus/checkpoint/spell_eye_flash")
  local cx, cy = menu:get_cursor_coordinates()
  cx = (cx - 1) * (menu.cell_width + menu.cell_spacing) + menu.edge_spacing + (menu.cell_width / 2)
  cy = (cy - 1) * (menu.cell_height + menu.cell_spacing) + menu.edge_spacing + (menu.cell_height - 3)
  eye_flash.cx, eye_flash.cy = cx, cy
  menu.eye_flash = eye_flash
  eye_flash:set_animation("flash", function()
    menu.eye_flash = nil
    menu.fx_surface:clear()
  end)

  sol.audio.play_sound"bell"
  sol.audio.play_sound"chimes-discordant-2"
  sol.audio.play_sound"spells/tentacle_lash"
end



return menu