--[[
Created by Max Mraz, licensed MIT
Basic options menu
--]]

--Create Options
local options_list = {
  "sound_volume",
  "music_volume",
  "fullscreen",
  "film_grain",
  "gameplay_options",
  "control_options",
  --"keybind",
}

local options_with_toggle = {
  fullscreen = true,
  film_grain = true,
}

local allowed_objects = {}
for _, option in pairs(options_list) do
  local object = {}
  object.name = option
  object.text_config = {text_key = "menu.options." .. option}
  object.text_offset = {x = 16, y = 8}

  --Add toggle switch for some options
  if options_with_toggle[option] then
    object.toggleable = true
    if option == "film_grain" then
      object.activation_function = function()
        return sol.main.get_game():get_value("option_film_grain_active") or true --this is temporary, just playing with it really
      end
    elseif option == "fullscreen" then
      object.activation_function = function()
        return sol.video.is_fullscreen()
      end
    end
  end

  table.insert(allowed_objects, object)
end

local config = require("scripts/menus/pause/options_menu_config").get_config({
  grid_size = {columns=1, rows = 6},
})
config.allowed_objects = allowed_objects

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)
--Add toggle feature:
require("scripts/menus/components/toggle_switches").apply_behavior(menu)

local gameplay_options_menu = require("scripts/menus/pause/options_gameplay")
local control_options_menu = require("scripts/menus/pause/options_controls")

local slider_factory = require"scripts/menus/components/slider"
local slider_config = {
  x = 416 / 2 - 108 / 2, y = 96,
  size = {width = 108, height = 16},
  line_length = 100,
  background_png = "menus/slider_background.png",
}
local sound_slider = slider_factory.create(slider_config)
local music_slider = slider_factory.create(slider_config)


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "confirm" or command == "action" then
    local selection = menu:get_selected_object().name
    menu:process_selection(selection)
    handled = true
  end
  return handled
end)


function menu:process_selection(selection)
  if selection == "sound_volume" then
    sound_slider:set_position(sol.audio.get_sound_volume()) --set the slider's initial position
    function sound_slider:process_change(new_level)
      sol.audio.set_sound_volume(new_level)
      sol.audio.play_sound"cursor"
    end
    sol.menu.start(menu, sound_slider)

  elseif selection == "music_volume" then
    music_slider:set_position(sol.audio.get_music_volume()) --set the slider's initial position
    function music_slider:process_change(new_level)
      sol.audio.set_music_volume(new_level)
      sol.audio.play_sound"cursor"
    end
    sol.menu.start(menu, music_slider)

  elseif selection == "gameplay_options" then
    gameplay_options_menu.parent_menu = menu
    sol.menu.start(menu, gameplay_options_menu)

  elseif selection == "control_options" then
    control_options_menu.parent_menu = menu
    sol.menu.start(menu, control_options_menu)

  elseif selection == "fullscreen" then
    local is_fullscreen = sol.video.is_fullscreen()
    sol.video.set_fullscreen(not is_fullscreen)
    menu:get_selected_object():toggle()

  elseif selection == "film_grain" then
    local shader = sol.main.film_grain_shader
    if shader.active then
      shader:set_uniform("strength", 0)
      shader.active = false
    else
      shader:set_uniform("strength", 5)
      shader.active = true
    end
    menu:get_selected_object():toggle()


  end
end


return menu
