local menu = {}

local dark_surface = sol.surface.create()
dark_surface:fill_color{0,0,0,200}

local filigree = sol.surface.create("menus/pause/pause_background.png")

filigree:draw(dark_surface)

function menu:on_draw(dst)
  dark_surface:draw(dst)
end

return menu
