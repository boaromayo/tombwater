--[[
Created by Max Mraz, licensed MIT
Some gameplay, accessibility, and approachability options
--]]

--Create Options
local options_list = {
  "blood",
  "damage_dealt",
  "damage_received",
  "aim_style",
}

local options_with_toggle = {
  blood = true,
}

local allowed_objects = {}
for _, option in pairs(options_list) do
  local object = {}
  object.name = option
  object.text_config = {text_key = "menu.options." .. option}
  object.text_offset = {x = 16, y = 8}

  --Add toggle switch for some options
  if options_with_toggle[option] then
    object.toggleable = true
    if option == "blood" then
      object.activation_function = function()
        return sol.main.get_game():get_value("option_show_blood")
      end
    end
  end

  table.insert(allowed_objects, object)
end

local config = require("scripts/menus/pause/options_menu_config").get_config({
  grid_size = {columns=1, rows=4},
})
config.allowed_objects = allowed_objects

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)
require("scripts/menus/components/toggle_switches").apply_behavior(menu)

--Sliders:
local slider_factory = require"scripts/menus/components/slider"
local common_config = { x = 416 / 2 - 108 / 2, y = 96, bg = "menus/slider_background.png", }
local damage_dealt_slider = slider_factory.create({
  x = common_config.x, y = common_config.y, background_png = common_config.bg,
  size = {width = 108, height = 16},
  line_length = 100,
  increment_size = 5,
})
local damage_received_slider = slider_factory.create({
  x = common_config.x, y = common_config.y, background_png = common_config.bg,
  size = {width = 108, height = 16},
  line_length = 100,
  increment_size = 10,
})


menu:register_event("on_started", function()
  menu:update_displayed_values()
end)


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "confirm" or command == "action" then
    local selection = menu:get_selected_object().name
    menu:process_selection(selection)
    handled = true

  end
  return handled
end)


function menu:update_displayed_values()
  local game = sol.main.get_game()
  for _, ob in pairs(menu.allowed_objects) do
    if (ob.name == "damage_dealt") or (ob.name == "damage_received") then
      ob.text = sol.language.get_string("menu.options." .. ob.name) .. ": " .. game:get_value("option_" .. ob.name .. "_modifier") .. "%"

    elseif ob.name == "aim_style" then
      ob.text = sol.language.get_string("menu.options." .. ob.name) .. ": " .. sol.language.get_string("menu.options.aim_style_" .. game:get_value("aim_style") )
    end
  end
  menu:update_objects()
end


menu:register_event("on_started", function()
  menu:update_displayed_values()
end)


function menu:process_selection(selection)
  if selection == "damage_dealt" then
    local game = sol.main.get_game()
    local initial_modifier = game:get_value("option_damage_dealt_modifier")
    damage_dealt_slider:set_position(initial_modifier / 4) --set the slider's initial position
    function damage_dealt_slider:process_change(new_level)
      game:set_value("option_damage_dealt_modifier", new_level * 4)
      sol.audio.play_sound"cursor"
      menu:update_displayed_values()
    end
    sol.menu.start(menu, damage_dealt_slider)

  elseif selection == "damage_received" then
    local game = sol.main.get_game()
    local initial_modifier = game:get_value("option_damage_received_modifier")
    damage_received_slider:set_position(initial_modifier / 2) --set the slider's initial position
    function damage_received_slider:process_change(new_level)
      game:set_value("option_damage_received_modifier", new_level * 2)
      sol.audio.play_sound"cursor"
      menu:update_displayed_values()
    end
    sol.menu.start(menu, damage_received_slider)

  elseif selection == "blood" then
    local game = sol.main.get_game()
    local should_show = not game:get_value("option_show_blood")
    game:set_value("option_show_blood", should_show)
    menu:get_selected_object():toggle()

  elseif selection == "aim_style" then
    local game = sol.main.get_game()
    local new_thing = game:get_value("aim_style") == "strafe" and "pivot" or "strafe"
    game:set_value("aim_style", new_thing)
    menu:update_displayed_values()

  end
end


return menu

