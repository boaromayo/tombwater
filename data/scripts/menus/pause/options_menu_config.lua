local manager = {}

function manager.get_config(overrides)
  overrides = overrides or {}
  local config = {
    grid_size = {columns=1, rows=5},
    origin = {x = 8, y = 32},
    background_offset = {x = 0, y = -6},
    cell_size = {width=192, height=16},
    cell_spacing = 4,
    edge_spacing = 4,
    background_png = "menus/inventory/options_background.png",
    cursor_style = "menus/arrow",
    cursor_offset = {x=4, y=5},
    cursor_sound = "cursor",
  }
  for k, v in pairs(overrides) do
    config[k] = v
    if v == "" then config[k] = nil end
  end
  return config
end

return manager
