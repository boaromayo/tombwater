local submenus = {
  require("scripts/menus/pause/pause_menu_new"),
  require("scripts/menus/inventory/equipping_screen"),
  require("scripts/menus/collection/categories_menu"),
  require("scripts/menus/pause/options"),
  require("scripts/menus/pause/options_gameplay"),
  require("scripts/menus/pause/options_controls"),
}
local pause_menu = require("scripts/menus/pause/pause_menu_new")
local pause_background = require("scripts/menus/pause/pause_background")


local game_meta = sol.main.get_metatable"game" --remember to require from features.lua

function game_meta:on_paused()
  sol.menu.start(self, pause_background)
  sol.menu.start(self, pause_menu)
end

function game_meta:on_unpaused()
  sol.menu.stop(pause_background)
  sol.menu.stop(pause_menu)
  
end


local function stop_and_unpause(menu)
  local game = sol.main.get_game()
  sol.menu.stop(menu)
  if game and game:is_paused() then game:set_paused(false) end
end


for i, menu in ipairs(submenus) do
  menu:register_event("on_command_pressed", function(menu, command)
    if command == "cancel" then
      local game = sol.main.get_game()
      if menu.parent_menu then
        sol.menu.stop(menu)
        if not sol.menu.is_started(menu.parent_menu) then
          sol.menu.start(game, menu.parent_menu)
        end
      else
        stop_and_unpause(menu)
      end
      handled = true

    elseif command == "pause" then
      if menu.parent_menu and sol.menu.is_started(menu.parent_menu) then
        sol.menu.stop(menu.parent_menu)
      end
      stop_and_unpause(menu)
      handled = true
    end

    return handled
  end)
end


