--[[
Created by Max Mraz, licensed MIT
Controls options menu
--]]

--Create Options
local options_list = {
  --TODO: implement a way to respect controller type, especiallty in menu_input_adapter script
  --"controller_type", --TODO, will set a variable so we know whether to read as Xbox, PS, or Switch controller
  "keybind", --opens keybind menu
  "display_controls_as", --will show button prompts in keyboard, kbm, xbox, PS, or Switch styles
  "disable_right_stick",
  "set_default_controls", --will reset controls to one of several defaults
}

local options_with_toggle = {
  disable_right_stick = true,
}

local allowed_objects = {}
for _, option in pairs(options_list) do
  local object = {}
  object.name = option
  object.text_config = {text_key = "menu.options." .. option}
  object.text_offset = {x = 16, y = 8}

  --Add toggle switch for some options
  if options_with_toggle[option] then
    object.toggleable = true
    if option == "disable_right_stick" then
      object.activation_function = function()
        return sol.main.get_game():get_value("option_disable_right_stick_aiming") or false
      end

    end
  end

  table.insert(allowed_objects, object)
end


local config = require("scripts/menus/pause/options_menu_config").get_config({
  grid_size = {columns=1, rows=4},
})
config.allowed_objects = allowed_objects


local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)
--Apply toggle switches
require("scripts/menus/components/toggle_switches").apply_behavior(menu)
local bind_menu = require("scripts/menus/button_mapping") --This will return the proper menu depending on engine version

--[[
local swiper_factory = require"scripts/menus/components/swiper"
local control_style_swiper = swiper_factory.create({
  x = 8, y = 50,
  background_png = "menus/slider_background.png",
  options = {
    {value = "keyboard", string_key = "menu.options.keyboard"},
    {value = "kbm", string_key = "menu.options.keyboard_and_mouse"},
    {value = "controller", string_key = "menu.options.controller"},
  },
})
function control_style_swiper:get_initial_index()
  local options = control_style_swiper:get_options()

  end
end
function control_style_swiper:on_changed(selection)
  game:set_value("option_display_controls_as", selection)
end
--]]


menu:register_event("on_started", function()
  menu:update_displayed_controls_value()
end)


function menu:update_displayed_controls_value()
  local game = sol.main.get_game()
  for _, ob in pairs(menu.allowed_objects) do
    if (ob.name == "display_controls_as")then
      ob.text = sol.language.get_string("menu.options." .. ob.name) .. ": " .. sol.language.get_string("menu.options." .. game:get_value("option_" .. ob.name))
    end
  end
  menu:update_objects()
end


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "confirm" or command == "action" then
    local selection = menu:get_selected_object().name
    menu:process_selection(selection)
    handled = true

  end
  return handled
end)


function menu:process_selection(selection)
  if selection == "keybind" then
    if not sol.menu.is_started(bind_menu) then
      sol.menu.start(menu, bind_menu)
    end

  elseif selection == "display_controls_as" then
    local game = sol.main.get_game()
    local current_setting = game:get_value("option_display_controls_as")
    local next_setting_matrix = {
      ["controller"] = "keyboard_and_mouse",
      ["keyboard_and_mouse"] = "keyboard",
      ["keyboard"] = "controller",
    }
    game:set_value("option_display_controls_as", next_setting_matrix[current_setting])
    sol.audio.play_sound"cursor"
    menu:update_displayed_controls_value()

  elseif selection == "disable_right_stick" then
    local game = sol.main.get_game()
    local new_thing = not game:get_value("option_disable_right_stick_aiming")
    game:set_value("option_disable_right_stick_aiming", new_thing)
    menu:get_selected_object():toggle()

  elseif selection == "set_default_controls" then
    local game = sol.main.get_game()
    game:start_dialog("menus.options.reset_default_controls", function(answer)
      if answer == 1 then
        if sol.main.old_controls_version then
          require("scripts/misc/command_binding_manager"):reset_defaults()
        else
          require("scripts/controls/controls_manager").reset_default_controls()
          game:start_dialog("menus.options.defaults_reset")
        end
      end
    end)

  elseif selection == "back" then


  end
end


return menu
