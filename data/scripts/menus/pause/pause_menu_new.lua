--[[
Pause menu
Created by Max Mraz, licensed MIT
--]]

local submenus = {
  equipment = require("scripts/menus/inventory/equipping_screen"),
  journal = require("scripts/menus/collection/categories_menu"),
  settings = require("scripts/menus/pause/options"),
}

local options_list = {
  "equipment",
  "journal",
  "settings",
  "save",
}

local allowed_objects = {}
for _, option in pairs(options_list) do
  local object = {}
  object.name = option
  object.text_config = {text_key = "menu.pause." .. option}
  object.text_offset = {x = 16, y = 8}
  table.insert(allowed_objects, object)
end


local config = {
  allowed_objects = allowed_objects,
  origin = {x = 48, y = 28},
  --background_offset = {x = 0, y = -6},
  grid_size = {columns=1, rows=4},
  cell_size = {width=128, height=18},
  cell_spacing = 4,
  edge_spacing = 4,
  cell_png = "menus/checkpoint/choice_cell.png",
  cursor_style = "menus/arrow",
  cursor_offset = {x=4, y=5},
  cursor_sound = "cursor",
}
--Show stats on the right side
local stats_menu = require("scripts/menus/pause/player_stats")
stats_menu.x, stats_menu.y = 216, 32
config.aux_menus = { stats_menu, }

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)


menu:register_event("on_command_pressed", function(menu, command, controls_ob)
  local handled = false

  if command == "confirm" or command == "action" then
    local selection = menu:get_selected_object().name
    menu:process_selection(selection)
    handled = true

  end
  return handled
end)


function menu:process_selection(selection)
  local game = sol.main.get_game()
  if selection == "equipment" or selection == "journal" or selection == "settings" then
    --TEMP: the journal is currently not implemented:
    if selection == "journal" then
      game:start_dialog("temp_doNotTranslate.feature_unavailable")
      return
    end
    local submenu = submenus[selection]
    sol.menu.stop(self)
    submenu.parent_menu = self
    sol.menu.start(game, submenu)

  elseif selection == "save" then
    local game = sol.main.get_game()
    game:save()
    sol.timer.start(game, 100, function()
      sol.main.reset()
    end)

  end
end




return menu
