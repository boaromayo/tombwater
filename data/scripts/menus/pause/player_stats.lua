local font, font_size = require("scripts/language_manager"):get_menu_font()

local menu = { x = 0, y = 0, }

local menu_surface = sol.surface.create()
local background = sol.surface.create("menus/player_stats_background.png")
menu.width, menu.height = background:get_size()

local hp_bar_width, exp_bar_width = 72, 72
local bar_height = 2

--Colors
local dark_grey = sol.colors.ui_grey_dark

--Line ---
local break_line = sol.surface.create(128, 1)
break_line:fill_color(dark_grey)

--HP
local hp_surface = sol.surface.create(menu.width, 48)
local hp_bar_bg = sol.surface.create(hp_bar_width, bar_height)
hp_bar_bg:fill_color(dark_grey)
local hp_bar_red = sol.surface.create(hp_bar_width, bar_height)
hp_bar_red:fill_color{180,0,10}
local hp_text = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

--EXP
local exp_surface = sol.surface.create(menu.width, 48)
local exp_bar_bg = sol.surface.create(exp_bar_width, bar_height)
exp_bar_bg:fill_color(dark_grey)
local exp_bar_fill = sol.surface.create(exp_bar_width, bar_height)
exp_bar_fill:fill_color{0,160,160}
local exp_text = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

--Level
local level_text = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

--Money
local money_text = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

--Stats
local stats_list = {
  "life",
  "lucidity",
  "strength",
  "mind",
  "guns",
  "alchemy",
}
local stat_surface = sol.surface.create(menu.width, 128)



function menu:on_started()
  menu:update()
end


function menu:update()
  local game = sol.main.get_game()
  menu_surface:clear()

  --Set HP
  hp_surface:clear()
  local current_hp = game:get_life()
  local max_hp = game:get_max_life()
  local life_ratio = current_hp / max_hp
  local bar_length = hp_bar_width * life_ratio
  hp_text:set_text(sol.language.get_string("menu.pause.player_stats.hp") .. ": " .. current_hp .. " / " .. max_hp)
  hp_text:draw(hp_surface, 0, 8)
  hp_bar_bg:draw(hp_surface, 0, 16)
  hp_bar_red:draw_region(0, 0, bar_length, bar_height, hp_surface, 0, 16)

  --Set EXP
  exp_surface:clear()
  local current_level = game:get_level()
  local current_exp = game:get_exp()
  local required_exp = game:get_required_exp(current_level + 1)
  local bar_length = current_exp / required_exp * exp_bar_width
  exp_text:set_text(sol.language.get_string("menu.pause.player_stats.exp") .. ": " .. current_exp .. " / " .. required_exp)
  exp_text:draw(exp_surface, 0, 8)
  exp_bar_bg:draw(exp_surface, 0, 16)
  exp_bar_fill:draw_region(0, 0, bar_length, bar_height, exp_surface, 0, 16)
  --Level
  level_text:set_text(sol.language.get_string("menu.pause.player_stats.level") .. ": " .. current_level)

  --Set money:
  money_text:set_text("$" .. game:get_money())

  --Set stats
  stat_surface:clear()
  for i, stat in ipairs(stats_list) do
    local stat_text = sol.text_surface.create{ font = font, font_size = font_size, }
    stat_text:set_text_key("menu.level_up.stat." .. stat)
    local stat_value = sol.text_surface.create{
      font = font, font_size = font_size,
      text = game:get_value("stat_" .. stat) or 0,
    }
    local line = sol.surface.create(128, 1)
    line:fill_color(dark_grey)
    local offset = (i - 1) * 10
    stat_text:draw(stat_surface, 8, 8 + offset)
    stat_value:draw(stat_surface, menu.width - 32, 8 + offset)
  end


  background:draw(menu_surface)
  level_text:draw(menu_surface, 8, 14)
  money_text:draw(menu_surface, 8, 27)
  break_line:draw(menu_surface, 8, 36)
  hp_surface:draw(menu_surface, 8, 40)
  exp_surface:draw(menu_surface, 8, 58)
  break_line:draw(menu_surface, 8, 82)
  stat_surface:draw(menu_surface, 0, 90)
end


function menu:notify()
  --menu:update()
end

function menu:on_draw(dst)
  menu_surface:draw(dst, menu.x, menu.y)
end

return menu
