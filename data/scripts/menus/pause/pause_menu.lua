local sub_menus = {
  "collection/categories_menu",
  "inventory/equipping_screen",
  "pause/options",
}

local category_icons = {
  ["pause/options"] = "options",
  ["inventory/equipping_screen"] = "weapons",
  ["collection/categories_menu"] = "journal",
}

local categories = {}

for i = 1, #sub_menus do
  local cat = {}
  cat.menu = require("scripts/menus/" .. sub_menus[i] )
  --cat.name = sol.language.get_string("menu.menu_names." .. sub_menus[i])
  cat.icon = "menus/inventory/category_" .. category_icons[sub_menus[i]]
  categories[i] = cat
end

local width, height = 192, 24
local menu = require("scripts/menus/trillium_menu_lib/category_selection_menu").create{
  categories = categories,
  origin = {x= 416 / 2 - width / 2, y = 8},
  size = {width = width, height = height},
  category_offset = {x = 0, y = -4},
  category_padding = 16,
  --background_png = "menus/inventory/category_selection_background.png",
  background_9slice_config = {
    source_png = "menus/panel_blocks/arrow_bar.png", tile_width = 16, tile_height = 8,
    width = width, height = height,
  }, --]]
  --background_color = {0,0,0,200},
  cursor_sound = "cursor_category",
}

menu.cursor_index = 2

--Add pause menu as context to use menu:start_submenu
for _, cat in pairs(categories) do
  cat.menu.context = menu
end

local dark_surface = sol.surface.create()
dark_surface:fill_color{0,0,0,200}
local geometric = sol.surface.create("menus/pause/pause_background.png")

--Make this the pause menu:
--This method means this menu needs to be required in features.lua to do itself
local game_meta = sol.main.get_metatable"game"

function game_meta:on_paused()
  sol.menu.start(self, menu)
end

function game_meta:on_unpaused()
  sol.menu.stop(menu)
end


menu:register_event("on_started", function()

end)


function menu:close()
  sol.menu.stop(menu)
end

menu:register_event("on_command_pressed", function(menu, command)
  local handled = false
  if command == "pause" then
    menu:close()
    handled = false --you can't handle this command, or else the command won't propogate to the game and unsuspend the game
  elseif command == "cancel" then
    menu:close()
    local game = sol.main.get_game()
    if game and game:is_paused() then game:set_paused(false) end --Is there a better way to do this?
    handled = false
  elseif command == "submenu_left" or command == "submenu_right" then
    menu:scroll(command:gsub("submenu_", ""))
    handled = true
  end
  return handled
end)


--NOTE: we override the "on_draw" totally because we want to draw this dark background and it would, otherwise, be drawn _over_ the menu lol
function menu:on_draw(dst)
  dark_surface:draw(dst)
  geometric:draw(dst)
  local menu = self
  menu.background_surface:draw(dst, menu.origin_x + menu.background_offset.x, menu.origin_y + menu.background_offset.y)
  menu.categories_surface:draw(dst, menu.origin_x, menu.origin_y)
  --This is a bit hacky, but
  if menu.active_submenu.unfocused and menu.cursor_line:get_opacity() == 0 then
    menu.cursor_line:set_opacity(255)
    menu:update()
  end
end


return menu
