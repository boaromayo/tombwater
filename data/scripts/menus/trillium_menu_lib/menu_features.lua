--[[
Written by Max Mraz, licensed MIT

Use this script to apply other features to all grid menus and category selection menus
These functions will be called just before the menu is returned, after it's been set up
--]]

local manager = {}


function manager.apply_grid_menu_features(menu)

end


function manager.apply_category_menu_features(menu)

end


function manager.apply_menu_features(menu)
  --Apply menu input adapter
  require("scripts/menus/menu_input_adapter").apply(menu)

end



return manager

