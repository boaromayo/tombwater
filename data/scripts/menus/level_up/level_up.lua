local language_manager = require"scripts/language_manager"
local font, font_size = language_manager:get_menu_font()

local origin_x, origin_y = 12, 43
local stat_width = 176

--Exp requirement formula:
--With skill points, this is never used
local function required_exp(current_level)
  return 1 --you only ever need 1 skill point to level up, exp -> sp is balanced elsewhere
end

local function life_increase(current_level)
  return 10
end

--Config:
local config = {
  origin = {x=origin_x, y=origin_y},
  grid_size = {columns = 1, rows = 6},
  cell_size = {width = stat_width, height = 16},
  cell_spacing = 4,
  edge_spacing = 8,
  cursor_style = "menus/arrow",
  cursor_offset = {x=-8, y=3},
  cursor_sound = "cursor",
  background_png = "menus/level_up_background.png",
  background_offset = {x=0, y=-3},
}

--Set up stats:
local stats_list = {
  "life",
  "lucidity",
  "strength",
  "mind",
  "guns",
  "alchemy",
}
local stats_max = {
  life = 20,
  lucidity = 20,
  strength = 20,
  mind = 20,
  guns = 20,
  alchemy = 20,
}
local allowed_objects = {}
for i, stat in pairs(stats_list) do
  local ob = {}
  ob.name = stat
  ob.text_config = {
    font = font,
    font_size = font_size,
    vertical_alignment = "top",
    text_key = "menu.level_up.stat." .. stat,
  }
  ob.text_offset = {x=4, y=0}
  allowed_objects[i] = ob
end
config.allowed_objects = allowed_objects

--Set up aux menus:
local stat_description_panel = require("scripts/menus/level_up/stat_description")
stat_description_panel.x, stat_description_panel.y = origin_x + 12, origin_y + 130

config.aux_menus = {
  stat_description_panel,
}

local menu = require("scripts/menus/trillium_menu_lib/grid_menu").create(config)

local requirements_surface = sol.surface.create()
local requirement_icon = sol.sprite.create"entities/items"
requirement_icon:set_animation("collectibles/soul_crystal")
local requirement_txt = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

menu:register_event("on_started", function()
  local game = sol.main.get_game()
  game:set_suspended()
  menu:update_stat_display()
end)


function menu:on_finished()
  local game = sol.main.get_game()
  if menu.parent_menu then
    sol.menu.stop(self)
    sol.menu.start(game, menu.parent_menu)
  else
    game:set_suspended(false)
  end
end


function menu:update_stat_display()
  local game = sol.main.get_game()
  --Set stat levels
  for _, stat_ob in pairs(menu.allowed_objects) do
    local stat = stat_ob.name
    local stat_level = game:get_value("stat_" .. stat) or 0
    local predicted_size = sol.text_surface.get_predicted_size(font, font_size, sol.language.get_string("menu.level_up.stat." .. stat) )
    local num_spaces = math.floor((stat_width - predicted_size) / 6)
    local final_text = sol.language.get_string("menu.level_up.stat." .. stat)
    for i = 1, num_spaces do
      final_text = final_text .. " "
    end
    final_text = final_text .. stat_level
    stat_ob.text_config.text = final_text
    if stat_level >= stats_max[stat] then
      --ideally, do some color modulation to indicate its at max level
    end
  end
  --Update stat text
  menu:update_objects()

  --Set levelup exp requirement
  requirements_surface:clear()
  requirement_icon:draw(requirements_surface, 16, 13)
  local hero_level = game:get_value("hero_level") or 1
  local current_exp = game:get_value("hero_skill_points") or 0
  local req_txt = sol.language.get_string("menu.level_up.requirement")
  requirement_txt:set_text(req_txt .. " " .. current_exp )
  requirement_txt:draw(requirements_surface, 24, 8)
end


menu:register_event("on_draw", function(self, dst)
  requirements_surface:draw(dst, origin_x, origin_y + 152)
end)


menu:register_event("on_command_pressed", function(self, command)
  local game = sol.main.get_game()
  local handled = false

  if command == "action" or command == "confirm" then
    menu:process_stat_increase()
    handled = true
--[[
  elseif command == "right" or command == "left" or command == "up" or command == "down" then
    handled = false
--]]
  --elseif command == "attack" or command == "pause" or command == "dodge" or command == "item_1" or command == "item_2" then
  else
    sol.menu.stop(menu)
    handled = true
  end
  return handled
end)


function menu:process_stat_increase()
  local game = sol.main.get_game()
  local stat = menu:get_selected_object().name
  local stat_save_id = "stat_" .. stat
  local stat_string = sol.language.get_string("menu.level_up.stat." .. stat)
  local current_level = game:get_value(stat_save_id) or 0
  local hero_level = game:get_value("hero_level") or 1
  --Check stat caps
  if current_level >= stats_max[stat] then
    sol.audio.play_sound"wrong"
    return
  end
  --Check if you have enough exp
  local exp_cost = required_exp(hero_level)
  local current_exp = game:get_value("hero_skill_points") or 0
  if current_exp < exp_cost then
    sol.audio.play_sound"wrong"
    return
  end
  --Ask player for confirmation
  game:start_dialog("menus.level_up.confirm_level", {
      v1 = stat_string,
    }, function(answer)
    if answer == 2 then return end
    game:set_value("hero_skill_points", current_exp - exp_cost)

    game:set_value(stat_save_id, current_level + 1)
    game:set_value("hero_level", hero_level + 1)
    menu:update_stat_display()

    if stat == "life" then
      game:add_max_life(life_increase(current_level))
      game:set_life(game:get_max_life())
    elseif stat == "magic" then
      game:set_max_magic(game:get_max_magic() + 10)
      game:set_magic(game:get_max_magic())
    elseif stat == "lucidity" then
      local current_resistance = game:get_value("status_resistance_madness") or 100
      game:set_value("status_resistance_madness", current_resistance + 20)
    end
  end)
end


return menu
