--Create inventory equipment menu
local item_list = {
  "inventory/healing_flask",
--=======
  "consumables/cure_poison",
  "consumables/cure_burn",
  "consumables/silver_bitters",
  "consumables/remedial_mushroom",
  "consumables/pickled_chicken_foot",
--=======
  "potions/increase_strength",
  "potions/increase_guns",
  "potions/defense_type_physical",
  "potions/defense_type_bullet",
  "potions/vengence",
  "potions/resistance_poison",
  "potions/resistance_burn",
  "potions/regen_health",
  "potions/madness_recovery",
}

local allowed_objects = {}

for _, weapon in pairs(item_list) do
  local object = {}
  object.name = weapon
  object.sprite = "_item"
  object.display_function = "_item"
  local wep_string = string.gsub(weapon, "/", ".")
  object.description_name_string = "items." .. wep_string
  object.description_dialog = "item_descriptions." .. weapon:gsub("/", ".")
  object.description_sprite = "entities/items"
  object.description_sprite_animation = weapon
  object.text_function = function()
    local equip_item = sol.main.get_game():get_item(object.name)
    if equip_item and equip_item:has_amount() then
      return equip_item:get_amount()
    end
  end
  object.text_offset = { x = 16, y = 24}
  object.text_config = {
    font = "white_digits",
  }
  table.insert(allowed_objects, object)
end

local description_panel_config = require("scripts/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = allowed_objects,
  aux_menus = { description_panel },
}

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)


function menu:set_active_item_slot(slot)
  menu.item_slot = slot
end


function menu:assign_item(slot, item)
  local game = sol.main.get_game()
  local other_slot = (slot == 1 and 2) or 1
  if game:get_item_assigned(other_slot) == item then
    local other_item = game:get_item_assigned(slot)
    game:set_item_assigned(slot, item)
    game:set_item_assigned(other_slot, other_item)
  else
    game:set_item_assigned(slot, item)
  end
end


menu:register_event("on_command_pressed", function(self, command)
  --Add control behaviour here.
  local game= sol.main.get_game()
  local handled = false
  if command == "action" or command == "confirm" then
    if not menu:get_selected_object() then return end
    handled = true
    local item_id = menu:get_selected_object().name
    --If item is alread selected, unequip
    if item_id == game:get_quickswap_item(menu.quickswap_category, menu.item_slot) then
      game:set_quickswap_item(menu.quickswap_category, menu.item_slot, nil)
    else --otherwise, equip to slot
      game:set_quickswap_item(menu.quickswap_category, menu.item_slot, item_id)
    end    
    sol.audio.play_sound"equip_item"
    sol.menu.stop(menu)
  elseif command == "cancel" or command == "item_1" or command == "item_2" or command == "dash" or command == "pause" then
    handled = true
    --menu:quit_to_parent()
    sol.menu.stop(menu)
  end
  return handled
end)


return menu
