--Create inventory equipment menu
local item_list = {
  "guns/pistol",
  "guns/pistol_viper",
  "guns/pistol_ivory",
  "guns/pistol_magic",
  "guns/zeliska",
  "guns/wheellock_carbine",
  "guns/shotgun",
  "guns/cannon_carbine",
  "guns/shotgun_fire",
  "guns/shotgun_boreal",
  "guns/rifle",
  "guns/rifle_powered",
  "guns/sniper",
  "guns/arquebus",
  "guns/edison_cannon",
  "guns/harpoon",
  "guns/grenade_launcher",
  "guns/cannon",
  "guns/gatling",
  "guns/oilsprayer",
}

local allowed_objects = {}

for _, weapon in pairs(item_list) do
  local object = {}
  object.name = weapon
  object.sprite = "_item"
  object.display_function = "_item"
  local wep_string = string.gsub(weapon, "/", ".")
  object.description_name_string = "items." .. wep_string
  object.description_dialog = "item_descriptions.guns." .. weapon:gsub("guns/", "")
  object.description_sprite = "entities/items"
  object.description_sprite_animation = weapon
  object.text_function = function()
    local equip_item = sol.main.get_game():get_item(object.name)
    if equip_item and equip_item:has_amount() then
      return equip_item:get_amount()
    end
  end
  object.text_offset = { x = 16, y = 24}
  object.text_config = {
    font = "white_digits",
  }
  table.insert(allowed_objects, object)
end

--Damage aux menu:
local damage_panel = require("scripts/menus/inventory/damage_panel").create({
  origin = {x = 220, y = 16},
})
damage_panel.damage_function = function(object)
  local gun_tables = require("items/guns/gun_tables")
  if not object then return nil end
  return gun_tables[object.name].projectile_properties.damage or gun_tables[object.name].damage or "ERROR"
end

local description_panel_config = require("scripts/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = allowed_objects,
  aux_menus = { description_panel, damage_panel },
  cursor_edge_behavior = "increment",
}

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)


menu:register_event("on_command_pressed", function(self, command)
  --Add control behaviour here.
  local game= sol.main.get_game()
  local handled = false
  if command == "action" or command == "confirm" then
    if not menu:get_selected_object() then return end
    local item_id = menu:get_selected_object().name
    --If item is alread selected, unequip
    if item_id == game:get_quickswap_item(menu.quickswap_category, menu.item_slot) then
      game:set_quickswap_item(menu.quickswap_category, menu.item_slot, nil)
    else --otherwise, equip to slot
      game:set_quickswap_item(menu.quickswap_category, menu.item_slot, item_id)
    end    
    sol.audio.play_sound"equip_gun"
    --menu:quit_to_parent()
    sol.menu.stop(menu)
    handled = true
  elseif command == "cancel" or command == "item_1" or command == "item_2" or command == "dash" or command == "pause" then
    handled = true
    --menu:quit_to_parent()
    sol.menu.stop(menu)
  end
  return handled
end)

return menu