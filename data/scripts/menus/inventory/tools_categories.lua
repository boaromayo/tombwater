local sub_menus = {
  "tools_projectiles",
  "tools_spells",
  "tools_other",
}

local categories = {}

for i = 1, #sub_menus do
  local cat = {}
  cat.menu = require("scripts/menus/inventory/" .. sub_menus[i] )
  --cat.name = sub_menus[i]
  cat.icon = "menus/inventory/category_" .. sub_menus[i]
  categories[i] = cat
end

local menu = require("scripts/menus/trillium_menu_lib/category_selection_menu").create{
  categories = categories,
  origin = {x=116, y = 16},
  size = {width = 184, height = 16},
  category_padding = 4,
  background_png = "menus/inventory/category_selection_background.png",
  cursor_sound = "cursor_category",
}


--Pass along item slot and quickswap category to each category grid menu:
menu:register_event("on_started", function()
  for _, cat in pairs(sub_menus) do
    local submenu = require("scripts/menus/inventory/" .. cat )
    --submenu.item_slot = menu.item_slot
    --submenu.quickswap_category = menu.quickswap_category

    submenu:register_event("on_command_pressed", function(self, command)
      --Add control behaviour here.
      local game= sol.main.get_game()
      local handled = false
      if command == "action" or command == "confirm" then
        if not submenu:get_selected_object() then return end
        handled = true
        local item_id = submenu:get_selected_object().name
        --If item is alread selected, unequip
        if item_id == game:get_quickswap_item(menu.quickswap_category, menu.item_slot) then
          game:set_quickswap_item(menu.quickswap_category, menu.item_slot, nil)
        else --otherwise, equip to slot
          game:set_quickswap_item(menu.quickswap_category, menu.item_slot, item_id)
        end    
        sol.audio.play_sound"equip_item"
        --menu:quit_to_parent()
        sol.menu.stop(menu)
      elseif command == "submenu_left" or command == "submenu_right" then
        menu:scroll(command:gsub("submenu_", ""))
        handled = true
      elseif command == "cancel" or command == "item_1" or command == "item_2" or command == "dash" or command == "pause" then
        handled = true
        --menu:quit_to_parent()
        sol.menu.stop(menu)
      end
      return handled
    end)

  end

end)


return menu
