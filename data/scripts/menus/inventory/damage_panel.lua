--Shows how much damage a weapon deals
-- Licensed under MIT. Authored by Max Mraz.

local multi_events = require"scripts/multi_events"
local default_settings = require("scripts/menus/trillium_menu_lib/default_settings")

local factory = {}

local menu_prototype = {}
multi_events:enable(menu_prototype)

local menu_metatable = {__index = menu_prototype}


menu_prototype:register_event("on_started", function(self)
  local menu = self

  menu.description_surface = sol.surface.create(menu.width, menu.height)
  menu.text_surface = sol.text_surface.create{ font = menu.font, font_size = menu.font_size, color = menu.font_color}

end)


function menu_prototype:notify(grid_menu)
  local menu = self
  local object = grid_menu:get_selected_object()
  menu.damage = menu.damage_function(object)
  menu:update()
end


function menu_prototype:update()
  local menu = self
  menu.description_surface:clear()
  if menu.damage then
    menu.text_surface:set_text(sol.language.get_string("menu.labels.base_damage") .. " " .. menu.damage)
  else
    menu.text_surface:set_text("")
  end
  menu.text_surface:draw(menu.description_surface, 0, menu.text_surface_y_offset)
end


menu_prototype:register_event("on_draw", function(self, screen)
  self.description_surface:draw(screen, self.origin_x, self.origin_y)
end)


function factory.create(config)
    local menu = {}
    menu.width = config.panel_size and config.panel_size.width or 192
    menu.height = config.panel_size and config.panel_size.height or 200
    menu.origin_x = config.origin and config.origin.x or 30
    menu.origin_y = config.origin and config.origin.y or 40
    menu.text_surface_width = config.text_surface_size and config.text_surface_size.width or 192
    menu.text_surface_height = config.text_surface_size and config.text_surface_size.height or 56
    menu.sprite_surface_y_offset = config.sprite_surface_y_offset or 0
    menu.text_surface_y_offset = config.text_surface_y_offset or 56
    menu.font = config.font or default_settings.font
    menu.font_size = config.font_size or default_settings.font_size
    menu.font_color = config.font_color or default_settings.font_color
    menu.text_margin = config.text_margin or 4
    menu.background_color = config.background_color or {0,0,0,0}
    menu.background_png = config.background_png

    setmetatable(menu, menu_metatable)

    return menu
end

return factory