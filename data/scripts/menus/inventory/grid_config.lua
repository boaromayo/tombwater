--cell width is width * columns + cell_spacing * (columns - 1) + edge spacing + 2
--WIDTH (32x32 cells, 4 spacing, 4 edge)(5 cells wide): 

local manager = {}

function manager.get_config(overrides)
  local config = {
    origin = {x = 25, y = 38},
    grid_size = {columns=4, rows=4},
    cell_size = {width=32, height=32},
    cell_spacing = 4,
    edge_spacing = 4,
    background_png = "menus/inventory/grid_background.png",
    background_offset = {x = -4, y = -6},
    cell_png = "menus/inventory/circle_cell.png",
    cursor_sound = "cursor",
    --category_scroll_lock = true,
    --cursor_edge_behavior = "increment",
    --cell_color = so.colors.ui_grey_dark
    --category_scroll_lock = true,
  }
  for k, v in pairs(overrides) do
    config[k] = v
    if v == "" then config[k] = nil end
  end
  return config
end

return manager
