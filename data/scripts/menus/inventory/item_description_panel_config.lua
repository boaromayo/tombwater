local language_manager = require("scripts/language_manager")

local font, font_size, font_spacing = language_manager:get_menu_font()
return {
  panel_size = {width=256, height=186},
  origin = {x = 172, y = 32},
  name_surface_offset = {x = 48, y = 20},
  sprite_surface_size = {width = width, height = 48},
  text_surface_size = {width = 256, height = 256},
  sprite_surface_y_offset = -20,
  text_surface_y_offset = 72,
  text_margin = 8,
  font = font,
  font_size = font_size,
  font_color = sol.colors.ui_off_white,
  background_png = "menus/inventory/description_panel_background.png",
}