--Create weapon objects
local weapon_list = {
  "cavalry_sabre",
  "sabre_rusty",
  "jian",
  "fire_sword",
  "vampire_sword",
  "sawblade",
  "barbed_wire_sword",
  "basic_dagger",
  "hunters_dagger",
  "railroad_spike",
  "bullet_knife",
  "coil_baton",
  "surgeons_knife",
  "frost_dagger",
  "obsidian_knife",
  "shovel",
  "timber_axe",
  "pickaxe",
  "anchor",
  "conquistador_axe",
  "greatsword",
  "augur_drillbit",
  "streetlamp",
  "cactus_bat",
  "holy_greatsword",
  "pitchfork",
  "branding_iron",
  "liturgical_staff",
  "whip",
}

local allowed_objects = {}

local i = 1
for _, weapon in pairs(weapon_list) do
  local object = {}
  object.name = "solforge/" .. weapon
  object.sprite = "_item"
  --object.sprite_shader = "outline"
  --object.text_offset = {x = 8, y = 8}
  object.description_name_string = "items.solforge." .. weapon
  object.description_dialog = "item_descriptions.weapons." .. weapon
  object.description_sprite = "entities/items"
  object.description_sprite_animation = "solforge/" .. weapon
  object.display_function = "_item"
  table.insert(allowed_objects, object)
end

--Damage aux menu:
local damage_panel = require("scripts/menus/inventory/damage_panel").create({
  origin = {x = 220, y = 16},
})
damage_panel.damage_function = function(object)
  local game = sol.main.get_game()
  if not object then return nil end
  return game:get_value(object.name:gsub("/", "_") .. "_attack_power")
end

local description_panel_config = require("scripts/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = allowed_objects,
  aux_menus = { description_panel, damage_panel },
  cursor_edge_behavior = "increment",
}

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

menu.cute_name = "weapons"

menu:register_event("on_command_pressed", function(self, command)
  --Add control behaviour here.
  local game= sol.main.get_game()
  local handled = false
  if command == "action" or command == "confirm" then
    if not menu:get_selected_object() then return end
    local item_id = menu:get_selected_object().name
    --If item is alread selected, unequip
    if item_id == game:get_quickswap_item(menu.quickswap_category, menu.item_slot) then
      game:set_quickswap_item(menu.quickswap_category, menu.item_slot, nil)
    else --otherwise, equip to slot
      game:set_quickswap_item(menu.quickswap_category, menu.item_slot, item_id)
    end    
    sol.audio.play_sound"equip_weapon"
    --menu:quit_to_parent()
    sol.menu.stop(menu)
    handled = true
  elseif command == "cancel" or command == "item_1" or command == "item_2" or command == "dash" or command == "pause" then
    handled = true
    --menu:quit_to_parent()
    sol.menu.stop(menu)
  end
  return handled
end)

return menu