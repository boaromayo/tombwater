local choices_list = {
  {category = "melee", slot = 1},
  {category = "melee", slot = 2},
  {category = "melee", slot = 3},
  {category = "gun", slot = 1},
  {category = "gun", slot = 2},
  {category = "gun", slot = 3},
  {category = "item_2", slot = 1},
  {category = "item_2", slot = 2},
  {category = "item_2", slot = 3},
  {category = "item_1", slot = 1},
  {category = "item_1", slot = 2},
  {category = "item_1", slot = 3},
}

local submenu_key = {
  melee = "weapons",
  gun = "guns",
  item_2 = "tools_potions",
  item_1 = "tools_categories",
}

local allowed_objects = {}

for _, choice in pairs(choices_list) do
  local object = {}
  object.category = choice.category
  object.slot = choice.slot
  object.name = choice.category .. "_" .. choice.slot
  object.sprite = "entities/items"
  object.display_function = function() return true end
  table.insert(allowed_objects, object)
end


--Show description panel
local description_panel_config = require("scripts/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/menus/trillium_menu_lib/description_panel").create(description_panel_config)

local config = {
  origin = {x = 48, y = 24},
  grid_size = {columns=3, rows=4},
  cell_size = {width=32, height=36},
  cell_spacing = 4,
  edge_spacing = 4,
  background_png = "menus/inventory/equipping_background.png",
  background_offset = {x = -34, y = 0},
  cursor_sound = "cursor",
  cursor_offset = { x = 0, y = 4},
  aux_menus = { description_panel },
}
config.allowed_objects = allowed_objects

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)
menu.cute_name = "equip"


function menu:update_sprites()
  local game = sol.main.get_game()
  for _, object in pairs(allowed_objects) do
    local item_id = game:get_quickswap_item(object.category, object.slot)
    if item_id == nil then item_id = "empty" end
    object.animation = item_id
    --Set values for description panel to read:
    if item_id ~= "empty" then
      object.description_name_string = "items." .. item_id:gsub("/", ".")
      object.description_sprite = "entities/items"
      object.description_sprite_animation = item_id
      object.description_dialog = "item_descriptions." .. item_id:gsub("/", "."):gsub("solforge", "weapons")
    else
      object.description_name_string = nil
      object.description_sprite = nil
      object.description_sprite_animation = nil
      object.description_dialog = nil
    end
  end
end


menu:register_event("on_started", function()
  sol.timer.start(menu, 0, function() --There simply MUST be a better way to do this
    menu:update_sprites()
    menu:update_objects()
    return 100
  end)
  sol.timer.start(menu, 200, function()
    sol.main.get_game():start_tutorial("equipping_items")
  end)
end)


menu:register_event("on_command_pressed", function(self, command)
  --Add control behaviour here.
  local game= sol.main.get_game()
  local handled = false
  if command == "action" or command == "confirm" then
    local sel_ob = menu:get_selected_object()
    if not sel_ob then return end
    sol.audio.play_sound"ok"
    local category = sel_ob.category
    local slot = sel_ob.slot

    local submenu = require("scripts/menus/inventory/" .. submenu_key[category])
    if category == "item_1" then submenu = require("scripts/menus/inventory/tools_categories") end
    submenu.item_slot = slot
    submenu.quickswap_category = category
    --menu:start_submenu(menu.context, submenu)
    sol.menu.start(menu, submenu)

    handled = true

  end
  return handled
end)

return menu
