--Create inventory equipment menu
local item_list = {
  "spells/arcane_bolt",
  "spells/silverbolt_burst",
  "spells/holy_strike",
  "spells/silver_firecracker",
  "spells/buff_melee",
  "spells/buff_spell",
  "spells/buff_defense",
  "spells/holy_ground",
  "spells/silverstorm",
  "spells/hyperbeam",

  "spells/cinder",
  "spells/coal_fist",
  "spells/backdraft",
  "spells/bonfire",
  "spells/blackflame_arc",
  "spells/firebreak",
  "spells/firebreak_greater",
  "spells/wraithblade",
  "spells/wraith_shot",
  "spells/impaling_shadow",
  "spells/wraith_dive",
  "spells/wraith_turret",
  "spells/wraith_ally",
  "spells/firing_squad",
  "spells/bounty",

  "spells/blood_bullet",
  "spells/grasp",
  "spells/spark",
  "spells/cactus_growth",
  "spells/cactus_skin",
  "spells/wagon_wheel",
  "spells/piercing_toll",
  "spells/howl",
  "spells/tombstone",
  "spells/fatebind",
  "spells/ox_fist",

  "spells/chomp",

  "spells/vile_flies",
  "spells/vampire_bat",
  "spells/moth_swarm",
  "spells/leech_life",
  "spells/drainfield",
  "spells/spiderflesh",
  "spells/spiderwound",
  "spells/ravencloud",
  "spells/rats",

  "spells/tentacle_lash",
  "spells/seeking_worms",
  "spells/yearning_sea",
  "spells/worm_cannon",
  "spells/ichor_spray",
  "spells/blood_rain",
}

local allowed_objects = {}

for _, weapon in pairs(item_list) do
  local object = {}
  object.name = weapon
  object.sprite = "_item"
  object.display_function = "_item"
  local wep_string = string.gsub(weapon, "/", ".")
  object.description_name_string = "items." .. wep_string
  object.description_dialog = "item_descriptions.spells." .. weapon:gsub("spells/", "")
  object.description_sprite = "entities/items"
  object.description_sprite_animation = weapon
  object.text_function = function()
    local equip_item = sol.main.get_game():get_item(object.name)
    if equip_item and equip_item:has_amount() then
      return equip_item:get_amount()
    end
  end
  object.text_offset = { x = 16, y = 24}
  object.text_config = {
    font = "white_digits",
  }
  table.insert(allowed_objects, object)
end

local description_panel_config = require("scripts/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = allowed_objects,
  aux_menus = { description_panel },
}

local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)


function menu:set_active_item_slot(slot)
  menu.item_slot = slot
end


function menu:assign_item(slot, item)
  local game = sol.main.get_game()
  local other_slot = (slot == 1 and 2) or 1
  if game:get_item_assigned(other_slot) == item then
    local other_item = game:get_item_assigned(slot)
    game:set_item_assigned(slot, item)
    game:set_item_assigned(other_slot, other_item)
  else
    game:set_item_assigned(slot, item)
  end
end

return menu
