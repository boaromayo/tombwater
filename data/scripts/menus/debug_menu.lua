local menu = {}

local option_surface = sol.surface.create()
local cursor_sprite = sol.sprite.create("menus/arrow")

local options = {
  "debug_room",
  "set_control_solarus_style",
  "reset_enemy_table",
  "barrel_of_swords",
  "show_collision_boxes",
}


for i, option in ipairs(options) do
  local text_surface = sol.text_surface.create{
    font = "enter_command", font_size = 16, text = option
  }
  text_surface:draw(option_surface, 16, 16 + 16 * i)
end

function menu:on_started()
  menu.cursor_index = 1
  sol.main.get_game():set_suspended(true)
end

function menu:on_finished()
  sol.main.get_game():set_suspended(false)
end

function menu:on_draw(dst)
  option_surface:draw(dst)
  cursor_sprite:draw(dst, 8, 14 + 16 * menu.cursor_index)
end


function menu:on_command_pressed(cmd)
  if cmd == "action" or cmd == "confirm" then
    menu:process_selection(options[menu.cursor_index])
  elseif cmd == "down" then
    menu.cursor_index = menu.cursor_index + 1
    if menu.cursor_index > #options then menu.cursor_index = 1 end
  elseif cmd == "up" then
    menu.cursor_index = menu.cursor_index - 1
    if menu.cursor_index <= 0 then menu.cursor_index = #options end
  end
end

function menu:process_selection(option)
  local game = sol.main.get_game()

  if option == "set_control_solarus_style" then
    print("Setting controls to solarus defaults")
    sol.audio.play_sound"danger"
    require("scripts/misc/command_binding_manager"):reset_solarus_default()

  elseif option == "barrel_of_swords" then
    game:get_item("collectibles/barrel_of_swords"):on_obtaining()
    sol.audio.play_sound"danger"

  elseif option == "debug_room" then
    game:get_hero():teleport("debug")
    sol.menu.stop(menu)

  elseif option == "reset_enemy_table" then
    game:set_value("respawn_enemy_table", nil)
    sol.audio.play_sound"danger"

  elseif option == "show_collision_boxes" then
    sol.audio.play_sound"danger"
    game:get_map():draw_entity_bounding_boxes()

  end
end




return menu
