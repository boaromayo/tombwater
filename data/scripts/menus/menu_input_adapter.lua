--[[
Created by Max Mraz, licensed MIT
Intercepts button/key inputs for a menu, and translates them into "confirm" and "cancel" commands
NOTE: this will ignore any button mapping set by the player if used

Usage:
--Apply input adapter
require("scripts/menus/menu_input_adapter").apply(menu)
--]]

local manager = {}


local function apply_controls(menu)
  --A few keys are hardcoded still:
  local confirm_keys = {
    space = true,
    ['return'] = true, --return is a reserved keyword so we need to format this differently haha
  }
  local cancel_keys = {
    --escape = true,
    backspace = true,
  }
  local direction_keys = {
    up = "up",
    down = "down",
    left = "left",
    right = "right",
  }
  local buttons = {
    a = "confirm",
    b = "cancel",
    dpad_right = "right",
    dpad_up = "up",
    dpad_left = "left",
    dpad_down = "down",
    left_shoulder = "submenu_left",
    right_shoulder = "submenu_right",
  }

  menu:register_event("on_key_pressed", function(menu, key, modifiers)
    if menu.input_adapter_bypass then
      return false
    end
    local handled = false
    if confirm_keys[key] then
      menu:on_command_pressed("confirm")
      handled = true
    elseif cancel_keys[key] then
      menu:on_command_pressed("cancel")
      handled = true
    end
    return handled
  end)

  menu:register_event("on_joypad_button_pressed", function(self, button, joypad)
    if menu.input_adapter_bypass then return false end
    local handled = false
    if buttons[button] then
      menu:on_command_pressed(buttons[button])
      handled = true
    end
    return handled
  end)

end





--For compatibility with Solarus <= 1.6
local function apply_old_controls(menu)
  local confirm_keys = {
    space = true,
    ['return'] = true, --return is a reserved keyword so this one is formatted differently
  }
  local cancel_keys = {
    --escape = true,
    backspace = true,
  }
  local direction_keys = {
    up = "up",
    down = "down",
    left = "left",
    right = "right",
  }
  local xbox_buttons = {
    [0] = "confirm",
    [1] = "cancel",
    [4] = "submenu_left",
    [5] = "submenu_right",
  }

  local xbox_hats = {
    [0] = {
      [0] = "right",
      [2] = "up",
      [4] = "left",
      [6] = "down",
    }
  }

  menu:register_event("on_key_pressed", function(menu, key, modifiers)
    if menu.input_adapter_bypass then return false end
    local handled = false
    if confirm_keys[key] then
      menu:on_command_pressed("confirm")
      handled = true
    elseif cancel_keys[key] then
      menu:on_command_pressed("cancel")
      handled = true
--[[ For some reason this prevent category selection menus from working with the keyboard. Don't have time right now to figure out why.
    elseif direction_keys[key] then
      menu:on_command_pressed(direction_keys[key])
      handled = true
--]]
    end
    return handled
  end)


  menu:register_event("on_joypad_button_pressed", function(menu, button)
    --NOTE: this is hardcoded for Xbox controllers
    local buttonset = xbox_buttons
    if menu.input_adapter_bypass then return false end
    local handled = false
    if buttonset[button] then
      return menu:on_command_pressed(buttonset[button])
    end
    return handled
  end)

  menu:register_event("on_joypad_hat_moved", function(menu, hat, dir8)
    --NOTE: this is also hardcoded for Xbox controllers
    local buttonset = xbox_hats
    if menu.input_adapter_bypass then return false end
    local handled = false
    if buttonset[hat] and buttonset[hat][dir8] then
      return menu:on_command_pressed(buttonset[hat][dir8])
    end
    return handled
  end)


  menu:register_event("on_command_pressed", function(menu, command)
    if menu.input_adapter_bypass then return false end
    local handled = false
    if command == "action" then
      menu:on_command_pressed("confirm")
      handled = true
    end
    return handled
  end)
end



function manager.apply(menu)
  if sol.main.old_controls_version or ( tonumber(sol.main.get_solarus_version():match("(%d+.%d+)")) < 1.7 ) then
    apply_old_controls(menu)
  else
    --Don't use this in 1.7, instead use new controls manager which creates a menu controls object
    --apply_controls(menu)
  end
end


return manager
