--A menu that simply starts a game
local game_manager = require("scripts/game_manager")

local menu = {}

function menu:on_started()
  local game = game_manager:create("save1.dat")
  game:start()
end

return menu

