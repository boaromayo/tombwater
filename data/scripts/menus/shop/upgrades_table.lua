
--Quantities:
local grenade_quantities = {3,5,8,10}
local advanced_grenade_quantities = {2,4,6,8}
local dart_quantities = {5,8,12,15}
local potion_quantities = {1,2,3,4,5,6}
local abundant_potion_quantities = {1,3,5,7,9,10}
--Prices
local default_prices = {150,200,500,1000,2000}
local grenade_prices = {150,200,500,1000}
local potion_prices = {150,200,500,1000,2000}
local advanced_potion_prices = {250,500,1000,2000,3000}


local upgrade_items = {
  --Grenades
  {
    id = "inventory/grenade",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_cluster",
    quantities = advanced_grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_cluster_stun",
    quantities = advanced_grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_cactus",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_fire",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_ice",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_magic",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_pitch",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_poison",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/grenade_stun",
    quantities = grenade_quantities,
    prices = grenade_prices,
  },
  {
    id = "inventory/dynamite",
    quantities = {5,7,10,12},
    prices = grenade_prices,
  },


  --Potions
  {
    id = "potions/defense_type_bullet",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/defense_type_physical",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/increase_guns",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/increase_strength",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/madness_recovery",
    quantities = abundant_potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/regen_health",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/resistance_burn",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/resistance_poison",
    quantities = potion_quantities,
    prices = potion_prices,
  },
  {
    id = "potions/vengence",
    quantities = potion_quantities,
    prices = potion_prices,
  },



  --Others
  {
    id = "inventory/dart",
    quantities = dart_quantities,
    prices = default_prices,
  },
  {
    id = "inventory/dart_stun",
    quantities = dart_quantities,
    prices = default_prices,
  },
  {
    id = "inventory/dart_ice",
    quantities = dart_quantities,
    prices = default_prices,
  },
  {
    id = "inventory/dart_poison",
    quantities = dart_quantities,
    prices = default_prices,
  },
  {
    id = "inventory/mine_proximity",
    quantities = {3,5,7,9,10},
    prices = default_prices,
  },
  {
    id = "inventory/mine_cactus",
    quantities = {3,5,7,9,10},
    prices = default_prices,
  },


}



return upgrade_items