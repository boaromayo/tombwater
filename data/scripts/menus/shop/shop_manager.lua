--[[
Shops / Merchants menus
Created by Max Mraz, licensed MIT

Usage:
call: game:open_shop(shop_id, inventory)
Example inventory table:
{
  {id = "consumables/remedial_mushroom", price = 10,}, --item_id and price and required
  {id = "charms/flamestep", price = 200, quantity = 1,}, --quantity is optional, otherwise there will be an infinite quantity in the shop
  {id = "inventory/boomerang", price = 500, variant = 2, quantity = 1,}, --variant is another optional field
}
--]]

local manager = {}
local game_meta = sol.main.get_metatable"game"

local font, font_size = require("scripts/language_manager"):get_menu_font()


local function filter_inventory(inventory)
  local allowed_objects = {}
  for i, item in ipairs(inventory) do
    local item_id = item.id
    local name_key = string.gsub("items." .. item_id, "/", ".")
    local object = {
      name = item_id,
      itype = item.itype,
      price = item.price,
      variant = item.variant or 1,
      quantity = item.quantity,
      sprite = "_item",
      sprite_offset = {x=16, y=19},
      display_function = function(object)
        if not object.quantity then return true
        else return object.quantity > 0 end
      end,
      description_name_string = name_key,
      description_sprite = "entities/items",
      description_sprite_animation = item_id,
      text_offset = { x = 3, y = 26},
      text_config = {
        font = font,
      },
      text_function = function()
        return "$" .. item.price
      end,
    }
    table.insert(allowed_objects, object)
  end
  return allowed_objects
end


local function update_object_in_inventory(object, inventory)
  for _, item in pairs(inventory) do
    if object.name == item.id then
      item.quantity = object.quantity
    end
  end
  return inventory
end


function manager.get_shop_menu(shop_id, inventory)
  assert(type(inventory) == "table", "bad argument #2 to 'open_shop', type should be table")
  local shop_savegame_id = "shop_inventory_" .. shop_id
  local game = sol.main.get_game()

  --Note: this requires savegame table serialization.
  local saved_inventory = game:get_table_value(shop_savegame_id)
  if saved_inventory then
    inventory = saved_inventory
  end

  --Filter out items that have a 0 quantity
  local allowed_objects = filter_inventory(inventory)

  --Name box
  local name_box = require("scripts/menus/collection/name_box")
  name_box.x, name_box.y = 104, 205
  --Description box
  local description_box = require("scripts/menus/shop/item_description_box")
  description_box.x, description_box.y = 180, 42

  --Set config
  local config = require("scripts/menus/inventory/grid_config").get_config{
    allowed_objects = allowed_objects,
    origin = {x = 16, y = 48},
    grid_size = {columns = 4, rows = 4},
    background_png = nil,
    background_9slice_config = {
      width = 160, height = 160
    },
    background_offset = {x = -6, y = -6},
    aux_menus = { description_box },
  }
  --Create the menu
  local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

  --Suspend the game when the menu is open
  menu:register_event("on_started", function()
    sol.main.get_game():set_suspended(true)
    menu.initial_money_hud_display_state = game.force_money_display
    game.force_money_display = true
  end)
  menu:register_event("on_finished", function()
    sol.main.get_game():set_suspended(false)
    game.force_money_display = menu.initial_money_hud_display_state or false
  end)


  --Callback for purchasing an item
  local function purchase(object)
    local id, price, variant = object.name, object.price, object.variant
    local item = game:get_item(id)
    --give the item, call relevant callbacks:
    item:set_variant(variant)
    if item.on_obtaining then item:on_obtaining(variant) end
    if item.on_obtained then item:on_obtained(variant) end
    --manually show the item panel:
    item:show_panel(variant)
    --Remove quantity from merchant's inventory:
    if object.quantity then object.quantity = object.quantity - 1 end
    --Update the metchant's inventory table in savegame:
    inventory = update_object_in_inventory(object, inventory)
    game:set_table_value(shop_savegame_id, inventory)
    --Update objects currently displayed on menu
    allowed_objects = filter_inventory(inventory)
    menu:update_objects()
    game:remove_money(price)
    sol.audio.play_sound("money_spend")
  end


  --Handle choosing an item:
  menu:register_event("on_command_pressed", function(self, command)
    local handled = false
    if command == "action" or command == "confirm" then
      local object = menu:get_selected_object()
      local id, price, variant = object.name, object.price, object.variant

      if game:get_money() < price then
        game:start_dialog("menus.shop.insufficient_funds")
      else
        game:start_dialog("menus.shop.purchase_confirm",
          { v1 = sol.language.get_string("items." .. id:gsub("/", ".")),
            v2 = price,
          },
          function(answer)
          if answer == 1 then
            purchase(object)
          end
        end)
      end
      local handled = true
    elseif command == "cancel" or command == "dodge" or command == "item_1" or command == "item_2" or command == "pause" then
      sol.menu.stop(menu)
      handled = true
    end
    return handled
  end)

  return menu
end


function game_meta:open_shop(shop_id, inventory)
  assert(type(shop_id) == "string", "bad argument #1 to 'open_shop', type should be string")
  assert(type(inventory) == "table", "bad argument #2 to 'open_shop', type should be table")
  local game = self

  local shop_menu = manager.get_shop_menu(shop_id, inventory)
  sol.menu.start(game, shop_menu)

  return shop_menu
end


function game_meta:open_sell_shop(inventory)
  local sell_menu = require"scripts/menus/shop/collector"
  sell_menu:set_inventory(inventory)
  sol.menu.start(self, sell_menu)
end


function game_meta:open_upgrades_shop()
  local shop = require"scripts/menus/shop/upgrades"
  sol.menu.start(self, shop)
end


return manager