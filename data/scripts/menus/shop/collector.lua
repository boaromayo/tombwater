local default_inventory = {
  {id = "collectibles/relics/decorative_dagger", price = 50,},
  {id = "collectibles/relics/eupheme_cameo", price = 50,},
  {id = "collectibles/relics/ring_malachite", price = 50,},
  {id = "collectibles/relics/ring_opal", price = 50,},
  {id = "collectibles/relics/vision_statue", price = 50,},
}

local function process_inventory_list(inventory)
  inventory = inventory or default_inventory
  local allowed_objects = {}
  for i, item in ipairs(inventory) do
    local item_id = item.id
    local name_key = string.gsub("items." .. item_id, "/", ".")
    local object = {
      name = item_id,
      price = item.price,
      sprite = "_item",
      sprite_offset = {x = 16, y = 21},
      --display_function = "_item",
      display_function = function()
        return sol.main.get_game():get_item(item_id):has_amount(1)
      end,
      description_name_string = name_key,
      description_sprite = "entities/items",
      description_sprite_animation = item_id,
      text_offset = { x = 36, y = 16},
      text_config = {
        --font = font,
      },
      text_function = function()
        return "$" .. item.price
      end,
    }
    table.insert(allowed_objects, object)
  end
  return allowed_objects
end


--Description box
local description_box = require("scripts/menus/shop/item_description_box")
description_box.x, description_box.y = 156, 42

--Set config
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = process_inventory_list(),
  origin = {x = 56, y = 48},
  grid_size = {columns = 1, rows = 4},
  cell_size = {width = 96, height = 32,},
  --cell_color = {50,50,50},
  background_png = nil,
  background_9slice_config = {
    width = 96, height = 160
  },
  background_offset = {x = -6, y = -6},
  aux_menus = { description_box },
}

--Create the menu
local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

--Suspend the game when the menu is open
menu:register_event("on_started", function()
  local game = sol.main.get_game()
  game:set_suspended(true)
  game.force_money_display = true
end)
menu:register_event("on_finished", function()
  local game = sol.main.get_game()
  game:set_suspended(false)
  game.force_money_display = false
end)


--allow to purchase different items
function menu:set_inventory(inventory)
  menu.allowed_objects = process_inventory_list(inventory)
end


function menu:sell_item(object)
  local game = sol.main.get_game()
  local item = game:get_item(object.name)
  if item:get_amount() > 0 then
    sol.audio.play_sound("money_spend")
    game:add_money(object.price)
    item:remove_amount(1)
    menu:update_objects()
  end
end


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "action" or command == "confirm" then
    local game = sol.main.get_game()
    local object = menu:get_selected_object()
    if not object then return end
    game:start_dialog("menus.collector.confirm", function(answer)
      if answer == 1 then
        menu:sell_item(object)
      end
    end)
    handled = true
  elseif command == "cancel" or command == "item_1" or command == "item_2" or command == "dodge" or command == "pause" then
    sol.menu.stop(menu)
    handled = true
  end
  return handled
end)

return menu
