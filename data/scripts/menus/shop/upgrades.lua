local upgrade_items = require("scripts/menus/shop/upgrades_table")


local function get_level_savegame_id(item_id)
  local upgrade_savegame_prefix = "item_upgrade_level_"
  return upgrade_savegame_prefix .. item_id:gsub("/", "_")
end

local function process_inventory_list(inventory)
  local allowed_objects = {}
  for i, item in ipairs(inventory) do
    local item_id = item.id
    local name_key = string.gsub("items." .. item_id, "/", ".")
    local object = {
      name = item_id,
      prices = item.prices,
      quantities = item.quantities,
      sprite = "_item",
      sprite_offset = {x = 16, y = 21},
      display_function = function(item)
        local game = sol.main.get_game()
        local level = game:get_value(get_level_savegame_id(item_id)) or 1
        local should_show = item.quantities[level + 1] ~= nil and game:has_item(item_id)
        return should_show
      end,
      description_name_string = name_key,
      description_sprite = "entities/items",
      description_sprite_animation = item_id,
    }
    table.insert(allowed_objects, object)
  end
  return allowed_objects
end


--Description box
local description_box = require("scripts/menus/shop/upgrades_panel")
description_box.x, description_box.y = 188, 42

--Set config
local config = require("scripts/menus/inventory/grid_config").get_config{
  allowed_objects = process_inventory_list(upgrade_items),
  origin = {x = 32, y = 48},
  grid_size = {columns = 4, rows = 4},
  cell_size = {width = 32, height = 32,},
  --cell_color = {50,50,50},
  background_png = nil,
  background_9slice_config = {
    width = 160, height = 160
  },
  background_offset = {x = -6, y = -6},
  aux_menus = { description_box },
}

--Create the menu
local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)


--Suspend the game when the menu is open
menu:register_event("on_started", function()
  local game = sol.main.get_game()
  game:set_suspended(true)
  game.force_money_display = true
end)
menu:register_event("on_finished", function()
  local game = sol.main.get_game()
  game:set_suspended(false)
  game.force_money_display = false
end)


--allow to purchase different items
function menu:set_inventory(inventory)
  menu.allowed_objects = process_inventory_list(inventory)
end



local function purchase(object)
  local game = sol.main.get_game()
  local item = game:get_item(object.name)
  local name_key = object.name:gsub("/", ".")
  local level = game:get_value(get_level_savegame_id(object.name)) or 1
  local price = object.prices[level]
  local current_amount = object.quantities[level]
  local new_amount = object.quantities[level + 1]

  sol.audio.play_sound("money_spend")
  sol.audio.play_sound("fabric_rustle")
  item:set_max_amount(new_amount)
  item:set_amount(new_amount)
  item:show_panel()
  game:set_value(get_level_savegame_id(object.name), level + 1)
  game:remove_money(price)
  menu:update_objects()
end


menu:register_event("on_command_pressed", function(self, command)
  local handled = false
  if command == "action" or command == "confirm" then
    local game = sol.main.get_game()
    local object = menu:get_selected_object()
    if not object then return end
    local name_key = object.name:gsub("/", ".")
    local level = game:get_value(get_level_savegame_id(object.name)) or 1
    local price = object.prices[level]

    if game:get_money() < price then
      game:start_dialog("menus.shop.insufficient_funds")
    else
        game:start_dialog("menus.shop.upgrade_confirm",
        {
          v1 = sol.language.get_string("items." .. name_key),
          v2 = price,
        },
          function(answer)
          if answer == 1 then
            purchase(object)
          end
        end)
    end

    handled = true
  elseif command == "cancel" or command == "item_1" or command == "item_2" or command == "dodge" or command == "pause" then
    sol.menu.stop(menu)
    handled = true
  end
  return handled
end)

return menu
