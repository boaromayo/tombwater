local font, font_size = require("scripts/language_manager"):get_menu_font()

local dark_grey = sol.colors.ui_grey_dark

local menu = {x = 0, y = 0}
local width, height = 208, 64

local menu_surface = sol.surface.create()

local bg_surface = require("scripts/menus/trillium_menu_lib/9slice_background_manager").get_surface{
  width = width, height = height,
  source_png = "menus/panel_blocks/small.png",
  tile_width = 8, tile_height = 8,
}

--[[
Attributes:
- name
- prices
- quantities
----savegame: "item_upgrade_level_" .. name
--]]

local name_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

local price_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

local quantity_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

local line = sol.surface.create(width - 16, 1)
line:fill_color(dark_grey)


local function get_level_savegame_id(item_id)
  local upgrade_savegame_prefix = "item_upgrade_level_"
  return upgrade_savegame_prefix .. item_id:gsub("/", "_")
end


local function clear_text()
  name_surface:set_text""
  price_surface:set_text""
  quantity_surface:set_text""
  menu:update()
end

function menu:notify(parent)
  local game = sol.main.get_game()
  local sel_ob = parent:get_selected_object()
  if not sel_ob then
    clear_text()
    return
  end
  local item_id = sel_ob.name
  local item = game:get_item(item_id)
  local name = sel_ob.name:gsub("/", ".")
  local level = game:get_value(get_level_savegame_id(item_id)) or 1
  local price = sel_ob.prices[level]
  local current_amount = sel_ob.quantities[level]
  local new_amount = sel_ob.quantities[level + 1]

  name_surface:set_text(sol.language.get_string("items." .. name))
  price_surface:set_text(sol.language.get_string("menu.shop.price_label") .. " " .. price)
  quantity_surface:set_text(sol.language.get_string("menu.shop.upgrade_quantity") .. " " .. current_amount .. " > " .. new_amount)

  menu:update()
end


function menu:update()
  local displaying_quantity = quantity_surface:get_text() ~= ""
  menu_surface:clear()
  bg_surface:draw(menu_surface)
  name_surface:draw(menu_surface, 8, 16)
  price_surface:draw(menu_surface, 8, 34)
  quantity_surface:draw(menu_surface, 8, 52)

end


function menu:on_draw(dst)
  menu_surface:draw(dst, menu.x, menu.y)
end


return menu
