--[[
Requirements to add a category:
item list in scripts/menus/collections/{category_name}
icon in sprites/menus/inventory/category_{category_name}
--]]

local sub_menu_cats = {
  "key_items",
  "weapons",
  "tools",
  "spells",
  "potions",
  "charms",
}

local categories = {}

for i, cat_name in ipairs(sub_menu_cats) do
  local cat = {}
  cat.menu = require("scripts/menus/collection/menu_template").make_menu(require("scripts/menus/collection/" .. cat_name))
  cat.icon = "menus/inventory/category_" .. cat_name
  categories[i] = cat
end


local menu = require("scripts/menus/trillium_menu_lib/category_selection_menu").create{
  categories = categories,
  origin = {x=116, y = 16},
  size = {width = 184, height = 16},
  category_padding = 4,
  background_png = "menus/inventory/category_selection_background.png",
  cursor_sound = "cursor_category",
}


return menu
