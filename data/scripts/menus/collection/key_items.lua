return {
  --Gear
  "gear/climbing_gear",
  "gear/hookcaster",
  "gear/lantern",
  "gear/silverwater_pearl",

  --Keys
  "collectibles/keys/mining_co",
  "collectibles/keys/boarding_house",
  "collectibles/keys/settlement_tunnels_gate",
  --Things like keys:
  "collectibles/misc/mining_co_hq_contraption_gear",

  --Collectibles
  "collectibles/charm_stone",
  "collectibles/ghost_seed",
  "collectibles/healing_flask_shard",
  "collectibles/healing_flask_silverdust",
  "collectibles/bandolier_strap",
  "collectibles/crafting_book/crafting_book_alchemist",
  "collectibles/crafting_book/crafting_book_apothecary",
  "collectibles/crafting_book/crafting_book_artificer",

  --Relics
  "collectibles/relics/decorative_dagger",
  "collectibles/relics/eupheme_cameo",
  "collectibles/relics/ring_malachite",
  "collectibles/relics/ring_opal",
  "collectibles/relics/vision_statue",


  --Crafting
  "materials/crafting/flask",
  "materials/crafting/silverwater",
  "materials/crafting/foxglove",
  "materials/crafting/st_laurence_wort",
  "materials/crafting/yarrow",
  "materials/crafting/blackpowder",
  "materials/crafting/antimony",
  "materials/crafting/needle",
  "materials/crafting/iron_filings",
  "materials/crafting/pine_resin",
}