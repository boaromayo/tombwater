return {
--=======
  "potions/increase_strength",
  "potions/increase_guns",
  "potions/defense_type_physical",
  "potions/defense_type_bullet",
  "potions/vengence",
  "potions/resistance_poison",
  "potions/resistance_burn",
  "potions/regen_health",
  "potions/madness_recovery",
--=======
  "consumables/cure_poison",
  "consumables/cure_burn",
  "consumables/silver_bitters",
  "consumables/remedial_mushroom",
  "consumables/pickled_chicken_foot",
}
