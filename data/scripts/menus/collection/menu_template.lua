local manager = {}

local name_box = require("scripts/menus/collection/name_box").create()
name_box.x, name_box.y = 104, 200

function manager.make_menu(item_list)
  local allowed_objects = {}

  for _, weapon in pairs(item_list) do
    local object = {}
    object.name = weapon
    object.sprite = "_item"
    object.display_function = "_item"
    local wep_string = string.gsub(weapon, "/", ".")
    object.description_name_string = "items." .. wep_string
    object.description_dialog = "item_descriptions.weapons.basic_axe"
    object.description_sprite = "entities/items"
    object.description_sprite_animation = weapon
    --[[object.text_function = function()
      local equip_item = sol.main.get_game():get_item(object.name)
      if equip_item and equip_item:has_amount() then
        return equip_item:get_amount()
      end
    end--]]
    object.text_offset = { x = 16, y = 24}
    object.text_config = {
      font = "white_digits",
    }
    table.insert(allowed_objects, object)
  end

  local config = require("scripts/menus/inventory/grid_config").get_config{
    allowed_objects = allowed_objects,
    origin = {x = 96, y = 42},
    grid_size = {columns = 6, rows = 4},
    background_png = nil,
    background_9slice_config = {
      width = 240, height = 160,
      source_png = "menus/panel_blocks/journal.png",
    },
    background_offset = {x = -10, y = -6},
    cell_color = {185, 180, 165, 100},
    cell_png = "",
    aux_menus = { name_box },
  }

  local menu = require"scripts/menus/trillium_menu_lib/grid_menu".create(config)

  menu:register_event("on_command_pressed", function(self, command)
    local game = sol.main.get_game()
    local handled = false
    if command == "action" or command == "confirm" then
      if not menu:get_selected_object() then return end
      handled = true
      local item_id = menu:get_selected_object().name
      game:start_dialog("item_long_descriptions." .. item_id:gsub("/", "."))
    end

    return handled
  end)

  return menu
end

return manager
