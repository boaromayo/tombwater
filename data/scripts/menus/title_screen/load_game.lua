local game_manager = require("scripts/game_manager")

local menu = {}

local black = sol.surface.create()
black:fill_color{0,0,0}
local loading_icon = sol.sprite.create("menus/title_screen/loading_icon")

function menu:set_filename(filename)
  menu.filename = filename
end


function menu:on_started()
  assert(menu.filename, "No game was set for load_game menu va menu:set_filename()")
  local start_time = os.time()

  sol.timer.start(menu, 100, function()
    local game = game_manager:create(menu.filename)
    print("Load menu, game created in: ", os.time() - start_time)
    sol.timer.start(menu, 400, function()
      game:register_event("on_started", function()
        sol.menu.stop(menu)
      end)
      game:start()
    end)
  end)
end


function menu:on_draw(dst)
  black:draw(dst)
  loading_icon:draw(dst, 208, 120)
end

return menu
