local confirm_menu = require("scripts/menus/title_screen/confirm")
local load_game_menu = require("scripts/menus/title_screen/load_game")

local save_filename = "save1.dat"

local common_config = require("scripts/menus/title_screen/common_config"):get_config()
common_config.options = {
  "continue",
  "new_game",
  "options",
  "quit",
}
local menu = require("scripts/menus/title_screen/title_menu_factory").new(common_config)


local function load_game(filename)
  load_game_menu:set_filename(filename)
  menu.process_selection = function() end --remove processing so we can't start multiple games at once TODO: why is that possible even????
  sol.menu.start(sol.main, load_game_menu)
end


function menu:process_selection(option)
  if option == "continue" then
    if menu.previous_save_exists then
      load_game(save_filename)
    else
      sol.audio.play_sound("wrong")
    end
  elseif option == "new_game" then
    confirm_menu.parent_menu = menu
    confirm_menu.confirmation_callback = function()
      sol.game.delete(save_filename)
      load_game(save_filename)
    end
    menu:switch_to_menu(confirm_menu)
  elseif option == "options" then
    menu:switch_to_menu(require("scripts/menus/title_screen/options"))
  elseif option == "quit" then
    sol.main.exit()
  end
end

function menu:on_started()
  menu.previous_save_exists = sol.game.exists(save_filename)
  if not menu.previous_save_exists then
    menu:set_option_color("continue", sol.colors.ui_grey_med)
    menu:set_cursor_index(2)
  else
    menu:set_option_color("continue", {255,255,255})
  end
end


return menu
