local menu = {}

local ground_speed = 120

local fade_black = sol.surface.create()
fade_black:fill_color{0,0,0}

local bg_layers = {
--[[
  {
    surface = sol.surface.create"menus/title_screen/background_layers/0_bg.png",
    speed = 0,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_layers/1_far_mountains.png",
    speed = 20,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_layers/1_mid_mountains.png",
    speed = 40,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_layers/2_ground.png",
    speed = 100,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_layers/scrub_1.png",
    speed = 100,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_layers/scrub_2.png",
    speed = 130,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_layers/3_tracks.png",
    speed = 150,
  },
--]]
  --Version 2:
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/background.png"),
    speed = 0,
  },
  --[[{
    surface = sol.surface.create("menus/title_screen/background_2/bg_being.png"),
    speed = 0,
  },--]]
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/clouds.png"),
    speed = 0,
  },
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/bg-far_mountains.png"),
    speed = 20,
  },
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/mg_mountains_1.png"),
    speed = 40,
  },
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/mg_field_far.png"),
    speed = 100,
  },
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/mg_trees.png"),
    speed = 160,
  },
  {
    surface = sol.surface.create("menus/title_screen/background_2_blur/mg_field_mid.png"),
    speed = 200,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_2_blur/mg_tracks.png",
    speed = 200,
  },
--]]
}

local fg_layers = {
  {
    surface = sol.surface.create"menus/title_screen/background_2_blur/fg_telegraph_poles.png",
    speed = 230,
  },
  {
    surface = sol.surface.create"menus/title_screen/background_2_blur/fg_trees.png",
    speed = 400,
  },
  --[[{
    surface = sol.surface.create"menus/title_screen/background_2/fg_trees_2.png",
    speed = 450,
  }, --]]
  {
    surface = sol.surface.create"menus/title_screen/background_2_blur/fg_bushes.png",
    speed = 480,
  },
}




local train_surface = sol.surface.create()
local train = {
  sol.sprite.create("menus/title_screen/train_engine"),
  sol.sprite.create("menus/title_screen/train_car"),
  sol.sprite.create("menus/title_screen/train_car"),
  sol.sprite.create("menus/title_screen/train_car"),
  sol.sprite.create("menus/title_screen/train_car"),
}
local train_origin = {x = 380, y = 220}
local car_width = 45


local overlay = sol.surface.create("menus/title_screen/background_layers/gradient.png")
--overlay:fill_color{180,150,50,50}
overlay:set_opacity(20)


--We'll make a better logo later:
--[[local title_surface = sol.text_surface.create{
  font = "RootBeer",
  text = "Tombwater",
  font_size = 16,
  horizontal_alignment = "center",
}--]]
title_surface = sol.surface.create("menus/title_screen/background_2/title.png")
title_surface:set_opacity(0)


function menu:on_started()
  fade_black:fade_out(60)

  --title slam
  sol.timer.start(menu, 4700, function()
    title_surface:set_opacity(255)
  end)

  --background movements
  for _, layer in ipairs(bg_layers) do
    local m = sol.movement.create"straight"
    m:set_speed(layer.speed)
    m:set_angle(0)
    m:start(layer)
  end

  --foreground_movements
  for _, layer in ipairs(fg_layers) do
    local m = sol.movement.create"straight"
    m:set_speed(layer.speed)
    m:set_angle(0)
    m:start(layer)
    layer.x, layer.y = 0, 0
  end

  --little train bumps
  local bump_delay = 70
  sol.timer.start(menu, 1000, function()
    for i, car in ipairs(train) do
      sol.timer.start(menu, bump_delay * i, function()
        car:set_animation("bump", function()
          car:set_animation"choo_choo"
        end)
      end)
    end
    --repeat
    return math.random(3000, 3500)
  end)
end


function tile_draw(surface, x_offset, y_offset, dst_surface, x, y)
  x = x or 0
  y = y or 0
  local width, height = surface:get_size()
  local region_x = x_offset % width
  local region_y = y_offset % height

  local region_width = width - region_x
  local region_height = height - region_y

  --draw region 4
  surface:draw_region(region_x, region_y, region_width, region_height, dst_surface, x, y)

  --draw region 3
  if region_width>0 then
    surface:draw_region(0, region_y, region_x, region_height, dst_surface, x+width-region_x, y)
  end
end


function menu:on_draw(dst_surface)
  train_surface:clear()
  --draw bg
  for _, layer in ipairs(bg_layers) do
  	  tile_draw(
      layer.surface,
      layer.x,
      layer.y,
    	  	dst_surface, 0, 0
  	  	)
  end
  --draw train
  for i, car in ipairs(train) do
    car:draw(train_surface, train_origin.x - (i * car_width), train_origin.y)
  end
  train_surface:draw(dst_surface)
  --draw fg
  for _, layer in ipairs(fg_layers) do
  	  tile_draw(
      layer.surface,
      layer.x,
      layer.y,
    	  	dst_surface, 0, 0
  	  	)
  end
  overlay:draw(dst_surface)
  --title_surface:draw(dst_surface, 208, 50) --this is the text surface one
  title_surface:draw(dst_surface, 0, 0)
  fade_black:draw(dst_surface)
end

return menu

