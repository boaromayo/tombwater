local manager = {}
local hero_meta = sol.main.get_metatable"hero"
  
local state = sol.state.create("skeleton")
state:set_visible(true)
state:set_can_control_direction(true)
state:set_can_control_movement(true)
state:set_gravity_enabled(true)
state:set_can_come_from_bad_ground(false)
state:set_can_be_hurt(true)
state:set_can_use_sword(false)
state:set_can_use_shield(false)
state:set_can_use_item(false)
state:set_can_interact(true)
state:set_can_grab(false)
state:set_can_push(false)
state:set_can_pick_treasure(true)
state:set_can_use_teletransporter(false)
state:set_can_use_switch(true)
state:set_can_use_stream(true)
state:set_can_use_stairs(true)
state:set_can_use_jumper(false)
state:set_carried_object_action("throw")
 
state.birthplace = {}
state.hero_body_location = {}

function manager:get_state()
  return state
end

function hero_meta:start_being_bones(bonepile)
  local hero = self
  local map = hero:get_map()
  local hdirection = hero:get_direction()
  state.bonepile = bonepile
  state.hero_body_location.x, state.hero_body_location.y, state.hero_body_location.z = hero:get_position()

  --leave a hero dummy in place
  local hdummy = map:create_custom_entity{
        x = state.hero_body_location.x,
        y = state.hero_body_location.y,
        layer = state.hero_body_location.z,
        width = 16, height = 16, direction = hdirection, 
        sprite = "hero/tunic1",
      }
  hdummy:set_drawn_in_y_order(true)
  state.hdummy = hdummy
  local hdummy_sprite = hdummy:get_sprite()
  hdummy_sprite:set_animation("slumping", function() hdummy_sprite:set_animation("sitting_head_down") end)

  --move hero to new location
  hero:freeze()
  hero:set_visible(false)
  local m = sol.movement.create("target")
  m:set_ignore_obstacles(true)
  m:set_target(bonepile)
  m:set_speed(500)
  bonepile:set_traversable_by(true)
  m:start(hero, function()
    hero:set_position(bonepile:get_position())
    bonepile:set_visible(false)
    --maybe the entity should have the transform animation, then we hide it, reveal hero
    hero:start_state(state)
    hero:set_direction(0)
    hero:set_tunic_sprite_id("enemies/tombwater_enemies/skeleton")
    hero:set_visible(true)
    hero:set_animation("assembling", function()
      hero:set_direction(3)
      hero:set_animation"stopped"
    end)
  end)  
end

function hero_meta:stop_being_bones()
  local current_state, state_ob = self:get_state()
  if (current_state ~= "custom") or (state_ob:get_description() ~= "skeleton") then
    error("Cannot exit skeleton state while not in skeleton state")
    return
  end
  local hero = self
  local map = hero:get_map()
  local game = hero:get_game()
  local hdirection = hero:get_direction()
  local hx, hy, hz = hero:get_position() 
  
  hero:set_invincible(true)
  hero:set_visible(false)

  --leave a skeleton dummy in place
  local skdummy = map:create_custom_entity{
      x = hx,
      y = hy,
      layer = hz,
      width = 16, height = 16, direction = 0, 
      sprite = "enemies/tombwater_enemies/skeleton",
    }
  skdummy:get_sprite():set_animation("breaking_apart",function()
    skdummy:get_sprite():set_animation("bonepile")
    game:start_flash({0,0,0},5)
    sol.timer.start(hero, 500, function()
      --change heros sprite back
      hero:set_tunic_sprite_id('hero/tunic1')
      --move back to body
      hero:set_position(state.hdummy:get_position())
      hero:set_visible(true)
      hero:set_invincible(false)
      --remove body
      state.hdummy:remove()
      skdummy:remove()
      game:stop_flash(5)
      --reshow bonepile
      state.bonepile:set_visible(true)
      state.bonepile:set_traversable_by(false)
      hero:unfreeze()
      --reset world??    
    end)
  end)
end

function state:on_started()
  local map = state:get_map()
  local hero = map:get_hero()
end

function state:on_finished()
  local hero = state:get_entity()
  hero:stop_being_bones()
end

function state:on_movement_changed(m)
  local hero = state:get_entity()
  --check if we should play walking or stopped animation
  if m:get_speed() > 0 then
    hero:set_animation("walking")
  else
    hero:set_animation("stopped")
  end
end


function state:on_command_pressed(command)
  local handled = false
  local hero = state:get_entity()
  if command == "fire" or command == "aim" or command == "item_1" or command == "item_2" then
    hero:unfreeze()
    handled = true
  end
  return handled
end

return manager