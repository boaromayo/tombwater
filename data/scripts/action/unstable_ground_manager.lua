local manager = {}

local map_meta = sol.main.get_metatable"map"
local game_meta = sol.main.get_metatable"game"
local hero_meta = sol.main.get_metatable"hero"
local separator_meta = sol.main.get_metatable"separator"


function map_meta:is_unstable_ground(x, y, z)
  local map = self
  local ground = map:get_ground(x, y, z)
  local is_unstable = false
  if (ground == "hole") or (ground == "lava") or (ground == "deep_water") then
    is_unstable = true
  end
  for e in map:get_entities_in_rectangle(x, y, 1, 1) do
    if e:get_property("unstable_floor") == "true" and e:get_layer() == z then
      is_unstable = true
    end
  end

  return is_unstable
end


function hero_meta:set_stable_ground_position() --note: only call if you know the player is not over an unstable ground
  local hero = self
  local x, y, z = hero:get_position()
  hero.saved_stable_ground_position = {
    x = x, y = y, layer = z,
  }
end


function hero_meta:get_stable_ground_position()
  local hero = self
  local pos = hero.saved_stable_ground_position or {x = nil, y = nil, layer = nil}
  return pos.x, pos.y, pos.layer
end


hero_meta:register_event("on_position_changed", function(hero)
  --Save stable ground position unless we shouldn't
  local state, state_ob = hero:get_state()
  if state == "back to solid ground" or state == "falling" then
    return
  elseif (state_ob and not state_ob:get_can_come_from_bad_ground()) then
    return
  else
    local map = hero:get_map()
    local x, y, z = hero:get_ground_position()
    if not map:is_unstable_ground(x, y, z) then
      hero:set_stable_ground_position()
    end
  end
end)


--Overwrite the engine's built-in recall behavior when you fall in a hole or whatever
--NOTE: this must manually be called whenever hero:reset_solid_ground() is called, otherwise they'll just respawn at the engine-determined last solid ground, rather than ours
function hero_meta:activate_bad_ground_recall()
  local hero = self
  hero:save_solid_ground(function() --return an x, y, and layer to return the hero to
    return hero:get_stable_ground_position()
  end)
end


map_meta:register_event("on_opening_transition_finished", function(map)
  local hero = map:get_hero()
  if hero:get_state() == "stairs" then --if you're on stairs, we need to make an exception until you're off
    hero.save_ground_once_stairs_are_finished = true
  else --Otherwise, save our new position and reactive bad ground recall behavior
    hero:set_stable_ground_position()
    hero:activate_bad_ground_recall()
  end
end)


separator_meta:register_event("on_activated", function(separator)
  local hero = separator:get_map():get_hero()
  hero:set_stable_ground_position()
  hero:activate_bad_ground_recall()
end)


hero_meta:register_event("on_state_changing", function(hero, old_state, new_state)
  if old_state == "stairs" and new_state == "free" and hero.save_ground_once_stairs_are_finished then
    hero:set_stable_ground_position()
    hero:activate_bad_ground_recall()
  elseif new_state == "back to solid ground" then
    --Bypass "back to solid ground" movement and state
    hero:set_position(hero:get_stable_ground_position())
  end
end)




return manager
