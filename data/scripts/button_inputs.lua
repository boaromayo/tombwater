require("scripts/multi_events")
local hero_meta = sol.main.get_metatable"hero"

local menu = {}

local MENU_DIRECTIONS = {
	prev_menu = "left",
	next_menu = "right",
}


function process_command_pressed(command, state)
  local handled = false
  local game = sol.main.get_game()

  if game:is_suspended() then --don't process anything if the game is suspended (menu commands should be handled a different way)
    handled = false

  elseif command == "attack" and state == "aiming" then
    --Let the aiming state handle the attack command itself to charge the Dead Man's Shot
    handled = false

  elseif command == "attack" then
    game:queue_command_until_free"attack"
    handled = true

  elseif command == "aim" then
    game:queue_command_until_free("aim", 500)
    handled = true
  --Note: "fire" while aiming is handled by the aim state

  elseif command == "fire" and state == "free" and game:get_value("can_use_hookshot") then
    game:get_item("inventory/hookshot"):on_using()
    handled = true

  elseif command == "dodge" then
    game:queue_command_until_free"dodge"
    handled = true

  --Item Quickswaps:
  elseif command == "quickswap_melee" then
    game:quickswap"melee"
    handled = true
  elseif command == "quickswap_gun" then
    game:quickswap"gun"
    handled = true
  elseif command == "quickswap_item_1" then
    game:quickswap"item_1"
    handled = true
  elseif command == "quickswap_item_2" then
    game:quickswap"item_2"
    handled = true

  end

  return handled
end


function menu:initialize(game)
  local button_mapping_menu = require"scripts/menus/button_mapping"

  --Local command functions---------------------------------------------
  local function on_command_pressed(command)
    local hero = game:get_hero()
    local state, state_ob = hero:get_state()
    local map = game:get_map()
    local handled = false
    --Pass along command input to custom states, which won't pick up custom commands themselves in 1.6:
    if sol.main.old_controls_version and state == "custom" then
      if state_ob.on_command_pressed then
        state_ob:on_command_pressed(command)
      end
    end

    if state == "custom" then state = state_ob:get_description() end --Translate custom states to their description for convenience

    handled = process_command_pressed(command, state)

    return handled
  end


  function on_command_released(command)
    local hero = game:get_hero()
    --Pass along command input to custom states, which won't pick up custom commands themselves
    local state, state_ob = hero:get_state()
    if sol.main.old_controls_version and state == "custom" then
      if state_ob.on_command_released then
        state_ob:on_command_released(command)
      end
    end
  end


  --Hook game into local command functions -----------------------------------------------
  game:register_event("on_command_pressed", function(self, command)
    return on_command_pressed(command)
  end)

  game:register_event("on_command_released", function(self, command)
    return on_command_released(command)
  end)




  game:register_event("on_key_pressed", function(self, key, modifiers)
    local handled = false
    local hero = game:get_hero()

    if key == "f1" and not game:is_dialog_enabled() and not game:is_paused() then
      if not sol.menu.is_started(button_mapping_menu) then
        sol.menu.start(game, button_mapping_menu)
      else
        sol.menu.stop(button_mapping_menu)
      end
      handled = true

    --[[elseif key == "return" then --this interferes with the "menu_input_adapter" script, by intercepting the return key before it can
      game:simulate_command_pressed"action"
      handled = true
    --]]

    end

    return handled
  end)


  --Mouse commands are currently hardcoded:
  function game:on_mouse_pressed(button, x, y)
    if button == "left" then
      on_command_pressed("attack")
    elseif button == "right" then
      game.mouse_aiming = true
      on_command_pressed("aim")
    elseif (button == "middle") or (button == "x1") or (button == "x2") then
      on_command_pressed("dodge")
    end
  end


  function game:on_mouse_released(button, x, y)
    if button == "right" then
      game.mouse_aiming = false
      on_command_released("aim")
    end
  end



  local function check_if_still_holding_aim()
    local still_aiming = false
    if not game:get_value("equipped_gun") then return false end --cancel if we don't have a gun equipped
    --compat with <= 1.6 check:
    if sol.main.old_controls_version then
      local command_manager = require"scripts/misc/command_binding_manager"
      if command_manager:is_command_held("aim") or sol.input.is_mouse_button_pressed("right") then
        still_aiming = true
      end
    elseif sol.controls.get_main_controls():is_pressed("aim") or sol.input.is_mouse_button_pressed("right") then
      still_aiming = true
    end
    return still_aiming
  end


  local function check_can_do_command(command)
    local hero = game:get_hero()
    local state, state_ob = hero:get_state()
    if state_ob then state = state_ob:get_description() end --If we've a state object, then the state is just "custom" which isn't super useful.
    local can_do = false

    if state == "free" then
      can_do = true
    elseif state == "aiming" and command == "dodge" then
      can_do = true

    end
    return can_do
  end


  function game:queue_command_until_free(command, lifespan)
    lifespan = lifespan or 300 --length in which a command can sit in the queue
    local hero = game:get_hero()
    local hero_state, state_object = hero:get_state()
    if check_can_do_command(command) then
      game:do_queued_command(command)
    elseif command == "attack" and state_object and state_object:get_description() == "aiming" then
      --hack to prevent attack from queuing when pressing the fire/attack button guns. Otherwise, you use your queued attack when you stop aiming.
    else
      if game.queued_command_timer then game.queued_command_timer:stop() end
      local queue_lifetime = 0
      game.queued_command_timer = sol.timer.start(game, 10, function()
        queue_lifetime = queue_lifetime + 10
        if check_can_do_command(command) then
          game:do_queued_command(command)
        elseif queue_lifetime < lifespan then
          return true
        end
      end)
    end
  end


  function game:do_queued_command(command)
    local hero = game:get_hero()
    if command == "dodge" and hero:get_controlling_stream() == nil then
      if not game:is_suspended() then
        hero:dash()
      end
      handled = true

    elseif command == "attack" then
      hero:set_direction(game:get_direction_held() / 2)
      local weapon_id = game:get_value("equipped_weapon")
      if weapon_id == nil then return end --cancel if we don't have a weapon equipped
      local weapon = game:get_item(weapon_id)
      weapon:on_using()

    elseif command == "aim" then --double check we still want to start aiming, could have let go of the command since it was queued:
      if check_if_still_holding_aim() then
        hero:start_aiming()
      end

    end
  end


  --Check if commands are being input when state changes (mostly just useful for things that should swap if the command has been held for a while and the hero just became free):
  hero_meta:register_event("on_state_changed", function(hero, state)
    if state == "free" then
      --Check if we've still got the aim button held, if so, start aiming
      if check_if_still_holding_aim() then hero:start_aiming() end

    end
  end)



  --Compatibility with Solarus 1.6:
  if sol.main.old_controls_version then
    local command_manager = require"scripts/misc/command_binding_manager"

    function game:on_joypad_button_pressed(button)
      local command = command_manager:get_command_from_button(button)
      on_command_pressed(command)
    end

    function game:on_joypad_button_released(button)
      local command = command_manager:get_command_from_button(button)
      on_command_released(command)
    end

    game:register_event("on_joypad_axis_moved", function(self, axis, state)
      --Watch out, axis are fucken tricky
      local command = command_manager:get_command_from_axis(axis, state)
      on_command_pressed(command)
      --Pass along command input to custom states, which won't pick up custom commands themselves
      local hero = game:get_hero()
      local state, state_ob = hero:get_state()
      if state == "custom" then
        if state_ob.on_joypad_axis_moved then
          state_ob:on_joypad_axis_moved(axis, state)
        end
      end
    end)

    function game:on_joypad_hat_moved(hat, direction8)
      local command = command_manager:get_command_from_hat(hat, direction8)
      on_command_pressed(command)
    end

    game:register_event("on_key_pressed", function(self, key, modifiers)
      local command = command_manager:get_command_from_key(key)
      on_command_pressed(command)
    end)

    game:register_event("on_key_released", function(self, key)
      local command = command_manager:get_command_from_key(key)
      on_command_released(command)
    end)

  end

  
end


return menu