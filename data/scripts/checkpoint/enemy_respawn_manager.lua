--[[
Created by Max Mraz, licensed MIT
Sets up a Dark Souls style respawn system for enemies. Enemies will not respawn unless you die, or map:respawn_enemies() is called
Enemies can ignore this (and respawn whenever you leave/come back to the map) by setting enemy.respawn_manager_ignore = true
--]]

local map_meta = sol.main.get_metatable"map"
local game_meta = sol.main.get_metatable"game"
local enemy_meta = sol.main.get_metatable"enemy"

local manager = {}

local master_enemy_table = {}


local function save_enemy_table()
  local game = sol.main.get_game()
  game:set_table_value("respawn_enemy_table", master_enemy_table)
end

game_meta:register_event("on_started", function(self)
  master_enemy_table = self:get_table_value("respawn_enemy_table") or {}
end)


local function reset_enemy_table()
  for map_id, map_table in pairs(master_enemy_table) do
    for cid, enemy in pairs(map_table) do
      enemy.killed = false
    end
  end
end


function map_meta:manage_spawns() --called on map:on_started
  local map = self
  local map_id = map:get_id()

  local function init_map_table()
    master_enemy_table[map_id] = {}
    --create an environment to read the map.dat, set a metatable function so most entries on the map.dat will be functions that return nothing
    local env = setmetatable({}, {__index = function() return function() end end})
    --process enemy entiries on the map.dat --each "enemy" on the map.dat is a function with a single argument, a table of properties. Save that table to master_enemy_table[current_map_id]
    function env.enemy(props)
      local x, y, layer, breed = props.x, props.y, props.layer, props.breed
      local cid = x .. "," .. y .. "," .. layer .. "," .. breed
      local enemy_table = {
        cid = cid,
        killed = false,
      }
      for k, v in pairs(props) do
        enemy_table[k] = v
      end
      master_enemy_table[map_id][cid] = enemy_table
    end
    local chunk = sol.main.load_file("maps/" .. map_id .. ".dat")
    setfenv(chunk, env)
    chunk()
  end

  if not master_enemy_table[map_id] then --first map load, save enemies to memory
    init_map_table()
  end

  for enemy in map:get_entities_by_type("enemy") do
    if not enemy.respawn_do_not_index then
      local x, y, layer = enemy:get_position()
      local breed = enemy:get_breed()
      local cid = x .. "," .. y .. "," .. layer .. "," .. breed
      --Add a check: during development you'll often load a save where enemies have been moved on the map, so CID causes issues:
      if not master_enemy_table[map_id][cid] then
        init_map_table()
      end
      --Save enemies to table when killed
      enemy:register_event("on_dying", function()
        if enemy.respawn_manager_ignore then return end
        master_enemy_table[map_id][cid].killed = true
        save_enemy_table()
      end)
      --if enemy has already been killed, remove it
      if master_enemy_table[map_id][cid].killed then
        enemy:remove()
      end
    end
  end

end



function map_meta:respawn_enemies()
  local map = self
  local map_id = map:get_id()
  reset_enemy_table()
  save_enemy_table()
  --Remove all enemies from the map
  for enemy in map:get_entities_by_type("enemy") do
    --remove any weapons they might be using:
    if enemy.attack_entities then
      for _, weapon_entity in pairs(enemy.attack_entities) do weapon_entity:remove() end
    end
    enemy:remove()
  end
  --Create a new enemy for every enemy in the table for the current map
  sol.timer.start(map, 10, function()
    for cid, enemy in pairs(master_enemy_table[map_id]) do
      local created_enemy = map:create_enemy{
        name = enemy.name,
        x = enemy.x, y = enemy.y, layer = enemy.layer,
        direction = enemy.direction,
        breed = enemy.breed,
        savegame_variable = enemy.savegame_variable,
        treasure_name = enemy.treasure_name, treasure_variant = enemy.treasure_variant, treasure_savegame_variable = enemy.treasure_savegame_variable,
        enabled_at_start = enemy.enabled_at_start,
        properties = enemy.properties,
      }
      --If savegame variable exists and is already set to true (for bosses you've killed already, etc) then created_enemy will be nil
      if created_enemy then
        --Save enemies to table when killed
        created_enemy:register_event("on_dying", function()
          master_enemy_table[map_id][cid].killed = true
          save_enemy_table()
        end)
      end
    end
  end)

end


map_meta:register_event("on_started", function(self)
  self:manage_spawns()
end)


game_meta:register_event("on_game_over_started", function(self)
  reset_enemy_table()
  save_enemy_table()
  --self:get_map():respawn_enemies()
end)


