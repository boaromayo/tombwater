local manager = {}
local game_meta = sol.main.get_metatable"game"

local vignette = require"scripts/fx/vignette"
local film_grain_shader = sol.shader.create"film_grain_3"
sol.main.film_grain_shader = film_grain_shader
--local tone_mapping_shader = sol.shader.create"tone_mapping"

sol.main.film_grain_shader:set_uniform("strength", 5)
sol.main.film_grain_shader.active = true
sol.video.set_shader(film_grain_shader)


game_meta:register_event("on_started", function()
  if not sol.menu.is_started(vignette) then
    sol.menu.start(sol.main, vignette)
  end
end)


function game_meta:get_vignette()
  return vignette
end


return manager