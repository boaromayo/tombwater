local menu = {}

local vignette = sol.surface.create"effects/fog_vignette.png"
local default_opacity = 190
local opacity = default_opacity
vignette:set_opacity(opacity)

function menu:on_draw(dst)
  vignette:draw(dst)
end

function menu:set_opacity(new_opacity)
  opacity = new_opacity
  vignette:set_opacity(opacity)
end


return menu