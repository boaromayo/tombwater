local menu = {}

local num_vision_flashes = 10
local num_vision_images = 13
local vision_delay = 30

local flash_surface = sol.surface.create()
local eye_burn = sol.surface.create("hud/madness_flash/eye.png")
local edge_effect = sol.surface.create("hud/madness_flash/edge_effect.png")


function menu:on_draw(dst)
  flash_surface:draw(dst)
  eye_burn:draw(dst)
  edge_effect:draw(dst, 0, (edge_effect.y_offset or 0) * -240)
end


function menu:on_started()
  local visions_seen = 0
  eye_burn:set_opacity(0)
  edge_effect:set_opacity(0)
  sol.timer.start(menu, 0, function()
    flash_surface:clear()
    if visions_seen < num_vision_flashes then
      visions_seen = visions_seen + 1
      vision_id = "hud/madness_flash/" .. string.format("%03d", math.random(1, num_vision_images)) .. ".png" --NOTE: all pngs must be named like 001.png, 003.png, 021.png, etc (3 digits)
      sol.surface.create(vision_id):draw(flash_surface)
      return vision_delay
    else
      --Show fading eye
      eye_burn:set_opacity(255)
      eye_burn:fade_out(30)
      edge_effect:set_opacity(100)
    end
  end)
end


function menu:start()
  local game = sol.main.get_game()
  --stop the madness effect if already started
  if sol.menu.is_started(menu) then sol.menu.stop(menu) end
  sol.menu.start(game, menu)
  --sound effect:
  sol.audio.play_sound("madness_start")
  --animate edge effect:
  sol.timer.start(menu, 30, function()
    edge_effect.y_offset = (((edge_effect.y_offset or 0) + 1) % 4)
    return true
  end)
end


function menu:stop()
  edge_effect:fade_out()
  sol.timer.start(menu, 300, function()
    if sol.menu.is_started(menu) then sol.menu.stop(menu) end
  end)
end

return menu