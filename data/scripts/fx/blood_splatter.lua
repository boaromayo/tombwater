local enemy_meta = sol.main.get_metatable"enemy"
local hero_meta = sol.main.get_metatable"hero"
local map_meta = sol.main.get_metatable"map"
local custom_entity_meta = sol.main.get_metatable"custom_entity"
local npc_meta = sol.main.get_metatable"npc"

function blood_splatter(entity, angle, amount)
  --Skip depending on game options:
  local game = entity:get_game()
  if not entity:get_game():get_value("option_show_blood") then return end
  local map = entity:get_map()
  amount = amount or 10
  local x, y, z = entity:get_center_position()
  local emitter = map:create_particle_emitter(x, y, z)
  emitter:set_preset("burst")
  emitter.width = entity:get_size() / 2
  emitter.angle = angle
  emitter.angle_variance = math.rad(45) + math.rad(10 * (amount / 10 - 1))
  emitter.particles_per_loop = 20 * (amount / 10)
  emitter.particle_sprite = "effects/blood_drop"
  emitter.particle_animation_loops = false
  emitter.particle_acceleration = -10 * (10 / amount)
  emitter.particle_fade_speed = 0
  emitter.particle_speed = 100 * (amount / 10)
  emitter:emit()

  local emitter_2 = map:create_particle_emitter(x, y, z)
  emitter_2:set_preset"burst"
  --emitter_2.frequency = 20
  emitter_2.width = entity:get_size() / 2
  emitter_2.angle = angle
  emitter_2.angle_variance = math.pi / 4
  emitter_2.particles_per_loop = 20 * (amount / 10)
  emitter_2.particle_sprite = "effects/smoke_particle"
  emitter_2.particle_color = {200, 5, 0}
  emitter_2.particle_opacity = {40,80}
  emitter_2.particle_fade_speed = 30
  emitter_2:emit()
--]]
--[[ Old way, no particle emitters:
  local spread = math.rad(70)
  local num_drops = 26
  if not map.blood_drops then map.blood_drops = {} end

  if amount then num_drops = amount end
  if not angle then
    angle = math.pi / 2
  end

  for i = 1, num_drops do
    local drop = sol.sprite.create("effects/blood_drop")
    drop.x, drop.y = x, y
    drop:set_direction(math.random(0, drop:get_num_directions() - 1))
    map.blood_drops[drop] = drop
    function drop:on_animation_finished()
      map.blood_drops[drop] = nil
    end
    local m = sol.movement.create"straight"
    m:set_angle(angle - (spread / 2) + (spread / num_drops * i))
    if i < num_drops / 3 then
      m:set_angle(math.random(0, math.pi * 2))
    end
    m:set_ignore_obstacles(true)
    m:set_speed(250)
    m:start(drop)
    sol.timer.start(map, 20, function()
      m:set_speed(math.max(m:get_speed() - 25, 1))
      return true
    end)
  end
--]]
end

--[[ For no-particle-emitter method:
map_meta:register_event("on_draw", function(self)
  local map = self
  if map.blood_drops then
    for _, drop in pairs(map.blood_drops) do
      map:draw_visual(drop, drop.x, drop.y)
    end
  end
end)
--]]

function enemy_meta:blood_splatter(amount)
  local hero = self:get_map():get_hero()
  blood_splatter(self, hero:get_angle(self), amount)
end


function hero_meta:blood_splatter(enemy, amount)
  local angle
  if enemy then
    angle = enemy:get_angle(self)
  else
    angle = math.pi / 2
  end
  blood_splatter(self, angle, amount)
end


local function other_blood_splatter(entity, amount, angle)
  angle = angle or math.pi / 2
  blood_splatter(entity, angle, amount)
end

function custom_entity_meta:blood_splatter(amount, angle)
  other_blood_splatter(self, amount, angle)
end

function npc_meta:blood_splatter(amount, angle)
  other_blood_splatter(self, amount, angle)
end

