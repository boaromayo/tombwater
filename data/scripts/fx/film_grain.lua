local surfaces = {}
local add_surfaces = {}
local intermed_surface = sol.surface.create()

local frequency = 30
local opacity = 50
local color_mod = {255,255,230}

for i = 0, 5 do
  surfaces[i] = sol.surface.create("fogs/noise/noise_0" .. i .. ".png")
  surfaces[i]:set_opacity(opacity)
  surfaces[i]:set_blend_mode"multiply"
  surfaces[i]:set_color_modulation(color_mod)
end
for i = 0, 5 do
  add_surfaces[i] = sol.surface.create("fogs/noise/noise_0" .. i .. ".png")
  add_surfaces[i]:set_opacity(opacity)
  add_surfaces[i]:set_blend_mode"add"
  add_surfaces[i]:set_color_modulation(color_mod)
end

intermed_surface:set_opacity(opacity)


local menu = {}

function menu:on_started()
  menu.noise_counter = 0
  sol.timer.start(menu, frequency, function()
    add_surfaces[menu.noise_counter]:draw(intermed_surface, 0, 0)
    surfaces[menu.noise_counter]:draw(intermed_surface, 0, 0)
    menu.noise_counter = ( menu.noise_counter + 1 ) % #surfaces
    return true
  end)
end

function menu:on_draw(dst)
  --add_surfaces[menu.noise_counter]:draw(dst, 0, 0)
  --surfaces[menu.noise_counter]:draw(dst, 0, 0)
  intermed_surface:draw(dst, 0, 0)
end


function sol.main.start_film_grain()
  sol.menu.start(sol.main, menu)
end

function sol.main.stop_film_grain()
  sol.menu.stop(menu)
end


local game_meta = sol.main.get_metatable"game"
game_meta:register_event("on_started", function(game)
  sol.menu.start(game, menu)
end)

function game_meta:set_film_grain_opacity(new_op)
  intermed_surface:set_opacity(new_op)
end

