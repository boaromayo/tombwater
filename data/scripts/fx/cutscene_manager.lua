local map_meta = sol.main.get_metatable"map"


function map_meta:start_cutscene()
  local map = self
  local hero = map:get_hero()
  local game = map:get_game()

  game:set_hud_enabled(false)
  hero:stop_dash()
  hero:freeze()
end

function map_meta:stop_cutscene()
  local map = self
  local hero = map:get_hero()
  local game = map:get_game()

  game:set_hud_enabled(true)
  hero:unfreeze()
  
end