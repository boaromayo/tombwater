local manager = {}

local hero_meta = sol.main.get_metatable"hero"

hero_meta:register_event("on_created", function(hero)
  local sprite = hero:get_sprite()
  sprite:register_event("on_frame_changed", function(self, animation, frame)
    if animation == "walking" and (frame == 0 or frame == 4) then
      if hero:get_ground_below() == "traversable" then
        local step_no = string.format("%02d", math.random(1, 10))
        sol.audio.play_sound("footsteps/step_" .. step_no)
      end
    end
  end)
end)

return manager