--Initializes values for a new savegame:

local menu = {}

-- Sets initial values to a new savegame file.
function menu:initialize_new_savegame(game)

  if sol.main.debug_mode then
    game:set_starting_location("debug", "destination")  -- Starting location.
  else
    --game:set_starting_location("demo/demo_1/mode_choice", "destination")
    game:set_starting_location("canyon_floor/canyon_floor", "destination")
  end
  --game:set_starting_location("demo/demo_1/tutorial", "destination")

  game:set_max_life(100)
  game:set_life(game:get_max_life())
  game:set_max_money(99999)
  game:set_max_magic(100)
  game:set_magic(100)
  game:set_value("max_amount_healing_flask",3)
  game:set_ability("lift", 1)
  game:set_ability("sword", 0)
  game:set_ability("swim", 1)
  game:set_max_oxygen_level(10)
  game:set_value("healing_flask_healing_amount", 100)

  game:set_value("charm_slots", 5)
  game:set_max_bullets(6)
  game:add_bullets(6)
  game:set_value("aim_style", "strafe")

  --Starting items:
  game:set_value("possession_homeward_talisman", 1)
  local starting_weapon = "solforge/cavalry_sabre"
  local starting_gun = "guns/pistol"
  game:get_item(starting_weapon):set_variant(1)
  game:get_item(starting_gun):set_variant(1)
  local healing_flask = game:get_item("inventory/healing_flask")
  healing_flask:set_variant(1)
  healing_flask:set_amount(3)
    --Equip them:
  game:set_value("equipped_weapon", starting_weapon)
  game:set_quickswap_item("melee", 1, starting_weapon)
  game:set_value("equipped_gun", starting_gun)
  game:set_quickswap_item("gun", 1, starting_gun)
  game:set_item_assigned(2, healing_flask)
  game:set_quickswap_item("item_2", 1, "inventory/healing_flask")
--]]

  --Items/Whatever for starting at scrublands:
  game:get_item("guns/shotgun"):set_variant(1)
  game:get_item("guns/shotgun"):set_variant(1)
  game:get_item("inventory/grenade"):set_variant(1)
  game:get_item("spells/arcane_bolt"):set_variant(1)

  --]]


  --Default Options Settings:
  game:set_value("option_show_blood", true)
  game:set_value("option_damage_dealt_modifier", 100)
  game:set_value("option_damage_received_modifier", 80)
  game:set_value("option_display_controls_as", "keyboard_and_mouse")

end

return menu
