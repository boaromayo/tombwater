local enemy_meta = sol.main.get_metatable"enemy"

--Fire Reactions:
function enemy_meta:react_to_fire(fire)
  local enemy = self
  if enemy.flammible then
    enemy:start_status_effect("burn")
  elseif enemy.build_up_status_effect then
    enemy:build_up_status_effect("burn", 20)
  end
  if not enemy.fire_immunity then
    enemy:process_hit(fire.damage or 20, "fire")
  end
end


--Ice Reactions:
function enemy_meta:react_to_ice(ice)
  local enemy = self
  if not enemy.ice_immunity then
    enemy:process_hit(ice.damage or 10, "ice")
  end
  if enemy.build_up_status_effect then
    enemy:build_up_status_effect("cold", 20)
  end
end


--Lightning Reactions:
function enemy_meta:react_to_lightning(lightning)
  local enemy = self
  if not enemy.lightning_immunity then
    enemy:process_hit(lightning.damage or 20, "lightning")
  end
end

function enemy_meta:react_to_lightning_bolt(lightning)
  local enemy = self
  if not enemy.lightning_immunity then
    enemy:process_hit(lightning.damage or 50, "lightning")
    enemy:react_to_fire(lightning)
    if enemy.build_up_status_effect then
      enemy:build_up_status_effect("burn", 95)
    end
  end
end
