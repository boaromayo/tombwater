local map_meta = sol.main.get_metatable"map"
local game_meta = sol.main.get_metatable"game"

local blood_ghost

game_meta:register_event("on_game_over_started", function(self)
  self:register_blood_ghost()
end)

function game_meta:register_blood_ghost()
  local game = self
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local money_amount = game:get_money()

  --Allow for manipulation of this amount from a charm
  money_amount = money_amount * (game.blood_ghost_money_percentage_charm_multiplier or 1)

  game:set_value("blood_ghost_map", hero:get_map():get_id())
  game:set_value("blood_ghost_x", x)
  game:set_value("blood_ghost_y", y)
  game:set_value("blood_ghost_layer", z)
  game:set_value("blood_ghost_exists", true)
  game:set_value("blood_ghost_money", money_amount)
  game:remove_money(money_amount)
end


map_meta:register_event("on_started", function(self)
  local map = self
  local game = map:get_game()
  if not game:get_value("blood_ghost_exists") then return end
  if map:get_id() ~= game:get_value("blood_ghost_map") then return end
  local x = game:get_value("blood_ghost_x")
  local y = game:get_value("blood_ghost_y")
  local z = game:get_value("blood_ghost_layer")
  --create a custom entity at coordinates
  map:create_blood_ghost(x, y, z)
end)


function map_meta:create_blood_ghost(x, y, z)
  local map = self
  local game = map:get_game()
  local ghost = map:create_custom_entity{
    x = x, y = y, layer = z,
    width = 16, height = 16, direction = 0,
    sprite = "hero/blood_ghost",
  }
  ghost:set_drawn_in_y_order(true)
  ghost:get_sprite():set_opacity(230)

  local heal_range = 48
  local heal_rate = 200
  local heal_amount = 2

  sol.timer.start(ghost, heal_rate, function()
    local hero = map:get_hero()
    if ghost:get_distance(hero) <= heal_range then
      game:add_life(heal_amount)
      if (not hero:get_sprite("blood_ghost_healing_sprite")) and (game:get_life() < game:get_max_life()) then
        local healing_sprite = hero:create_sprite("entities/lantern_sparkle", "blood_ghost_healing_sprite")
        healing_sprite:set_animation"summoning_circle"
        healing_sprite:set_opacity(80)
        sol.timer.start(hero, heal_rate, function()
          sol.audio.play_sound("spells/heal_blip")
          if (hero:get_distance(ghost) > heal_range) or (game:get_life() >= game:get_max_life()) or (not ghost:exists()) then
            hero:remove_sprite(healing_sprite)
          else
            return true
          end
        end)
      end

    end
    return true
  end)

  function ghost:die()
    ghost:remove()
    sol.audio.play_sound"ghost_killed"
    map:create_custom_entity{
      x = x, y = y, layer = z,
      width = 16, height = 16, direction = 0,
      sprite = "enemies/enemy_killed",
      model = "ephemeral_effect",
    }
    game:add_money(game:get_value("blood_ghost_money"))
    game:set_value("blood_ghost_exists", false)
  end

  function ghost:react_to_solforge_weapon()
    ghost:die()
  end

  function ghost:react_to_hero_projectile()
    ghost:die()
  end

  function map:remove_blood_ghost()
    if ghost:exists() then ghost:remove() end
  end
end


