local black = {}
local black_surface = sol.surface.create()
black_surface:fill_color{0,0,0}

function black:on_started()
  black_surface:set_opacity(0)
  black_surface:fade_in(2)
end

function black:on_draw(dst)
  black_surface:draw(dst)
end

local game_meta = sol.main.get_metatable"game"

local just_died = false

game_meta:register_event("on_started", function(game)
  local hero = game:get_hero()

  if not sol.menu.is_started(black) then
    sol.menu.start(game, black)
  end
  sol.timer.start(game, 1000, function()
    game:set_suspended(false)
    sol.menu.stop(black)
  end)
  game:respawn_at_checkpoint()
  if just_died then
    just_died = false
    sol.timer.start(game, 1000, function()
      if game:get_hero():get_state() == "free" then
        game:start_tutorial("death")
      else
        return 500
      end
    end)
  end
end)


game_meta:register_event("on_game_over_started", function(self)
  local game = self
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()

  hero:set_visible(false)
  sol.audio.stop_music()
  sol.audio.play_sound("church_bell_death")
  just_died = true
  local cam_surface = map:get_camera():get_surface()
  local dark_steps = 1
  local darkDelta = 3
  sol.timer.start(sol.main, 40, function()
    local rgb = cam_surface:get_color_modulation()
    cam_surface:set_color_modulation({math.max(rgb[1]-darkDelta, 0), math.max(rgb[2]-darkDelta, 0), math.max(rgb[3]-darkDelta, 0)})
    dark_steps = dark_steps + 1
    if dark_steps <= 20 then
      return true
    end
  end)

  local sprite_id = hero:get_sprite():get_animation_set()
  local dummy = map:create_custom_entity({
    x=x, y=y, layer=z, direction=0, width=16,height=16,
    sprite = sprite_id,
  })
  local sprite = dummy:get_sprite()
  sprite:set_ignore_suspend()
  sprite:set_animation("dying", function()
    sprite:set_animation"dead"
    sol.timer.start(game, 3000, function()
      sol.menu.start(sol.main, black)
      game:start()
    end)
  end)

  local hat = map:create_custom_entity{
    x = x, y = y, layer = z, width = 16, height = 16, direction = 0,
    sprite = "hero/hat",
  }
  local hat_sprite = hat:get_sprite()
  hat_sprite:set_ignore_suspend()

  local m = sol.movement.create("straight")
  m:set_angle(math.rad(340))
  m:set_speed(65)
  m:set_ignore_suspend(true)
  m:set_ignore_obstacles(true)
  m:start(hat)

  local i = 1
  sol.timer.start(game, 30, function()
    -- amplitude * math.sin( i / step)
    hat_sprite:set_xy( 10 * math.sin(i / 7), 0)
    i = i + 1
    return true
  end)

  --NOTE: add a "you died" thing here later?
end)



