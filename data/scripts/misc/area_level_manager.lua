--[[
Created by Max Mraz, licensed MIT
Automatically scales several enemy stats by map name
Adds a value: enemy.level to every enemy, which is the area level. This level scales damage, life, exp, and money dropped.

Note: to overwrite this for any enemy, give the enemy a property called "level", which will set the enemy's level, ignoring area level
This overwrite is useful if you want a tougher enemy in a lower-level area.
--]]

local map_meta = sol.main.get_metatable"map"

levels_by_prefix = {
  central_tombwater = 1,
  canyon_settlement = 2,
}


map_meta:register_event("on_started", function(self)
  local map = self
  local area_level = map.area_level --allow any map to overwrite its base level explicitly

  if not area_level then
    for prefix, table_level in pairs(levels_by_prefix) do
      if map:get_id():match(prefix) then
        area_level = table_level
      end
    end
  end

  if not area_level then --fallback in case the map isn't in the prefix table
    area_level = 1
  end
  --Set enemy stats
  for enemy in map:get_entities_by_type("enemy") do
    if not enemy.level then
      enemy.level = area_level - 1
    elseif enemy:get_property"level" then
      enemy.level = enemy:get_property"level"
    end

    --Set Life
    local base_life = enemy:get_life()
    enemy:set_life( base_life + base_life * .2 * enemy.level )

    --Note: enemy's damage is upped in the hero_meta function hero:process_hit()

    --Set Exp
    if enemy.exp then
      enemy.exp = enemy.exp + (enemy.exp * .1 * area_level)
    end

    --Set Money
    if enemy.money then
      enemy.money = enemy.money + (enemy.money * .2 * area_level)
    end
  end

end)

