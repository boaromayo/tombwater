local manager = {}

local enemy_meta = sol.main.get_metatable"enemy"
local game_meta = sol.main.get_metatable"game"

local GROWTH_CONST = 1.35


function game_meta:get_level()
  return self:get_value("hero_level") or 0
end

function game_meta:get_exp()
  return self:get_value("hero_exp") or 0
end


function game_meta:add_exp(exp)
  local game = self
  local current_level = game:get_value("hero_level") or 0
  local current_exp = game:get_value("hero_exp") or 0
  -- we want to add the base xp multipliers together
  exp = exp * (game.exp_mod or 1)
  game:set_value("hero_exp", current_exp + exp)
  manager:check_level_up()
end


function game_meta:get_required_exp(target_level)
  return math.floor( 30 * (target_level ^ GROWTH_CONST))
end


enemy_meta:register_event("on_dead", function(self)
  local enemy = self
  local game = self:get_game()
  local exp = enemy.exp
  if not exp then
    local current_level = game:get_value("hero_level") or 0
    exp = (current_level + 5) ^ GROWTH_CONST
  end
  game:add_exp(exp)
end)


function manager:check_level_up()
  local game = sol.main.get_game()
  local current_level = game:get_value("hero_level") or 0
  local current_exp = game:get_value("hero_exp") or 0


  local leveled_up = false
  while current_exp >= game:get_required_exp(current_level + 1) do
    --Remove exp cost to level up
    current_exp = current_exp - game:get_required_exp(current_level)
    game:set_value("hero_exp", cuurrent_exp)
    --Add level
    current_level = current_level + 1
    game:set_value("hero_level", current_level)
    --Add skill point
    local current_skill_points = game:get_value("hero_skill_points") or 0
    game:set_value("hero_skill_points", current_skill_points + 1)
    leveled_up = true
  end

  if leveled_up then
    sol.audio.play_sound"level_up"
    --Show level up message:
    game:show_hud_message{
      message = sol.language.get_string("game.level_up_alert"),
      sprite = "hud/message_queue_background",
      animation = "flash",
    }

    local hero = game:get_hero()
    local map = game:get_map()
    local x, y, z = hero:get_position()
    local sparkle = map:create_custom_entity{
      x=x, y=y, layer=z,
      width = 16, height = 16, direction = 0,
      sprite = "effects/level_up_effect",
      model = "ephemeral_effect",
    }
    sol.timer.start(sparkle, 20, function()
      local x, y, z = hero:get_position()
      sparkle:set_position(x, y+1, z)
      return true
    end)
    local emitter = map:create_particle_emitter(x, y, z)
    emitter.target = hero
    emitter.width = 16
    emitter.duration = 100
    emitter.angle = math.pi / 2
    emitter.angle_variance = math.rad(10)
    emitter.particle_sprite = "effects/particle_line_vertical"
    emitter.particle_speed = 50
    emitter.particle_acceleration = 50
    emitter.particle_fade_speed = 30
    emitter.particle_color = {255,200,30}
    emitter:emit()
  end
end


return manager