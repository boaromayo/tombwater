--Script removed
--Was used to support custom commands and remapping in 1.6, now unneeded
--Would do backward compatibility, except it's GPL code so publisher doesn't want to ship with it

local manager = {}

function manager:init()

end

function manager:get_command_keyboard_binding(command)
  return {}
end


function manager:set_command_keyboard_binding(command, key)
end

function manager:get_command_joypad_binding(command)
  return ""
end


function manager:set_command_joypad_binding(command, joypad_string)
end

function manager:is_command_held(command)
  return false
end

function manager:get_command_from_key(key)
  return ""
end

function manager:get_command_from_joypad(joypad_string)
  return ""
end

function manager:get_command_from_button(button)
  return ""
end

function manager:get_command_from_axis(axis, state)
  return ""
end

function manager:get_command_from_hat(hat, direction8)
return ""
end

function manager:capture_command_binding(command, callback)
end

--// Resets all control bindings to defaults
function manager:reset_defaults()
end

--Little convenience hack, allow to reset commands to solarus defaults, even if those aren't game defaults:
function manager:reset_solarus_default()
end


return manager
