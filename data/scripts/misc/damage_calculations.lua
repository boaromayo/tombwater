local game_meta = sol.main.get_metatable"game"
local enemy_meta = sol.main.get_metatable"enemy"
local hero_meta = sol.main.get_metatable"hero"

--Config:
local bleed_damage_multiplier = 1.45
local cold_damage_miltiplier = 1.2


--Hero damage OUTPUT
function game_meta:calculate_hero_output_damage(base_damage, scaling_stat) --For calculating how much damges the hero does
  local game = self
  local damage = base_damage

  --Stat scaling
  if scaling_stat then
    local level = game:get_value("stat_" .. scaling_stat) or 0
    damage = damage + base_damage * .1 * level
  end

  --Potions that affect damage
  local potion_damage_multipliers = game.potion_damage_multipliers or {}
  if potion_damage_multipliers[scaling_stat] then
    damage = damage * (potion_damage_multipliers[scaling_stat] or 1)
  end

  --Charms that affect damage
  if game.charm_damage_multiplier then
    damage = damage * game.charm_damage_multiplier
  end
  if game.charm_damage_multipliers then
    damage = damage * (game.charm_damage_multipliers[scaling_stat] or 1)
  end

  --Spells that affect damage:
  if game.spell_damage_multipliers then
    damage = damage * (game.spell_damage_multipliers[scaling_stat] or 1)
  end

  --Gameplay Options Modifier:
  damage = damage * (game:get_value("option_damage_dealt_modifier") / 100)

  return damage
end


--Hero damage INPUT
function hero_meta:calculate_input_damage(damage, damage_type)
  local game = self:get_game()

  --reduce damage based on defense:
  local defense = game:get_value("stat_defense") or 0
  damage = damage - (defense * 1.3)

  --Madness: increased damage, extra increased damage from magic attacks
  if self:is_status_effect_active("madness") then
    damage = damage * 1.5
    if damage_type == "magic" then damage = damage * 1.3 end
  end

  --Status Effects:
  if self:is_status_effect_active"bleed" then
    damage = damage * bleed_damage_multiplier
  end
  if self:is_status_effect_active"cold" then
    damage = damage * cold_damage_multiplier
  end

  --Charms that affect damage
  if game.charm_hero_damage_multiplier then
    damage = damage * game.charm_hero_damage_multiplier
  end

  --Spells that affect damage:
  if game.spell_defense_multiplier then
    damage = damage * (game.spell_defense_multiplier or 1)
  end

  --Gameplay Options Modifier:
  damage = damage * (game:get_value("option_damage_received_modifier") / 100)

  --Minimum of 1 damage:
  if damage < 1 then damage = 1 end

  return damage
end


--Enemy Damage INPUT
function enemy_meta:calculate_damage_input(damage, damage_type)
  local enemy = self
  local game = self:get_game()

  --Damage type weaknesses/resistances:
  if enemy.damage_type_mods and enemy.damage_type_mods[damage_type] then
    damage = damage * enemy.damage_type_mods[damage_type]
  end

  --Spells that affect damage:
  if game.spell_damage_multipliers then
    damage = damage * (game.spell_damage_multipliers[damage_type] or 1)
  end

  --Charm effects:
  local charm_damage_multipliers = game.charm_damage_multipliers or {}
  if charm_damage_multipliers[damage_type] then
    damage = damage * charm_damage_multipliers[damage_type]
  end
  --Status damager charm (this is for a specific charm):
  if enemy:is_status_effect_active() and game.charm_active_status_damage_multiplier then
    damage = damage * game.charm_active_status_damage_multiplier
  end

  --Status Effects:
  if enemy:is_status_effect_active("bleed") then
    damage = damage * bleed_damage_multiplier
  end
  if enemy:is_status_effect_active"cold" then
    damage = damage * cold_damage_multiplier
  end

  return damage
end

