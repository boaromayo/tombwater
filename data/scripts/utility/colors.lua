sol.colors = {
  --Feature colors
  amber = {227, 173, 0},
  gunmetal = {129, 120, 105},

  --Menus:
  ui_black = {25, 24, 24},
  ui_grey_light = {161, 157, 153},
  ui_grey_med = {113, 105, 97},
  ui_grey_dark = {49, 47, 46},

  ui_off_white = {240,230,200},
}