local sensor_meta = sol.main.get_metatable"sensor"

function sensor_meta:on_activated()
  local sensor = self
  local map = self:get_map()

  --Enemy ambushes
  local ambush_enemy_id = self:get_property("ambush_enemy_id")
  if ambush_enemy_id then
    local enemy = map:get_entity(ambush_enemy_id)
    if not enemy then return end
    assert(enemy.ambush_attack, "Error in ambush sensor. Enemy with id: '" .. ambush_enemy_id .. "' does not have enemy:ambush_attack() defined")
    if not enemy.aggro then
      enemy.aggro = true
      sol.timer.stop_all(enemy)
      enemy:ambush_attack()
    end
    sensor:remove()
  end

  --Aggro nearby enemies
  local enemy_aggro_range = self:get_property("nearby_enemy_aggro_range")
  if enemy_aggro_range then
    local x, y, z = sensor:get_position()
    for entity in map:get_entities_in_rectangle(x - enemy_aggro_range, y - enemy_aggro_range, enemy_aggro_range * 2, enemy_aggro_range * 2) do
      if entity:get_type() == "enemy" and entity.start_aggro then
        entity:start_aggro()
      end
    end
  end

  --Open / close doors
  local open_doors_prefix = self:get_property("open_doors_prefix")
  if open_doors_prefix then
    map:open_doors(open_doors_prefix)
    self:remove()
  end
  local close_doors_prefix = self:get_property("close_doors_prefix")
  if close_doors_prefix then
    map:close_doors(close_doors_prefix)
    self:remove()
  end

  --Layer Up
  if sensor:get_property("layer_up") then
    local hero = map:get_hero()
    hero:set_layer(hero:get_layer() + 1)
  end

  if sensor:get_property("layer_down") then
    local hero = map:get_hero()
    hero:set_layer(hero:get_layer() - 1)
  end

  --Mad stagecoach:
  local is_stagecoach_sensor = self:get_property("mad_stagecoach")
  if is_stagecoach_sensor then
    map:start_mad_stagecoach_encounter()
    self:remove()
  end

end

