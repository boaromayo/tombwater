local item_meta = sol.main.get_metatable("item")

function item_meta:setup_amount_stuff(default_max_amount)
  --Uses item name to set savegame variable, amount variable, and max amount
  local item = self
  local game = item:get_game()

  default_max_amount = default_max_amount or 1
  local save_name = item:get_name():gsub("/", "_")
  item:set_savegame_variable("possession_" .. save_name)
  item:set_amount_savegame_variable("amount_" .. save_name)
  local max_amount_string = save_name .. "_max_amount"

  local max_amount = game:get_value(max_amount_string)
  if not max_amount then
    max_amount = default_max_amount
    game:set_value(max_amount_string, default_max_amount)
  end
  item:set_max_amount(max_amount)
  item:set_assignable(true)
  item:set_ammo("_amount")
end


function item_meta:set_fill_on_checkpoint(refill_bool)
  if refill_bool == nil then refill_bool = true end
  local item = self
  local game = self:get_game()
  if not game.fill_on_checkpoint_items then game.fill_on_checkpoint_items = {} end
  game.fill_on_checkpoint_items[item:get_name()] = (refill_bool and item or nil)
end


item_meta:register_event("on_created", function(item)
  item:set_brandish_when_picked(false)
end)


--Show item popup modal:
local popup_menu = require"scripts/menus/components/item_popup"
function item_meta:show_popup(variant)
  local item = self
  popup_menu:set_item(item:get_name(), variant)
  if sol.menu.is_started(popup_menu) then sol.menu.stop(popup_menu) end
  sol.menu.start(sol.main.get_game(), popup_menu)


  --Show tutorials after closing the popup for some items:
  local function queue_tutorial(game, id)
    sol.timer.start(game, 0, function()
      if game:get_hero():get_state() == "free" then
        game:start_tutorial(id)
      else
        return 50
      end
    end)
  end

  function popup_menu:on_finished()
    local id = item:get_name()
    local game = item:get_game()
    if id:match("solforge/") then
      queue_tutorial(game, "weapon")

    elseif id:match("charms/") then
      queue_tutorial(game, "charms")

    elseif id:match("guns/") then
      queue_tutorial(game, "new_gun")

    elseif id:match("gear/dead_mans_shot") then
print"GOT THE SHOT"
      queue_tutorial(game, "wraith_shot")

    end
  end
end


--Show item_panel HUD element:
function item_meta:show_panel(variant)
  local item = self
  if item.prevent_panel then return end
  item.last_panel_shown_time = sol.main.get_elapsed_time()
  local game = item:get_game()
  variant = variant or item:get_variant()

  local hud = game:get_hud() or {}
  local menu = hud.elements and hud.elements.item_panels
  if menu then menu:add_item(item, variant) end --display a panel for the item on the hud
end
--]]


--Returns an array of nearby enemies, sorted by distance to the hero (closest first) -- can be limited to X number of enemies with a second argument
function item_meta:select_nearby_enemies(range, limit)
  local map = self:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  local enemies = {}
  local enemies_captured = 0

  for ent in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
    local distance = ent:get_distance(hero)
    if (ent:get_type() == "enemy") and (distance <= range) then
      enemies_captured = enemies_captured + 1
      ent.snedist = distance
      enemies[enemies_captured] = ent
    end
  end
  --Sort table by distance
  table.sort(enemies, function(a, b)
    return a.snedist < b.snedist
  end)
  --Truncate:
  local limited_enemies = {}
  for i = 1, limit do
    limited_enemies[i] = enemies[i]
  end

  return limited_enemies
end

