-- Initialize hero behavior specific to this quest.

require("scripts/multi_events")

local hero_meta = sol.main.get_metatable("hero")



hero_meta:register_event("on_created", function(self)
  local hero = self
  hero:set_walking_speed(90)
end)


hero_meta:register_event("on_taking_damage", function(hero, damage)
  local game = hero:get_game()

  if damage < 1 then
    damage = 1
  end
  --if this attack would kill you in 1 hit at above a certain percent of max life
  local guts_save_percentage = .4
  local guts_save_cooldown = 60 * 1000
  if damage >= game:get_life()
  and game:get_life() >= game:get_max_life() * guts_save_percentage
  and damage >= game:get_max_life() * (1 - guts_save_percentage)
  and not game.guts_save_used then
    --leave you with 1hp
    damage = game:get_life() - 1
    sol.audio.play_sound"ohko"
    --set this mechanic on a cooldown
    game.guts_save_used = true
    sol.timer.start(game, guts_save_cooldown, function() game.guts_save_used = false end)
  elseif damage >= game:get_max_life() * .5 then
    sol.audio.play_sound"oh_lotsa_damage"
  end

  game:remove_life(damage)

  --Trigger charm event:
  if game.charm_manager then game.charm_manager:trigger_event("on_hurt") end

  hero:start_iframes() --defined below

  --Tutorial to use flask to heal
  sol.timer.start(game, 500, function() game:start_tutorial("flask") end)
end)


function hero_meta:process_hit(props)
  local hero = self
  local game = hero:get_game()
  local damage = props.damage or 1
  local enemy = props.enemy
  local damage_type = props.damage_type or "physical"
  local non_staggering = props.non_staggering or false
  local state, state_ob = hero:get_state()

  --Increase enemy damage based on level - this allows reusing the same enemies in later areas but they do more damage
  if enemy then
    enemy.level = enemy.level or 1 --default to level 1
    damage = damage + (damage * .15 * enemy.level)
  end

  if hero.calculate_input_damage then
    damage = hero:calculate_input_damage(damage, damage_type)
  end

  if hero.blood_splatter then hero:blood_splatter(enemy) end

  --Trigger charm event:
  if game.charm_manager then game.charm_manager:trigger_event("on_hit", enemy, damage) end

  if non_staggering then
    game:remove_life(damage)
    hero:start_iframes()
  else
    hero:start_hurt(damage)
  end
  --[[
  --Hitstop
  game:set_suspended(true)
  sol.timer.start(game, 20, function() game:set_suspended(false) end)
  --]]
  hero:get_map():get_camera():shake{shake_count = 12, amplitude = 3, zoom_scale = 1.02}
end


--Note: this is kind of a messy function, just random shit that happens on state change is getting shoved in here:
hero_meta:register_event("on_state_changed", function(self, state)
  local hero = self
  local game = sol.main.get_game()

  if state == "back to solid ground" then
    hero:freeze()
    hero:set_visible(false)
    sol.timer.start(hero, 300, function()
      hero:set_visible(true)
      hero:unfreeze()
      hero:set_blinking(true, 1200)
      hero:set_invincible(true,1500)
    end)

  elseif state == "treasure" then
    --TODO: figure this out once there's treasure dialogs, should there be brandishing or not?
    hero:unfreeze()
    sol.timer.start(hero, 500, function()
      --hero:unfreeze()
    end)

  end
end)


hero_meta:register_event("on_state_changing", function(hero, old_state, next_state)
  if old_state == "jumping" then
    sol.audio.play_sound("hero_lands")
  end
end)


local MAX_BUFFER_SIZE = 48
hero_meta:register_event("on_position_changed", function(self, x, y, z)
  local hero = self
  if not hero.position_buffer then hero.position_buffer = {} end
  local hero = self
  local dir = hero:get_sprite():get_direction()
  table.insert(hero.position_buffer, 1, {x=x,y=y,layer=l,direction=dir})

  if #hero.position_buffer > MAX_BUFFER_SIZE then
    table.remove(hero.position_buffer)
  end
end)


function hero_meta:get_can_be_hurt()
  local hero = self
  local can_be_hurt = not hero:is_invincible()
  local state, state_ob = hero:get_state()
  if state == "custom" then state = state_ob:get_description() end
  if (state == "falling") or (state == "stairs") or (state == "treasure") or (state == "victory")
  or (state == "feather_jumping") then
    can_be_hurt = false
  end
  return can_be_hurt
end


function hero_meta:start_iframes(length)
  local hero = self
  local game = hero:get_game()
  local base_iframe_length = length or 200
  local iframe_length = base_iframe_length + (game:get_value("hurt_iframes_bonus") or 0)
  hero:set_invincible(true, iframe_length)
  hero:set_blinking(true, iframe_length)
end


function hero_meta:ragdoll(direction, distance)
  local hero = self
  local state = sol.state.create("ragdolling")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_traverse_ground("hole", true)
  state:set_can_traverse_ground("deep_water", true)
  state:set_can_traverse_ground("lava", true)
  state:set_affected_by_ground("hole", false)
  state:set_affected_by_ground("deep_water", false)
  state:set_affected_by_ground("lava", false)
  state:set_gravity_enabled(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:set_direction(sol.main.get_direction4(direction))
  hero:start_state(state)
  hero:set_animation"ragdolling"

  local function end_movement()
    hero:unfreeze()
    hero:freeze()
    hero:start_knock_down()
  end

  local m = sol.movement.create"straight"
  m:set_speed(300)
  m:set_angle(direction)
  m:set_max_distance(distance)
  m:start(hero, function() end_movement() end)
  function m:on_obstacle_reached()
    end_movement()
  end
end


function hero_meta:start_knock_down(duration)
  local hero = self
  local game = self:get_game()
  duration = duration or 700
  local state = sol.state.create("knocked_down")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:start_state(state)
  hero:set_animation"knocked_down"
  sol.timer.start(hero, duration - 200, function()
    if not game.playing_knock_down_getup_sound then
      game.playing_knock_down_getup_sound = true
      sol.audio.play_sound"fabric_rustle"
      sol.timer.start(game, 100, function() game.playing_knock_down_getup_sound = false end)
    end
    hero:set_animation("getting_up", function()
      hero:set_direction(3)
      hero:unfreeze()
    end)
  end)
end


function hero_meta:knock_up(stun_duration)
  local hero = self
  hero:freeze()
  hero:set_animation("knocked_up", function()
    local dir = (hero:get_direction() + 2) % 4
    hero:set_direction(dir)
    hero:start_knock_down(stun_duration)
  end)
end


function hero_meta:step_forward(distance, speed)
  local hero = self
  local map = self:get_map()
  distance = distance or 8
  speed = speed or 90
  local speed_mod = (distance - 24) * 4
  if distance > 24 then speed = speed + speed_mod end
  local angle = hero:get_direction() * math.pi / 2

--Check whether an enemy is in your way, don't step forward if so:
--Removed this because I figured out how to make enemies non-traversable to just the hero (added a collider entity that always moves with them)
--[[
  local x, y, z = hero:get_position()
  local is_obstacle = false
  for i = 0, distance do
    dx = math.cos(angle) * i
    dy = math.sin(angle) * i * -1
    local ground = map:get_ground(x + dx, y + dy, z)
    if hero:test_obstacles(dx, dy) or ground == "deep_water" or ground == "hole" or ground == "lava" then
      is_obstacle = true
      distance = math.max(0, i - 4)
      break
    end
    local enemy_catch_w, enemy_catch_h = 16, 16
  end
  local capture_dist = 8
  local rect_dx = {[0] = 0, [1] = -4, [2] = capture_dist * -1, [3] = -4}
  local rect_dy = {[0] = -4, [1] = capture_dist * -1, [2] = -4, [3] = 0}
  local direction = hero:get_direction()
  local is_horiz = ((direction == 0) or (direction == 2)) and true or false
  local rect_width, rect_height = 8, 8
  if is_horiz then rect_width = capture_dist
  else rect_height = capture_dist end
  for ent in map:get_entities_in_rectangle(x + rect_dx[direction], y + rect_dy[direction], rect_width, rect_height) do
    if ent:get_type() == "enemy" then
      is_obstacle = true
      distance = 0
    end
  end
  if distance < 4 then return end
--]]

  local m = sol.movement.create("straight")
  m:set_angle(angle)
  m:set_speed(speed)
  m:set_max_distance(distance)
  m:start(hero)
end


function hero_meta:has_los(entity) 
  local los = true
  local map = self:get_map()
  local x, y, z = self:get_position()
  local dx, dy = 0, 0
  local distance = self:get_distance(entity)
  local angle = self:get_angle(entity)
  for i=0, distance do
    dx, dy = math.floor(math.cos(angle)*i), -math.floor(math.sin(angle)*i)
    if self:test_obstacles(dx, dy) then
        local ground = map:get_ground(x + dx, y + dy, z)
        if ground ~= "deep_water" and ground ~= "shallow_water" and ground ~= "hole" and ground ~= "lava" then
          los = false
          break
        end
    end
  end

  return los
end


function hero_meta:start_spellcasting(duration,callback)
  local casting_state = sol.state.create("casting")
  local hero = self
  casting_state:set_can_control_direction(false)
  casting_state:set_can_control_movement(false)
  casting_state:set_can_use_item(false)
  hero:start_state(casting_state)
  sol.timer.start(casting_state, duration, function()
    callback()
  end)
end


function hero_meta:stun(duration)
  local hero = self
  duration = duration or 2000 --default duration: 2000ms
  local state = sol.state.create("stunned")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:start_state(state)
  hero:set_animation("stunned")
  sol.timer.start(hero, duration, function()
    hero:set_animation"stopped"
    hero:unfreeze()
  end)
end


return true