local switch_meta = sol.main.get_metatable"switch"


function switch_meta:on_activated()
  local switch = self
  local map = self:get_map()

  local door_prefix = switch:get_property("door_prefix")
  if door_prefix then
    map:open_doors(door_prefix)
  end

  local tripwire_hazard = switch:get_property("tripwire_hazard")
  if tripwire_hazard then
    local hazard = map:get_entity(tripwire_hazard)
    assert(hazard, "Could not find entity with name:" .. tripwire_hazard)
    hazard:go(hazard:get_sprite():get_direction() * (math.pi / 2))
  end

  --Dart shooter trap
  local dart_shooter_prefix = switch:get_property("dart_shooter_prefix")
  if dart_shooter_prefix then
    sol.audio.play_sound"switch"
    for shooter in map:get_entities(dart_shooter_prefix) do
      shooter:shoot()
    end
    sol.timer.start(switch, 6000, function()
      switch:set_activated(false)
    end)
  end

end


function switch_meta:toggle()
  local switch = self
  if not switch:is_activated() then
    sol.audio.play_sound("switch")
    switch:set_activated(true)
    switch:on_activated()
  else
    sol.audio.play_sound("switch")
    switch:set_activated(false)
    if switch.on_inactivated then switch:on_inactivated() end
  end
end


switch_meta:register_event("on_created", function(switch)
  --Create an interactable entity on top of solid switches so you can use the action button instead of needing to use a sword or whatever:
  if not switch:is_walkable() then
    local map = switch:get_map()
    local x, y, z = switch:get_position()
    local width, height = switch:get_size()
    local entity = map:create_custom_entity{
      x = x, y = y, layer = z, width = width, height = height, direction = 0,
    }
    entity:set_origin(switch:get_origin())

    function entity:on_interaction()
      switch:toggle()
    end
  end
end)


