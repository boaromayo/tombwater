local chest_meta = sol.main.get_metatable"chest"

function chest_meta:on_created()
  local chest = self
  local game = self:get_game()
  local treasure, variant, savegame_var = chest:get_treasure()
  if not savegame_var then
    local x, y, z = self:get_position()
    local map_id = self:get_map():get_id():gsub("/", "_")
    savegame_var = map_id .. "_chest_" .. treasure:gsub("/", "_") .. "_" .. x .. "_" .. y .. "_" .. z
    if game:get_value(savegame_var) then --already opened chest
      chest:set_open(true)
    else
      chest:set_treasure(treasure, variant, savegame_var)
    end
  end

  --Add a sparkle to chests so they stand out against background clutter:
  sol.timer.start(chest, 100, function()
    if not chest:is_open() then
      local sparkle = chest:create_sprite("entities/sparkle")
      sparkle:set_xy(math.random(-8, 8), math.random(-24, 0))
      sol.timer.start(chest, 500, function()
        chest:remove_sprite(sparkle)
      end)
      return math.random(1200, 1900)
    end
  end)

end


chest_meta:register_event("on_opened", function(chest, item, variant, savegame_variable)
  item:show_popup()
  sol.main.get_game():get_hero():start_treasure(item:get_name(), variant, variable)
end)


function chest_meta:react_to_solforge_weapon()
  local chest = self
  local game = self:get_game()
  local treasure_name, variant, variable = chest:get_treasure()
  local item = game:get_item(treasure_name)
  sol.audio.play_sound("chest_open")
  item:show_popup()
  game:get_hero():start_treasure(treasure_name, variant, variable)

  chest:set_open(true)
end

