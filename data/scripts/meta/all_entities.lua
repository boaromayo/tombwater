local metas = {
  sol.main.get_metatable"hero",
  sol.main.get_metatable"dynamic_tile",
  sol.main.get_metatable"teletransporter",
  sol.main.get_metatable"destination",
  sol.main.get_metatable"pickable",
  sol.main.get_metatable"destructible",
  sol.main.get_metatable"carried_object",
  sol.main.get_metatable"chest",
  sol.main.get_metatable"shop_treasure",
  sol.main.get_metatable"enemy",
  sol.main.get_metatable"npc",
  sol.main.get_metatable"block",
  sol.main.get_metatable"jumper",
  sol.main.get_metatable"switch",
  sol.main.get_metatable"sensor",
  sol.main.get_metatable"separator",
  sol.main.get_metatable"wall",
  sol.main.get_metatable"crystal",
  sol.main.get_metatable"crystal_block",
  sol.main.get_metatable"stream",
  sol.main.get_metatable"door",
  sol.main.get_metatable"stairs",
  sol.main.get_metatable"bomb",
  sol.main.get_metatable"explosion",
  sol.main.get_metatable"fire",
  sol.main.get_metatable"arrow",
  sol.main.get_metatable"hookshot",
  sol.main.get_metatable"boomerang",
  sol.main.get_metatable"camera",
  sol.main.get_metatable"custom_entity",
}


local entity_methods = {

  --Plays a sound effect that is automatically attenuated based on the entity's proximity to the hero
  make_sound = function(entity, sound_id, falloff_rate)
    --NOTE: falloff rate affects how much the sound is affected by distance to hero.
    --Higher numbers mean the sound will carry further
    --A falloff rate of 1 means 1% sound drop per pixel to hero
    --A falloff rate of 5 means 0.2% sound drop per pixel to hero
    local map = entity:get_map()
    local hero = map:get_hero()
    falloff_rate = falloff_rate or 3
    local distance = entity:get_distance(hero)
    local sfx = sol.sound.create(sound_id)
    local max_volume = sol.audio.get_sound_volume()
    local vol = max_volume * (100 - (distance / falloff_rate)) / 100
    sfx:set_volume(vol)
    if vol > 0 then sfx:play() end
  end,

}


local function apply_function(k, fn)
  for _, meta in pairs(metas) do
    meta[k] = fn
  end
end


for k, fn in pairs(entity_methods) do
  apply_function(k, fn)
end


