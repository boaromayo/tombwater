local debug_keys = {}

function debug_keys:initialize(game)

  local DEBUG_MODE = sol.main.debug_mode
  local ignoring_obstacles

  function game:set_debug_mode(mode) DEBUG_MODE = mode end

  --Debug Keys
  game:register_event("on_key_pressed", function(self, key, modifiers)
    local hero = game:get_hero()

    if key == "f12" and DEBUG_MODE then
      local debug_menu = require("scripts/menus/debug_menu")
      if sol.menu.is_started(debug_menu) then
        sol.menu.stop(debug_menu)
      else
        sol.menu.start(game, debug_menu)
      end

    elseif key == "r"  and DEBUG_MODE then
      if hero:get_walking_speed() == 300 then
        hero:set_walking_speed(debug.normal_walking_speed)
      else
        debug.normal_walking_speed = hero:get_walking_speed()
        hero:set_walking_speed(300)
      end

    elseif key == "t" and DEBUG_MODE then
      if not ignoring_obstacles then
        hero:get_movement():set_ignore_obstacles(true)
        ignoring_obstacles = true
      else
        hero:get_movement():set_ignore_obstacles(false)
        ignoring_obstacles = false
      end

    elseif key == "h" and DEBUG_MODE and modifiers.control then
      if modifiers.shift then
        game:set_max_life(game:get_max_life() - 25)
      else
        game:add_max_life(25)
        game:set_life(game:get_max_life())
      end

    elseif key == "h" and DEBUG_MODE then
      if modifiers.shift then
        game:remove_life(25)
      else
        game:set_life(game:get_max_life())
      end

    elseif key == "j" and DEBUG_MODE then
      hero:clear_status_effect_buildup"madness"
      hero:stop_status_effect"madness"

    elseif key == "b" and DEBUG_MODE then
      game:set_bullets(game:get_max_bullets())

    elseif key == "l" and DEBUG_MODE then

    elseif key == "m" and DEBUG_MODE then
      print("You are on map: " .. game:get_map():get_id())
      local x, y, l = hero:get_position()
      print("at coordinates: " .. x .. ", " .. y .. ", " .. l)

    elseif key == "y" and DEBUG_MODE then
      --helicopter shot
      if not game.helicopter_cam then
        game:get_map():helicopter_cam()
      else
        game:get_map():exit_helicopter_cam()
        require("scripts/action/hole_drop_landing"):play_landing_animation()
      end

    elseif key == "u" and DEBUG_MODE then
      game:get_hud():set_enabled(not game:get_hud():is_enabled())

    elseif key == "p" and DEBUG_MODE then
      if game:is_suspended() then
        game:set_suspended(false)
      else
        game:set_suspended(true)
      end

    end
  end)

end

return debug_keys
