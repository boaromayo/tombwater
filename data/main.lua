-- Main Lua script of the quest.
print"Boutta start"
require("scripts/multi_events")
require("scripts/features")
local settings = require"scripts/settings"
local initial_menus_manager = require("scripts/menus/initial_menus")


function sol.main:on_started()

  settings:load()
  math.randomseed(os.time())

  --Determine version info: solarus version, debug mode, etc.
  sol.main.debug_mode = sol.file.exists("debug.lua")
  sol.main.version = {}
  sol.main.version.major, sol.main.version.minor, sol.main.version.patch = sol.main.get_solarus_version():match("(%d+).(%d+).(%d+)")
  for k, ver in pairs(sol.main.version) do sol.main.version[k] = tonumber(ver) end
  if sol.main.version.major <= 1 and sol.main.version.minor <= 6 then
    print("WARNING: Solarus version <= 1.6 detected. This quest has been updated to 1.7, but we'll try to account for everything")
    sol.main.old_controls_version = true
  end

  --Hide cursor
  sol.video.set_cursor_visible(false)

  --Set default volumes
  if not settings:get_value("default_volumes_set") then
    sol.audio.set_music_volume(40)
    sol.audio.set_sound_volume(40)
    settings:set_value("default_volumes_set", true)
  end

  --Initial menus:
  initial_menus_manager:start()
end


-- Event called when the program stops.
function sol.main:on_finished()
  settings:save()
end


-- Event called when the player pressed a keyboard key.
sol.main:register_event("on_key_pressed", function(self, key, modifiers)

  local handled = false
  if key == "f11" or
    (key == "return" and (modifiers.alt or modifiers.control)) then
    -- F11 or Ctrl + return or Alt + Return: switch fullscreen.
    sol.video.set_fullscreen(not sol.video.is_fullscreen())
    sol.video.set_cursor_visible(not sol.video.is_fullscreen())
    handled = true
  elseif key == "f4" and modifiers.alt then
    -- Alt + F4: stop the program.
    sol.main.exit()
    handled = true
  end

  return handled
end)


