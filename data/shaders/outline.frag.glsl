//4-direction costomizable border
#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

uniform sampler2D sol_texture;
COMPAT_VARYING vec2 sol_vtex_coord;
COMPAT_VARYING vec4 sol_vcolor;
uniform vec2 sol_input_size;
uniform vec4 border_color = vec4(0.90, 0.85, 0.75, 1.0);
//uniform float src_x = 0.0;
//uniform float src_y = 16.0;
//uniform float width = 16.0;
//uniform float height = 16.0;

//returns whether a pixel at given coordinates is transparent (alpha is equal to zero), which implies that it must be in the range. If not, consider it transparent.
bool is_transparent(vec2 c){
  //Note: commented out because it's annoying to need to pass in the src and size
  //if we're outside the bounds of the sprite frame, declare it transparent
  //if (c.x < (src_x / sol_input_size.x) || c.y < (src_y / sol_input_size.y) || c.x > ((src_x + width) / sol_input_size.x) || c.y > (src_y + height) / sol_input_size.y ) {
    //return true;
  //}
  if (c.x>0.0 && c.x<1.0 && c.y>0.0 && c.y<1.0) { //ok, we are in the surface
    return (COMPAT_TEXTURE(sol_texture, c).a==0.0);
  }
  return true; //we are outside the texture, so just declare it transparent
}

void main() {
    vec2 xy=sol_vtex_coord;
    float dx=1.0/sol_input_size.x;
    float dy=1.0/sol_input_size.y;
    vec4 color=COMPAT_TEXTURE(sol_texture, sol_vtex_coord);  

    FragColor = color; 
    if (is_transparent(xy)){
      bool left = is_transparent(vec2(xy.x-dx, xy.y));
      bool right = is_transparent(vec2(xy.x+dx, xy.y));
      bool top = is_transparent(vec2(xy.x, xy.y+dy));
      bool bottom = is_transparent(vec2(xy.x, xy.y-dy));        
      if (!left || !right || !top || !bottom) {
         FragColor=border_color;
      }
    }
}