#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

//inputs from vertex shader
COMPAT_VARYING vec2 sol_vtex_coord;

uniform vec2 sol_input_size;
uniform vec2 sol_output_size;
uniform int sol_time;
uniform sampler2D sol_texture;


float random(vec2 st) {
  return fract(sin(dot(st.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

void main() {
  vec4 input_color = COMPAT_TEXTURE(sol_texture, sol_vtex_coord);
  float strength = .5;

  vec2 uv = sol_vtex_coord.xy;
  float seed = (uv.x + uv.y * sol_output_size.x) * 0.1;
  vec2 offset = vec2(random(vec2(seed, sol_time * 0.001)), random(vec2(seed, sol_time * 0.001 + 10.0)));
  float noise = fract(sin(dot(uv + offset, vec2(12.9898, 78.233))) * 43758.4543);
  vec3 grain = vec3(noise * strength);
  vec3 output_color = mix(input_color.rgb, grain, 0.5);
  FragColor = vec4(output_color, input_color.a);

  //This is the best one so far:
  //vec4 color = COMPAT_TEXTURE(sol_texture, sol_vtex_coord);
  //float noiseMod = fract(10000 * sin((uv.x + uv.y) * float(sol_time) ));
  //vec4 grain = vec4(mod((mod(noiseMod, 13.0) + 1.0) * (mod(noiseMod, 123.0) + 1.0), 0.01)-0.005) * strength;
  //FragColor = color + grain;

}