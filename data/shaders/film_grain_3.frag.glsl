#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

in vec2 sol_vtex_coord;
out vec4 outColor;

uniform vec2 sol_input_size;
uniform vec2 sol_output_size;
uniform int sol_time;
uniform sampler2D sol_texture;
uniform float strength = 5.0;


//Simple Perlin noise
float noise(vec2 p) {
  return fract(sin(dot(p, vec2(12.9898, 78.233))) * 43758.5453);
}


void main() {
  //Film Grain:
  vec2 uv = sol_vtex_coord.xy;
  vec4 color = texture(sol_texture, uv);

  float seed = (uv.x + uv.y * sol_output_size.x) * 0.1;
  vec2 offset = vec2(mod(sol_time, 10000) / 1000.0); //offsets based on time when sampling the perlin noise texture
  offset *= fract(sin(seed));
  float noiseMod = noise(uv + offset);
  vec4 grain = vec4(mod((mod(noiseMod, 13.0) + 1.0) * (mod(noiseMod, 123.0) + 1.0), 0.01)-0.005) * strength;

  color += grain;

  //Tone Mapping:
  float darks = 1.2;
  float lights = 1.2;

  color.rgb = pow(color.rgb, vec3(darks));  // Gamma correction
  color.rgb = color.rgb * (lights + color.rgb * 0.25) / (1.0 + color.rgb * 0.25);  // Filmic curve
  outColor = color;
}
