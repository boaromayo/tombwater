#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

in vec2 sol_vtex_coord;
out vec4 outColor;

uniform vec2 sol_input_size;
uniform vec2 sol_output_size;
uniform int sol_time;
uniform sampler2D sol_texture;
uniform float aberration_amount = 2;


vec4 desaturate(vec4 color, float amount)
{
    vec4 gray = vec4(dot(vec4(0.2126,0.7152,0.0722, 1.0), color));
    return vec4(mix(color, gray, amount));
}

void main() {
  float strength = aberration_amount / 1000; // amount of chromatic aberration
  float brightnessMakeup = 2;
  float saturationMod = .5;
  vec2 uv = sol_vtex_coord.xy;
  vec4 color = texture(sol_texture, uv);
  
  vec4 red = texture(sol_texture, uv + vec2(strength, 0));
  vec4 green = texture(sol_texture, uv);
  vec4 blue = texture(sol_texture, uv - vec2(strength, 0));

  color = desaturate(color, 0.4);

  outColor = vec4(red.r, green.g, blue.b, 1.0) * color * brightnessMakeup;
}
