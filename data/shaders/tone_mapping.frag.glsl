#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

in vec2 sol_vtex_coord;
out vec4 outColor;

uniform vec2 sol_input_size;
uniform vec2 sol_output_size;
uniform int sol_time;
uniform sampler2D sol_texture;


void main() {
  vec2 uv = sol_vtex_coord.xy;
  vec4 color = COMPAT_TEXTURE(sol_texture, sol_vtex_coord);

  float darks = 1.3;
  float lights = 1.2;

  //Tone Mapping:
  color.rgb = pow(color.rgb, vec3(darks));  // Gamma correction
  color.rgb = color.rgb * (lights + color.rgb * 0.25) / (1.0 + color.rgb * 0.25);  // Filmic curve
  outColor = color;
}
