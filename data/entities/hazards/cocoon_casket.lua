local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local check_frequency = 50
local range = 48


function entity:on_created()
  local hero = map:get_hero()
  entity.enemy_breed = entity:get_property"entity_breed" or "tombwater_enemies/walking_corpse"
  entity.range = tonumber(entity:get_property"range") or range

  sol.timer.start(entity, check_frequency, function()
    if entity:get_distance(hero) <= entity.range then
      entity:destroy()
    else
      return true
    end
  end)
end


function entity:react_to_solforge_weapon()
  entity:destroy()
end


function entity:destroy()
  if entity.destroyed then return end
  entity.destroyed = true
  sol.audio.play_sound("breaking_crate")
  entity:get_sprite():set_animation("breaking", function()
    entity:remove()
  end)
  local x, y, z = entity:get_position()
  local enemy = map:create_enemy{
    x = x, y = y, layer = z, direction = 0,
    breed = entity.enemy_breed,
  }
  if enemy.ambush_attack then
    enemy:ambush_attack()
  end
  enemy.aggro = true
end
