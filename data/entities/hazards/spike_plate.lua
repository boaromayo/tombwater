local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()

local base_damage = 10
local enemy_modifier = 3.5 --if you lure enemies into this I want it to be satisfying
local activation_time = 250 --how long between being stepped on and springing


function entity:on_created()

  entity:set_tiled(true)
  entity:set_traversable_by(true)
  entity.damage = base_damage

  if entity:get_property("timed") or entity:get_property("frequency") then
    local frequency = entity:get_property("frequency")
    local offset = entity:get_property("offset") or 1000
    sol.timer.start(entity, offset, function()
      if entity:get_distance(hero) <= 200 then
        sol.audio.play_sound"trap_blade_2"
      end
      entity:activate()
      return frequency
    end)
  else
    entity:add_collision_test("overlapping", function(entity, other_entity)
      if not entity.activated then
        entity.activated = true
        sol.audio.play_sound"trap_blade_2"
        sol.timer.start(entity, activation_time, function()
          entity:activate()
        end)
      end
    end)
  end

end

function entity:activate()
  local sprite = entity:get_sprite()
  sprite:set_animation("activated", function()
    sprite:set_animation"set"
    entity.activated = false
  end)
  for other_entity in map:get_entities_in_rectangle(entity:get_bounding_box()) do
    if entity:overlaps(other_entity, "overlapping") then
      --Damage hero:
      if other_entity:get_type() == "hero" and other_entity.process_hit then
        other_entity:process_hit{ damage = entity.damage }
      elseif other_entity:get_type() == "hero" then
        other_entity:start_hurt(entity, entity.damage)
      --Damage enemies:
      elseif other_entity:get_type() == "enemy" and other_entity.process_hit then
        other_entity:process_hit(entity.damage * enemy_modifier)
      elseif other_entity:get_type() == "enemy" then
        other_entity:hurt(entity.damage * enemy_modifier)
      end
    end
  end
end
