local entity = ...
local reacted = false
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()

local skeleton_state = require("scripts/action/skeleton_manager"):get_state()

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by(false)

  --hint rune
  local rune_sprite = entity:create_sprite("enemies/tombwater_enemies/skeleton")
  rune_sprite:set_direction(0)
  rune_sprite:set_animation("rune")
  rune_sprite:set_xy(0, -24)
end

function entity:react_to_wraith_bullet()
  hero:start_being_bones(entity)
end
