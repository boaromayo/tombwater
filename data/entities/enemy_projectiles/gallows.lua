local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local duration = 3000

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by(function(entity, other)
    return other:overlaps(entity)
  end)
  local sprite = entity:get_sprite()
  local swing_time = math.random(300,1300)

  sprite:set_animation("rising", function()
    sprite:set_animation("swinging")
    sol.timer.start(entity, swing_time, function()
      sprite:set_animation("snapping", function()
        sprite:set_animation("snapped")
        local x, y, z = entity:get_position()
        local enemy = map:create_enemy{
          x = x + 16, y = y, layer = z, direction = 3, breed = "tombwater_enemies/walking_corpse",
        }
        enemy:start_aggro()
        sol.timer.start(entity, duration, function()
          sprite:fade_out(50)
          sol.timer.start(entity, 1000, function() entity:remove() end)
        end)

      end)
    end)
  end)
end
