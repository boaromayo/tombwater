local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  local sprite = entity:get_sprite()
  entity:set_drawn_in_y_order(true)
  entity.hookshot_target = true
  entity:set_size(32,16)
  entity:set_origin(16,13)
  entity:set_traversable_by(false)

  local x, y, z = entity:get_position()
  local id = map:get_id():gsub("/", "_")
  entity.state_save_var = "adelaide_vine_state_" .. id .. x .. y .. z
  if game:get_value(entity.state_save_var) == "removed" then
    entity:remove()
  end

  local init_state = entity:get_property("initial_state")
  if init_state == "down" then
    sprite:set_animation("hidden")
    entity:set_traversable_by(true)
  end
end


function entity:destroy()
  game:set_value(entity.state_save_var, "removed")
  entity:retract(function()
    entity:remove()
  end)
end


function entity:retract(callback)
  local sprite = entity:get_sprite()
  sprite:set_animation("retracting", function()
    sprite:set_animation("hidden")
    if callback then callback() end
  end)
end


function entity:emerge()
  local sprite = entity:get_sprite()
  sprite:set_animation("growing", function()
    sprite:set_animation("stopped")
  end)
end


function entity:flower()
  local sprite = entity:get_sprite()
  sprite:set_animation("flowering", function()
    sprite:set_animation("flowered")
  end)
end


--Little helper function, probably not the best to put it here, but it is convenient
local map_meta = sol.main.get_metatable"map"
function map_meta:destroy_adelaide_vines(prefix)
  for e in map:get_entities(prefix) do
    e:destroy()
  end
end


