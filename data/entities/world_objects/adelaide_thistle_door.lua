local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  entity.hookshot_target = true
  entity:set_size(32,16)
  entity:set_origin(16,13)
  entity:set_traversable_by(false)
end


function entity:burn_up()
  if entity.caught_fire then return end
  entity.caught_fire = true
  local x, y, z = entity:get_position()
  local sprite = entity:get_sprite()
  sprite:set_animation("burning", function()
    entity:remove()
  end)
  local em = map:create_particle_emitter(x, y + 8, z + 1, "fire_burst_small")
  em.width = 32
  em.particle_speed = 65
  em:emit()
end


function entity:react_to_wraith_bullet()
  entity:burn_up()
end

function entity:react_to_fire()
  entity:burn_up()
end
