local entity = ...
local game = entity:get_game()
local map = entity:get_map()


function entity:on_created()
  local sprite = entity:get_sprite()
  sprite:set_animation("unlit")
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by(false)
end

function entity:react_to_fire()
  local sprite = entity:get_sprite()
  local torch_group = entity:get_property("torch_group")

  sprite:set_animation("lit")
  entity.lit = true
  local full_group_lit = true
  for t in map:get_entities_by_type("custom_entity") do
    if t:get_model() == "world_objects/torch" and t:get_property("torch_group") == torch_group and t:is_lit() == false then
      full_group_lit = false
      break
    end
  end
  if full_group_lit then
    map:open_doors(torch_group)
    if map.on_torch_group_lit then
      map:on_torch_group_lit(entity)
    end
  end

  if map.on_torch_lit then
    map:on_torch_lit(torch_group)
  end
end

function entity:is_lit()
  return entity.lit or false
end
