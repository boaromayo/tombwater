local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity:set_traversable_by(false)
  entity:set_drawn_in_y_order(true)
end


function entity:react_to_wraith_bullet()
  local sprite = entity:get_sprite()
  sprite:set_animation("activated", function()
    if entity.on_activated then
      entity:on_activated()
    end
    if entity:get_property("vine_prefix") then
      map:destroy_adelaide_vines(entity:get_property"vine_prefix")
    end
    entity:remove()
  end)
end
