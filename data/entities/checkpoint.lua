local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local sprite

local checkpoint_menu = require("scripts/menus/checkpoint/checkpoint")

function entity:on_created()
  entity:set_traversable_by(false)
  entity:set_drawn_in_y_order(true)
  sprite = entity:get_sprite()
  entity:set_property("lighting_effect_type", "torch")
  --entity.lighting_effect_type = "torch"
end


function entity:on_interaction()
  game:start_dialog("checkpoint.rest_question", function(answer)
    if answer == 1 then
      entity:activate()
    end
  end)
end


function entity:activate()
  sol.audio.play_sound"fire_light"
  game:start_flash()
  entity:sparkle_effect()
  entity:save_checkpoint()
  entity:heal_hero()
  game:refill_respawn_items()
  map:respawn_enemies()
  map:trigger_checkpoint_callbacks()
  game:save()
  game:stop_flash(10)
  sprite:set_animation"lighting"
  sprite:set_ignore_suspend(true)
  checkpoint_menu.cursor_index = 1
  sol.menu.start(game, checkpoint_menu)
end


function entity:save_checkpoint()
  game:save_checkpoint()
end


function entity:heal_hero()
  local hero = game:get_hero()
  game:set_life(game:get_max_life())
  game:set_magic(game:get_max_magic())
  hero:cure_all_status_effects()
end



function entity:sparkle_effect()
  local x, y, z = entity:get_position()
  for i=1, 12 do
    local sparkle = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=8, height=16,
      sprite = "entities/lantern_sparkle",
    }
    local sparkle_sprite = sparkle:get_sprite()
    sparkle_sprite:set_animation("sparkle_" .. math.random(1,2), function()
      sparkle:remove()
    end)
    sparkle:get_sprite():set_ignore_suspend(true)
    sparkle:set_drawn_in_y_order(true)
    local m = sol.movement.create"straight"
    m:set_speed(120)
    m:set_angle(math.random(100) * 2 * math.pi / 100)
    m:set_max_distance(math.random(16, 32))
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    m:start(sparkle)
  end
end
