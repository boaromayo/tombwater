local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local sprites = {}

local dropoff_sets = {
  [1] = {
    "tombwater_enemies/townsfolk_cleaver",
    "tombwater_enemies/townsfolk_cleaver",
    "tombwater_enemies/townsfolk_rifleman",
  },
  [2] = {
    "tombwater_enemies/townsfolk_axe",
    "tombwater_enemies/townsfolk_rifleman",
  },
}

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  --Make all the pieces:
  if entity:get_sprite() then entity:remove_sprite() end
  sprites = {}
  sprites.coach = entity:create_sprite("enemies/tombwater_enemies/boss/mad_stagecoach_driver")
  sprites.coach:set_animation"coach"
  sprites.reigns_back = entity:create_sprite("enemies/tombwater_enemies/boss/mad_stagecoach_driver")
  sprites.reigns_back:set_animation("reigns_back")
  sprites.horse_1 = entity:create_sprite("enemies/tombwater_enemies/boss/mad_stagecoach_driver")
  sprites.horse_1:set_animation("horse_walking")
  sprites.horse_1:set_xy(0, -16)
  sprites.horse_2 = entity:create_sprite("enemies/tombwater_enemies/boss/mad_stagecoach_driver")
  sprites.horse_2:set_animation("horse_walking")
  sprites.reigns_front = entity:create_sprite("enemies/tombwater_enemies/boss/mad_stagecoach_driver")
  sprites.reigns_front:set_animation("reigns_front")

  for _, sprite in pairs(sprites) do
    sprite:set_opacity(0)
  end

end


function entity:face_direction(dir)
  for _, sprite in pairs(sprites) do
    sprite:set_direction(dir)
  end
end


function entity:appear()
  --TODO: play horse sounds
  for _, sprite in pairs(sprites) do
    sprite:fade_in()
  end

  local dropoff_target = map:get_entity("mad_stagecoach_dropoff_target")
  local exit_target = map:get_entity("mad_stagecoach_exit_target")
  assert(dropoff_target:exists(), "No entity found named 'dropoff_target' for mad stagecoach driver")
  assert(exit_target:exists(), "No entity found named 'exit_target' for mad stagecoach driver")

  local m = sol.movement.create"target"
  m:set_ignore_obstacles(true)
  m:set_speed(150)
  m:set_target(dropoff_target)
  m:start(entity, function()
    entity:drop_off_dudes()
  end)
end


function entity:drop_off_dudes()
  local exit_target = map:get_entity("mad_stagecoach_exit_target")
  local x, y, z = entity:get_position()
  local dropoff_set = dropoff_sets[math.random(1, #dropoff_sets)]
  for _, enemy_breed in pairs(dropoff_set) do
    local enemy = map:create_enemy{
      x = x + math.random(16, -16), y = y - 16, layer = z, direction = 0,
      breed = enemy_breed,
    }
    enemy.aggro = true
    enemy:restart()
  end

  local angle = entity:get_angle(exit_target)
  local m = sol.movement.create"straight"
  m:set_angle(angle)
  m:set_ignore_obstacles(true)
  m:set_speed(150)
  m:start(entity)
  sol.timer.start(entity, 5000, function()
    for _, sprite in pairs(sprites) do
      sprite:fade_out(40)
    end
    sol.timer.start(entity, 5000, function() entity:remove() end)
  end)
end

