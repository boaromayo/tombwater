local map_meta = sol.main.get_metatable"map"

--Add helper function for sensors to trigger:
function map_meta:start_mad_stagecoach_encounter()
  local map = self
  local game = map:get_game()
  local appear_odds = game:get_value("mad_stagecoach_odds") or 100
  if math.random(1, 100) > appear_odds then
    --Don't appear, but make it more likely to
    game:set_value("mad_stagecoach_odds", appear_odds + 15)
  else
    --Make stagecoach appear:
    local coach = map:get_entity("mad_stagecoach")
    assert(coach, "Could not start mad stagecoach encounter, no entity named 'mad_stagecoach' found on map")
    coach:appear()
    game:set_value("mad_stagecoach_odds", 10)
  end

end

