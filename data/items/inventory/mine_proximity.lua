local item = ...
local game = item:get_game()
local default_max_amount = 3

local slide_distance = 32

function item:on_started()
  item:set_savegame_variable("possession_mine_proximity")
  item:set_amount_savegame_variable("amount_mine_proximity")
  local amount_string = "mine_proximity_max_amount"

  local max_amount = game:get_value(amount_string)
  if not max_amount then
    max_amount = default_max_amount
    game:set_value(amount_string, default_max_amount)
  end
  item:set_max_amount(max_amount)
  item:set_assignable(true)
  item:set_ammo("_amount")

  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(3)
end
 

function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:freeze()
    sol.audio.play_sound"throw"
    hero:set_animation("tossing", function()
      hero:set_animation"stopped"
      item:set_finished()
      hero:unfreeze()
    end)
    sol.timer.start(item, 100, function()
      local mine = map:create_custom_entity{
        x=x, y=y, layer=z,
        direction = 0, width=16, height=16,
        sprite = "items/mine_proximity",
        model = "world_objects/mine",
      }
      mine.ignore_hero = true
      mine.ignore_enemies = false
      mine.fuse_length = 100
      mine.range = 40
      local m = sol.movement.create"straight"
      m:set_angle(direction * math.pi / 2)
      m:set_max_distance(slide_distance)
      m:set_speed(100)
      m:start(mine)
      sol.audio.play_sound("device_set")
    end)
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
