local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_homeward_talisman")
  item:set_assignable(true)
end

function item:on_using()
  --Can't use if for some reason there's nowhere to warp back to:
  if not game:get_value"checkpoint_map" and not game:get_starting_location() then
    sol.audio.play_sound"wrong"
    item:set_finished()
    return
  end
  --Can't use in a boss battle:
  if game:is_boss_battle_active() then
    sol.audio.play_sound"wrong"
    item:set_finished()
    return
  end
  local hero = game:get_hero()
  local map = game:get_map()
  local charging_state = item:get_charging_state()
  local emitters = {}
  function charging_state:on_finished()
    for _, em in pairs(emitters) do
      em:remove()
    end
  end

  game:start_dialog("game.item.homeward_coal_confirmation", function(answer)
    if answer == 1 then

      local x, y, z = hero:get_position()
      hero:set_animation("kneeling")
      local em = map:create_particle_emitter(x, y + 2, z)
      table.insert(emitters, em)
      --em:set_preset("smoke")
      em.particle_sprite = "entities/lantern_sparkle"
      em.particle_animation_loops = false
      em.particle_fade_speed = 0
      em.particle_speed = 12
      em.frequency = 30
      em.particle_opacity = {100,240}
      em.angle = math.rad(80)
      em.angle_variance = math.rad(50)
      em:emit()
      local em_2 = map:create_particle_emitter(x, y + 1, z)
      table.insert(emitters, em_2)
      em_2:set_preset("smoke")
      em_2:emit()

      sol.audio.play_sound"world_warp"
      hero:start_state(charging_state)
      sol.timer.start(item, 1000, function()
        game:start_flash()
        sol.timer.start(game, 300, function()
          local homeward_map = game:get_value"checkpoint_map"
          local homeward_x = game:get_value"checkpoint_x"
          local homeward_y = game:get_value"checkpoint_y"
          local homeward_z = game:get_value"checkpoint_z"
          local backup_map, backup_destination = game:get_starting_location()
          local destination = (homeward_x and homeward_y and homeward_z) and "_same" or backup_destination
          hero:teleport(homeward_map or backup_map, destination, "immediate")
          local map = hero:get_map() --update map since we've teleported
          if homeward_x and homeward_y and homeward_z then
            hero:set_position(homeward_x, homeward_y, homeward_z)
          end
          hero:set_visible(true)
          hero:set_direction(3)
          --Trigger respawn to make sure you don't get your world into a weird state by warping out at a weird time:
          map:respawn_enemies()
          map:trigger_checkpoint_callbacks()
          game:stop_flash()
          item:set_finished()
        end)
      end)

    else --answer != yes
      hero:unfreeze()
      item:set_finished()

    end
  end)


end


function item:get_charging_state()
  local state = sol.state.create("charging")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_be_hurt(true)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")

  return state
end
