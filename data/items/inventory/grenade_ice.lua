local item = ...
local game = item:get_game()
local default_max_amount = 3

local num_ice = 3
local ice_radius = 32

function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(3)
end


local function create_ice(x, y, z)
  local map = item:get_map()
  local ice = map:create_ice_blast(x, y, z)
  ice.harmless_to_hero = true
  ice:remove_sprite(ice:get_sprite())
  ice:create_sprite("elements/ice_sparkle")
end

local function explosion_callback(grenade)
  local map = item:get_map()
  local x, y, z = grenade:get_position()
  sol.timer.start(map, 400, function()
    local twist = math.random(1, 360)
    for i = 1, num_ice do
      local angle = math.pi * 2 / num_ice * i + math.rad(twist)
      local dx = math.cos(angle) * ice_radius
      local dy = math.sin(angle) * ice_radius
      create_ice(x + dx, y + dy, z)
    end
  end)
end


function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "items/explosion_ice",
      explosion_props = {
        ignore_hero = true,
        status_effect_type = "cold",
        enemy_status_amount = 65,
        hero_status_amount = 35,
      },
      explosion_callback = explosion_callback,
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
