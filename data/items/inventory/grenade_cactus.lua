local item = ...
local game = item:get_game()
local default_max_amount = 3

local num_needles = 8
local needle_base_damage = 20


function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(default_max_amount)
end


local function explosion_callback(grenade)
  local x, y, z = grenade:get_position()
  local map = item:get_map()
  sol.timer.start(map, 200, function()
    for i = 1, num_needles do
      local projectile = map:create_custom_entity{
        x = x, y = y, layer = z,
        width = 8, height = 8, direction = 0,
        model = "hero_projectiles/general",
        sprite = "hero_projectiles/cactus_needle",
      }
      projectile.damage = game:calculate_hero_output_damage(needle_base_damage, "alchemy")
      projectile.damage_type = "physical"
      projectile.speed = 250
      projectile:shoot(2 * math.pi / num_needles * i)
    end
  end)
end


function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "items/explosion_small",
      explosion_callback = explosion_callback,
      explosion_props = {
        ignore_hero = true,
        base_damage = 100,
      }
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
