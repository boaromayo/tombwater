local item = ...
local game = item:get_game()
local default_max_amount = 3

local num_flames = 8
local displacement_radius = 24

function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(default_max_amount)
end


local function explosion_callback(grenade)
  local x, y, z = grenade:get_position()
  local map = item:get_map()
  sol.timer.start(map, 200, function()
    for i = 1, num_flames do
      map:create_fire({
        x = x + math.random(displacement_radius * -1, displacement_radius),
        y = y + math.random(displacement_radius * -1, displacement_radius),
        layer = z,
        properties = {{key = "burn_duration", value = "2700",}}
      })
    end
  end)
end


function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "entities/explosion",
      explosion_callback = explosion_callback,
      explosion_props = {
        ignore_hero = true,
        status_effect_type = "burn",
        enemy_status_amount = 70,
      }
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
