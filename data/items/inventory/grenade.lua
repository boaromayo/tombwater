local item = ...
local game = item:get_game()
local fuse_duration = 600
local initial_speed = 300
local speed_loss = 40
local default_max_amount = 3

function item:on_started()
  item:setup_amount_stuff(default_max_amount)

  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(3)
end
 

function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "items/explosion_grenade",
      explosion_props = {
        enemy_status_amount = 25,
      },
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
