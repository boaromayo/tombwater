local item = ...
local game = item:get_game()
local fuse_duration = 600
local initial_speed = 300
local speed_loss = 40
local default_max_amount = 30

function item:on_started()
  item:set_savegame_variable("possession_dynamite")
  item:set_amount_savegame_variable("amount_dynamite")
  local amount_string = "dynamite_max_amount"

  local max_amount = game:get_value(amount_string)
  if not max_amount then
    max_amount = default_max_amount
    game:set_value(amount_string, default_max_amount)
  end
  item:set_max_amount(max_amount)
  item:set_assignable(true)
  item:set_ammo("_amount")

  item:set_fill_on_checkpoint()
end


function item:on_obtaining(variant)
  item:add_amount(5)
end
 

function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      throwing_animation = "tossing",
      throwing_delay = 20,
      sprite = "hero_projectiles/dynamite",
      fuse_duration = 1000,
      initial_speed = 150,
      speed_loss = 60,
      explosion_sprite = "items/explosion_grenade",
      explosion_sound = "explosion_deep",
      explosion_props = {
        ignore_hero = true,
        enemy_damage = 200,
        demolishing = true,
      }
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
