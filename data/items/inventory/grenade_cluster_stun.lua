local item = ...
local game = item:get_game()
local default_max_amount = 2

local num_blasts = 7
local displacement_radius = 60

function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(default_max_amount)
end


local function explosion_callback(grenade)
  local x, y, z = grenade:get_position()
  local map = item:get_map()
  for i = 1, num_blasts do
  sol.timer.start(map, 70 * i, function()
      local dx = math.cos(math.pi  *2 / num_blasts * i) * displacement_radius
      local dy = math.sin(math.pi  *2 / num_blasts * i) * displacement_radius
      local explosion = map:create_explosion({
        x = x + dx,
        y = y + dy,
        layer = z,
        sprite = "items/explosion_concussion_small",
      })
      explosion.ignore_hero = true
      explosion.enemy_damage = 5
      explosion.stagger_duration = 3000
    end)
  end
end


function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "items/explosion_concussion",
      explosion_callback = explosion_callback,
      explosion_props = {
        ignore_hero = true,
        enemy_damage = 5,
      }
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
