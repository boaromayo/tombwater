local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_healing_flask")
  item:set_amount_savegame_variable("amount_healing_flask")
  item:set_max_amount(game:get_value("max_amount_healing_flask") or 3)
  item:set_assignable(true)
  item:set_fill_on_checkpoint()
end

function item:on_using()
  
  if item:has_amount(1) and game:get_life() < game:get_max_life() then
    
    local hero = game:get_hero()
    sol.audio.play_sound("uncork")
    hero:set_animation("drinking", function()
      local heal_amount = game:get_value("healing_flask_healing_amount") or 100
      heal_amount = heal_amount * (game.charm_healing_mod or 1)
      game:add_life(heal_amount)
      item:remove_amount(1)
      item:set_finished()
    end)
  else
    item:set_finished()   
  end
end
