local item = ...
local game = item:get_game()
local fuse_duration = 600
local initial_speed = 300
local speed_loss = 40
local default_max_amount = 3

local num_clouds = 7
local cloud_radius = 48

function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(3)
end


local function make_gas_ring(x, y, z, num_clouds, cloud_radius)
  local map = item:get_map()
  for i = 1, num_clouds do
    local cloud = map:create_custom_entity{
      x=x,
      y=y,
      layer=z,
      width = 32, height = 32, direction = 0,
      model = "attacks/poison_gas",
      sprite = "entities/enemy_projectiles/gas",
    }
    cloud:set_origin(16, 29)
    cloud.ignore_hero = true
    cloud.enemy_poison_amount = 10

    local angle = math.pi * 2 / num_clouds * i
    local m = sol.movement.create"straight"
    m:set_angle(angle)
    m:set_max_distance(cloud_radius)
    m:set_speed(70)
    m:set_ignore_obstacles(true)
    m:start(cloud)
  end
end


local function explosion_callback(grenade)
  local x, y, z = grenade:get_position()
  make_gas_ring(x, y, z, num_clouds, cloud_radius)
  make_gas_ring(x, y, z, num_clouds, cloud_radius / 2)
end


function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "items/explosion_small",
      explosion_callback = explosion_callback
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
