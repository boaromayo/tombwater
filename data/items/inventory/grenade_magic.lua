local item = ...
local game = item:get_game()
local default_max_amount = 3

function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()
end


function item:on_variant_changed()
  item:add_amount(3)
end
 

function item:on_using()
  local hero = game:get_hero()
  if item:try_spend_ammo(1) then
    local map = game:get_map()
    hero:throw_explosive({
      explosion_sprite = "items/explosion_magic",
      explosion_props = {
        ignore_hero = true,
        enemy_damage = 180,
        damage_type = "magic",
        enemy_status_amount = 0,
      },
    })
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
