local item = ...
local game = item:get_game()

local recovery_window = 3000 --how long you have to attack enemies to recover lost health

function item:on_started()
  local savegame_variable = "possession_charm_wounded_restoration"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 2

  game.charm_manager:subscribe("on_hurt", function()
    if game:is_charm_equipped(item:get_name()) then
      if item.duration_timer then
        item.duration_timer:stop()
      end
      item.active = true
      item.duration_timer = sol.timer.start(game, recovery_window, function()
        item.active = false
      end)
    end
  end)

  game.charm_manager:subscribe("on_enemy_attacked", function(enemy, damage)
    if game:is_charm_equipped(item:get_name()) and item.active == true then
      game:add_life(damage * .2)
    end
  end)
end
