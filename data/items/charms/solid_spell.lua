--Increases spell damage
local item = ...
local game = item:get_game()

local spell_multiplier = 1.5
local scaling_stat = "mind"

function item:on_started()
  local savegame_variable = "possession_charm_solid_spell"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3
end


function item:on_activated()
  game.charm_damage_multipliers[scaling_stat] = spell_multiplier
end


function item:on_unequipped()
  game.charm_damage_multipliers[scaling_stat] = 1
end


