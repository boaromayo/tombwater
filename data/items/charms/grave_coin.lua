local item = ...
local game = item:get_game()

function item:on_started()
  local savegame_variable = "possession_charm_grave_coin"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

end


function item:on_activated()
  game.blood_ghost_money_percentage_charm_multiplier = 0.5
end


function item:on_unequipped()
  game.blood_ghost_money_percentage_charm_multiplier = 1
end
