local item = ...
local game = item:get_game()


function item:on_started()
  local savegame_variable = "possession_charm_madness_cut"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  game.charm_manager:subscribe("on_enemy_attacked", function(enemy, damage)
    if game:is_charm_equipped(item:get_name()) then
     local hero = game:get_hero()
     hero:build_up_status_effect("madness", ((game:get_value("status_resistance_madness") or 100) * .1 * -1))
    end
  end)
end