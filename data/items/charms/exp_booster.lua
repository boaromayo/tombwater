--Increases gained exp (by 1.5)

local item = ...
local game = item:get_game()

local boost_amount = .5

function item:on_started()
  local savegame_variable = "possession_charm_exp_booster"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 4

end

function item:on_activated()
  local current_mult = game.exp_mod or 1
  game.exp_mod = current_mult + boost_amount
end


function item:on_unequipped()
  local current_mult = game.exp_mod
  game.exp_mod = current_mult - boost_amount
end
