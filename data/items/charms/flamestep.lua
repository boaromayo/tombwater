local item = ...
local game = item:get_game()

local frequency = 60
local num_entities = 10
local num_entities_line = 6

function item:on_started()
  local savegame_variable = "possession_charm_flamestep"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  --Flames throughout:
  --
  game.charm_manager:subscribe("on_dashing", function()
    if game:is_charm_equipped(item:get_name()) then
      local hero = game:get_hero()
      local map = game:get_map()
      local entities_created = 0
      sol.timer.start(hero, 0, function()
        local x, y, z = hero:get_position()
        local entity = map:create_fire{
          x=x, y=y, layer=z,
          properties = {{key = "burn_duration", value = 0,}}
        }
        entity.harmless_to_hero = true
        entities_created = entities_created + 1
        if entities_created < num_entities_line and hero:get_movement():get_speed() > 0 then
          return frequency
        end
      end)
    end
  end)
  --]]

  --Flame Burst
  --[[
  game.charm_manager:subscribe("on_dashing", function()
    if game:is_charm_equipped(item:get_name()) then
      local hero = game:get_hero()
      local map = game:get_map()
      for i = 1, num_entities do
        local x, y, z = hero:get_position()
        local entity = map:create_fire{
          x=x, y=y, layer=z,
          properties = {{key = "burn_duration", value = 0,}}
        }
        entity.harmless_to_hero = true
        local m = sol.movement.create"straight"
        m:set_angle(math.pi * 2 / num_entities * i)
        m:set_max_distance(24)
        m:set_speed(200)
        m:start(entity)
        sol.timer.start(entity, 10, function()
          local decel = 6
          m:set_speed(m:get_speed() - decel)
          if m:get_speed() > decel then return true end
        end)
      end
    end
  end)
  --]]
end
