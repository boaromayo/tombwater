local item = ...
local game = item:get_game()

local reduction_amount = 0.2

function item:on_started()
  local savegame_variable = "possession_charm_damage_reduction_total"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3
end

function item:on_activated()
  local prev_mult = game.charm_hero_damage_multiplier or 1
  game.charm_hero_damage_multiplier = prev_mult - reduction_amount
end


function item:on_unequipped()
  local prev_mult = game.charm_hero_damage_multiplier or 1
  game.charm_hero_damage_multiplier = prev_mult + reduction_amount
end
