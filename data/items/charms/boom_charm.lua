local item = ...
local game = item:get_game()

local boost_amount = .6

function item:on_started()
  local savegame_variable = "possession_charm_boom_charm"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

end

function item:on_activated()
  local current_mult = game.explosion_charm_damage_mod or 1
  game.explosion_charm_damage_mod = current_mult + boost_amount
end


function item:on_unequipped()
  local current_mult = game.explosion_charm_damage_mod
  game.explosion_charm_damage_mod = current_mult - boost_amount
end
