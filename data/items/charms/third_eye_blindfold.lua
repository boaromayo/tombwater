local item = ...
local game = item:get_game()

function item:on_started()
  local savegame_variable = "possession_charm_"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 1

  game.charm_manager:subscribe("on_example", function()
    if game:is_charm_equipped(item:get_name()) then

    end
  end)
end

function item:on_equipped()
  game:set_value("spell_madness_safety", true)
end


function item:on_unequipped()
  game:set_value("spell_madness_safety", false)
end
