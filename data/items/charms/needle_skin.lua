local item = ...
local game = item:get_game()

local base_damage = 8
local scaling_stat = "life"
local num_needles = 14
local cooldown_time = 12000

function item:on_started()
  local savegame_variable = "possession_charm_needle_skin"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  game.charm_manager:subscribe("on_hurt", function()
    if game:is_charm_equipped(item:get_name()) then
      --Cooldown:
      if item.cooldown_active then return end
      item.cooldown_active = true
      sol.timer.start(game, cooldown_time, function() item.cooldown_active = false end)

      local hero = game:get_hero()
      local map = game:get_map()
      local x, y, z = hero:get_position()
      local random_offset = math.rad(math.random(1, 360))
      for i = 1, num_needles do
        local projectile = map:create_custom_entity{
          x = x, y = y, layer = z,
          width = 8, height = 8, direction = 0,
          model = "hero_projectiles/general",
          sprite = "hero_projectiles/cactus_needle",
        }
        projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
        projectile.damage_type = "physical"
        projectile.speed = 250
        projectile:shoot(2 * math.pi / num_needles * i + random_offset)
      end
    end
  end)
end
