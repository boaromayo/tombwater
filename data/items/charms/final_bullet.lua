local item = ...
local game = item:get_game()

function item:on_started()
  local savegame_variable = "possession_charm_"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 2

  game.charm_manager:subscribe("on_bullet_fired", function(bullet)
    if game:is_charm_equipped(item:get_name()) and game:get_bullets() <= 0 then
      bullet.damage = bullet.damage * 1.5
    end
  end)
end
