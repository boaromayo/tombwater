--Restores a small amount of HP when hitting enemies
local item = ...
local game = item:get_game()

--local amount = 3
local max_life_percentage = .02

function item:on_started()
  local savegame_variable = "possession_charm_lifecut"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 4

  game.charm_manager:subscribe("on_enemy_attacked", function(enemy)
    if game:is_charm_equipped(item:get_name()) then
      local amount = game:get_max_life() * max_life_percentage
      game:add_life(amount)
    end
  end)
end


