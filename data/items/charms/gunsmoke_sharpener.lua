local item = ...
local game = item:get_game()

function item:on_started()
  local savegame_variable = "possession_charm_gunsmoke_sharpener"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 1

  game.charm_manager:subscribe("on_enemy_attacked", function(enemy, damage)
    if game:is_charm_equipped(item:get_name()) and (game:get_bullets() <= 0) then
      enemy:remove_life(damage * .4)
    end
  end)
end
