local item = ...
local game = item:get_game()

local frequency = 60
local num_entities_dash = 4
local num_entities_burst = 5

local status_type = "cold"
local status_amount = 20
local status_frequency = 100
local duration = 10
local damage = 5
local harmless_to_hero = true

function item:on_started()
  local savegame_variable = "possession_charm_chillstep"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  --attacks throughout:
  --[[
  game.charm_manager:subscribe("on_dashing", function()
    if game:is_charm_equipped(item:get_name()) then
      local hero = game:get_hero()
      local map = game:get_map()
      local entities_created = 0
      sol.timer.start(hero, 0, function()
        local x, y, z = hero:get_position()
        local entity = map:create_custom_entity{
          x=x, y=y, layer=z, direction=0, width=16, height=16,
          model="attacks/status_cloud", sprite = "items/cloud",
        }
        entity.status_type = status_type
        entity.enemy_status_amount = status_amount
        entity.status_frequency = status_frequency
        entity.duration = duration
        entity.harmless_to_hero = harmless_to_hero
        entities_created = entities_created + 1
        if entities_created < num_entities_dash and hero:get_movement():get_speed() > 0 then
          return frequency
        end
      end)
    end
  end)
  --]]

  --Attack Burst
  --
  game.charm_manager:subscribe("on_dashing", function()
    if game:is_charm_equipped(item:get_name()) then
      local hero = game:get_hero()
      local map = game:get_map()
      for i = 1, num_entities_burst do
        local x, y, z = hero:get_position()
        local entity = map:create_custom_entity{
          x=x, y=y, layer=z, direction=0, width=16, height=16,
          model="attacks/status_cloud", sprite = "items/cloud",
        }
        entity.status_type = status_type
        entity.enemy_status_amount = status_amount
        entity.status_frequency = status_frequency
        entity.damage = damage
        entity.duration = duration
        entity.harmless_to_hero = harmless_to_hero
        local m = sol.movement.create"straight"
        m:set_angle(math.pi * 2 / num_entities_burst * i)
        m:set_max_distance(8)
        m:set_speed(100)
        m:start(entity)
        sol.timer.start(entity, 10, function()
          local decel = 2
          m:set_speed(m:get_speed() - decel)
          if m:get_speed() > decel then return true end
        end)
      end
    end
  end)
  --]]
end
