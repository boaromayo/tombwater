local item = ...
local game = item:get_game()
local lighting_manager = require("scripts/fx/lighting/lighting_manager")

local frequency = 600
local ranges = {64, 112, 144}
local base_damage = 5
local scaling_stat = "life"

function item:on_started()
  local savegame_variable = "possession_charm_wisp_lantern"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

end


function item:on_activated()
  item.lantern_level = game:get_item("gear/lantern"):get_variant()
  item.effect_timer = sol.timer.start(game, frequency, function()
    item:damage_nearby_enemies()
    return true
  end)
  item.effect_timer:set_suspended_with_map(true)
end


function item:on_unequipped()
  if item.effect_timer then
    item.effect_timer:stop()
  end
end


function item:damage_nearby_enemies()
  if not lighting_manager:get_lantern_active() then return end
  if item.lantern_level < 1 then return end
  local map = game:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  local range = ranges[item.lantern_level]

  for ent in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
    if ent:get_type() == "enemy" and ent:get_distance(hero) <= range then
      local damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
      ent:process_hit(damage, "fire")
      local em = map:create_particle_emitter(ent:get_position())
      em:set_preset("fire_burst_small")
      em:emit()
    end
  end
end
