--Restores some life when killing an enemy

local item = ...
local game = item:get_game()

local base_amount = 10 --life restored when killing enemy

function item:on_started()
  local savegame_variable = "possession_charm_warrior_health"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  game.charm_manager:subscribe("on_enemy_killed", function(enemy)
    if game:is_charm_equipped(item:get_name()) then
      local amount = game:calculate_hero_output_damage(base_amount, "life")
      game:add_life(amount)      
    end
  end)
end

