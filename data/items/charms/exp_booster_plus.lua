--Increases gained exp (double)

local item = ...
local game = item:get_game()

local boost_amount = 1

function item:on_started()
  local savegame_variable = "possession_charm_exp_booster_plus"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 6

end

function item:on_activated()
  local current_mult = game.charm_exp_mod or 1
  game.charm_exp_mod = current_mult + boost_amount
end


function item:on_unequipped()
  local current_mult = game.charm_exp_mod
  game.charm_exp_mod = current_mult - boost_amount
end
