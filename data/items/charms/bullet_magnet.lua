--Lowers number of times you're required to hit an enemy to refill a bullet

local item = ...
local game = item:get_game()

local lowered_amount = 1

function item:on_started()
  local savegame_variable = "possession_charm_bullet_magnet"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 4

  game.charm_manager:subscribe("on_example", function()
    if game:is_charm_equipped(item:get_name()) then

    end
  end)
end

function item:on_equipped()
  local required_hits = game:get_value("melee_bullet_recharge_required_hits") or 2
  local required_hits = math.max(required_hits - lowered_amount, 1)
  game:set_value("melee_bullet_recharge_required_hits", required_hits)
end


function item:on_activated()

end


function item:on_unequipped()
  local required_hits = game:get_value("melee_bullet_recharge_required_hits")
  game:set_value("melee_bullet_recharge_required_hits", required_hits + lowered_amount)
end
