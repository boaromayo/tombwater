local item = ...
local game = item:get_game()

local activation_life_percent = 25
local damage_type = "physical"
local damage_multiplier = 1.3

if not game.charm_damage_multipliers then game.charm_damage_multipliers = {} end

function item:on_started()
  local savegame_variable = "possession_charm_damage_up_physical"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  game.charm_manager:subscribe("on_hurt", function()
    local life, max_life = game:get_life(), game:get_max_life()
    if ((life / max_life * 100) <= activation_life_percent) and game:is_charm_equipped(item:get_name()) and not item.is_timer_already then
      game.charm_damage_multipliers[damage_type] = damage_multiplier
    end
  end)

  game.charm_manager:subscribe("on_healed", function(amount_healed)
    local life, max_life = game:get_life(), game:get_max_life()
    if life / max_life * 100 > activation_life_percent then
      game.charm_damage_multipliers[damage_type] = 1
    end
  end)
end

function item:on_unequipped()
  game.charm_damage_multipliers[damage_type] = 1
end
