--inflicts extra damage to enemies affected by a status effect

local item = ...
local game = item:get_game()

local multiplier = 1.4

function item:on_started()
  local savegame_variable = "possession_charm_status_damager"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

end

function item:on_activated()
  game.charm_active_status_damage_multiplier = multiplier
end


function item:on_unequipped()
  game.charm_active_status_damage_multiplier = 1
end

