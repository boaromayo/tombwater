local item = ...
local game = item:get_game()

local status = "cold"
local status_amount = 30

function item:on_started()
  local savegame_variable = "possession_charm_revenge_cold"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  game.charm_manager:subscribe("on_hit", function(enemy, damage)
    if game:is_charm_equipped(item:get_name()) then
      enemy:build_up_status_effect(status, status_amount)
    end
  end)
end

