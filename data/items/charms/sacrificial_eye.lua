--Reduces madness cost for spellcasting

local item = ...
local game = item:get_game()

local reduction_amount = .35

function item:on_started()
  local savegame_variable = "possession_charm_sacrificial_eye"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3
end


function item:on_activated()
  game.madness_cost_multiplier = (game.madness_cost_multiplier or 1)- reduction_amount
end


function item:on_unequipped()
  game.madness_cost_multiplier = (game.madness_cost_multiplier or 1) + reduction_amount
end

