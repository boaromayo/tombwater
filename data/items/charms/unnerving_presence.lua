--Constantly deals damage in a small radius around the player

local item = ...
local game = item:get_game()

local range = 24
local damage = 5
local damage_type = "magic"
local frequency = 500

function item:on_started()
  local savegame_variable = "possession_charm_unnerving_presence"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 4

end


local function check_enemies()
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  for entity in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
    if (entity:get_layer() == z)
    and ( entity:get_distance(hero) <= range )
    and not game:is_suspended() then
      if entity:get_type() == "enemy" then
        entity:process_hit(damage, damage_type)
      end
    end
  end
end


function item:on_activated()
  sol.timer.start(game, frequency, function()
    if game:is_charm_equipped(item:get_name()) then
      if not game:is_suspended() then
        check_enemies()
      end
      return true
    end
  end)
end


function item:on_unequipped()

end

