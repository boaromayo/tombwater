local item = ...
local game = item:get_game()


function item:on_started()
  local savegame_variable = "possession_charm_dash_deflect"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 1

   game.charm_manager:subscribe("on_dashing", function()
    if game:is_charm_equipped(item:get_name()) then
      local map = game:get_map()
      local hero = game:get_hero()
      local x,y,z =  hero:get_position()
      local deflector = map:create_custom_entity{
        x = x,
        y = y,
        layer = z,
        width = 16, height = 16, direction = 0 
      }
      local deflected_entities = {}

      deflector:add_collision_test("touching", function(entity,other_entity)
        if other_entity.deflect and not deflected_entities[other_entity] then
          deflected_entities[other_entity] = other_entity
          other_entity:deflect()
        end
      end)
      
      local hmove = hero:get_movement()
      local angle = hmove:get_angle()
      local speed = hmove:get_speed()
      local max_distance = hmove:get_max_distance()

      local m = sol.movement.create("straight")
      m:set_speed(speed)
      m:set_angle(angle)
      m:set_max_distance(max_distance)
    
      m:start(deflector, function()
        deflector:remove()
      end)
      function m:on_obstacle_reached()
        deflector:remove()
      end
    end
  end)
end
