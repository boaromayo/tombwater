local item = ...
local game = item:get_game()

local status_type = "burn"
local status_amount = 10

function item:on_started()
  local savegame_variable = "possession_charm_inflict_burn"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 2

  game.charm_manager:subscribe("on_enemy_attacked", function(enemy)
    if game:is_charm_equipped(item:get_name()) then
      enemy:build_up_status_effect(status_type, status_amount)
    end
  end)
end

