--Make healing flask more effective
local item = ...
local game = item:get_game()

local modifier = 1.3

function item:on_started()
  local savegame_variable = "possession_charm_deep_draught"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

end

function item:on_activated()
  game.charm_healing_mod = modifier
end


function item:on_unequipped()
  game.charm_healing_mod = 1
end
