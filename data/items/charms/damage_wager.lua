--Do extra damage, but take extra damage

local item = ...
local game = item:get_game()

local give_mult = 1.7
local take_mult = 1.5

function item:on_started()
  local savegame_variable = "possession_charm_damage_wager"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  if not game.charm_damage_multiplier then game.charm_damage_multiplier = 1 end
  if not game.charm_hero_damage_multiplier then game.charm_hero_damage_multiplier = 1 end
end


function item:on_activated()
  game.charm_damage_multiplier = give_mult
  game.charm_hero_damage_multiplier = take_mult
end


function item:on_unequipped()
  game.charm_damage_multiplier = 1
  game.charm_hero_damage_multiplier = 1
end
