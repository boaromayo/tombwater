--Replenishes a bullet when taking damage

local item = ...
local game = item:get_game()

local amount = 1

function item:on_started()
  local savegame_variable = "possession_charm_bulletbiter"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 3

  game.charm_manager:subscribe("on_hurt", function()
    if game:is_charm_equipped(item:get_name()) then
      game:add_bullets(amount)
    end
  end)
end

