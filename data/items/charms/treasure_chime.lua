local item = ...
local game = item:get_game()

local range = 200
local timer

function item:on_started()
  local savegame_variable = "possession_charm_treasure_chime"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 1
end

function item:on_activated()
  timer = sol.timer.start(game, 3000, function()
    local map = game:get_map()
    local hero = map:get_hero()
    local x, y, z = hero:get_position()
    local is_treasure = false
    for entity in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
      if entity:get_distance(hero) <= range and not game:is_suspended() then
        if entity:get_type() == "pickable" or (entity:get_type() == "chest" and not entity:is_open()) then
          is_treasure = true
        end
      end
    end
    if is_treasure then
      sol.audio.play_sound"bell_wave"
    end
    return true
  end)
end

function item:on_unequipped()
  timer:stop()
end
