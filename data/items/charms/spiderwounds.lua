local item = ...
local game = item:get_game()

local base_damage = 8
local scaling_stat = "life"
local num_spiders = 1
local spider_duration = 7000

function item:on_started()
  local savegame_variable = "possession_charm_spiderwounds"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 2

  game.charm_manager:subscribe("on_hurt", function()
    if game:is_charm_equipped(item:get_name()) then
      local hero = game:get_hero()
      local map = game:get_map()
      local x, y, z = hero:get_position()

      --Create spiders:
      for i = 1, num_spiders do
        local spider = map:create_custom_entity{
          x = x, y = y, layer = z,
          width = 16, height = 8, direction = 0,
          sprite = "items/vile_spider",
          model = "animals/familiar"
        }
        local sprite = spider:get_sprite()
        sprite:set_animation("falling", function()
          sprite:set_animation"walking"
        end)

        spider.is_spiderflesh_spider = true
        spider.damage_cooldown = 1500
        spider.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
        spider.duration = spider_duration + 1000

        local m = sol.movement.create("straight")
        m:set_angle(math.rad(math.random(0,360)))
        m:set_max_distance(32)
        m:set_speed(250)
        m:start(spider, function()
          spider:start()
        end)
        m.on_obstacle_reached = spider.start

      end
    end
  end)
end
