local item = ...
local game = item:get_game()

local amount = 25 --how much more weight you can pull with the hookshot when equipped

function item:on_started()
  local savegame_variable = "possession_charm_heavyhook_plus"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 6
end

function item:on_equipped()

end


function item:on_activated()
  if not game.hookshot_pull_weight then game.hookshot_pull_weight = 10 end
  game.hookshot_pull_weight = game.hookshot_pull_weight + amount
end


function item:on_unequipped()
  game.hookshot_pull_weight = game.hookshot_pull_weight - amount
end
