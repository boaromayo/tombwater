local item = ...
local game = item:get_game()

local frequency = 15000
local duration = 15000
local base_damage = 10
local scaling_stat = "life"

function item:on_started()
  local savegame_variable = "possession_charm_spawn_ally"
  item:set_savegame_variable(savegame_variable)
  item.charm_cost = 5

end

function item:on_equipped()

end


function item:on_activated()
  item.effect_timer = sol.timer.start(game, frequency, function()
    item:spawn_ally()
    return true
  end)
  item.effect_timer:set_suspended_with_map(true)
end


function item:on_unequipped()
  if item.effect_timer then
    item.effect_timer:stop()
  end
end


function item:spawn_ally()
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()

    local spider = map:create_custom_entity{
      x = x, y = y, layer = z,
      width = 16, height = 8, direction = 0,
      sprite = "items/cthulu_buddy",
      model = "animals/familiar"
    }
    local sprite = spider:get_sprite()

    spider.damage_cooldown = 1500
    spider.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    spider.duration = duration + 1000
    spider:set_can_traverse_ground("hole", true)
    spider:set_can_traverse_ground("shallow_water", true)
    spider:set_can_traverse_ground("deep_water", true)
    spider:set_can_traverse_ground("lava", true)

    local m = sol.movement.create("straight")
    m:set_angle(math.rad(math.random(0,360)))
    m:set_max_distance(32)
    m:set_speed(250)
    m:start(spider, function()
      spider:start()
    end)
    m.on_obstacle_reached = spider.start
end

