local menu = {
  x = 10, y = 22,
}

local width = 36
local height = 1

local bar_bg = sol.surface.create(width + 2, height + 2)
bar_bg:fill_color(sol.colors.ui_black)
local bar_fg = sol.surface.create(width, height)
bar_fg:fill_color(sol.colors.gunmetal)

function menu:set_duration(duration)
  menu.elapsed_time = 0
  menu.duration = duration
  if sol.menu.is_started(menu) then
    sol.menu.stop(menu)
  end
end

function menu:on_started()
  local step = 10
  menu.elapsed_time = 0
  assert(type(menu.duration) == "number", "Call gun_reload_hud_menu:set_duration(duration) before starting this menu")
  sol.timer.start(menu, step, function()
    menu.elapsed_time = menu.elapsed_time + step
    if menu.elapsed_time < menu.duration then
      return step
    else
      sol.audio.play_sound"gun_revolver_hammer_cock"
      menu:flash()
    end
  end)
end

function menu:flash()
  bar_bg:fill_color(sol.colors.ui_off_white)
  sol.timer.start(menu, 100, function()
    bar_bg:fill_color(sol.colors.ui_black)
    sol.menu.stop(menu)
  end)
end


function menu:on_draw(dst)
  bar_bg:draw(dst, menu.x, menu.y)
  bar_fg:draw_region(0, 0, (menu.elapsed_time / menu.duration) * width, height, dst, menu.x + 1, menu.y + 1)
end

return menu
