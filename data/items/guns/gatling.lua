local item = ...
local game = item:get_game()

local reload_hud = require("items/guns/gun_reload_hud")
local jitter_amount = 10 --degrees of firing inaccuracy

function item:on_started()
  item:set_savegame_variable("possession_gun_gatling")
end

function normalize_angle(angle)
  local new_angle = ((angle + math.pi) % (2 * math.pi)) - (2 * math.pi)
  return new_angle
end

function item:fire_override(gun_props)
  local hero = game:get_hero()
  local map = game:get_map()
  local animation = hero:get_animation()

  game:remove_bullets(gun_props.ammo_cost)

  --local aim_angle = hero:get_aim_angle()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2
  local dx = game:dx(8)[hero:get_direction()]
  local dy = game:dy(8)[hero:get_direction()]

  local init_aim_angle
  for i = 1, gun_props.num_bullets do
    sol.timer.start(hero, 50 * (i - 1), function()
      sol.audio.play_sound(gun_props.fire_sound)
      local projectile = map:create_custom_entity{
        x = x + dx,
        y = y + dy,
        layer = z,
        direction = 0,
        width = 8, height = 8, 
        sprite = gun_props.bullet_sprite,
        model = "hero_projectiles/bullet",
      }
      projectile:set_origin(4, 5)
      projectile:set_can_traverse("hero", true)
      for k, v in pairs(gun_props.projectile_properties) do
        projectile[k] = v
      end

      --Hook for charms
      --game.charm_manager:trigger_event("on_bullet_fired", projectile)

      local aim_angle = init_aim_angle or hero:get_aim_angle()
      if not init_aim_angle then init_aim_angle = aim_angle end --lock in the aim angle since the player could exit the aim state before all bullets have been fired
      angle = aim_angle + math.rad(math.random(jitter_amount * -1, jitter_amount))

      local facing_direction = sol.main.get_direction4(aim_angle)
      hero:set_direction(facing_direction)
      projectile:shoot(angle)
    end)
  end

  --Recoil/re-aim
  hero:recoil(24)

  --Cooldown / chamber next bullet
  sol.timer.start(hero, gun_props.chamber_sound_delay or 100, function()
    if gun_props.chamber_sound then sol.audio.play_sound(gun_props.chamber_sound) end
  end)
  hero.fire_cooldown = true
  sol.timer.start(hero, gun_props.chamber_time, function() hero.fire_cooldown = false end)
  --Reloading bar:
  if gun_props.chamber_time >= 400 then
    reload_hud:set_duration(gun_props.chamber_time)
    sol.menu.start(game, reload_hud)
  end
end
