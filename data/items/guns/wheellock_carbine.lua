local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_wheellock_carbine")
end

item:register_event("on_using", function()
  local hero = game:get_hero()
  hero:recoil(8)
end)
