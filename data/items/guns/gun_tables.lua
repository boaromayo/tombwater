--[[
Aim animations:
the hero needs two animations per gun: stopped and walking. The system will append "_stopped" or "_walking" to the end of the "aim_animation" value specified here.
--]]

return {
  example = {
    aim_animation = "gun_pistol",
    aim_sound = "gun_pistol",
    ammo_cost = 1,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet",
    bullet_model = "hero_projectiles/bullet", --this is the default model
    num_bullets = 1,
    spread = 0,
    projectile_properties = {
      damage = 45,
      speed = 450,
      range = 500,
      obstacle_collision_callback = function(bullet)
        local x, y, z = bullet:get_position()
        local map = bullet:get_map()
        map:create_explosion{x=x, y=y, layer=z}
      end,
      enemy_collision_callback = function(bullet, enemy)
        enemy:stagger(2000)
      end,
    },
  },

  ["guns/pistol"] = {
    ammo_cost = 1,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 45,
      speed = 450,
      range = 500,
    },
    chamber_time = 100,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/pistol_viper"] = {
    ammo_cost = 1,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet_small",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 35,
      speed = 450,
      range = 500,
      enemy_collision_callback = function(self, enemy) enemy:build_up_status_effect("poison", 55) end,
    },
    chamber_time = 100,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/pistol_ivory"] = {
    ammo_cost = 1,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet_silver",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_big_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 40,
      damage_type = "ice",
      speed = 400,
      range = 300,
      enemy_collision_callback = function(self, enemy) enemy:build_up_status_effect("cold", 34) end,
      obstacle_collision_callback = function(self)
        local map = self:get_map()
        local x, y, z = self:get_position()
        local mist = map:create_custom_entity{x=x, y=y, layer=z, width=16, height=16, direction=0, sprite="items/cloud",models="attacks/status_cloud",}
        mist:get_sprite():set_color_modulation{160,200,255,150}
        mist.ignore_hero = true
        mist.status_type = "cold"
      end,
    },
    chamber_time = 300,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/pistol_magic"] = {
    ammo_cost = 1,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet_silver",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 35,
      damage_type = "magic",
      speed = 400,
      range = 300,
    },
    chamber_time = 300,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/wheellock_carbine"] = {
    ammo_cost = 1,
    fire_sound = "gunshot_pistol-big",
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_big_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 60,
      speed = 400,
      range = 300,
    },
    chamber_time = 1200,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/zeliska"] = {
    ammo_cost = 3,
    fire_sound = "gunshot_pistol-big",
    bullet_sprite = "hero_projectiles/bullet_big",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_big_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 100,
      speed = 550,
      range = 500,
    },
    chamber_time = 1200,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/shotgun"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_shotgun",
    bullet_sprite = "hero_projectiles/bullet_small",
    num_bullets = 5,
    spread = 75,
    aim_animation = "gun_shotgun",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 30,
      speed = 300,
      range = 72,
    },
    chamber_time = 1200,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/cannon_carbine"] = {
    ammo_cost = 3,
    fire_sound = "gunshot_shotgun",
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 7,
    spread = 85,
    aim_animation = "gun_shotgun",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 50,
      speed = 300,
      range = 72,
    },
    chamber_time = 1500,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/shotgun_fire"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_shotgun",
    bullet_sprite = "hero_projectiles/bullet_fire",
    num_bullets = 5,
    spread = 55,
    aim_animation = "gun_shotgun",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 25,
      damage_type = "fire",
      speed = 250,
      range = 80,
    },
    chamber_time = 1200,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/shotgun_boreal"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_shotgun",
    bullet_sprite = "hero_projectiles/bullet_silver",
    num_bullets = 4,
    spread = 45,
    aim_animation = "gun_shotgun",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 25,
      damage_type = "ice",
      speed = 250,
      range = 96,
    },
    chamber_time = 1200,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/rifle"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_rifle",
    damage = 10,
    bullet_sprite = "hero_projectiles/bullet_small",
    num_bullets = 1,
    spread = 0,
    range = 600,
    aim_animation = "gun_rifle",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 90,
      speed = 500,
      range = 600,
      is_piercing = true,
    },
    chamber_time = 2000,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/rifle_powered"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_rifle",
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 1,
    spread = 0,
    range = 600,
    aim_animation = "gun_rifle",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 100,
      speed = 600,
      range = 600,
      is_piercing = true,
    },
    chamber_time = 2500,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/sniper"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_rifle",
    damage = 10,
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 1,
    spread = 0,
    range = 600,
    aim_animation = "gun_rifle",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 110,
      speed = 700,
      range = 600,
      is_piercing = true,
    },
    chamber_time = 2400,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/arquebus"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_rifle",
    damage = 10,
    bullet_sprite = "hero_projectiles/bullet_fire",
    range = 600,
    aim_animation = "gun_rifle",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 75,
      damage_type = "fire",
      speed = 550,
      range = 600,
      is_piercing = true,
      enemy_collision_callback = function(self, enemy) enemy:build_up_status_effect("burn", 35) end,
    },
    chamber_time = 1700,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
  },

  ["guns/edison_cannon"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_shotgun",
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 1,
    aim_animation = "gun_edison_cannon",
    aim_sound = "gun_aim_shotgun",
    chamber_time = 1500,
    chamber_sound_delay = 600,
    chamber_sound = "gun_cock",
    projectile_properties = {
      damage = 50,
      damage_type = "fire",
    },
  },

  ["guns/harpoon"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_harpoon",
    --damage = 10,
    bullet_sprite = "hero_projectiles/harpoon",
    num_bullets = 1,
    spread = 0,
    range = 600,
    aim_animation = "gun_harpoon",
    aim_sound = "gun_aim_harpoon",
    projectile_properties = {
      damage = 55,
      speed = 600,
      range = 600,
      is_piercing = true,
    },
    chamber_time = 2400,
    chamber_sound_delay = 600,
    chamber_sound = "gun_load_harpoon",
  },

  ["guns/grenade_launcher"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_pistol-big",
    bullet_sprite = "hero_projectiles/grenade_launched",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_big_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 35,
      speed = 300,
      range = 160,
      non_rotational = true,
      obstacle_collision_callback = function(bullet)
        local x, y, z = bullet:get_position()
        local map = bullet:get_map()
        map:create_explosion{x=x, y=y, layer=z}
      end,
    },
    chamber_time = 1200,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/cannon"] = {
    ammo_cost = 3,
    fire_sound = "gunshot_cannon",
    bullet_sprite = "hero_projectiles/cannonball",
    num_bullets = 1,
    spread = 0,
    aim_animation = "gun_cannon",
    aim_sound = "gun_load_harpoon",
    projectile_properties = {
      damage = 35,
      speed = 400,
      range = 180,
      non_rotational = true,
      obstacle_collision_callback = function(bullet)
        local x, y, z = bullet:get_position()
        local map = bullet:get_map()
        map:create_explosion{x=x + 32, y=y, layer=z}
        map:create_explosion{x=x - 32, y=y, layer=z}
        map:create_explosion{x=x, y=y + 32, layer=z}
        map:create_explosion{x=x, y=y - 32, layer=z}
      end,
    },
    chamber_time = 1200,
    chamber_sound = "gun_load_harpoon",
  },

  ["guns/gatling"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet",
    num_bullets = 5,
    spread = 0,
    aim_animation = "gun_gatling",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 17,
      speed = 450,
      range = 300,
    },
    chamber_time = 400,
    chamber_sound = "gun_revolver_hammer_cock",
  },

  ["guns/oilsprayer"] = {
    ammo_cost = 2,
    fire_sound = "gunshot_harpoon",
    bullet_sprite = "hero_projectiles/oil_drop_dropping",
    num_bullets = 6,
    spread = 55,
    aim_animation = "gun_oilsprayer",
    aim_sound = "gun_aim_shotgun",
    projectile_properties = {
      damage = 3,
      dying_sprite_id = "hero_projectiles/oil_drop_dropping",
      speed = 300,
      range = 48,
      range_variance = 12,
      speed_variance = 25,
      enemy_collision_callback = function(self, enemy)
        enemy.damage_type_mods = enemy.damage_type_mods or {}
        enemy.status_resistances = enemy.status_resistances or {}
        enemy.damage_type_mods["fire"] = 2
        enemy.status_resistances["burn"] = 40
      end,
      obstacle_collision_callback = function(bullet)
        local x, y, z = bullet:get_position()
        local map = bullet:get_map()
        --Create oil puddle
      end,
    },
    chamber_time = 600,
    --chamber_sound_delay = 600,
    chamber_sound = "mechanical_track_very_short",
  },

}

