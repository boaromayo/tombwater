local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_cannon")
end

item:register_event("on_using", function()
  local hero = game:get_hero()
  local angle = hero:get_direction() * (math.pi / 2) + math.pi
  hero:ragdoll(angle, 48)
end)

