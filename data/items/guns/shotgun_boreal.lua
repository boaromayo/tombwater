local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_shotgun_boreal")
end


item:register_event("on_using", function()
  local hero = game:get_hero()
  local map = hero:get_map()
  local direction = hero:get_direction()
  local x, y, z = hero:get_position()
  local target = map:create_custom_entity{
    x=x, y=y, layer=z, width=16, height=16, direction=0,
  }
  local m = sol.movement.create"straight"
  m:set_speed(200)
  m:set_max_distance(120)
  m:set_angle(direction * math.pi / 2)
  m:start(target)
  local counter = 0
  sol.timer.start(map, 100, function()
    local x, y, z = target:get_position()
    local cloud = map:create_custom_entity{
      x=x + math.random(-8, 8), y=y + math.random(-8, 8), layer=z,
      width=16, height=16, direction=0,
      model = "attacks/status_cloud", sprite = "items/cloud",
    }
    cloud:get_sprite():set_color_modulation{160,200,255,150}
    cloud.status_type = "cold"
    cloud.ignore_hero = true
    counter = counter + 1
    return (counter < 5)
  end)
end)
