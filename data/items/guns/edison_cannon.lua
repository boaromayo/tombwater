local item = ...
local game = item:get_game()

local charge_time = 1000
local range = 256

function item:on_started()
  item:set_savegame_variable("possession_edison_cannon")
end

function normalize_angle(angle)
  local new_angle = ((angle + math.pi) % (2 * math.pi)) - (2 * math.pi)
  return new_angle
end

function item:fire_override(gun_props)
  local hero = game:get_hero()
  local map = game:get_map()
  local animation = hero:get_animation()
  hero:freeze()
  hero:set_animation("gun_edison_cannon_stopped")
  sol.audio.play_sound("bot_charging")
  sol.timer.start(hero, charge_time, function()
    game:remove_bullets(gun_props.ammo_cost)
    sol.audio.play_sound"electrified"
    sol.audio.play_sound"frost2"

    --local aim_angle = hero:get_aim_angle()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    local angle = direction * math.pi / 2
    local dx = game:dx(8)[hero:get_direction()]
    local dy = game:dy(8)[hero:get_direction()]
    local zap = map:create_custom_entity{
      x = x + dx, y = y + dy - 4, layer = z,
      direction = 0,
      width = 16, height = 16,
      sprite = "items/zap_beam",
      --model = "damaging_entity", --shouldn't need any of the other collision checking stuff, but this isn't detecting collision for some reason
    }
    zap.damage = game:calculate_hero_output_damage(gun_props.projectile_properties.damage, "guns") or 50
    local zap_sprite = zap:get_sprite()
    zap_sprite:set_rotation(angle)

    --Compare coordinates to decide collision:
    local beam_width = 24
    local is_horiz = (direction == 0 or direction == 2) and true or false
    for ent in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
      local ex, ey, ez = ent:get_position()
      if ez == z and ( is_horiz and (math.abs(y - ey) <= beam_width) or (math.abs(x - ex) <= beam_width) ) and (hero:get_distance(ent) <= range) and (hero:get_direction4_to(ent) == direction) then
        local ent_type = ent:get_type()
        --Enemies:
        if ent_type == "enemy" then
          if ent.process_hit then
            ent:process_hit(zap.damage or 1, zap.damage_type or "physical")
          elseif ent.hurt then
            ent:hurt(zap.damage or 1)
          end
          map:create_lightning{x = ex, y = ey, layer = ez}
        end
        if ent.react_to_lightning then
          ent:react_to_lightning(zap)
        end
      end
    end
    --]]

    --[[Find angle to decide collision: not great at close range
    for ent in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
      local angle_to = normalize_angle(hero:get_angle(ent))
      if math.abs(normalize_angle(angle) - angle_to) <= math.rad(5) then
        if ent.process_hit then
          ent:process_hit(zap.damage or 1, zap.damage_type or "physical")
        elseif ent.hurt then
          ent:hurt(zap.damage or 1)
        end
      end
    end
    --]]

    --[[ Sprite Collision Test: isn't working because of some engine bug?
    zap.collided_entities = {}
    zap:add_collision_test("sprite", function(entity, other, entity_sprite, other_sprite)
print("Collided:", other:get_type(), other_sprite:get_animation_set())
      if other:get_type() == "enemy" and not entity.collided_entities[other] then
        entity.collided_entities[other] = other
        sol.timer.start(entity, 200, function() entity.collided_entities[other] = nil end)
        if other.process_hit then
          other:process_hit(entity.damage or 1, entity.damage_type or "physical")
        else
          other:hurt(entity.damage or 1)
        end
      end
    end)
    --]]

    --Recoil/re-aim
    hero:start_aiming()
    hero:recoil(24)
  end)
end
