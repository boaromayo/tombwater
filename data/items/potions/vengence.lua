local item = ...
local game = item:get_game()

local default_max_amount = 1

local damage_amount = 15
local duration = 90 * 1000

function item:on_started()
  item:setup_amount_stuff(default_max_amount)

  item:set_fill_on_checkpoint()  

  game.charm_manager:subscribe("on_hurt", function()
    if item.active then
      local hero = game:get_hero()
      local map = hero:get_map()
      local x, y, z = hero:get_position()
      local blast = map:create_custom_entity{
        x=x, y=y - 1, layer=z, width=16, height=16, direction=0,
        model = "damaging_entity",
        sprite = "items/shockwave_3x3",
      }
      blast.damage = game:calculate_hero_output_damage(damage_amount, "alchemy")
    end
  end)

end


function item:on_using()
  require("items/potions/potion_manager"):start_potion(item, function()
    item:set_finished()
    item.active = true
    sol.timer.start(game, duration, function()
      item.active = false
    end)
  end)
end


function item:on_obtaining()
  item:add_amount(default_max_amount)
end
