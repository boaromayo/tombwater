local item = ...
local game = item:get_game()

local default_max_amount = 1
local status = "madness"
local new_amount = 3
local duration = 60 * 1000

function item:on_started()
  item:setup_amount_stuff(default_max_amount)

  item:set_fill_on_checkpoint()  
end


function item:on_using()
  require("items/potions/potion_manager"):start_potion(item, function()
    local hero = game:get_hero()
    local old_decrement = hero:get_status_buildup_decrement_amount(status)
    hero:set_status_buildup_decrement_amount(status, new_amount)
    sol.timer.start(game, duration, function()
      hero:set_status_buildup_decrement_amount(status, old_decrement)
    end)
    item:set_finished()
  end)
end


function item:on_obtaining()
  item:add_amount(default_max_amount)
end
