local item = ...
local game = item:get_game()

local default_max_amount = 1
local rate = 700
local base_amount = 1
local duration = 60 * 1000

function item:on_started()
  item:setup_amount_stuff(default_max_amount)

  item:set_fill_on_checkpoint()  
end


function item:on_using()
  require("items/potions/potion_manager"):start_potion(item, function()
    local elapsed_time = 0
    local heal_amount = game:calculate_hero_output_damage(base_amount, "alchemy") - base_amount + 1
    sol.timer.start(game, rate, function()
      elapsed_time = elapsed_time + rate
      game:add_life(heal_amount)
      if elapsed_time < duration then return true end
    end)
    item:set_finished()
  end)
end


function item:on_obtaining()
  item:add_amount(default_max_amount)
end
