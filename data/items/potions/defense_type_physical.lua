local item = ...
local game = item:get_game()

local default_max_amount = 1

local damage_type = "physical"
local multiplier = .6
local duration = 30 * 1000

function item:on_started()
  item:setup_amount_stuff(default_max_amount)
  item:set_fill_on_checkpoint()  
end

function item:on_using()
  require("items/potions/potion_manager"):start_damage_multiplier_potion(item, damage_type, multiplier, duration)
end


function item:on_obtaining()
  item:add_amount(1)
end
