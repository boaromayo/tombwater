local manager = {}

local game_meta = sol.main.get_metatable("game")

--Clear potion effects on gameover
game_meta:register_event("on_game_over_started", function(self)
  self.potion_damage_multipliers = {}
end)


function manager:start_potion(item, callback)
  local game = item:get_game()
  local hero = game:get_hero()

  if item:has_amount(1) then
    hero:set_animation("drinking", function()
      item:remove_amount(1)
      callback()
    end)
  else
    item:set_finished()
  end
end


function manager:start_damage_multiplier_potion(item, damage_type, multiplier, duration)
  local game = item:get_game()
  local hero = game:get_hero()

  if not game.potion_damage_multipliers then game.potion_damage_multipliers = {} end
  local function callback()
      item:set_finished()
      --TODO play sound effect, animation
      if game["potion_damage_multiplier_timer" .. damage_type] then game["potion_damage_multiplier_timer" .. damage_type]:stop() end
      game.potion_damage_multipliers[damage_type] = multiplier
      game["potion_damage_multiplier_timer" .. damage_type] = sol.timer.start(game, duration, function()
        game.potion_damage_multipliers[damage_type] = nil
      end)
  end

  manager:start_potion(item, callback)
end


return manager

