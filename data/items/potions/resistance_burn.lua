local item = ...
local game = item:get_game()

local default_max_amount = 1
local duration = 45 * 1000

function item:on_started()
  item:setup_amount_stuff(default_max_amount)

  item:set_fill_on_checkpoint()  
end


function item:on_using()
  local hero = game:get_hero()
  require("items/potions/potion_manager"):start_potion(item, function()
    hero:start_status_immunity("burn", duration)
    item:set_finished()
  end)
end


function item:on_obtaining()
  item:add_amount(default_max_amount)
end
