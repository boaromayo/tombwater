local item = ...
local game = item:get_game()

local magic_cost = 25
local base_damage = 25
local damage_type = "magic"
local scaling_stat = "mind"

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_wraithblade")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  if item:can_spend_ammo(magic_cost) then
    item:remove_ammo(magic_cost)
    item:attack()
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:attack()
  local hero = game:get_hero()
  local map = game:get_map()
  local sword_item = game:get_item("solforge/wraithblade")

  local x, y, z = hero:get_position()
  local em = map:create_particle_emitter(x, y, z)
  em.target = hero
  em.particle_sprite = "effects/black_flame"
  em.particle_animation_loops = false
  em.particle_opacity = {200,250}
  em.duration = 500
  em.particles_per_loop = 1
  em.frequency = 30
  em.particle_speed = 50
  em.angle_variance = math.rad(5)
  em.target = hero
  em:emit()

  sword_item:on_using()
  sol.audio.play_sound("spells/wraith_appear")
  sol.timer.start(hero, 200, function()
    sol.audio.play_sound("spells/fireball")
  end)

  --Also, shoot a sword beam. It'll be fun!
  --[[
  sol.timer.start(hero, 200, function()
    sol.audio.play_sound("fireball")
    local x, y, z = hero:get_position()
    local beam = map:create_custom_entity{
      x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
      sprite = "hero_projectiles/black_flame_blade_beam",
      model = "hero_projectiles/general",
    }
    beam.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    beam.damage_type = damage_type
    beam:get_sprite():set_opacity(180)
    beam.obstacle_callback = function() beam:remove() end
    beam:shoot(hero:get_direction() * math.pi / 2)
  end)
  --]]

end
