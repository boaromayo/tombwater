local item = ...
local game = item:get_game()

local magic_cost = 80
local casting_time = 600
local postcast_time = 400
local base_damage = 25
local damage_type = "magic"
local scaling_stat = "mind"

local wraith_lifespan = 15000 --how long wraith will last
local wraith_range = 400 --how close for an enemy to be chosen as a target by the wraith
local wraith_shoot_range = 160
local aim_delay = 400
local wraith_melee_range = 80

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_wraith_ally")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("wraith_flash", function() hero:set_animation("stopped") end)
    sol.audio.play_sound("spells/wraith_charge")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:create_wraith()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:create_wraith()
  local map = game:get_map()
  local hero = map:get_hero()
  local direction = hero:get_direction()
  local x, y, z = hero:get_position()
  --[[x = x + game:dx(24)[direction]
  y = y + game:dy(24)[direction]
  map:create_poof(x, y + 1, z)--]]
  sol.audio.play_sound("spells/wraith_appear")
  local wraith = map:create_custom_entity{
    x = x, y = y + 3, layer = z,
    width = 16, height = 16, direction = direction,
    sprite = "hero/emberwraith"
  }

  wraith:set_drawn_in_y_order(true)
  wraith:set_traversable_by(false)
  wraith:set_traversable_by("hero", true)
  wraith:set_can_traverse(true)
  wraith:set_can_traverse_ground("shallow_water", true)
  wraith:set_can_traverse_ground("hole", true)
  wraith:set_can_traverse_ground("lava", true)
  wraith:set_can_traverse_ground("wall", true)
  wraith:set_can_traverse_ground("low_wall", true)

  --Set lifespan
  sol.timer.start(wraith, wraith_lifespan, function()
    wraith:stop_movement()
    sol.timer.stop_all(wraith)
    wraith:get_sprite():fade_out()
    sol.timer.start(wraith, 500, function()
      wraith:remove()
    end)
  end)

  local sprite = wraith:get_sprite()
  sprite:set_animation("appearing", function()
    sprite:set_animation("stopped")
    wraith:choose_enemy()
  end)


  function wraith:on_movement_changed(m)
    wraith:get_sprite():set_direction(m:get_direction4())
  end


  function wraith:choose_enemy()
    local x, y, z = wraith:get_position()
    local possible_enemies = {}
    for e in map:get_entities_in_rectangle(x - wraith_range, y - wraith_range, wraith_range * 2, wraith_range * 2) do
      if e:get_type() == "enemy" and wraith:get_distance(e) <= wraith_range then
        table.insert(possible_enemies, e)
      end
    end
    if not possible_enemies[1] then
      wraith:wander()
    else
      local rand = math.random(1, #possible_enemies)
      local chosen_enemy = possible_enemies[rand]
      wraith:target_enemy(chosen_enemy)
    end
  end


  function wraith:wander()
    local m = sol.movement.create"random"
    m:start(wraith)
    sol.timer.start(wraith, 1000, function()
      wraith:choose_enemy()
    end)
  end


  function wraith:target_enemy(target_enemy)
    local m = sol.movement.create"target"
    m:set_target(target_enemy)
    m:set_speed(80)
    m:start(wraith)
    sol.timer.start(wraith, 50, function()
      local random = math.random(1, 100)
      if random <= 10 or wraith:get_distance(target_enemy) <= wraith_melee_range then
        m:stop()
        wraith:melee_attack(target_enemy)
      elseif wraith:get_distance(target_enemy) <= wraith_shoot_range then
        m:stop()
        wraith:ranged_attack(target_enemy)
      else
        return true
      end
    end)
  end


  function wraith:ranged_attack(enemy)
    sprite:set_animation("aiming_unarmed")
    sol.timer.start(wraith, aim_delay, function()
      local wx, wy = wraith:get_position()
      local projectile = map:create_custom_entity{
        x = wx, y = wy, layer = z, width = 8, height = 8, direction = 0,
        sprite = "hero_projectiles/black_fireball",
        model = "hero_projectiles/bullet",
      }
      projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
      projectile.damage_type = damage_type
      projectile.is_piercing = true
      aim_angle = enemy and wraith:get_angle(enemy) or angle
      projectile:shoot(aim_angle)
      wraith:make_sound("spells/fireball_short")
      wraith:make_sound("fireball")
      sol.timer.start(wraith, 300, function()
        sprite:set_animation("stopped")
        wraith:wander()
      end)
    end)
  end


  function wraith:melee_attack(enemy)
    local collided_entities = {}
    local damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    local angle = wraith:get_angle(enemy)
    sprite:set_animation("dashing")
    wraith:make_sound("spells/fireball")
    local attack_sprite = wraith:create_sprite("hero_projectiles/black_flame_blade_beam")
    attack_sprite:set_rotation(angle)
    attack_sprite:set_animation("shooting", function()
      wraith:remove_sprite(attack_sprite)
    end)

    wraith:add_collision_test("sprite", function(wraith, other, colliding_sprite, other_sprite)
      if (colliding_sprite == attack_sprite) and not collided_entities[other] and (other:get_type() == "enemy") then
        collided_entities[other] = other
        if other.process_hit then
          other:process_hit(damage, damage_type)
        else
          other:hurt(damage)
        end
      end
    end)

    local function end_movement()
      sprite:set_animation("stopped")
      wraith:wander()
    end

    local m = sol.movement.create"straight"
    m:set_angle(angle)
    m:set_speed(160)
    m:set_max_distance(96)
    m:start(wraith, function()
      end_movement()
    end)
    function m:on_obstacle_reached() end_movement() end
  end


end
