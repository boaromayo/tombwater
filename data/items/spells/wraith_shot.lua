local item = ...
local game = item:get_game()

local magic_cost = 45
local base_damage = 35
local damage_type = "bullet"
local scaling_stat = "mind"
local casting_time = 10
local aim_delay = 500
local num_shots = 3
local fire_frequency = 100

function item:on_started()
  item:set_savegame_variable("possession_spell_wraith_shot")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local hero = game:get_hero()
  
  if item:can_spend_ammo(magic_cost) then
  sol.audio.play_sound"gun_aim_pistol"
  hero:set_animation("gun_pistol_stopped")
  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    item:cast()
    sol.timer.start(hero, aim_delay, function()
      hero:set_animation("stopped")
      item:set_finished()
      hero:unfreeze()
    end)
  end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end

function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2
  local shots_fired = 0

  sol.audio.play_sound("spells/wraith_appear")
  local dx = x + game:dx(24)[direction] * -1
  local dy = y + game:dy(24)[direction] * -1
  local wraith = map:create_custom_entity{
    x = dx, y = dy, layer = z, width = 16, height = 16, direction = 0,
    sprite = "hero/emberwraith",
  }
  wraith:set_drawn_in_y_order(true)
  local wraith_sprite = wraith:get_sprite()
  wraith_sprite:set_animation("appearing", function()
    wraith_sprite:set_animation("aiming")
    wraith_sprite:set_direction(direction)
  end)
  wraith_sprite:set_frame_delay(40) --speed up animation for gameplay purposes
  sol.timer.start(wraith, aim_delay, function()
    local function shoot()
      local wx, wy = wraith:get_position()
      local projectile = map:create_custom_entity{
        x = wx, y = wy, layer = z, width = 8, height = 8, direction = 0,
        sprite = "hero_projectiles/wraith_bullet",
        model = "hero_projectiles/bullet",
      }
      projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
      projectile.damage_type = damage_type
      projectile.is_piercing = true
      aim_angle = angle + math.rad(math.random(-10, 10))
      projectile:shoot(aim_angle)
      sol.audio.play_sound("gunshot_pistol")
    end

    sol.timer.start(wraith, 0, function()
      shoot()
      shots_fired = shots_fired + 1
      if shots_fired < num_shots then
        return fire_frequency
      else
        sol.timer.start(wraith, 300, function()
          wraith_sprite:set_animation("disappearing", function()
            wraith:remove()
          end)
        end)
      end
    end)

  end)

end
