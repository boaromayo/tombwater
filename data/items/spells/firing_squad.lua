local item = ...
local game = item:get_game()

local magic_cost = 60
local base_damage = 25
local damage_type = "bullet"
local scaling_stat = "mind"
local casting_time = 100
local aim_delay = 1000
local num_wraiths = 5
local range = 180

function item:on_started()
  item:set_savegame_variable("possession_spell_firing_squad")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local hero = game:get_hero()
  
  if item:can_spend_ammo(magic_cost) then
  hero:set_animation("raised_arm")
  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    item:cast()
    sol.timer.start(hero, 200, function()
      hero:set_animation("stopped")
      item:set_finished()
      hero:unfreeze()
    end)
  end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end

function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()

  local enemies = item:select_nearby_enemies(range, num_wraiths)
  sol.audio.play_sound("spells/wraith_appear")
  for i = 1, num_wraiths do
    local enemy_no = ((i - 1) % #enemies) + 1
    local enemy = enemies[enemy_no]
    local angle = 2 * math.pi / num_wraiths * i
    local aim_angle = enemy and hero:get_angle(enemy) or angle
    local dx = x + 32 * math.cos(angle) + math.random(-8, 8)
    local dy = y + 32 * math.sin(angle) + math.random(-8, 8)
    local wraith = map:create_custom_entity{
      x = dx, y = dy, layer = z, width = 16, height = 16, direction = 0,
      sprite = "hero/emberwraith",
    }
    wraith:set_drawn_in_y_order(true)
    local wraith_sprite = wraith:get_sprite()
    wraith_sprite:set_animation("appearing", function()
      wraith_sprite:set_animation("aiming")
      wraith_sprite:set_direction(sol.main.get_direction4(aim_angle))
    end)
    sol.timer.start(wraith, aim_delay + math.random(-100,100), function()
      local wx, wy = wraith:get_position()
      local projectile = map:create_custom_entity{
        x = wx, y = wy, layer = z, width = 8, height = 8, direction = 0,
        sprite = "hero_projectiles/wraith_bullet",
        model = "hero_projectiles/bullet",
      }
      projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
      projectile.damage_type = damage_type
      projectile.is_piercing = true
      aim_angle = enemy and wraith:get_angle(enemy) or angle
      projectile:shoot(aim_angle)
      sol.audio.play_sound("gunshot_pistol")
      sol.audio.play_sound("spells/fireball_short")
      sol.timer.start(wraith, 300, function()
        wraith_sprite:set_animation("disappearing", function()
          wraith:remove()
        end)
      end)
    end)
  end
end
