local item = ...
local game = item:get_game()

local magic_cost = 65
local casting_time = 300
local postcast_time = 300 --the spell starts, but you're still locked in the animation
local base_damage = 7
local damage_type = "magic"
local scaling_stat = "mind"
local num_attacks = 10
local duration = 10000
local average_delay = 100
local delay_variance = 90
local spell_range = 96
local spell_width = 90

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_cactus_growth")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")
    local x, y, z = hero:get_position()
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_summoning()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_summoning()
  local map = item:get_map()
  local hero = map:get_hero()
  local attacks = 0
  local angle = hero:get_direction() * (math.pi / 2)
  local x, y, z = hero:get_position()
  sol.timer.start(hero, 0, function()
    attacks = attacks + 1
    item:create_attack(x, y, z, angle)
    if attacks < num_attacks then
      return average_delay + math.random(delay_variance * -1, delay_variance)
    end
  end)
end


function item:create_attack(x, y, z, angle)
  local map = item:get_map()
  local angle = angle + math.rad(math.random(spell_width * 100 / 2 * -1, spell_width * 100 / 2) / 100)
  tx = x + math.random(32, spell_range) * math.cos(angle)
  ty = y + ( math.random(32, spell_range) * math.sin(angle) + math.pi ) * -1
  local ground_type = map:get_ground(tx, ty, z)
  if ground_type ~= "traversable" then
    item:create_attack(x, y, z, angle)
    return
  end
  sol.audio.play_sound("razorgrass")
  sol.audio.play_sound("spells/ground_burst_soft_" .. math.random(1,4))
  local attack = map:create_custom_entity{
    x = tx, y = ty, layer = z,
    width = 8, height = 8, direction = 0,
    sprite = "items/cactus",
    model = "damaging_entity",
  }
  attack.duration = duration + 5000
  attack.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
  attack.damage_type = damage_type
  attack.damage_cooldown = 2000
  local attack_sprite = attack:get_sprite()
  sol.timer.start(attack, duration, function()
    attack_sprite:set_animation("retreating", function()
      attack:remove()
    end)
  end)
end
