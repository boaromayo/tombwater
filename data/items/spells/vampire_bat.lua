local item = ...
local game = item:get_game()

local magic_cost = 25
local casting_time = 200
local range = 400
local base_damage = 25
local damage_type = "physical"
local scaling_stat = "mind"

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_vampire_bat")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("punch_windup")
    sol.timer.start(hero, casting_time, function()
      hero:set_animation("punching", function()
        hero:set_animation"pointing"
      end)
      hero:start_spellcasting(casting_time, function()
        item:remove_ammo(magic_cost)
        item:shoot_bolt()
      end)
    end)

  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)

function item:shoot_bolt()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()

  sol.audio.play_sound("enemies/rat_big_02")
  sol.audio.play_sound("bird_flapping")
  local projectile = map:create_custom_entity{
    x=x, y=y, layer=z, width = 8, height = 8, direction = 0,
    model = "hero_projectiles/general",
    sprite = "items/vampire_bat",
  }
  projectile.non_rotational = true
  projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
  projectile.damage_type = damage_type
  projectile.speed = 80
  projectile:start_seeking(56)
  projectile.on_hit_enemy = function(projectile, enemy)
    hero:add_life(projectile.damage / 2)
  end
  local aim_angle = (direction * math.pi/2)
  projectile:shoot(aim_angle)
  sol.timer.start(projectile, 350, function() --start flying faster after flapping a second
    sol.audio.play_sound("bird_flapping_short")
    local m = projectile:get_movement()
    if not m or (m:get_speed() <= 0) then return end
    local sprite = projectile:get_sprite()
    sprite:set_animation("diving")
    sprite:set_direction(m:get_direction4())
    m:set_speed(300)
  end)
  sol.timer.start(map, 200, function()
    hero:unfreeze()
    item:set_finished()
  end)
end
