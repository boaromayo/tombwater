local item = ...
local game = item:get_game()

local magic_cost = 30
local casting_time = 150
local num_bursts = 3
local range = 32
local spread = math.rad(80) --in deg
local base_damage = 45
local damage_type = "magic"
local delay_min, delay_max = 50,150

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_silver_firecracker")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("punching", function()
      hero:set_animation"pointing"
    end)
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:create_bursts()
    end)
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)


function item:create_bursts()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2

  for i = 1, num_bursts do
    local da = (angle - spread / 2) + ((spread / num_bursts) * i)
    local dx = x + range * math.cos(da)
    local dy = ( y + range * math.sin(da) * -1 )
    sol.timer.start(map, math.random(delay_min, delay_max) * (i - 1), function()
      sol.audio.play_sound"spells/silver_explosion"
      local burst = map:create_custom_entity{
        x=dx, y=dy, layer=z, width = 8, height = 8, direction = 0,
        model = "damaging_entity",
        sprite = "hero_projectiles/silver_burst",
      }
      burst.damage = base_damage
      burst.damage_type = damage_type
    end)
  end

  sol.timer.start(hero, 100, function()
    hero:unfreeze()
    item:set_finished()
  end)
end

