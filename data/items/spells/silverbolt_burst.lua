local item = ...
local game = item:get_game()

local magic_cost = 55
local casting_time = 150
local range = 400
local base_damage = 20
local num_bolts = 5
local delay_min, delay_max = 100,200
local angle_variance = 16

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_silverbolt_burst")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("punching", function()
      hero:set_animation"pointing"
    end)
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:shoot_bolts()
    end)
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)


function item:shoot_bolts()
  local hero = game:get_hero()
  local bolts_shot = 0
  sol.timer.start(hero, 0, function()
    item:shoot_bolt()
    bolts_shot = bolts_shot + 1
    if bolts_shot < num_bolts then
      return math.random(delay_min, delay_max)
    else
      hero:unfreeze()
      item:set_finished()
    end
  end)
end


function item:shoot_bolt()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()

  sol.audio.play_sound("spells/arcane_bolt")
  local projectile = map:create_custom_entity{
    x=x, y=y, layer=z, width = 8, height = 8, direction = 0,
    model = "hero_projectiles/general",
    sprite = "hero_projectiles/arcane_bolt",
  }
  projectile.damage = game:calculate_hero_output_damage(base_damage, "mind")
  projectile.damage_type = "magic"
  projectile.speed = 250
  projectile.tracking_accuracy = 8
  projectile:start_seeking()
  local aim_angle = direction * (math.pi / 2) + math.rad(math.random(angle_variance * -1, angle_variance))
  projectile:shoot(aim_angle)
end


function item:choose_target(bolt)
  local hero = game:get_hero()
  local map = game:get_map()
  local aim_angle = hero:get_direction() * math.pi / 2
  local x, y, z = hero:get_position()
  local possible_enemies = {}
  local target_enemy = nil
  for e in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
    if e:get_type() == "enemy" then
      possible_enemies[e] = e
    end
  end
  for enemy in pairs(possible_enemies) do
    if hero:has_los(enemy) then
      local enemy_angle = hero:get_angle(enemy)
      local angle_diff = math.abs(aim_angle - enemy_angle)
      if angle_diff > math.pi then --impossible for difference to be more than 180, must be counting the long way around
        angle_diff = math.abs(enemy_angle - aim_angle)
      end
      enemy.spell_arrow_score = (angle_diff * 100) + hero:get_distance(enemy) * 2
      if (not target_enemy) or (target_enemy.spell_arrow_score > enemy.spell_arrow_score) then
        if (angle_diff < math.rad(100)) and (enemy:get_life() > 0) then --if it's more than 90 off, the enemy isn't remotely in front of us, also skip if enemy is already dead
          target_enemy = enemy
        end
      end
    end
  end
  return target_enemy
end
