local item = ...
local game = item:get_game()

local magic_cost = 33
local base_damage = 80
local damage_type = "magic"
local casting_time = 600
local mouth_radius = 32

function item:on_started()
  item:set_savegame_variable("possession_chomp")
  item:set_assignable(true)
  item:set_ammo("_madness")
end

local function bite(chomper)
  local map = game:get_map()
  chomper:set_drawn_in_y_order(true)
  local sprite = chomper:get_sprite()
  sol.audio.play_sound("spells/maw_chomp")
  sprite:set_animation("biting", function()
    chomper:set_drawn_in_y_order(false)
    sprite:set_animation("receeding", function()
      chomper:remove()
    end)
  end)
  for other_entity in map:get_entities_in_rectangle(chomper:get_bounding_box()) do
    if chomper:get_distance(other_entity) < mouth_radius then
      if other_entity:get_type() == "enemy" then
        --other_entity:process_hit(chomper.damage, damage_type)
        --Hurt enemies after a brief pause (pause so the mouth can close around them)
        other_entity:stagger(100)
        sol.timer.start(chomper, 100, function()
          other_entity:process_hit(chomper.damage or base_damage, damage_type)
        end)
      end
    end
  end
end

function item:on_using()
  local map = game:get_map()
  local hero = game:get_hero()
  --Chomp any chompers currently on the map to prevent stacking them up:
  for entity in map:get_entities("chomp_spell_chomper") do
    bite(entity)
  end

  if item:can_spend_ammo(magic_cost) then --check if you could cast the spell, but don't spend the ammo until you're done charging/casting
    hero:set_animation("kneeling")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:cast()
      item:set_finished()
      hero:unfreeze()
    end)
  else --not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end


function item:cast()
  local hero = game:get_hero()
  local map = game:get_map()
  local x, y, z = hero:get_position()

  local chomper = map:create_custom_entity{
    name = "chomp_spell_chomper",
    x = x,
    y = y,
    layer = z,
    direction = 0,
    width = mouth_radius,
    height = mouth_radius,
    sprite = "items/chomper"
  }

  chomper.damage = game:calculate_hero_output_damage(base_damage, "mind")

  local sprite = chomper:get_sprite()
  sol.audio.play_sound("spells/maw_appear")
  sprite:set_animation("setting", function()
    sprite:set_animation "set"
    chomper:add_collision_test("origin", function(chomper, other_entity)
      if other_entity:get_type() == "enemy" then
        chomper:clear_collision_tests()
        bite(chomper)
      end
    end)
  end)
end

