local item = ...
local game = item:get_game()

local magic_cost = 25
local casting_time = 200
local base_damage = 20
local max_distance = 120
local top_speed = 500
local min_speed = 100
local acceleration, accel_step = 7, 10

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_wagon_wheel")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local wheel = item:create_wheel()
    sol.audio.play_sound("throw")
    hero:set_animation("throwing")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      hero:unfreeze()
      wheel:fly(hero:get_direction() * math.pi / 2)
    end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()  
    hero:unfreeze()  
  end
end)


function item:create_wheel()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  x = x - game:dx(16)[direction]
  y = y - game:dy(16)[direction]
  local wheel = map:create_custom_entity{
    x = x, y = y, layer = z,
    direction = 0, height = 16, width = 16,
    model = "damaging_entity",
    sprite = "items/wagon_wheel_projectile",
  }
  wheel:get_sprite():set_animation"magic"
  wheel.duration = 25000
  wheel.damage = game:calculate_hero_output_damage(base_damage, "mind")
  wheel:set_can_traverse("teletransporter", true)
  wheel:set_can_traverse_ground("shallow_water", true)
  wheel:set_can_traverse_ground("deep_water", true)
  wheel:set_can_traverse_ground("hole", true)
  wheel:set_can_traverse_ground("lava", true)
  wheel:set_can_traverse_ground("low_wall", true)
  wheel:activate_collision()


  function wheel:return_to_hero(starting_speed)
    starting_speed = starting_speed or min_speed
    local m = sol.movement.create"straight"
    m:set_angle(wheel:get_angle(hero))
    m:set_ignore_obstacles(true)
    m:set_speed(starting_speed)
    m:start(wheel)
    --Acceleration
    sol.timer.start(wheel, accel_step, function()
      m:set_speed(m:get_speed() + acceleration)
      if m:get_speed() < top_speed then return true end
    end)
  end

  function wheel:fly(angle)
    local m = sol.movement.create"straight"
    m:set_max_distance(max_distance)
    m:set_speed(top_speed)
    m:set_ignore_obstacles(true)
    m:set_angle(angle)
    m:start(wheel, function()
      wheel:return_to_hero()
    end)
    function m:on_obstacle_reached()
      sol.audio.play_sound"impact_wood"
      wheel:return_to_hero(m:get_speed())
    end
    --Acceleration
    sol.timer.start(wheel, accel_step, function()
      m:set_speed(m:get_speed() - acceleration)
      if m:get_speed() > min_speed then
        return true
      else
        wheel:return_to_hero()
      end
    end)
  end

  function wheel:react_to_solforge_weapon()
    sol.audio.play_sound"impact_wood_2"
    wheel:fly(hero:get_angle(wheel))
  end

  --Particle effects on wheel:
  local em = game:get_map():create_particle_emitter(x, y, z)
  em.target = wheel
  em:set_preset("sparks")
  em:emit()

  return wheel
end
