local item = ...
local game = item:get_game()

local magic_cost = 65
local casting_time = 300
local postcast_time = 300 --the spell starts, but you're still locked in the animation
local base_damage = 20
local damage_type = "magic"
local spell_duration = 7000
local average_delay = 500
local delay_variance = 200
local spell_radius = 56

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_yearning_sea")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")
    local x, y, z = hero:get_position()
    portal_entity = map:create_custom_entity{
      x=x, y=y+4, layer=z, direction=0, width=16, height=16, sprite = "items/spell_circle_silver",
    }
    portal_entity:get_sprite():set_color_modulation{255,100,150}

    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      portal_entity:remove()
      item:start_summoning()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_summoning()
  local map = item:get_map()
  local spell_active = true
  --start spell, stop after a while
  sol.timer.start(map, spell_duration, function()
    spell_active = false
  end)
  --Create attack if active:
  sol.timer.start(map, 0, function()
    item:create_attack()
    if spell_active then
      return math.max(10, average_delay + math.random(delay_variance * -1, delay_variance))
    end
  end)
end


function item:create_attack()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  x = x + math.random(spell_radius * -1, spell_radius)
  y = y + math.random(spell_radius * -1, spell_radius)
  sol.audio.play_sound("ichor_burst_ground")
  local attack = map:create_custom_entity{
    x = x, y = y, layer = z,
    width = 8, height = 8, direction = 0,
    sprite = "items/tentacle_puddle_portal",
    model = "damaging_entity",
  }
  attack.start_without_collision = true
  attack.duration = 5000
  attack.damage = game:calculate_hero_output_damage(base_damage, "mind")
  attack.damage_type = damage_type
  local attack_sprite = attack:get_sprite()
  if math.random(0, 1) == 1 then attack_sprite:set_scale(-1, 1) end
  attack_sprite:set_animation("opening", function()
    attack:activate_collision()
    sol.audio.play_sound("weapons/blunt_02")
    attack_sprite:set_animation("attacking", function()
      attack_sprite:set_animation("retreating", function()
        attack_sprite:set_animation("closing", function()
          attack:remove()
        end)
      end)
    end)
  end)
end
