local item = ...
local game = item:get_game()

local magic_cost = 95
local base_damage = 20
local damage_type = "bullet"
local scaling_stat = "mind"
local casting_time = 500
local shoot_frequency = 1200
local num_ghosts = 5
local range = 100
local aim_delay = 400

function item:on_started()
  item:set_savegame_variable("possession_spell_bounty")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local hero = game:get_hero()
  
  if item:can_spend_ammo(magic_cost) then
  hero:set_animation("punch_windup")
  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    item:cast()
    hero:set_animation("punching", function()
      hero:set_animation("stopped")
      item:set_finished()
      hero:unfreeze()
    end)
  end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end


function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2

  sol.audio.play_sound("spells/fireball")
  --[[
  --print("hey ay", x, y, z, "     and     ", hero:get_position())
  local projectile = map:create_custom_entity{
    x = x, y = x, layer = z, width = 8, height = 8, direction = 0,
    sprite = "hero_projectiles/black_fireball",
    model = "hero_projectiles/general",
  }
  projectile.damage = 1
  projectile.obstacle_collision_callback = function(other_entity)
    --print("hit ob")
    if other_entity and other_entity:get_type() == "enemy" then
      item:mark_enemy(other_entity)
    end
  end

  --print("proj shoot is", projectile.shoot, projectile:get_model())
  projectile:shoot(angle)
  --]]
  local projectile = map:create_custom_entity{
    x = x, y = y, layer = z, width = 8, height = 8, direction = 0,
    sprite = "hero_projectiles/black_fireball",
  }
  projectile:set_drawn_in_y_order(true)
  projectile:add_collision_test("sprite", function(projectile, other)
    if other:get_type() == "enemy" then
      sol.audio.play_sound("spells/bounty_mark")
      item:mark_enemy(other)
      projectile:remove()
    end
  end)
  projectile:get_sprite():set_rotation(angle)
  local m = sol.movement.create("straight")
  m:set_angle(angle)
  m:set_speed(200)
  sol.timer.start(map,100,function() --little delay to line up with punching animation better ;-)
    m:start(projectile)
  end)
  function m:on_obstacle_reached()
    projectile:remove()
  end
end


function item:mark_enemy(enemy)
  local map = game:get_map()
  enemy:process_hit(1)

  for i = 1, num_ghosts do

    sol.timer.start(map, shoot_frequency * i, function()
      if not enemy or (not enemy:exists()) or enemy:get_life() <= 0 then
        return
      end
      local x, y, z = enemy:get_position()
      local wraith = map:create_custom_entity{
        x = x + math.random(range * -1, range),
        y = y + math.random(range * -1, range),
        layer = z,
        width = 16, height = 16, direction = 0,
        sprite = "hero/emberwraith",
      }
      local sprite = wraith:get_sprite()
      sol.audio.play_sound("spells/wraith_appear")
      sprite:set_animation("appearing", function()
        sprite:set_animation("aiming")
      end)
      sol.timer.start(wraith, aim_delay, function()
        local wx, wy, wz = wraith:get_position()
        local projectile = map:create_custom_entity{
          x = wx, y = wy, layer = wz, width = 8, height = 8, direction = 0,
          sprite = "hero_projectiles/wraith_bullet",
          model = "hero_projectiles/bullet",
        }
        projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
        projectile.damage_type = damage_type
        projectile.is_piercing = true
        projectile:shoot(wraith:get_angle(enemy))
        sol.audio.play_sound("gunshot_pistol")
        sol.audio.play_sound("spells/fireball")
        sol.timer.start(wraith, 250, function()
          sprite:set_animation("disappearing", function() wraith:remove() end)
        end)
      end)
    end)

  end
end
