local item = ...
local game = item:get_game()

local flies = {}

local magic_cost = 33
local num_flies = 3
local base_damage = 30
local damage_type = "magic"
local casting_time = 1000
local post_casting_vomit_time = 500
local duration = 10000

function item:on_started()
  item:set_savegame_variable("possession_vile_flies")
  item:set_assignable(true)
  item:set_ammo("_madness")
end

function item:on_using()
  local map = game:get_map()
  local hero = game:get_hero()

  -- if any circling flies exist, shoot one of them off
  local circling_flies_count = #flies
  if circling_flies_count > 0 and flies[circling_flies_count]:exists() then
    item:shoot_fly(flies[circling_flies_count], hero, map)
    flies[circling_flies_count] = nil
  else
    --check if you could cast the spell, but don't spend the ammo until you're done charging/casting
    if item:can_spend_ammo(magic_cost) then 
      hero:set_animation("vomiting")
      sol.audio.play_sound("spells/vomit")
      hero:start_spellcasting(casting_time, function()
        item:remove_ammo(magic_cost)
        item:summon_flies(map,hero)
        --Vomit just a little longer after creating the flies
        --hero:set_animation("vomiting")
        sol.timer.start(hero, post_casting_vomit_time, function()
          hero:unfreeze()
          item:set_finished()
        end)
      end)
    else --not enough magic
      sol.audio.play_sound("wrong")
      item:set_finished()
    end
  end
end

function item:shoot_fly(fly, hero, map)
  local closest_enemy = nil
  local max_distance = 150
  local closest_distance = 100000

  sol.audio.play_sound("spells/fly_shoot")
  for other_entity in map:get_entities() do
    if other_entity:get_type() == "enemy" then
      if other_entity:get_distance(hero) < closest_distance then
        closest_distance = other_entity:get_distance(hero)
        closest_enemy = other_entity
      end
    end
  end
  if closest_enemy == nil or closest_enemy:get_distance(hero) > max_distance then
    --move the fly straight out
    local m = sol.movement.create("target")
    m:set_target(hero)
    m:set_ignore_obstacles(true)
    m:set_speed(500)
    m:start(fly, function()
      local facing_angle = (hero:get_direction() * math.pi / 2) + 2 * math.pi
      local m2 = sol.movement.create("straight")
      m2:set_speed(500)
      m2:set_angle(facing_angle)
      m2:set_max_distance(150)
      m2:set_ignore_obstacles(true)
      m2:start(fly, function()
        fly:remove_sprite()
        local poof_sprite = fly:create_sprite("enemies/enemy_killed_projectile")
        function poof_sprite:on_animation_finished()
          fly:remove()
        end
      end)
    end)    
  else
    local m = sol.movement.create("target")
    m:set_target(closest_enemy)
    m:set_speed(500)
    m:start(fly, function()
      --has to be here in case enemy dies while a fly is en route
      fly:remove_sprite()
      local poof_sprite = fly:create_sprite("enemies/enemy_killed_projectile")
      function poof_sprite:on_animation_finished()
        fly:remove()
      end
    end)
  end
  item:set_finished()
end

function item:summon_flies(map,hero)
  local fly_generation_delay = 300
  local x,y,z = hero:get_position()
  sol.audio.play_sound("spells/flies")
  -- set flies array to zero in case our stored value is out of sync
  flies = {}
  -- create flies and have them move to the outer circle limit
  for i = 1, num_flies do
    local vile_fly = map:create_custom_entity({
      name = "vile_flies_fly",
      x = x, 
      y = y,
      layer = z,
      width=8, height=8, direction = 0,
      sprite = "items/vile_fly"
    })
    vile_fly:set_drawn_in_y_order(true)
    --vile_fly:set_can_traverse_ground("wall", true)
    vile_fly:add_collision_test("sprite", function(vile_fly, other_entity)
      if other_entity:get_type() == "enemy" then
        vile_fly:clear_collision_tests()
        other_entity:process_hit(base_damage, damage_type)
        vile_fly:remove()
        flies[i] = nil
      end
    end)
    flies[i] = vile_fly

    --start a timer that will remove the fly after a period of time
    sol.timer.start(vile_fly, duration, function()
      vile_fly:remove_sprite()
      local poof_sprite = vile_fly:create_sprite("enemies/enemy_killed_projectile")
      function poof_sprite:on_animation_finished()
        vile_fly:remove()
        flies[i] = nil
      end
    end)

    local m = sol.movement.create("circle")
    local angle = ((math.pi * 2) / num_flies) * i
    m:set_center(hero)
    m:set_radius(1)
    m:set_radius_speed(10)
    m:set_radius(32)
    m:set_angle_from_center(angle)
    m:set_ignore_obstacles(true)
    m:start(vile_fly)
  end
end