local item = ...
local game = item:get_game()

local magic_cost = 100
local casting_time = 300
local postcast_time = 300 --the spell starts, but you're still locked in the animation
local base_damage = 1
local damage_type = "magic"
local scaling_stat = "mind"
local duration = 10000
local heal_frequency = 100
local spell_radius = 44

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_holy_ground")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")
    local x, y, z = hero:get_position()
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_summoning()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_summoning()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  sol.audio.play_sound("spells/silver_summon")
  local circle = map:create_custom_entity{
    x=x, y=y, layer=z, width = 16, height = 16, direction = 0,
    sprite = "items/healing_circle",
  }
  local sprite = circle:get_sprite()
  sprite:set_animation("appearing", function()
    sprite:set_animation"glowing"
  end)
  local elapsed_time = 0
  sol.timer.start(map, heal_frequency, function()
    elapsed_time = elapsed_time + heal_frequency
    if hero:get_distance(circle) <= spell_radius then
      local adjusted_amount = game:calculate_hero_output_damage(base_damage, scaling_stat)
      game:add_life(adjusted_amount)
    end
    if elapsed_time < duration then
      return true
    else
      sprite:fade_out()
      sol.timer.start(circle, 1000, function() circle:remove() end)
    end
  end)
end

