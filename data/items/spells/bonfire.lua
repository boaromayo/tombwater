local item = ...
local game = item:get_game()

local magic_cost = 90
local casting_time = 500
local base_damage = 50
local scaling_stat = "mind"
local init_num_beams = 6
local beam_num_scaling = .5
local init_radius = 16
local radius_step = 24
local wave_delay = 300
local max_waves = 4

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_bonfire")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")

    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_sunbeam_waves()
      sol.timer.start(hero, 200, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


local active_sounds = {}
local function play_sound(sound_id)
  if not active_sounds[sound_id] then
    sol.audio.play_sound(sound_id)
    active_sounds[sound_id] = true
    sol.timer.start(sol.main, 50, function() active_sounds[sound_id] = nil end)
  end
end


function item:start_sunbeam_waves()
  local map = game:get_map()
  local x, y, z = game:get_hero():get_position()
  local waves_cast = 0
  sol.timer.start(map, 0, function()
    waves_cast = waves_cast + 1
    item:create_sunbeams(init_radius + waves_cast * radius_step, x, y, z, waves_cast)
    if waves_cast < max_waves then
      return wave_delay
    end
  end)
  local flame_beam = map:create_custom_entity{
    x=x, y=y, layer=z, width=16, height=16, direction=0,
    sprite = "items/flame_pillar",
    model = "damaging_entity",
  }
  flame_beam:get_sprite():set_scale(1, 2)
end


function item:create_sunbeams(radius, hx, hy, hz, wave)
  local map = game:get_map()
  radius = radius or init_radius
  local map = item:get_map()
  local hero = game:get_hero()
  if not hx or not hy or not hz then
    hx, hy, hz = hero:get_position()
  end
  local num_beams = (init_num_beams + init_num_beams * beam_num_scaling) * wave
  for i = 1, num_beams do
    local x = hx + math.cos(math.pi * 2 / num_beams * i) * radius
    local y = hy + math.sin(math.pi * 2 / num_beams * i) * radius
    local z = hz
    sol.timer.start(map, i * 20, function()
      item:create_sunbeam(x, y, z)
    end)
  end
end


function item:create_sunbeam(x, y, z)
  local map = item:get_map()
  play_sound("spells/fireball_big")
  local flame = map:create_fire{
    x=x, y=y, layer = z,
  }
  local pillar_sprite = flame:create_sprite("items/flame_pillar")
  pillar_sprite:set_animation("flame", function() flame:remove_sprite(pillar_sprite) end)
  flame.harmless_to_hero = true
  flame.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
end
