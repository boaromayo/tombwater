local item = ...
local game = item:get_game()

local magic_cost = 33
local spell_distance = 48
local spread = math.rad(45)
local casting_time = 120

local punch_damage = 5
local burst_damage = 20
local incubation_time = 5000
local heal_amount = 10

function item:on_started()
  item:set_savegame_variable("possession_leech_life")
  item:set_assignable(true)
  item:set_ammo("_madness")
end

-- Event called when the hero starts using this item.
function item:on_using()
  if item:can_spend_ammo(magic_cost) then
    local hero = game:get_hero()
    --throw a punch on the enemy, maybe does small damage
    hero:set_animation("punching", function()
      hero:unfreeze()
      item:set_finished()
    end)
    sol.audio.play_sound("weapons/blunt_01")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:find_enemies()
    end)
  else --not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end

function item:find_enemies()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()

  --Some particle effects to disguise the fact that the range on this is super weird:
  local dx, dy = game:dx(16)[direction], game:dy(16)[direction]
  local em = game:get_map():create_particle_emitter(x + dx, y + dy, z)
  em:set_preset("burst")
  em.particle_sprite = "effects/smoke_particle"
  em.particle_color = {40, 0, 20}
  em.particle_speed = 10
  em.angle_variance = math.rad(100)
  em.particles_per_loop = 20
  em:emit()

  --Check enemies by range
  for enemy in map:get_entities_in_rectangle(x - spell_distance, y - spell_distance, spell_distance * 2, spell_distance * 2) do
    local is_in_range = true
    if enemy:get_type() ~= 'enemy' then
      is_in_range = false
    end
    if enemy:get_distance(hero) > spell_distance then
      is_in_range = false      
    end
    if hero:get_direction4_to(enemy) ~= hero:get_direction() then
      is_in_range = false
    end
    if is_in_range then
      item:mark_enemy(enemy)
    end
  end
  --Check overlapping sprites:
  for enemy in map:get_entities_in_rectangle(x - spell_distance * 4, y - spell_distance * 4, spell_distance * 8, spell_distance * 8) do
    if enemy:get_type() == "enemy" and enemy:overlaps(hero, "sprite") then
      item:mark_enemy(enemy)
    end
  end
end


function item:mark_enemy(enemy)
  local map = game:get_map()
  enemy:process_hit(game:calculate_hero_output_damage(punch_damage, "strength"), "physical")
  sol.timer.start(map, incubation_time, function()
    if enemy:exists() then
      item:create_leech(enemy)
      enemy:process_hit(game:calculate_hero_output_damage(burst_damage, "mind"), "magic")
      enemy:stagger(1200)
    end
  end)
end


function item:create_leech(enemy)
  local hero = game:get_hero()
  local map = game:get_map()
  local x,y,z = enemy:get_position()
  local leech = map:create_custom_entity({
    name = "leech",
    x = x,
    y = y,
    layer = z,
    width=8, height=8, direction = 0,
    sprite = "items/leech"
  })
  
  leech:add_collision_test("sprite", function(leech, other_entity)
    if other_entity:get_type() == "hero" then
      leech:clear_collision_tests()
      game:add_life(game:calculate_hero_output_damage(burst_damage,"mind") * .5)
      sol.audio.play_sound("enemies/slime_03")
      sol.audio.play_sound"power_up_low"
      leech:remove()
    end
  end)
  local m = sol.movement.create("target")
  m:set_target(hero)
  m:start(leech)  
end