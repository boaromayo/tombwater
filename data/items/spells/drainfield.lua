local item = ...
local game = item:get_game()

local magic_cost = 70
local casting_time = 300
local postcast_time = 600 --the spell starts, but you're still locked in the animation
local base_damage = 1
local damage_type = "magic"
local scaling_stat = "mind"
local duration = 20000
local drain_frequency = 200
local spell_radius = 48

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_drainfield")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")
    local x, y, z = hero:get_position()
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


local function get_emitter(x, y, z)
  local map = item:get_map()
  local em = map:create_particle_emitter(x, y, z)
  em:set_preset("burst")
  em.angle_variance = math.rad(10)
  em.particles_per_loop = 1
  em.particle_opacity = {100,180}
  return em
end


function item:start_attack()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()

  sol.audio.play_sound"spells/folk_charge"
  sol.timer.start(map, 100, function()
    sol.audio.play_sound"enemies/moth_wings_long"
  end)

  local circle = map:create_custom_entity{
    x=x, y=y, layer=z, width = 16, height = 16, direction = 0,
    sprite = "items/vermin_circle",
  }
  local sprite = circle:get_sprite()
  sprite:set_animation("appearing")
  local elapsed_time = 0
  --Loop, check for enemies in field:
  sol.timer.start(map, drain_frequency, function()
    elapsed_time = elapsed_time + drain_frequency
    --Drain enemies:
    for ent in map:get_entities_in_rectangle(x - spell_radius, y - spell_radius, spell_radius * 2, spell_radius * 2) do
      local caught_one = false
      if ent:get_type() == "enemy" and ent:get_distance(circle) <= spell_radius then
        caught_one = true
        local em = get_emitter(ent:get_position())
        em.particle_color = {190, 30, 0}
        em:emit()
        game:add_life( game:calculate_hero_output_damage(base_damage, scaling_stat) )
      end
      if caught_one then
        get_emitter(hero:get_position()):emit()
        sol.audio.play_sound("spells/heal_blip")
      end
    end

    if elapsed_time < duration then
      return true
    else
      sprite:fade_out()
      sol.timer.start(circle, 1000, function() circle:remove() end)
    end
  end)

  --Butterflies for effect:
  for i = 1, math.random(3, 5) do
    local butterfly = map:create_custom_entity{
      x = x + math.random(-32, 32),
      y = y + math.random(-32, 32),
      layer = z,
      width = 8, height = 8, direction = math.random(0, 1),
      sprite = "animals/grave_butterfly",
    }
    butterfly:set_drawn_in_y_order(true)
    local m = sol.movement.create"random"
    m:set_speed(10)
    m:start(butterfly)

    sol.timer.start(butterfly, duration, function()
      butterfly:get_sprite():fade_out()
      sol.timer.start(butterfly, 300, function() butterfly:remove() end)
    end)
  end
end

