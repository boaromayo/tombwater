local item = ...
local game = item:get_game()

local magic_cost = 45
local casting_time = 400
local postcast_time = 100
local duration = 30000
local scaling_stat = "mind"
local scaling_amount = .65

function item:on_started()
  item:set_savegame_variable("possession_spell_buff_defense")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local hero = game:get_hero()
  
  if item:can_spend_ammo(magic_cost) then
  hero:set_animation("kneeling")
  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    item:cast()
    sol.audio.play_sound("spells/silver_bass_drop_short")
    sol.audio.play_sound("spells/silver_chime_01")
    sol.timer.start(hero, postcast_time, function()
      hero:set_animation("stopped")
      item:set_finished()
      hero:unfreeze()
    end)
  end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end


function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()

  local em = map:create_particle_emitter(x, y, z)
  em.target = hero
  em:set_preset("burst")
  em.angle_variance = math.rad(10)
  em.particles_per_loop = 20
  em:emit()

  game.spell_defense_multiplier = scaling_amount
  sol.timer.start(game, duration, function()
    game.spell_defense_multiplier = 1
  end)
end

