local item = ...
local game = item:get_game()

local magic_cost = 40
local base_damage = 10
local damage_type = "magic"
local scaling_stat = "mind"
local casting_time = 300
local spell_distance = 128
local max_push_distance = 120

function item:on_started()
  item:set_savegame_variable("possession_spell_ox_fist")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()
  
  local ox_head = map:create_custom_entity{
    x = x,
    y = y + 2,
    layer = z, width = 16, height = 16, direction = direction,
    sprite = "items/ox_fist"
  }
  local ox_sprite = ox_head:get_sprite()

  local collided_entities = {}  
  ox_head:add_collision_test("sprite", function(ox_head, other_entity)
    if other_entity:get_type() == "enemy" and not collided_entities[other_entity] then
      local other_entity_angle = hero:get_angle(other_entity)
      collided_entities[other_entity] = true
      other_entity:stagger(800)
      other_entity:process_hit(base_damage, damage_type)
      other_entity.lock_facing = true
      local m = sol.movement.create("straight")
      m:set_angle(other_entity_angle)
      m:set_speed(400)
      m:set_max_distance(max_push_distance)
      m:start(other_entity, function() other_entity.lock_facing = false end)
    end
  end)

  hero.weapon_entity = ox_head
  hero:dash()
  sol.timer.start(ox_head,800,function()
    ox_sprite:fade_out(10)
    sol.timer.start(ox_head, 200, function()
      ox_head:remove()
    end)
  end) 
 --create movement, all this may have to happen outside the cast so that it doesnt get immediately unfrozen
end

-- Event called when the hero starts using this item.
function item:on_using()
  local hero = game:get_hero()

  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("kneeling")
    sol.audio.play_sound("enemies/bull_snort")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:cast()
    end)
  else --not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end
