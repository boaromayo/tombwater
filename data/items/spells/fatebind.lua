local item = ...
local game = item:get_game()
local hero_meta = sol.main.get_metatable"hero"

local magic_cost = 80
local casting_time = 200
local postcast_time = 200

local base_damage = 60 --how much percent of damage taken is mirrored by affected enemies
local scaling_stat = "mind"
local range = 72
local duration = 60000

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_fatebind")
  item:set_assignable(true)
  item:set_ammo("_madness")

  item.bound_enemies = {}
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("raised_arm")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.audio.play_sound"spells/folk_charge"
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


hero_meta:register_event("on_taking_damage", function(hero, damage)
  if not item.bound_enemies then return end
  for _, enemy in pairs(item.bound_enemies) do
    if enemy:exists() then
      local adjusted_damage = damage * (game:calculate_hero_output_damage(base_damage, scaling_stat) / 100)
      if enemy.process_hit then
        enemy:process_hit(adjusted_damage, "magic")
      else
        enemy:hurt(adjusted_damage)
      end
    end
  end
end)


function item:start_attack()
  local map = game:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2
  item.bound_enemies = {}

  for ent in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
    if ent:get_type() == "enemy" and hero:get_distance(ent) <= range then
      local enemy = ent
      local x, y, z = enemy:get_position()
      local em = map:create_particle_emitter(x, y, z)
      em:set_preset("burst")
      em.target = ent
      em.angle_variance = 0
      em.particle_sprite = "effects/spark_particle"
      em.particles_per_loop = 20
      em.particle_fade_speed = 35
      em:emit()
      local width, height = enemy:get_sprite():get_size()
      local rune = map:create_custom_entity{
        x = x, y = y, layer = z, width = 16, height = 16, direction = 0,
        sprite = "items/rune_fatebind",
        model = "ephemeral_effect"
      }
      rune:get_sprite():set_xy(0, height * -1 + 8)

      table.insert(item.bound_enemies, ent)
    end
  end

  if item.bind_timer then
    item.bind_timer:stop()
  end
  item.bind_timer = sol.timer.start(map, duration, function()
    
  end)

end

