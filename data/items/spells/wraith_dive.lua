local item = ...
local game = item:get_game()

local magic_cost = 55
local casting_time = 10
local base_damage = 90
local damage_type = "magic"
local scaling_stat = "mind"
local shockwave_range = 128
local num_flames = 16

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_wraith_dive")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  if item:can_spend_ammo(magic_cost) then
    item:remove_ammo(magic_cost)
    item:dive()
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:dive()
  local map = game:get_map()
  local hero = game:get_hero()
  hero:set_direction(3)
  hero:set_invincible(true, 5000) --the 5s is just to make sure it doesn't somehow get stuck "on"
  sol.audio.play_sound("spells/wraith_charge")
  hero:set_animation("wraith_divebomb", function()
    hero:set_animation("wraith_divebomb_recovery", function()
      hero:set_invincible(false)
      hero:set_animation("stopped")
      hero:set_direction(3)
      item:set_finished()
    end)
    --sound and screenshake:
    sol.audio.play_sound("spells/fireball_big")
    sol.audio.play_sound("running_obstacle")
    map:screenshake()
    --shockwave and damage
    local x, y, z = hero:get_position()
    local shockwave = map:create_custom_entity({
      x = x, y = y - 2, layer = z, width = 16, height = 16, direction = 0,
      sprite = "items/shockwave_8x7",
      model = "damaging_entity",
    })
    shockwave.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    shockwave:get_sprite():set_scale(1.5, 1.5)
    --Flames
    for i = 1, num_flames do
      local flame = map:create_fire{
        x = x, y = y, layer = z,
        properties = { {key = "burn_duration", value = "10"} }
      }
      flame:remove_sprite()
      flame:create_sprite("elements/black_flame")
      local m = sol.movement.create"straight"
      m:set_speed(250)
      m:set_ignore_obstacles()
      m:set_angle(math.pi * 2 / num_flames * i)
      m:set_max_distance(shockwave_range * .5)
      m:start(flame)
    end
  end)
end



