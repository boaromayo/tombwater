local item = ...
local game = item:get_game()

local magic_cost = 95
local casting_time = 500
local base_damage = 55
local num_beams = 10
local init_radius = 48
local radius_step = 56
local wave_delay = 500
local max_waves = 3
local beam_fuse = 300

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_silverstorm")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")
    local x, y, z = hero:get_position()
    portal_entity = map:create_custom_entity{
      x=x, y=y+4, layer=z, direction=0, width=16, height=16, sprite = "items/spell_circle_silver",
    }
    --portal_entity:get_sprite():set_color_modulation{255,255,100}

    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      portal_entity:remove()
      item:start_sunbeam_waves()
      sol.timer.start(hero, 100, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_sunbeam_waves()
  local map = game:get_map()
  local x, y, z = game:get_hero():get_position()
  local waves_cast = 0
  sol.timer.start(map, 0, function()
    item:create_sunbeams(init_radius + waves_cast * radius_step, x, y, z)
    waves_cast = waves_cast + 1
    if waves_cast < max_waves then
      return wave_delay
    end
  end)
end


function item:create_sunbeams(radius, hx, hy, hz)
  local map = game:get_map()
  radius = radius or init_radius
  local map = item:get_map()
  local hero = game:get_hero()
  if not hx or not hy or not hz then
    hx, hy, hz = hero:get_position()
  end

  for i = 1, num_beams do
    local x = hx + math.cos(math.pi * 2 / num_beams * i) * radius
    local y = hy + math.sin(math.pi * 2 / num_beams * i) * radius
    local z = hz
    sol.timer.start(map, i * 20, function()
      item:create_sunbeam(x, y, z)
    end)
  end
end


local active_sounds = {}
local function play_sound(sound_id)
  if not active_sounds[sound_id] then
    sol.audio.play_sound(sound_id)
    active_sounds[sound_id] = true
    sol.timer.start(sol.main, 50, function() active_sounds[sound_id] = nil end)
  end
end


function item:create_sunbeam(x, y, z)
  local map = item:get_map()
  play_sound("fireball_2")
  local target = map:create_custom_entity{
    x=x, y=y, layer=z, width=16, height=16, direction=0,
    sprite = "items/spell_circle_silver",
  }
  sol.timer.start(target, beam_fuse, function()
    play_sound("spells/silver_beam_burst")
    local beam = map:create_custom_entity{
      x=x, y=y-8, layer=z,
      width = 32, height = 32, direction = 0,
      sprite = "items/silverbeam",
      model = "damaging_entity",
    }
    beam.damage = game:calculate_hero_output_damage(base_damage, "mind")
    beam:set_origin(16, 29)
    local sprite = beam:get_sprite()
    sol.timer.start(target, sprite:get_num_frames() * sprite:get_frame_delay(), function() target:remove() end)
  end)
end
