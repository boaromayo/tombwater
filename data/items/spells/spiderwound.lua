local item = ...
local game = item:get_game()
local hero_meta = sol.main.get_metatable"hero"

local magic_cost = 40
local casting_time = 300
local postcast_time = 150
local base_damage = 8
local scaling_stat = "mind"
local num_spiders = 3
local spider_duration = 7000

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_spiderwound")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("raised_arm")
    sol.audio.play_sound"spells/folk_charge_short"
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_attack()
  local hero = game:get_hero()
  local map = game:get_map()
  local x, y, z = hero:get_position()

  --Create spiders:
  for i = 1, num_spiders do
    local spider = map:create_custom_entity{
      x = x, y = y, layer = z,
      width = 16, height = 8, direction = 0,
      sprite = "items/vile_spider",
      model = "animals/familiar"
    }
    local sprite = spider:get_sprite()
    sprite:set_animation("falling", function()
      sprite:set_animation"walking"
    end)

    spider.is_spiderflesh_spider = true
    spider.damage_cooldown = 1500
    spider.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    spider.duration = spider_duration + 1000

    local m = sol.movement.create("straight")
    m:set_angle(math.rad(math.random(0,360)))
    m:set_max_distance(32)
    m:set_speed(250)
    m:start(spider, function()
      spider:start()
    end)
    m.on_obstacle_reached = spider.start

  end
end



