local item = ...
local game = item:get_game()
local hero_meta = sol.main.get_metatable"hero"

local magic_cost = 60
local casting_time = 150
local postcast_time = 150
local base_damage = 10
local scaling_stat = "mind"
local active_duration = 30000
local num_spiders = 1
local spider_duration = 7000

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_spiderflesh")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("raised_arm")
    sol.audio.play_sound"spells/folk_charge_short"
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


hero_meta:register_event("on_taking_damage", function(hero, damage)
  if not item.active then return end
  local map = hero:get_map()
  local x, y, z = hero:get_position()

  --Create spiders:
  for i = 1, num_spiders do
    local spider = map:create_custom_entity{
      x = x, y = y, layer = z,
      width = 16, height = 8, direction = 0,
      sprite = "items/vile_spider",
      model = "damaging_entity"
    }
    spider.is_spiderflesh_spider = true
    spider.damage_cooldown = 1500
    spider.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    spider.duration = spider_duration + 1000
    local sprite = spider:get_sprite()
    sprite:set_animation("falling", function()
      sprite:set_animation"walking"
    end)
    sol.timer.start(spider, spider_duration, function()
      sol.timer.stop_all(spider)
      local m = spider:get_movement()
      if m then m:stop() end
      sprite:fade_out()
      sol.timer.start(spider, 1000, function() spider:remove() end)
    end)

    --Don't tread on other spiders:
    spider:set_can_traverse("custom_entity", function(spider, other)
      if other.is_spiderflesh_spider then
        if spider:overlaps(other) then return true
        else return false end
      else
        return true
      end
    end)

    --Wander randomly:
    function spider:wander()
      if spider:get_distance(hero) >= 64 then
        local m = sol.movement.create"target"
        m:set_target(hero)
        m:set_speed(60)
        m:start(spider)
      else
        local m = sol.movement.create("random")
        m:start(spider)
      end
      sol.timer.start(spider, 1000, function()
        spider:find_enemy()
      end)
    end

    --Attack an enemy if nearby:
    function spider:find_enemy()
      local enemies = item:select_nearby_enemies(100, 4) --get 4 closest enemies
      local enemy = enemies[math.random(1, #enemies)]
      if not enemy then
        spider:wander()
        return
      end
      local m = sol.movement.create"target"
      m:set_target(enemy)
      m:set_speed(80)
      m:start(spider)

      function m:on_position_changed()
        local dist = spider:get_distance(enemy)
        if dist <= 6 then
          m:stop()
          sol.timer.start(spider, 1000, function()
            spider:find_enemy()
          end)
        end
      end
    end

    local m = sol.movement.create("straight")
    m:set_angle(math.rad(math.random(0,360)))
    m:set_max_distance(32)
    m:set_speed(250)
    m:start(spider, function()
      spider:find_enemy()
    end)
    m.on_obstacle_reached = spider.find_enemy

  end
end)


function item:start_attack()
  if item.spider_timer then item.spider_timer:stop() end
  item.active = true
  item.spider_timer = sol.timer.start(game, active_duration, function()
    item.active = false
  end)
  --Some particle effects so we know we've avtivated the spell
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local em = game:get_map():create_particle_emitter(x, y, z)
  em:set_preset("burst")
  em.particle_sprite = "effects/smoke_particle"
  em.particle_color = {40, 0, 20}
  em.particle_speed = 10
  em.angle_variance = math.rad(100)
  em.particles_per_loop = 20
  em:emit()
end



