local item = ...
local game = item:get_game()

local magic_cost = 40
local base_damage = 20
local damage_type = "bullet"
local scaling_stat = "mind"
local casting_time = 500
local shoot_frequency = 2000
local range = 200
local aim_delay = 250
local wraith_duration = 11000

function item:on_started()
  item:set_savegame_variable("possession_spell_wraith_turret")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local hero = game:get_hero()
  
  if item:can_spend_ammo(magic_cost) then
  hero:set_animation("kneeling")
  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    item:cast()
    sol.timer.start(hero, 100, function()
      hero:set_animation("stopped")
      item:set_finished()
      hero:unfreeze()
    end)
  end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end

function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2

  sol.audio.play_sound("spells/wraith_appear")
  local dx = x + game:dx(24)[direction]
  local dy = y + game:dy(24)[direction]
  local wraith = map:create_custom_entity{
    x = dx, y = dy, layer = z, width = 16, height = 16, direction = 0,
    sprite = "hero/emberwraith",
  }
  wraith:set_drawn_in_y_order(true)
  local wraith_sprite = wraith:get_sprite()
  wraith_sprite:set_animation("appearing", function()
    wraith_sprite:set_animation("stopped")
    wraith_sprite:set_direction(direction)
  end)
  local elapsed_time = 0

  local function shoot(aim_angle)
    local wx, wy = wraith:get_position()
    local projectile = map:create_custom_entity{
      x = wx, y = wy, layer = z, width = 8, height = 8, direction = 0,
      sprite = "hero_projectiles/wraith_bullet",
      model = "hero_projectiles/bullet",
    }
    projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    projectile.damage_type = damage_type
    projectile.is_piercing = true
    projectile:shoot(aim_angle)
    sol.audio.play_sound("gunshot_pistol")
    sol.timer.start(wraith, 250, function()
      wraith_sprite:set_animation"stopped"
    end)
  end

  local function check_for_target()
    local enemies = item:select_nearby_enemies(range, 1)
    local enemy = enemies[1]
    if not enemy then return end
    wraith_sprite:set_animation("aiming")
    wraith_sprite:set_direction(wraith:get_direction4_to(enemy))
    sol.timer.start(wraith, aim_delay, function()
      shoot(wraith:get_angle(enemy))
    end)
  end

  --Start checking for enemies:
  sol.timer.start(wraith, shoot_frequency, function()
    check_for_target()
    elapsed_time = elapsed_time + shoot_frequency
    if elapsed_time < wraith_duration then
      return shoot_frequency
    else
      sol.timer.stop_all(wraith)
      sol.audio.play_sound("spells/wraith_appear")
      wraith_sprite:set_direction(0)
      wraith_sprite:set_animation("disappearing", function() wraith:remove() end)
    end

  end)

end
