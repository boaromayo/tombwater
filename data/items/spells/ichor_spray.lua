local item = ...
local game = item:get_game()

local magic_cost = 50
local scaling_stat = "mind"
local damage_type = "magic"
local casting_time = 200
local base_damage = 30
local num_shots = 8
local shot_frequency = 200
local range = 104

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_ichor_spray")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("leaning_back")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:attack()
    end)
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)


function item:attack()
  local map = item:get_map()
  local hero = map:get_hero()

  sol.audio.play_sound("bugs_skittering")
  --Create spray animation:
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local spray = map:create_custom_entity{
    x=x, y=y, layer=z, direction=direction, width=16, height=16,
    sprite = "items/ichor_spray",
    model = "damaging_entity",
  }
  hero.weapon_entity = spray --this is using functionality from solforge weapons
  spray.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
  spray.damage_type = damage_type
  --Start projectiles:
  local blob_count = 0
  sol.timer.start(hero, 0, function()
    item:shoot_blob()
    blob_count = blob_count + 1
    if blob_count < num_shots then
      return shot_frequency
    else
      spray:remove()
      hero:set_animation"stopped"
      hero:unfreeze()
      item:set_finished()
    end
  end)
end


function item:shoot_blob()
  local map = game:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  local y_offset = -2
  if hero:get_direction() == 3 then y_offset = 0 end
  local aim_angle = hero:get_direction() * math.pi / 2
  sol.audio.play_sound("ichor_splash")
  local blob = map:create_custom_entity{
    x = x, y = y + y_offset, layer = z,
    direction=0, width=8, height=8,
    sprite = "entities/enemy_projectiles/ichor_blob",
    model = "hero_projectiles/general",
  }
  blob.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
  blob.damage_type = damage_type
  blob.speed = 160
  blob.range = range - math.random(0, 32)
  blob.non_rotational = true
  blob.obstacle_callback = function(projectile)
    local x, y, z = projectile:get_position()
    projectile:remove()
    map:create_custom_entity{
      x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
      model = "damaging_entity",
      sprite = "entities/enemy_projectiles/eldritch_thread_smoke",
    }
    --TODO: some particle effects on these?
  end
  blob:shoot(aim_angle + math.rad(math.random(-45, 45)))
end

