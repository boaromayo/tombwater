local item = ...
local game = item:get_game()

local casting_time = 400
local life_percent_cost = 25
local bullets_gained = 3

item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_blood_bullet")
  item:set_assignable(true)
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  local map = hero:get_map()
  local life = game:get_life()
  local max_life = game:get_max_life()
  local life_cost = max_life * life_percent_cost / 100

  if life <= life_cost then
    --Not enough life, this would kill you
    sol.audio.play_sound"wrong"
    item:set_finished()
    return
  end

  hero:set_animation("kneeling")
  hero:start_spellcasting(casting_time, function()
    sol.audio.play_sound"hero_hurt_small"
    hero:blood_splatter()
    hero:set_animation("flash", function()
      sol.audio.play_sound"power_up_low"
      game:remove_life(life_cost)
      game:add_bullets(bullets_gained)
      hero:unfreeze()
      item:set_finished()
    end)
  end)

end)


