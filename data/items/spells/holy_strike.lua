local item = ...
local game = item:get_game()

local magic_cost = 25
local windup_time = 210
local base_damage = 30
local scaling_stat = "mind"
local damage_type = "magic"
local delay_min, delay_max = 50,150

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_holy_strike")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    item:remove_ammo(magic_cost)
    item:attack()
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)


function item:attack()
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  if direction == 1 then y = y - 1 end
  sol.audio.play_sound("spells/silver_chime_01")
  local attack = map:create_custom_entity{
    x=x, y=y, layer=z, direction=direction,
    width=16, height=16,
    sprite = "solforge_weapons/liturgical_staff_phantom",
    model = "damaging_entity",
  }
  hero.weapon_entity = attack
  attack.duration = 1000
  attack.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
  attack.damage_type = "magic"
  attack.damage_cooldown = 1000
  function attack:collision_callback(enemy)
    game:add_life(attack.damage * .35)
  end
  local attack_sprite = attack:get_sprite()
  attack_sprite:set_animation("thrust_windup")
  hero:set_animation("thrust_windup")
  sol.timer.start(hero, windup_time, function()
    sol.audio.play_sound("weapons/blunt_08")
    hero:set_animation("thrust_attack", function() hero:set_animation"stopped" end)
    attack_sprite:set_animation("thrust_attack", function()
      attack:remove()
      item:set_finished()
    end)
  end)

  --particles
  sol.timer.start(attack, 200, function()
    local em = map:create_particle_emitter(x + game:dx(24)[direction], y + game:dy(24)[direction], z)
    em:set_preset("silver_magic")
    em.duration = 250
    em:emit()
  end)
end

