local item = ...
local game = item:get_game()

local magic_cost = 13
local casting_time = 50
local base_damage = 25
local recovery_delay = 200

function item:on_started()
  item:set_savegame_variable("possession_spell_tentacle_lash")
  item:set_assignable(true)
  item:set_ammo("_madness")
end
 

function item:on_using()
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = game:get_map()
    hero:freeze()
    hero:set_animation("punch_windup")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      sol.audio.play_sound("spells/tentacle_lash")
      hero:set_animation("punching", function()
        --Make the hero recover for a short time after casting
        hero:set_animation"stopped"
        sol.timer.start(hero, recovery_delay, function()
          hero:unfreeze()
          item:set_finished()
        end)
      end)
    end)
    sol.timer.start(map, 100, function()
      sol.audio.play_sound("fireball")
      local x, y, z = hero:get_position()
      local direction = hero:get_direction()
      if direction == 1 then y = y - 1 end
      local lash = map:create_custom_entity{
        x=x, y=y, layer=z, direction=direction,
        width=16, height=16,
        sprite = "enemies/tombwater_enemies/weapons/chest_tentacles",
        model = "damaging_entity",
      }
      lash.damage = game:calculate_hero_output_damage(base_damage, "mind")
      lash.damage_type = "magic"
      lash.damage_cooldown = 1000
      --adjust sprite offsets, the sprite is meant to line up with a big enemy
      local sox = {8, 0, -8, 0}
      local soy = {8, 2, 8, 16}
      lash:get_sprite():set_xy(sox[direction + 1], soy[direction + 1])
    end)
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
