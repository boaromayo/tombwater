local item = ...
local game = item:get_game()

local magic_cost = 33
local base_damage = 0
local damage_type = "magic"
local scaling_stat = "mind"
local casting_time = 100
local spell_distance = 128
local max_push_distance = 128

function item:on_started()
  item:set_savegame_variable("possession_spell_howl")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local hero = game:get_hero()
  
  if item:can_spend_ammo(magic_cost) then
  hero:set_animation("kneeling") --should be howl or whatever
  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    item:cast()
    sol.audio.play_sound"spells/ethereal_howl_2"
    sol.audio.play_sound"bass_drop_1s"
    hero:set_animation("howling")
    sol.timer.start(hero, 200, function()
      hero:set_animation("stopped")
      item:set_finished()
      hero:unfreeze()
    end)
  end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end

function item:cast()
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()

  local shockwave = map:create_custom_entity{
    name = "shockwave",
    x = x,
    y = y,
    layer = z,
    direction = 0,
    width = 8,
    height = 8
  }


  local shockwave_sprite = shockwave:create_sprite("items/shockwave_8x7")
  function shockwave_sprite:on_animation_finished()
    shockwave:remove()
  end

  for enemy in map:get_entities_in_rectangle(x - spell_distance, y - spell_distance, spell_distance * 2, spell_distance * 2) do
    local is_in_range = true
    local distance = enemy:get_distance(hero)
    if enemy:get_type() ~= 'enemy' then
      is_in_range = false
    end
    if distance > spell_distance then
      is_in_range = false      
    end
    if is_in_range then
      enemy:stagger(800)
      enemy.lock_facing = true
      local enemy_angle = hero:get_angle(enemy) + (2 * math.pi)
      local push_distance = (max_push_distance - distance) / ((enemy.weight or 10) * .1)
      local m = sol.movement.create("straight")
      m:set_angle(enemy_angle)
      m:set_speed(400)
      m:set_max_distance(push_distance)
      m:start(enemy, function() enemy.lock_facing = false end)
      function m:on_obstacle_reached() enemy.lock_facing = false end
    end
  end
end
