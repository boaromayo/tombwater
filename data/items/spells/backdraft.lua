local item = ...
local game = item:get_game()

local magic_cost = 25
local casting_time = 500
local base_damage = 20 --note, it will hit a few times
local speed = 290
local max_distance = 168

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_backdraft")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  if not item:can_spend_ammo(magic_cost) then
    item:set_finished()
    return
  end

  local map = item:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  hero:set_animation("punch_windup")
  local x, y, z = hero:get_position()

  hero:start_spellcasting(casting_time, function()
    item:remove_ammo(magic_cost)
    hero:set_animation("punching", function()
      hero:set_animation"stopped"
      item:set_finished()
      hero:unfreeze()
    end)
  end)

  sol.timer.start(hero, casting_time - 350, function() --delay to match animation
    item:create_attacks()
  end)
end)


function item:create_attacks()
  local map = item:get_map()
  local hero = game:get_hero()
  local hx, hy, z = hero:get_position()
  local direction = hero:get_direction()
  local offset = 24

  local x = hx + game:dx(offset)[direction]
  local y = hy + game:dy(offset)[direction]
  item:create_sunbeam(x, y, z)
end


function item:create_sunbeam(x, y, z)
  local map = item:get_map()
  local hero = map:get_hero()
  local num_flames = 4
  local flame_spread = math.rad(90)
  local offset = 24

  local beam = map:create_custom_entity{
    x=x, y=y, layer=z,
    width = 32, height = 32, direction = 0,
    sprite = "items/flame_pillar",
    model = "damaging_entity",
  }
  beam.start_without_collision = true
  beam.damage = game:calculate_hero_output_damage(base_damage, "mind")
  beam:set_origin(16, 29)
  local sprite = beam:get_sprite()

  --Create flame when pillar burns out
  sol.timer.start(map, sprite:get_num_frames() * sprite:get_frame_delay(), function()
    local flame = map:create_fire({x=x, y=y, layer=z})
    flame.harmless_to_hero = true
  end)

  --Create backdraft flames when pillar explodes
  local frame_delay = sprite:get_frame_delay()
  sol.timer.start(map, 5 * frame_delay, function()
    --Scale sprite for a sec
    sprite:set_scale(1.5, 1.5)
    sol.timer.start(map, 70, function() sprite:set_scale(1, 1) end)
    sol.audio.play_sound"fire_light"
    beam:activate_collision()

    local direction = hero:get_direction()
    local m = sol.movement.create"straight"
    m:set_angle(direction * math.pi / 2)
    m:set_speed(90)
    m:set_max_distance(8)
    m:start(hero)

    local hx, hy, hz = hero:get_position()
    sol.audio.play_sound"fireball_2"
    for i = 1, num_flames do
      local fx = hx -- + game:dx(offset)[direction]
      local fy = hy -- + game:dy(offset)[direction] 
      local flame = map:create_fire({x=fx, y=fy, layer=z})
      flame.harmless_to_hero = true
      local m = sol.movement.create"straight"
      local angle = (direction * math.pi / 2 + math.pi) - flame_spread / 2 + flame_spread / num_flames * i
      m:set_angle(angle)
      m:set_speed(180)
      m:set_max_distance(32)
      m:start(flame)
    end
  end)
end

