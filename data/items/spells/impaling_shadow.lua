local item = ...
local game = item:get_game()

local magic_cost = 20
local casting_time = 10
local base_damage = 30
local damage_type = "magic"
local scaling_stat = "mind"

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_impaling_shadow")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  if item:can_spend_ammo(magic_cost) then
    item:remove_ammo(magic_cost)
    item:dive()
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:dive()
  local map = game:get_map()
  local hero = game:get_hero()
  local hm = hero:get_movement()
  local angle = (hm and hm:get_speed() > 0) and hm:get_angle() or hero:get_direction() * math.pi / 2
  hero:set_invincible(true, 5000) --the 5s is just to make sure it doesn't somehow get stuck "on"
  hero:set_animation("dash_emberwraith")
  local m = sol.movement.create("straight")
  m:set_max_distance(72)
  m:set_speed(350)
  m:set_angle(angle)

  local x, y, z = hero:get_position()
  local attack_entity = map:create_custom_entity{
    x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
    sprite = "items/impaling_thrust_collision_mask",
    model = "damaging_entity",
  }
  hero.weapon_entity = attack_entity --piggyback off solforge functions to move with hero and remove if hurt, etc.
  attack_entity.duration = 5000 --5s to make sure it doesn't get stuck "on"
  attack_entity.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)

  local function get_emitter()
    local x, y, z = hero:get_position()
    local em = map:create_particle_emitter(x, y, z)
    em:set_preset("burst")
    em.particle_sprite = "effects/black_flame"
    em.particle_animation_loops = false
    em.particle_opacity = {200,250}
    em.particles_per_loop = 10
    em.angle_variance = math.pi / 4
    return em
  end

  local function do_fx()
    sol.audio.play_sound("spells/fireball_short")
    local em = get_emitter()
    em:emit()
  end

  local function end_dash()
    attack_entity:remove()
    hero.weapon_entity = nil
    hero:set_animation"stopped"
    item:set_finished()
    do_fx()
  end

  do_fx()
  m:start(hero, function()
    end_dash()
  end)
  function m:on_obstacle_reached()
    end_dash()
  end
  local em = get_emitter()
  em.duration = 500
  em.particles_per_loop = 1
  em.particle_speed = 50
  em.angle_variance = math.rad(5)
  em.target = hero
  em:emit()
end



