

local item = ...
local game = item:get_game()

local casting_time = 500
local magic_cost = 33
local base_damage = 25
local tombstone_duration = 2500
local max_range = 200
local target_speed = 200
local spawn_frequency = 80
local path_delta_major, path_delta_minor = 24, 8 --how far away from the target entity the tombstones might spawn
local spawnable_grounds = {
  traversable = true,
  shallow_water = true,
  grass = true,
}

function item:on_started()
  item:set_savegame_variable("possession_tombstone")
  item:set_assignable(true)
  item:set_ammo("_madness")
end


function item:on_using()
  local map = game:get_map()
  local hero = game:get_hero()

  if item:can_spend_ammo(magic_cost) then --check if you could cast the spell, but don't spend the ammo until you're done charging/casting
    hero:set_animation("kneeling")
  sol.audio.play_sound("spells/folk_charge_crows")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:cast()
      item:set_finished()
      hero:unfreeze()
    end)
  else --not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end


function item:cast()
  --create an invisible entity that moves
  --every period of time create a new entity that damages enemies when they collide
  local map = game:get_map()
  local hero = game:get_hero()
  local x,y,z = hero:get_position()
  local damage = game:calculate_hero_output_damage(base_damage, "mind")

  sol.timer.start(map, 1000, function()
    sol.audio.play_sound("crows_far")
  end)

  --doesnt have to be shockwave, but if this spell is charged up some kind of visual flair would be nice
  local shockwave = map:create_custom_entity{
    name = "shockwave", x = x, y = y, layer = z,
    direction = 0, width = 8, height = 8
  }
  local shockwave_sprite = shockwave:create_sprite("items/shockwave_8x7")
  function shockwave_sprite:on_animation_finished()
    shockwave:remove()
  end

  --Create a target entity for tombstones to spawn near as it moves:
  local path = map:create_custom_entity{
    name = "path", x = x, y = y, layer = z,
    direction = 0, width = 8, height = 8,
  }
  local m = sol.movement.create("straight")
  local direction = hero:get_direction()
  m:set_max_distance(max_range)
  m:set_ignore_obstacles(true)
  m:set_speed(target_speed)
  m:set_angle(direction * math.pi / 2 + 2 * math.pi)
  function m:on_obstacle_reached()
    path:remove()
  end
  m:start(path, function()
    path:remove()
  end)


  --Create tombstones every few ms roughly along the target entity's path of movement:
  sol.timer.start(map, spawn_frequency, function()
    if path:exists() then
      if not spawnable_grounds[path:get_ground_below()] then return spawn_frequency end
      --Determine tombstone spawn location:
      local x2,y2,z2 = path:get_position()
      local major_delta = math.random(path_delta_major * -1, path_delta_major)
      local minor_delta = math.random(path_delta_minor * -1, path_delta_minor)
      local dx = {minor_delta, major_delta, minor_delta,major_delta}
      local dy = {major_delta, minor_delta, major_delta, minor_delta}
      x2 = x2 + dx[direction + 1]
      y2 = y2 + dy[direction + 1]
      --Check for wall
      if map:get_ground(x2, y2, z2) == "wall" then
        return spawn_frequency
      end
      sol.audio.play_sound("spells/ground_burst_soft_" .. math.random(1, 7))
      local tombstone = map:create_custom_entity{
        name = "tombstone",
        x = x2, 
        y = y2,
        layer = z2,
        direction = 0, width = 8, height = 8,
        sprite = "items/tombstone"
      }
      tombstone:set_drawn_in_y_order(true)
      tombstone:set_traversable_by(function(tombstone, other)
        return tombstone:overlaps(other) --let other entities pass through the tombstone if they're already caught in it, otherwise it's impassible
      end)

      local collided_entities = {}
      tombstone:add_collision_test("sprite", function(tombstone, other_entity)
        if other_entity:get_type() == "enemy" and not collided_entities[other_entity] then
          other_entity:process_hit(damage, "mind")
          collided_entities[other_entity] = true
        end
      end)
      
      local sprite = tombstone:get_sprite()
      sprite:set_direction(math.random(0, sprite:get_num_directions() - 1))
      sprite:set_animation("emerge", function()
        sprite:set_animation("idle")
        tombstone:clear_collision_tests()
        sol.timer.start(tombstone, tombstone_duration, function()
          sprite:set_animation("descend", function()
            tombstone:remove()
          end) 
        end)
      end)
      return spawn_frequency
    end
  end)
end


