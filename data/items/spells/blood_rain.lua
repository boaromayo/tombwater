local item = ...
local game = item:get_game()

local magic_cost = 80
local casting_time = 300
local postcast_time = 300 --the spell starts, but you're still locked in the animation
local base_damage = 20
local damage_type = "magic"
local rain_duration = 7000
local min_drop_delay, max_drop_delay = 20, 400
local drop_delay_step = 10
local rain_radius = 200 --distance around hero in which it rains

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_blood_rain")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("kneeling")
    local x, y, z = hero:get_position()
    portal_entity = map:create_custom_entity{
      x=x, y=y+4, layer=z, direction=0, width=16, height=16, sprite = "items/spell_circle_silver",
    }
    portal_entity:get_sprite():set_color_modulation{255,100,150}

    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      portal_entity:remove()
      item:start_raining()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_raining()
  local map = item:get_map()
  local raining = true
  local drop_delay = max_drop_delay
  --Sound effects:
  item:start_sfx()
  --start raining, stop after a while
  sol.timer.start(map, rain_duration, function()
    raining = false
    item:stop_raining()
  end)
  --increase rain frequency gradually:
  sol.timer.start(map, 50, function()
    if drop_delay > min_drop_delay then
      drop_delay = drop_delay - drop_delay_step
      return true
    end
  end)
  --Make raindrop if raining:
  sol.timer.start(map, 0, function()
    item:create_raindrop()
    item:single_drop_sfx()
    if raining then
      return drop_delay
    end
  end)
  sol.timer.start(map, 100, function()
    item:create_raindrop()
    if raining then
      return drop_delay
    end
  end)--]]
end


function item:stop_raining()
  local map = item:get_map()
  local drop_delay = min_drop_delay
  sol.timer.start(map, 0, function()
    drop_delay = drop_delay + drop_delay_step * 2
    item:single_drop_sfx()
    item:create_raindrop()
    if drop_delay < max_drop_delay then
      return drop_delay
    end
  end)
end


function item:create_raindrop()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  x = x + math.random(rain_radius * -1, rain_radius)
  y = y + math.random(rain_radius * -1, rain_radius)
  local drop = map:create_custom_entity{
    x = x, y = y, layer = z,
    width = 8, height = 8, direction = 0,
    sprite = "items/blood_rain",
    model = "damaging_entity",
  }
  drop.damage = game:calculate_hero_output_damage(base_damage, "mind")
  drop.damage_type = damage_type
  sol.timer.start(drop, 250, function()
    local splash = map:create_custom_entity{
      x = x, y = y, layer = z,
      width = 8, height = 8, direction = 0,
      sprite = "items/blood_rain",
    }
    splash:get_sprite():set_animation("splash", function() splash:remove() end)
  end)
end


function item:start_sfx()
  local map = game:get_map()
  item.each_drop_sfx = true
  sol.audio.play_sound("spells/raindrop_" .. math.random(1,3))
  --Each drop make a sound at the start:
  sol.timer.start(map, 2000, function()
    item.each_drop_sfx = false
  end)
  --Full duration sfx:
  sol.audio.play_sound("spells/blood_rain")
  --Each drop makes a sound at the end:
  sol.timer.start(map, 7800, function()
    item.each_drop_sfx = true
  end)
end


function item:single_drop_sfx()
  --Play single sound if frequency is low enough
  if item.each_drop_sfx then
    sol.audio.play_sound("spells/raindrop_" .. math.random(1,8))
  end
end

