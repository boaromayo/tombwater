local item = ...
local game = item:get_game()

local magic_cost = 33
local base_damage = 10
local damage_type = "magic"
local casting_time = 800
local duration = 20000
local is_active = false


function item:on_started()
  item:set_savegame_variable("spell_cactus_skin")
  item:set_assignable(true)
  item:set_ammo("_madness")
  
  game.charm_manager:subscribe("on_hurt", function()
    if is_active then
      local hero = game:get_hero()
      local map = game:get_map()
      local x, y, z = hero:get_position()
      local thorns = map:create_custom_entity({
        x = x, y = y, layer = z, width = 64, height = 64, direction = 0,
        sprite = "items/cactus_skin",
        model = "damaging_entity",
      })
      thorns.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    end    
  end)
end

function item:on_using()
  local map = game:get_map()
  local hero = game:get_hero()

  if item:can_spend_ammo(magic_cost) then --check if you could cast the spell, but don't spend the ammo until you're done charging/casting
    hero:set_animation("raised_arm") -- max mentioned a hand over head
    sol.audio.play_sound"power_up_low"
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:cast()
      item:set_finished()
      hero:unfreeze()
    end)
  else --not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end


function item:cast()
  if item.active_timer then item.active_timer:stop() end
  is_active = true

  item.active_timer = sol.timer.start(item, duration, function()
     is_active = false 
  end)

  sol.audio.play_sound("razorgrass")
  --Particle effects to show it's been activated
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local em = game:get_map():create_particle_emitter(x, y, z)
  em.target = hero
  em:set_preset("burst")
  em.particle_sprite = "effects/smoke_particle"
  em.particle_color = {220, 230, 240}
  em.angle_variance = math.rad(10)
  em.particles_per_loop = 20
  em:emit()
end

