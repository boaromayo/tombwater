local item = ...
local game = item:get_game()

local magic_cost = 70
local casting_time = 200
local postcast_time = 400
local base_damage = 15 --is multiplied by 1.5 when they divebomb
local scaling_stat = "mind"
local num_ravens = 3
local raven_frequency = 600
local circle_range = 200
local circle_duration = 3000
local attack_range = 350
local fly_sprite_offset = 32

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_ravencloud")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("kneeling")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:create_raven()
  local map = item:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()

  --Some particle effects so we know we've avtivated the spell
  local em = game:get_map():create_particle_emitter(x, y - 6, z)
  em:set_preset("burst")
  em.particle_sprite = "effects/crow_feather"
  em.particle_speed = 10
  em.angle = 3 * math.pi / 2
  em.angle_variance = math.rad(140)
  em.particles_per_loop = 5
  em:emit()

  local raven = map:create_custom_entity{
    x = x, y = y, layer = z,
    width = 16, height = 8, direction = 0,
    sprite = "items/raven",
    model = "damaging_entity"
  }

  raven.duration = circle_duration * 3
  raven.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)

  local sprite = raven:get_sprite()
  local shadow_sprite = raven:create_sprite("items/raven", "shadow_sprite")
  shadow_sprite:set_animation("shadow")
  shadow_sprite.harmless = true


  function raven:fly_up()
    local max = fly_sprite_offset
    local i = 0
    sol.timer.start(raven, 0, function()
      i = i + 1
      sprite:set_xy(0, i * -1)
      if i < max then return 60 end
    end)
  end

  function raven:fly_down()
    local i = fly_sprite_offset
    sol.timer.start(raven, 0, function()
      i = i - 1
      sprite:set_xy(0, i * -1)
      if i > 0 then return 10 end
    end)
  end

  --Face movement direction:
  function raven:on_position_changed()
    local m = raven:get_movement()
    if not m then return end
    if m.get_radius then --circle movement
      local angle = m:get_angle_from_center()
      local facing_angle = angle + (m:is_clockwise() and (math.pi / 2 * -1) or math.pi / 2 )
      sprite:set_direction(sol.main.get_direction8(facing_angle))
    elseif m.get_angle then
      local dir8 = sol.main.get_direction8(m:get_angle())
      sprite:set_direction(dir8)
    else
      local dir4 = m:get_direction4()
      sprite:set_direction(dir4 * 2)
    end
  end

  function raven:fade_away()
    sprite:set_direction(0)
    sprite:set_animation("stopped")
    sprite:fade_out()
    sol.timer.start(raven, 500, function() raven:remove() end)
  end

  --Attack an enemy if nearby:
  function raven:find_enemy()
    local enemies = item:select_nearby_enemies(attack_range, 4) --get 4 closest enemies
    --local enemy = enemies[math.random(1, #enemies)]
    local enemy = enemies[1]
    if not enemy then
      raven:fade_away()
    end
    raven:fly_down()
    raven.damage = raven.damage * 1.5
    local m = sol.movement.create"straight"
    m:set_angle(raven:get_angle(enemy))
    m:set_speed(300)
    m:set_ignore_obstacles(true)
    m:start(raven)
    sol.timer.start(raven, 1500, function()
      raven:fade_away()
    end)
  end

  --Start swirling_around
  local m = sol.movement.create("circle")
  m.is_circle = true
  local angle = 0
  m:set_center(hero)
  m:set_radius(1)
  m:set_radius_speed(29)
  m:set_radius(circle_range)
  m:set_angular_speed(4)
  m:set_ignore_obstacles(true)
  m:start(raven)
  sol.timer.start(raven, circle_duration, function()
    raven:find_enemy()
  end)
  raven:fly_up()
end

function item:start_attack()
  local hero = game:get_hero()
  for i = 1, num_ravens do
    sol.timer.start(hero, raven_frequency * (i - 1), function()
      item:create_raven()
    end)
  end
  sol.audio.play_sound("spells/ravencloud")
end



