local item = ...
local game = item:get_game()

local magic_cost = 33
local casting_time = 500
local spell_distance = 128
local spread = math.rad(45)
local stagger_time = 4000

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("possession_grasp")
  item:set_assignable(true)
  item:set_ammo("_madness")
end

function item:on_using()
  local hero = game:get_hero()

  if item:can_spend_ammo(magic_cost) then
    
    hero:set_animation('kneeling')
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:cast()
      item:set_finished()
      hero:unfreeze()    
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished()
  end
end

function item:cast()
  local hero = game:get_hero()
  local map = game:get_map()
  local x,y,z = hero:get_position()

  for enemy in map:get_entities_in_rectangle(x - spell_distance, y - spell_distance, spell_distance * 2, spell_distance * 2) do
    local is_in_range = true

    if enemy:get_type() ~= 'enemy' then
      is_in_range = false
    end
    if enemy:get_distance(hero) > spell_distance then
      is_in_range = false      
    end
    local facing_angle = (hero:get_direction() * math.pi / 2) + 2 * math.pi
    local enemy_angle = hero:get_angle(enemy) + 2 * math.pi
    if enemy_angle < facing_angle - spread or enemy_angle > facing_angle + spread then
      is_in_range = false
    end
    if is_in_range then
      enemy:stagger(stagger_time)
    end
  end
end


