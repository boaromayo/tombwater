local item = ...
local game = item:get_game()

local magic_cost = 80
local casting_time = 250
local postcast_time = 400
local base_damage = 30
local scaling_stat = "mind"
local delay = 75
local range = 200
local base_offset = 24
local num_flames = 14

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_firebreak")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)



item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("raised_arm")
    sol.audio.play_sound"spells/wraith_charge_short"
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_attack()
  local map = game:get_map()
  local hero = map:get_hero()
  local x, y, z = hero:get_position()
  local angle = hero:get_direction() * math.pi / 2

  for i = 1, num_flames do
    local dist = (range / num_flames) * i
    local dx = x + math.cos(angle) * dist + math.random(-6, 6)
    local dy = y + math.sin(angle) * dist * -1 + math.random(-6, 6)
    sol.timer.start(map, delay * i, function()
      local beam = map:create_custom_entity{
        x = dx, y = dy, layer = z, width = 16, height = 16, direction = 0,
        model = "damaging_entity",
        sprite = "elements/black_flame",
      }
      if beam:test_obstacles(0, 0) then
        beam:remove()
        return
      end
      sol.audio.play_sound"spells/fireball"
      beam.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
      beam:get_sprite():set_animation("beaming_down", function()
        beam:remove()
        local flame = map:create_fire{x=dx, y = dy, layer = z, properties = {{key = "burn_duration", value = "60"}}}
        flame:remove_sprite()
        flame:create_sprite("elements/black_flame")
        flame.harmless_to_hero = true
      end)
    end)
  end

end
