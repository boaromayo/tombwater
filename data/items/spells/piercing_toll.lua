local item = ...
local game = item:get_game()

local casting_time = 500
local magic_cost = 75
local base_damage = 10
local damage_type = "magic"
local duration = 10000
local spell_distance = 150

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("possession_piercing_toll")
  item:set_assignable(true)
  item:set_ammo("_madness")
end

-- Event called when the hero starts using this item.
function item:on_using()
  local hero = game:get_hero()
  local map = game:get_map()

  if item:can_spend_ammo(magic_cost) and  not map:has_entities("tolling_bell") then
    hero:set_animation("kneeling")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:summon_bell()
      hero:unfreeze()
    end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()  
  end
end

function item:summon_bell()
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  x = x + game:dx(25)[direction]
  y = y + game:dy(25)[direction]

  sol.audio.play_sound("spells/ground_burst_soft_6")
  sol.audio.play_sound("spells/ground_burst_soft_3")

  local tolling_bell = map:create_custom_entity({
    name = "tolling_bell",
    x = x,
    y= y,
    layer = z,
    width=24, height=8, direction = 0,
    sprite = "items/tolling_bell"
  })
  tolling_bell:set_origin(12, 5)
  tolling_bell:set_drawn_in_y_order(true)
  tolling_bell:set_traversable_by(function(tolling_bell, other)
    return tolling_bell:overlaps(other) --let other entities pass through the bell if they're already caught in it, otherwise it's impassible
  end)
  local sprite = tolling_bell:get_sprite()
  sprite:set_animation("emerge", function()
    sprite:set_animation("idle")
  end)

  sol.timer.start(tolling_bell, duration, function()
    sol.timer.stop_all(tolling_bell)
    sol.audio.play_sound("spells/ground_burst_soft_6")
    sol.audio.play_sound("spells/ground_burst_soft_3")
    tolling_bell.is_descending = true
    sprite:set_animation("descend", function()
      tolling_bell:remove()
    end)
  end)

  function tolling_bell:get_rung()
    if not tolling_bell.is_ringing and not tolling_bell.is_descending then
      tolling_bell.is_ringing = true
      sol.audio.play_sound("church_bell_1")
      sprite:set_animation("ringing")
      sol.timer.start(tolling_bell, 1000, function()
        tolling_bell.is_ringing = false
        sprite:set_animation("idle")
      end)

      local x,y,z = tolling_bell:get_position()
      for enemy in map:get_entities_in_rectangle(x - spell_distance, y - spell_distance, spell_distance * 2, spell_distance * 2) do
        local is_in_range = true
        if enemy:get_type() ~= 'enemy' then
          is_in_range = false
        end
        if enemy:get_distance(tolling_bell) > spell_distance then
          is_in_range = false      
        end
        if is_in_range then
          enemy:process_hit(game:calculate_hero_output_damage(base_damage,"mind"), damage_type)
        end
      end
      map:create_custom_entity{
        x = x, y = y, layer = z,
        width = 16, height = 16, direction = 16,
        model = "ephemeral_effect",
        sprite = "items/shockwave_8x7",
      }
    end
  end

  function tolling_bell:react_to_solforge_weapon()
    tolling_bell:get_rung()
  end

  function tolling_bell:react_to_hero_projectile()
    tolling_bell:get_rung()
  end

  function tolling_bell:process_hit()
    tolling_bell:get_rung()
  end
end