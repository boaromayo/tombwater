local item = ...
local game = item:get_game()

local magic_cost = 10
local base_damage = 20

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spark")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local hero = game:get_hero()
    hero:set_animation("punching", function()
      item:set_finished()
      hero:unfreeze()
    end)
    hero:start_spellcasting(200, function()
      item:remove_ammo(magic_cost)
      local x, y, z = hero:get_position()
      local direction = hero:get_direction()
      x = x + game:dx(50)[direction]
      y = y + game:dy(50)[direction]
      local zap = map:create_lightning{x=x, y=y, layer=z, lightning_type="lightning_zap"}
      zap.damage = game:calculate_hero_output_damage(base_damage, "mind")
      sol.audio.play_sound("electric_zap_small")
    end)
  else
    sol.audio.play_sound("wrong")
    item:set_finished()    
  end
end)

