local item = ...
local game = item:get_game()

local magic_cost = 55
local casting_time = 200
local range = 400
local base_damage = 75

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_worm_cannon")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("punch_windup")
    sol.timer.start(hero, casting_time, function()
      hero:set_animation("punching", function()
        hero:set_animation"pointing"
      end)
      hero:start_spellcasting(casting_time, function()
        item:remove_ammo(magic_cost)
        item:shoot_bolt()
      end)
    end)

  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)

function item:shoot_bolt()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()

  sol.audio.play_sound("ichor_splash")
  sol.audio.play_sound("bass_drop_half")
  sol.audio.play_sound("ichor_burst")
  sol.audio.play_sound("spells/fireball")
  local projectile = map:create_custom_entity{
    x=x, y=y, layer=z, width = 8, height = 8, direction = 0,
    model = "hero_projectiles/general",
    sprite = "entities/enemy_projectiles/worm_cannon",
  }
  projectile:get_sprite():set_xy(0, -8)
  projectile.damage = game:calculate_hero_output_damage(base_damage, "mind")
  projectile.damage_type = "magic"
  projectile.speed = 250
  projectile:start_seeking(24)
  local aim_angle = (direction * math.pi/2)
  projectile:shoot(aim_angle)
  sol.timer.start(map, 200, function()
    hero:unfreeze()
    item:set_finished()
  end)
end
