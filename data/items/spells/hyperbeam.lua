local item = ...
local game = item:get_game()

local magic_cost = 50
local casting_time = 500
local base_damage = 25
local beam_duration = 2000

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_hyperbeam")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    local map = item:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    hero:set_animation("jumping_up")

    --Rune circle:
    portal_entity = map:create_custom_entity{
      x=x, y=y+4, layer=z, direction=0, width=16, height=16, sprite = "items/spell_circle_silver",
    }
    local em = map:create_particle_emitter(x,y,z)
    em:set_preset("burst")
    em.particle_sprite = "effects/particle_line_vertical"
    em.duration = casting_time
    em.particles_per_loop = 3
    em.angle_variance = 0
    em:emit()

    sol.audio.play_sound("spells/silver_summon")

    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      portal_entity:remove()
      hero:set_animation("punching", function()
        hero:set_animation"pointing"
      end)
      sol.audio.play_sound("spells/silver_explosion")
      sol.audio.play_sound("spells/silver_bass_drop")
      sol.audio.play_sound("spells/silver_beam_hum_flash")

      local offset = 12
      local hbx = x + game:dx(offset)[direction]
      local hby = y + game:dy(offset)[direction]

      local beam = map:create_custom_entity{
        x = hbx, y = hby - 8, layer = z,
        width = 16, height = 16, direction = 0,
        model = "damaging_entity",
        sprite = "items/hyper_beam",
      }
      hero.weapon_entity = beam --this is using functionality from solforge weapons
      beam.damage = game:calculate_hero_output_damage(base_damage, "mind")
      beam.duration = beam_duration
      local beam_sprite = beam:get_sprite()
      beam_sprite:set_scale(1.4, 1.2)
      beam_sprite:set_rotation(direction * math.pi / 2)

      sol.timer.start(hero, beam_duration, function()
        --Particle effects at the end:
        local beam_length = 160
        local x, y, z = hero:get_position()
        local em = game:get_map():create_particle_emitter(x + game:dx(beam_length / 2)[direction], y + game:dy(beam_length / 2)[direction], z)
        em:set_preset("burst")
        local w, h = beam_length, 16
        if (direction % 2) ~= 0 then w, h = 16, beam_length end
        em:set_size(w, h)
        em:set_origin(w / 2, h - 3)
        em.width, em.height = w, h
        em.particle_sprite = "effects/star_particle_small"
        em.particle_color = {220, 230, 240}
        em.angle_variance = math.rad(10)
        em.particles_per_loop = 40
        em:emit()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)