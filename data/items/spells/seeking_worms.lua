local item = ...
local game = item:get_game()

local magic_cost = 40
local scaling_stat = "mind"
local casting_time = 200
local range = 400
local base_damage = 30
local num_projectiles = 5

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_seeking_worms")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("pointing")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:shoot_bolts()
    end)
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)

function item:shoot_bolts()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local angle = direction * math.pi / 2
  local summon_offset = 24

  sol.audio.play_sound("ichor_burst_ground")

  for i = 1, num_projectiles do
    local adjusted_angle = angle + math.rad(math.random(-25, 25))
    local dx = x + summon_offset * math.cos(adjusted_angle + math.pi)
    local dy = y + summon_offset * math.sin(adjusted_angle + math.pi) * -1
    sol.timer.start(hero, math.random(50, 200) * (i - 1), function()
      local projectile = map:create_custom_entity{
        x=dx, y=dy, layer=z, width=8, height=8, direction=0,
        sprite = "items/worm_missile",
        model = "hero_projectiles/general",
      }
      local worm_sprite = projectile:get_sprite()
      worm_sprite:set_rotation(math.pi / 2)
      projectile.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
      projectile.damage_type = "magic"
      projectile.speed = 250
      projectile.tracking_accuracy = 8
      worm_sprite:set_animation("charging", function()
        sol.audio.play_sound("ichor_squish_0" .. math.random(1,3))
        worm_sprite:set_animation("shooting")
        local aim_angle = (direction* math.pi/2 + math.random(-.7, .7))
        projectile:start_seeking(48)
        projectile:shoot(aim_angle)
      end)
    end)
  end


  sol.timer.start(map, 200, function()
    hero:unfreeze()
    item:set_finished()
  end)
end




--[[
local item = ...
local game = item:get_game()

local magic_cost = 40
local casting_time = 200
local range = 400
local base_damage = 30
local num_projectiles = 5

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_seeking_worms")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("punch_windup")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:create_projectiles()
    end)
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)

function item:create_projectiles()
  local hero = game:get_hero()
  for i = 1, num_projectiles do
    sol.timer.start(hero, i * 50, function()
      item:create_projectile(i)
    end)
  end
  sol.timer.start(hero, 400, function()
    hero:unfreeze()
    item:set_finished()
  end)
end


function item:create_projectile(i)
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local init_angle = 2 * math.pi / num_projectiles * i

  sol.audio.play_sound("spells/vomit")
  local init_worm = map:create_custom_entity{
    x=x, y=y, layer=z, width=8, height=8, direction=0,
    sprite = "items/worm_missile",
  }
  local worm_sprite = init_worm:get_sprite()
  worm_sprite:set_rotation(init_angle)
  local m = sol.movement.create"straight"
  m:set_angle(init_angle + math.pi)
  m:set_max_distance(32)
  m:set_speed(120)
  m:set_ignore_obstacles(true)
  m:start(init_worm)

  worm_sprite:set_animation("charging", function()
    local projectile = map:create_custom_entity{
      x=x, y=y, layer=z, width = 8, height = 8, direction = 0,
      model = "hero_projectiles/general",
      sprite = "items/worm_missile",
    }
    projectile:get_sprite():set_animation("shooting")
    --projectile:set_origin(4, 5)
    --projectile:get_sprite():set_scale(1.3, 1.3)
    projectile.damage = game:calculate_hero_output_damage(base_damage, "mind")
    projectile.damage_type = "magic"
    projectile.speed = 250
    projectile:start_seeking(48)
    local target_enemy = item:choose_target(init_angle)
    local aim_angle = target_enemy and hero:get_angle(target_enemy) or init_angle
    projectile:shoot(aim_angle)
    if target_enemy then
      projectile.tracking_accuracy = 8
      projectile:track_target(target_enemy)
    end
  end)
end


function item:choose_target(aim_angle)
  local hero = game:get_hero()
  local map = game:get_map()
  aim_angle = aim_angle or (hero:get_direction() * math.pi / 2)
  local x, y, z = hero:get_position()
  local possible_enemies = {}
  local target_enemy = nil
  for e in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
    if e:get_type() == "enemy" then
      possible_enemies[e] = e
    end
  end
  for enemy in pairs(possible_enemies) do
    if hero:has_los(enemy) then
      local enemy_angle = hero:get_angle(enemy)
      local angle_diff = math.abs(aim_angle - enemy_angle)
      if angle_diff > math.pi then --impossible for difference to be more than 180, must be counting the long way around
        angle_diff = math.abs(enemy_angle - aim_angle)
      end
      enemy.spell_arrow_score = (angle_diff * 100) + hero:get_distance(enemy) * 2
      if (not target_enemy) or (target_enemy.spell_arrow_score > enemy.spell_arrow_score) then
        if (angle_diff < math.rad(100)) and (enemy:get_life() > 0) then --if it's more than 90 off, the enemy isn't remotely in front of us, also skip if enemy is already dead
          target_enemy = enemy
        end
      end
    end
  end
  return target_enemy
end
--]]
