local item = ...
local game = item:get_game()

local magic_cost = 35
local scaling_stat = "mind"
local casting_time = 500
local range = 80
local duration = 6000
local base_damage = 2
local status_amount = 12
local damage_type = "magic"
local scaling_stat = "mind"
local num_projectiles = 10

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_moth_swarm")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)

item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("kneeling")
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:shoot()
    end)
  else -- not enough magic
    sol.audio.play_sound("wrong")
    item:set_finished()
  end
end)

function item:shoot()
  local hero = game:get_hero()
  local map = item:get_map()
  local x, y, z = hero:get_position()
  local damage = game:calculate_hero_output_damage(base_damage, scaling_stat)

  sol.audio.play_sound"enemies/moth_wings_long"
  sol.timer.start(map, 2000, function()
    sol.audio.play_sound"enemies/moth_wings_long"
  end)

  for i = 1, num_projectiles do
    sol.timer.start(hero, 200 * i, function()
      local projectile = map:create_custom_entity({
        x = x, 
        y = y,
        layer = z,
        width=8, height=8, direction = 0,
        sprite = "animals/moth"
      })
      projectile:set_drawn_in_y_order(true)
      projectile:set_can_traverse_ground("wall", true)
      projectile.collided_entities = {}
      projectile:add_collision_test("sprite", function(projectile, other_entity)
        if other_entity:get_type() == "enemy" and not projectile.collided_entities[other_entity] then
          --other_entity:process_hit(base_damage, damage_type)
          other_entity:remove_life(base_damage)
          other_entity:blood_splatter(5)
          sol.audio.play_sound"razorgrass"
          other_entity:build_up_status_effect("poison", status_amount)
          projectile.collided_entities[other_entity] = true
          sol.timer.start(projectile, 500, function()
            projectile.collided_entities[other_entity] = nil
          end)
        end
      end)

      sol.timer.start(projectile, duration, function()
        projectile:get_sprite():fade_out()
        sol.timer.start(projectile, 500, function() projectile:remove() end)
      end)

      local m = sol.movement.create("circle")
      local angle = ((math.pi * 2) / num_projectiles) * i
      m:set_center(hero)
      m:set_radius(1)
      m:set_radius_speed(20 + math.random(-2, 6))
      m:set_radius(range)
      m:set_angular_speed(math.random(2,5))
      m:set_ignore_obstacles(true)
      m:start(projectile)
      sol.timer.start(projectile, 300, function()
        m:set_radius(math.random(4, range))
        m:set_angular_speed(math.random(2,5))
        return math.random(200,500)
      end)
    end)
  end


  sol.timer.start(map, 200, function()
    hero:unfreeze()
    item:set_finished()
  end)
end



