local item = ...
local game = item:get_game()
local hero_meta = sol.main.get_metatable"hero"

local magic_cost = 75
local casting_time = 300
local postcast_time = 150
local base_damage = 12
local scaling_stat = "mind"
local num_familiars = 4
local familiar_duration = 10000

-- Event called when the game is initialized.
item:register_event("on_started", function(self)
  item:set_savegame_variable("possession_spell_rats")
  item:set_assignable(true)
  item:set_ammo("_madness")
end)


item:register_event("on_using", function(self)
  local hero = game:get_hero()
  if item:can_spend_ammo(magic_cost) then
    hero:set_animation("raised_arm")
    sol.audio.play_sound"spells/folk_charge_short"
    hero:start_spellcasting(casting_time, function()
      item:remove_ammo(magic_cost)
      item:start_attack()
      sol.timer.start(hero, postcast_time, function()
        hero:unfreeze()
        item:set_finished()
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    item:set_finished() 
  end
end)


function item:start_attack()
  local hero = game:get_hero()
  local map = game:get_map()
  local x, y, z = hero:get_position()

  --Create rats:
  sol.audio.play_sound("enemies/rat_04")
  for i = 1, num_familiars do
    local rat = map:create_custom_entity{
      x = x, y = y, layer = z,
      width = 16, height = 8, direction = 0,
      sprite = "items/spellrat",
      model = "animals/familiar"
    }
    local sprite = rat:get_sprite()
    rat:set_visible(false)
    sol.timer.start(rat, math.random(20,250), function()
      rat:set_visible(true)
      sprite:set_animation("burrowing_up", function()
        sprite:set_animation"stopped"
        rat:start()
      end)
    end)

    rat.target_enemy_speed = 100
    rat.damage_cooldown = 1800
    rat.damage = game:calculate_hero_output_damage(base_damage, scaling_stat)
    rat.duration = familiar_duration

    --This little movement is just a lil' hacky way to make them spawn around you, hopefully movement is so fast you can't see it
    local m = sol.movement.create("straight")
    m:set_angle(math.rad(math.random(0,360)))
    m:set_max_distance(24)
    m:set_speed(900)
    m:start(rat)

    function rat:react_to_fire(fire)
      local entity = rat
      if entity.on_fire then return end
      entity.on_fire = true
      local fire_sprite = entity:create_sprite("elements/smolder")
      entity.idle_speed = 150
      entity.follow_hero_speed = 150
      entity.target_enemy_speed = 150
      --entity.damage = entity.damage * 1.5
      entity.collision_callback = function(other)
        if other.react_to_fire then other:react_to_fire() end
      end
      sol.timer.start(entity, 1000, function()
        local x, y, z = entity:get_position()
        map:create_fire({x = x, y = y, layer = z})
        return true
      end)
    end
  end
end



