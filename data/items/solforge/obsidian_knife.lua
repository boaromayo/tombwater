local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16, 110)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/obsidian_knife",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/obsidian_knife",
        weapon_attack_animation = "backslash",
        weapon_sound = "sword1",
      },
      {
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/obsidian_knife",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash",
        weapon_sound = "sword1",
        attack_power_bonus = 1,
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/obsidian_knife",
        weapon_attack_animation = "backslash",
        weapon_sound = "sword1",
        attack_power_bonus = 1,
      },
      --
      {
        windup_duration = 50,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/obsidian_knife",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        callback = step_forward,
        recovery_duration = 200,
        hero_recovery_animation = "sword_windup",
        attack_power_bonus = 2,
      },
      --]]
    },


    weapon_parameters = {
      attack_power = 15,
      damage_type = "magic",
      scaling_stat = "mind",
    },

  }
)



