local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 140,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/shovel",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/slash_07",
        callback = step_forward,
      },
      {
        windup_duration = 30,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/shovel",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "weapons/slash_05",
        callback = step_forward,
      },
      {
        windup_duration = 110,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/shovel",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "weapons/slash_06",
        attack_power_bonus = 1,
        callback = function()
          local hero = sol.main.get_game():get_hero()
          hero:step_forward(40)
        end,
      },
    },


    weapon_parameters = {
      attack_power = 20,
    },

  }
)
