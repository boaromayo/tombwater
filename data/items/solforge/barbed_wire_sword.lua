local item = ...

local step_forward = function(dist)
  dist = dist or 16
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(dist)
end


require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 110,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/barbed_wire_sword",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/high_slash_01",
        callback = step_forward,
      },
      {
        windup_duration = 50,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/barbed_wire_sword",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "weapons/high_slash_02",
        callback = step_forward,
      },
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/barbed_wire_sword",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/high_slash_03",
        callback = step_forward,
        recovery_duration = 100,
        hero_recovery_animation = "stopped",
      },
    },


    weapon_parameters = {
      attack_power = 15,
    },

  }
)


function item:enemy_hit_callback(enemy)
  enemy:build_up_status_effect("bleed", 20)
end
