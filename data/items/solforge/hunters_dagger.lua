local item = ...
local game = item:get_game()

local step_forward = function(dist)
  return function()
    local hero = game:get_hero()
    hero:step_forward(dist or 16, 110)
  end
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 200,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        callback = step_forward,
        recovery_duration = 200,
        hero_recovery_animation = "sword_windup",
        attack_power_bonus = 10,
        callback = step_forward(64),
      },
      {
        windup_duration = 10,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_windup_animation = "backslash_windup",
        weapon_attack_animation = "backslash",
        weapon_sound = "sword1",
        attack_power_bonus = -5,
        callback = step_forward(8),
      },
      --
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash",
        weapon_sound = "sword1",
        attack_power_bonus = -5,
        callback = step_forward(8),
      },
      --]]
      {
        windup_duration = 300,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        callback = step_forward,
        hero_recovery_animation = "sword_windup",
        callback = step_forward(32),
      },
    },


    weapon_parameters = {
      attack_power = 15,
    },

  }
)
