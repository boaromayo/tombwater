local item = ...
local game = item:get_game()

item.regen_cooldown = 0
local regen_freq = 2 --how often to regen bullets

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16, 110)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_attack_animation = "backslash",
        weapon_sound = "sword1",
      },
      {
        windup_duration = 200,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/dagger_1",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        callback = step_forward,
        recovery_duration = 200,
        hero_recovery_animation = "sword_windup",
        attack_power_bonus = 10,
        callback = step_forward,
      },
    },


    weapon_parameters = {
      attack_power = 9,
    },

  }
)


function item:enemy_hit_callback(enemy)
  if item.regen_cooldown > 1 then
    item.regen_cooldown = item.regen_cooldown - 1
    return
  end
  item.regen_cooldown = regen_freq
  game:add_bullets(1)
end

