local item = ...
local game = item:get_game()

local function step_forward(amount)
  return function()
    local hero = game:get_hero()
    hero:step_forward(amount or 16)
  end
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        hero_windup_animation = "sword_windup",
        windup_duration = 350,
        hero_attack_animation = "sword",
        weapon_sprite = "solforge_weapons/pickaxe",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash_attack",
        weapon_sound = "sword1",
        callback = step_forward(),
      },
      {
        hero_windup_animation = "sword_windup_reverse",
        windup_duration = 300,
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/pickaxe",
        weapon_windup_animation = "backslash_windup",
        weapon_attack_animation = "backslash_attack",
        weapon_sound = "sword1",
        callback = step_forward(),
      },
      {
        windup_duration = 350,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/pickaxe",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        attack_power_bonus = -15,
        callback = step_forward(32)
      },
    },

    weapon_parameters = {
      attack_power = 40,
    }
  }
)
