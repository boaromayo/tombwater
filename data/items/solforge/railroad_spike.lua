local item = ...
local game = item:get_game()

local step_forward = function(dist)
  return function()
    local hero = game:get_hero()
    hero:step_forward(dist or 16, 110)
  end
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 200,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/railroad_spike",
        weapon_windup_animation = "backslash_windup",
        weapon_attack_animation = "backslash",
        weapon_sound = "sword1",
        callback = step_forward(),
      },
      --
      {
        windup_duration = 200,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/railroad_spike",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash",
        weapon_sound = "sword1",
        attack_power_bonus = 1,
        callback = step_forward(),
      },
      --]]
      {
        windup_duration = 100,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/railroad_spike",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        callback = step_forward,
        hero_recovery_animation = "sword_windup",
        attack_power_bonus = 5,
        callback = step_forward(32),
      },
      {
        windup_duration = 100,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/railroad_spike",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        callback = step_forward,
        recovery_duration = 200,
        hero_recovery_animation = "sword_windup",
        attack_power_bonus = 5,
        callback = step_forward(48),
      },
    },


    weapon_parameters = {
      attack_power = 17,
    },

  }
)



function item:enemy_hit_callback(enemy)
  enemy:build_up_status_effect("cold", 20)
end

