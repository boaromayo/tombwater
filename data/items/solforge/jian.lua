local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/jian",
        weapon_windup_animation = "backslash_windup",
        weapon_attack_animation = "backslash_attack",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/jian",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash_attack",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/jian",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword1",
        attack_power_bonus = 1,
        callback = function()
          local hero = sol.main.get_game():get_hero()
          hero:step_forward(40)
        end,
      },
    },


    weapon_parameters = {
      attack_power = 20,
    },

  }
)

function item:enemy_hit_callback(enemy)
  enemy:build_up_status_effect("cold", 70)
end
