local item = ...

local step_forward = function(dist)
  dist = dist or 16
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(dist)
end


local function fire_attack(dist)
  local game = sol.main.get_game()
  local hero = game:get_hero()
  local map = hero:get_map()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()
  sol.timer.start(map, 200, function()
    sol.audio.play_sound"fireball"
    local fire = map:create_fire{
      x=x + game:dx(dist)[direction],
      y=y + game:dy(dist)[direction],
      layer=z
    }
    fire.harmless_to_hero = true
  end)
end


require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 400,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/streetlamp",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/blunt_08",
        callback = step_forward,
      },
      {
        windup_duration = 200,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/streetlamp",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "weapons/blunt_07",
        callback = step_forward,
      },
      {
        windup_duration = 350,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/streetlamp",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        recovery_duration = 50,
        hero_recovery_animation = "stopped",
        weapon_sound = "weapons/blunt_02",
        attack_power_bonus = -10,
        callback = function()
          local hero = sol.main.get_game():get_hero()
          hero:step_forward(16)
          fire_attack(56)
        end,
      },
      {
        windup_duration = 90,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/streetlamp",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        recovery_duration = 50,
        hero_recovery_animation = "stopped",
        weapon_sound = "weapons/blunt_02",
        attack_power_bonus = -10,
        callback = function()
          local hero = sol.main.get_game():get_hero()
          hero:step_forward(16)
          fire_attack(56)
        end,
      },
      {
        windup_duration = 90,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/streetlamp",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        recovery_duration = 300,
        hero_recovery_animation = "stopped",
        weapon_sound = "weapons/blunt_02",
        attack_power_bonus = -10,
        callback = function()
          local hero = sol.main.get_game():get_hero()
          hero:step_forward(16)
          fire_attack(56)
        end,
      },
    },


    weapon_parameters = {
      attack_power = 35,
      damage_type = "fire",
    },

  }
)
