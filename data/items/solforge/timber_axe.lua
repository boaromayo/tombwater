local item = ...
local game = item:get_game()

local step_forward = function(dist)
  return function()
    local hero = game:get_hero()
    hero:step_forward(dist or 16, 110)
  end
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        hero_windup_animation = "sword_windup",
        windup_duration = 400,
        hero_attack_animation = "sword",
        weapon_sprite = "solforge_weapons/timber_axe",
        weapon_windup_animation = "windup",
        weapon_attack_animation = "axe",
        weapon_sound = "sword1",
        callback = step_forward(8),
      },
      {
        hero_windup_animation = "sword_windup_reverse",
        windup_duration = 300,
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/timber_axe",
        weapon_windup_animation = "windup_reverse",
        weapon_attack_animation = "axe_reverse",
        weapon_sound = "sword1",
        callback = step_forward(8),
      },
    },

    weapon_parameters = {
      attack_power = 30,
    }
  }
)
