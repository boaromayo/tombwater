local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(32)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 250,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/wraithblade",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/blunt_08",
        callback = step_forward,
      },
      {
        windup_duration = 250,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/wraithblade",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "weapons/blunt_08",
        callback = step_forward,
      },
--[[
      {
        windup_duration = 50,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        recovery_duration = 400,
        hero_recovery_animation = "stopped",
        weapon_sprite = "solforge_weapons/wraithblade",
        weapon_windup_animation = "backhand_windup",
        --weapon_attack_animation = "endcombo",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "sword1",
        attack_power_bonus = 15,
        callback = step_forward,
      },
--]]
    },


    weapon_parameters = {
      attack_power = 65,
    },

  }
)


function item:enemy_hit_callback(enemy)
  enemy:build_up_status_effect("burn", 20)
  enemy:build_up_status_effect("cold", 20)
end

