local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 200,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/sawblade",
        weapon_windup_animation = "backslash_windup",
        weapon_attack_animation = "backslash_attack",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 120,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/sawblade",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash_attack",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 120,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/sawblade",
        weapon_windup_animation = "backslash_windup",
        weapon_attack_animation = "backslash_attack",
        weapon_sound = "sword1",
        attack_power_bonus = 5,
        callback = step_forward,
      },
      {
        windup_duration = 180,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/sawblade",
        weapon_windup_animation = "slash_windup",
        weapon_attack_animation = "slash_attack",
        recovery_duration = 200,
        hero_recovery_animation = "stopped",
        weapon_sound = "sword1",
        attack_power_bonus = 10,
        callback = step_forward,
      },
    },


    weapon_parameters = {
      attack_power = 25,
    },

  }
)
