local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 500,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/greatsword",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 300,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/greatsword",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        windup_duration = 500,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        recovery_duration = 400,
        hero_recovery_animation = "stopped",
        weapon_sprite = "solforge_weapons/greatsword",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "endcombo",
        weapon_sound = "sword1",
        attack_power_bonus = 15,
        callback = step_forward,
      },
    },


    weapon_parameters = {
      attack_power = 40,
    },

  }
)
