local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(26)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 250,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "weapons/blunt_08",
        callback = step_forward,
      },
      {
        windup_duration = 50,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "weapons/blunt_07",
        callback = step_forward,
      },
      {
        windup_duration = 150,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "weapons/blunt_09",
        callback = function()
          local hero = sol.main.get_game():get_hero()
          hero:step_forward(48)
        end,
      },
    },



    weapon_parameters = {
      attack_power = 30,
    }
  }
)
