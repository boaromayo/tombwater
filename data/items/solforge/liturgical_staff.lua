local item = ...
local game = item:get_game()

local step_forward = function()
  local hero = game:get_hero()
  hero:step_forward(combo == 3 and 48 or 26)
  --[[
  local map = hero:get_map()
  local combo = hero.combo_number or 1
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  for i = 1, 12 do
    sol.timer.start(map, 20 * i, function()
      local spark = map:create_custom_entity{
        x = x + game:dx(40 + i * math.random(1, 3))[direction] + math.random(-8, 8),
        y = y + game:dy(40 + i * math.random(1, 3))[direction] + math.random(-8, 8),
        layer = z,
        direction = 0, width = 8, height = 8,
        sprite = "entities/star",
        model = "damaging_entity",
      }
      spark.damage = game:calculate_hero_output_damage(3, "mind")
    end)
  end
  --]]
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 250,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/liturgical_staff",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        windup_duration = 50,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/liturgical_staff",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        windup_duration = 150,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/liturgical_staff",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "sword4",
        callback = step_forward,
      },
    },



    weapon_parameters = {
      attack_power = 30,
      damage_type = "magic",
    }
  }
)


