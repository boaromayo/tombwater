local item = ...

local step_forward = function(dist)
  dist = dist or 16
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(dist)
end


require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 110,
        hero_windup_animation = "punch_windup",
        hero_attack_animation = "punching",
        weapon_sprite = "solforge_weapons/whip",
        weapon_windup_animation = "attack_windup",
        weapon_attack_animation = "attack",
        weapon_sound = "weapons/whip",
        recovery_duration = 100,
        hero_recovery_animation = "punch_windup",
        callback = step_forward,
      },
    },


    weapon_parameters = {
      attack_power = 15,
    },

  }
)


function item:enemy_hit_callback(enemy)
  local hero = enemy:get_map():get_hero()
  if hero:get_distance(enemy) > 54 then
    enemy:remove_life(15) --does extra damage at the very end of the whip
  end
end
