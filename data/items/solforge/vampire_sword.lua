local item = ...
local game = item:get_game()

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 110,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/vampire_sword",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/slash_04",
        callback = step_forward,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/vampire_sword",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "weapons/slash_03",
        callback = step_forward,
      },
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/vampire_sword",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/slash_01",
        recovery_duration = 100,
        hero_recovery_animation = "stopped",
        attack_power_bonus = 1,
        callback = step_forward,
      },
    },


    weapon_parameters = {
      attack_power = 10,
    },

  }
)



function item:enemy_hit_callback(enemy)
  local hero = game:get_hero()
  game:add_life(game:calculate_hero_output_damage(2 * hero.combo_number, "alchemy"))
end

