local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(18)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_attack_animation = "spear",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_attack_animation = "spear",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_attack_animation = "spear",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_attack_animation = "heavy_attack",
        weapon_sound = "sword4",
        attack_power_bonus = 2,
      },
    },



    weapon_parameters = {
      attack_power = 25,
    }
  }
)
