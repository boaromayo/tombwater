local item = ...
local game = item:get_game()

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end


local function make_lightning(dist)
  dist = dist or 40
  local hero = game:get_hero()
  local map = hero:get_map()
  local x,y,z = hero:get_position()
  local direction = hero:get_direction()
  sol.audio.play_sound"electrified"
  sol.timer.start(map, 200, function()
    local zap = map:create_lightning{
      x=x + game:dx(dist)[direction],
      y=y + game:dy(dist)[direction],
      layer=z
    }
    zap.harmless_to_hero = true
  end)
end


require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 110,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/coil_baton",
        weapon_windup_animation = "backhand_windup",
        weapon_attack_animation = "backhand_attack",
        weapon_sound = "weapons/high_slash_03",
        callback = step_forward,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "sword_windup_reverse",
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/coil_baton",
        weapon_windup_animation = "forehand_windup",
        weapon_attack_animation = "forehand_attack",
        weapon_sound = "weapons/high_slash_04",
        callback = step_forward,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "thrust_windup",
        hero_attack_animation = "thrust_attack",
        weapon_sprite = "solforge_weapons/coil_baton",
        weapon_windup_animation = "thrust_windup",
        weapon_attack_animation = "thrust_attack",
        weapon_sound = "weapons/high_slash_01",
        recovery_duration = 200,
        hero_recovery_animation = "stopped",
        attack_power_bonus = 5,
        callback = function()
          step_forward()
          make_lightning()
        end,
      },
    },


    weapon_parameters = {
      attack_power = 14,
    },

  }
)




