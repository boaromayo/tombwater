local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  hero:step_forward(16)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 100,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/cavalry_sabre",
        weapon_windup_animation = "swing_windup",
        weapon_attack_animation = "swing",
        weapon_sound = "weapons/slash_04",
        callback = step_forward,
      },
      {
        hero_attack_animation = "sword_swing_forehand",
        weapon_sprite = "solforge_weapons/cavalry_sabre",
        weapon_attack_animation = "reverse_swing",
        weapon_sound = "weapons/slash_03",
        callback = step_forward,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "sword_windup",
        hero_attack_animation = "sword_swing",
        weapon_sprite = "solforge_weapons/cavalry_sabre",
        weapon_windup_animation = "swing_windup",
        weapon_attack_animation = "swing",
        weapon_sound = "weapons/slash_01",
        attack_power_bonus = 1,
        callback = step_forward,
      },
    },


    weapon_parameters = {
      attack_power = 20,
    },

  }
)
