local item = ...
local game = item:get_game()

local resistance_duration = 10000

function item:on_started()
  item:set_savegame_variable("possession_cure_burn")
  item:set_assignable(true)
  item:set_amount_savegame_variable("amount_cure_burn")
  item:set_max_amount(999)
  item:set_ammo("_amount")
end


function item:on_using()
  local hero = game:get_hero()
  if not item:try_spend_ammo(1) then
    item:set_finished()
    return
  end
  hero:set_animation("eating", function()
    item:set_finished()
  end)
  sol.audio.play_sound("steam_woosh")
  hero:stop_status_effect("burn")
end


function item:on_obtaining(variant)
  amounts = {1, 3, 5, 10}
  item:add_amount(amounts[variant])
end
