local item = ...
local game = item:get_game()
local boost_amount = .5


function item:on_started()
  item:set_savegame_variable("possession_pickled_foot")
  item:set_assignable(true)
  item:set_amount_savegame_variable("amount_pickled_foot")
  item:set_max_amount(999)
  item:set_ammo("_amount")
end


function item:on_using()
  local hero = game:get_hero()

    if item.already_active or not item:try_spend_ammo(1) then
      item:set_finished()
      return
    end
    item.already_active = true
    hero:set_animation("eating", function()
      item:set_finished()
    end)
    sol.audio.play_sound("use_consumable")
    local current_mult = game.exp_mod or 1
    game.exp_mod = current_mult + boost_amount
    local timer = sol.timer.start(game,120000,function()
      local current_mult = game.exp_mod
      game.exp_mod = current_mult - boost_amount
      item.already_active = false
    end)
    timer:set_suspended_with_map(true)

end

function item:on_obtaining(variant)
  amounts = {1, 3, 5, 10}
  item:add_amount(amounts[variant])
end
