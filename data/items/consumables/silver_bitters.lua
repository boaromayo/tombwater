local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_silver_bitters")
  item:set_assignable(true)
  item:set_amount_savegame_variable("amount_silver_bitters")
  item:set_max_amount(999)
  item:set_ammo("_amount")
end

function item:on_using()

  local hero = game:get_hero()
  local healing_flask = game:get_item("inventory/healing_flask")

  if not item:try_spend_ammo(1) then
    item:set_finished()
    return
  end
  hero:set_animation("eating", function()
    item:set_finished()
  end)
  sol.audio.play_sound("use_consumable")
  healing_flask:add_amount(2)
end

function item:on_obtaining(variant)
  amounts = {1, 2, 3, 5}
  item:add_amount(amounts[variant])
end
