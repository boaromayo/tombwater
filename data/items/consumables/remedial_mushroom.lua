local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_remedial_mushroom")
  item:set_assignable(true)
  item:set_amount_savegame_variable("amount_remedial_mushroom")
  item:set_max_amount(999)
  item:set_ammo("_amount")
end

function item:on_using()
  local hero = game:get_hero()
  local frequency = 300
  local amount = 1
  local duration = 30000

  if not item:try_spend_ammo(1) then
    item:set_finished()
    return
  end
  hero:set_animation("eating", function()
    item:set_finished()
  end)
  sol.audio.play_sound("use_consumable")
  
  local elapsed_time = 0
  local timer = sol.timer.start(game, frequency, function()

    game:add_life(amount)
    elapsed_time = elapsed_time + frequency
    if elapsed_time < duration then
      return true
    end
  end)
  timer:set_suspended_with_map(true)
end

function item:on_obtaining(variant)
  amounts = {1,3,5}
  item:add_amount(amounts[variant])
end