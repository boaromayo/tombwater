--[[
local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("posession_bandolier_strap")
  item:set_amount_savegame_variable("amount_bandolier_strap")
  item:set_max_amount(999)
end

function item:on_obtaining()
  item:add_amount(1)
  while item:get_amount() >= 1 do
    game:set_max_bullets(game:get_max_bullets() + 1)
    sol.audio.play_sound("victory")
    item:set_amount(item:get_amount() - 1)
  end
end
--]]

--[[
local item = ...
local game = item:get_game()

function item:on_obtaining()
  game:set_max_bullets(game:get_max_bullets() + 1)
  sol.audio.play_sound("victory")
end

--]]

local item = ...
local game = item:get_game()

game:register_event("on_started", function()
  --This is just to keep the "amount found" HUD display consistent:
  item:set_amount(game:get_max_bullets())
end)

function item:on_started()
  item:set_savegame_variable("posession_bandolier_strap")
  item:set_amount_savegame_variable("amount_bandolier_strap")
  item:set_max_amount(999)
end

function item:on_obtaining()
  item:add_amount(1)
  game:set_max_bullets(game:get_max_bullets() + 1)
  game:set_bullets(game:get_max_bullets())
  sol.audio.play_sound("victory")
end

