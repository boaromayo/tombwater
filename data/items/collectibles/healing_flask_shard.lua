local item = ...
local game = item:get_game()

local num_needed_for_upgrade = 3 --how many shards needed to add a charge to the flask

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("posession_healing_flask_shard")
  item:set_amount_savegame_variable("amount_healing_flask_shard")
  item:set_max_amount(999)
end

function item:on_obtaining(variant)
  local amounts = {1, 3}
  item:add_amount(amounts[variant])
  while item:get_amount() >= num_needed_for_upgrade do
    local healing_flask = game:get_item("inventory/healing_flask")
    local max_amount = (game:get_value("max_amount_healing_flask") + 1)

    sol.audio.play_sound("victory")
    game:set_value("max_amount_healing_flask", max_amount)
    healing_flask:set_max_amount(max_amount)
    healing_flask:set_amount(max_amount)
    item:set_amount(item:get_amount() - num_needed_for_upgrade)
  end
end