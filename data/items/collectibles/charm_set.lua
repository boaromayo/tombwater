local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_charm_set")
end

function item:on_obtaining(variant)
  local crafting_materials = sol.main.get_items_in_directory("items/charms")
  for _, item_name in pairs(crafting_materials) do
    if item_name ~= "charms/charm_manager" then
      local item = game:get_item(item_name)
      item:set_variant(1)
    end
  end
  game:set_value("charm_slots", 20)
end
