local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
  item:set_savegame_variable("posession_healing_flask_silverdust")
  item:set_amount_savegame_variable("amount_healing_flask_silverdust")
  item:set_max_amount(999)
end

function item:on_obtaining()
  item:add_amount(1)
  while item:get_amount() >= 1 do
    local new_heal_amount = (game:get_value("healing_flask_healing_amount") + 50)

    sol.audio.play_sound("victory")
    game:set_value("healing_flask_healing_amount", new_heal_amount)
    item:set_amount(item:get_amount() - 1)
  end
end