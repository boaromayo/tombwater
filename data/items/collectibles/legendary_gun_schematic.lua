local item = ...
local game = item:get_game()

function item:on_started()
end

function item:on_obtaining()
  local savegame_name = "legendary_gun_schematics_collected"
  local current_amount = game:get_value(savegame_name) or 0
  game:set_value(savegame_name, current_amount + 1)
end

