local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_barrel_of_swords")
end

function item:on_obtaining(variant)
  --Get tables strings for every item ID that we want the player to get
  local item_tables = {
    sol.main.get_items_in_directory("items/solforge"),
    sol.main.get_items_in_directory("items/guns"),
    sol.main.get_items_in_directory("items/inventory"),
    sol.main.get_items_in_directory("items/spells"),
  }
  --Put all those strings on one table
  local all_items = {}
  for _, item_table in pairs(item_tables) do
    for _, item in pairs(item_table) do
      table.insert(all_items, item)
    end
  end
  --Double check each is _actually_ an item before we try to give it to the player
  for k, entry in pairs(all_items) do
    if pcall(function() game:get_item(entry) end) then
    else
      all_items[k] = nil
    end
  end
  --Give each item to the player
  for _, item_name in pairs(all_items) do
    local item = game:get_item(item_name)
    item:set_variant(1)
  end

  --Misc other items:
  game:get_item("gear/hookcaster"):on_obtained()
  game:get_item("gear/climbing_gear"):set_variant(1)
  game:get_item("gear/dead_mans_shot"):set_variant(1)
end
