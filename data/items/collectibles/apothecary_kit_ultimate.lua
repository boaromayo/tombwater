local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_apothecary_kit")
end

function item:on_obtaining(variant)
  local crafting_materials = sol.main.get_items_in_directory("items/materials/crafting")
  for _, item_name in pairs(crafting_materials) do
    local item = game:get_item(item_name)
    item:set_variant(1)
    item:set_amount(99)
  end
end
