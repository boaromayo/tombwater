local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("posession_mirror_shard")
  item:set_amount_savegame_variable("posession_mirror_shard")
  item:set_max_amount(999)
end

function item:on_obtaining()
  item:add_amount(1)
end