local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("posession_mining_co_hq_contraption_gear")
  item:set_amount_savegame_variable("amount_mining_co_hq_contraption_gear")
  item:set_max_amount(4)
end

function item:on_obtaining()
  item:add_amount(1)
end
