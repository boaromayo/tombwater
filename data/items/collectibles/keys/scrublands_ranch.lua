local item = ...
local game = item:get_game()

function item:on_started()
  local name = item:get_name():match("keys/(.*)")
  item:set_savegame_variable("possession_key_" .. name)
end

