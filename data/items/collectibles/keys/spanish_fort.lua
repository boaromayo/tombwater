local item = ...
local game = item:get_game()

function item:on_started()
  local name = item:get_name():match("keys/(.*)")
  item:set_savegame_variable("possession_key_" .. name)
end

--Only add this for keys received as pickable items, not ones you get from chests:
function item:on_obtained(variant, savegame_variable)
  item:show_popup()
end
