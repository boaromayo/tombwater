--[[
Created by Max Mraz, licensed MIT
Allows players to use the hookshot while aiming
--]]

local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_hookcaster")
end

item:register_event("on_obtaining", function()
  game:set_value("can_use_hookshot", true)
end)


item:register_event("on_obtained", function()
  game:start_tutorial("hookshot")
end)
