--[[
Assigns savegame variables and amounts to items in specified folders
--]]


local game_meta = sol.main.get_metatable"game"
local item_meta = sol.main.get_metatable"item"

local manager = {}

local amt_items = {} --table of item IDs that need a savegame value and an amount

local amt_tables = { --tables of item IDs
  sol.main.get_items_in_directory("items/collectibles/relics"),
  sol.main.get_items_in_directory("items/materials/crafting"),
  sol.main.get_items_in_directory("items/spell_eyes"),
}
--Put all itemIDs from the amt_tables into the amt_items table
for _, amt_table in pairs(amt_tables) do
  for _, item_id in pairs(amt_table) do
    amt_items[#amt_items + 1] = item_id
  end
end


game_meta:register_event("on_started", function(game)

  --Create common behavior for all amt_items
  for _, item_id in pairs(amt_items) do
    local item = game:get_item(item_id)
    local save_name = item:get_name():gsub("/", "_")
    item:set_savegame_variable("possession_" .. save_name)
    item:set_amount_savegame_variable("amount_" .. save_name)
    item:set_brandish_when_picked(not game:has_item(item:get_name()))

    function item:on_obtaining(variant)
      item:set_brandish_when_picked(not game:has_item(item:get_name()))
      item:add_amount(variant or 1)
    end
  end

  --Other behavior?

end)


return manager
