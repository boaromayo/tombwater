local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("canyon_mist")
  map:set_darkness_level{240,230,205}

  for entity in map:get_entities("windmill_blades") do
    local sprite = entity:get_sprite()
    sol.timer.start(map, 80, function()
      sprite:set_rotation(sprite:get_rotation() - math.rad(2))
      return true
    end)
    --TODO: Took this shader out as it was GPL. Need to replace
    --sprite:set_shader(sol.shader.create("noise_reducer"))
  end

end)
