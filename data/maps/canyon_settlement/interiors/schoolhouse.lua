local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level("interior_1")

  --Unlock schoolhouse front door once you've made it inside:
  game:set_value("canyon_settlement_schoolhouse_front_door_state", true)
end)
