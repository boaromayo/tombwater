local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level("interior_1")

end)


--Into tunnels, make dark:
function from_upstairs:on_activated()
  map:set_darkness_level(4)
end


--Out of tunnels, lighten
function from_downstairs:on_activated()
  map:set_darkness_level("interior_1")
end

