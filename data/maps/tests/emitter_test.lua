-- Lua script of map tests/emitter_test.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  sol.timer.start(map, 500, function()
--
    for i = 1, 1 do
      local emitter = map:create_particle_emitter(target:get_position())
      emitter.particle_speed = 150
      emitter.particle_speed_variance = 100
      emitter.duration = 2000
      emitter:emit()
    end
--]]
  end)
end)


