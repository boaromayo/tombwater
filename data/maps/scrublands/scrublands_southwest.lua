local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level{240,230,205}

  map:activate_next_phantom()

  --stupid bridge:
  bridge_l2_helper_1:set_visible(false)
  bridge_l2_helper_2:set_visible(false)
end)



--Phantom Rider
function map:activate_next_phantom()
  if game:has_item("spell_eyes/bounty") then
    return
  end
  local location_index = game.scrublands_phantom_rider_location_index or 1
  game.scrublands_phantom_rider_location_index = location_index
  local target_ent = map:get_entity("phantom_horseman_location_" .. location_index)
  local next_ent = map:get_entity("phantom_horseman_location_" .. location_index + 1)
  local x, y, z = target_ent:get_position()
  local direction = next_ent and target_ent:get_direction4_to(next_ent) or 1
  local rider = map:create_custom_entity{
    x=x, y=y, layer=z, width=32, height=32,
    direction = direction,
    sprite = "npc/phantom_rider",
  }
  rider:set_traversable_by(false)
  rider:set_drawn_in_y_order(true)
  rider:set_origin(16,29)

  function rider:point()
    local sprite = rider:get_sprite()
    sprite:set_animation("pointing")
    sol.timer.start(rider, 3000, function()
      sprite:set_animation("lowering_arm", function() sprite:set_animation"stopped" end)
      sprite:fade_out(25)
      sol.timer.start(rider, 1000, function() rider:remove() end)
      if next_ent then
        location_index = location_index + 1
        game.scrublands_phantom_rider_location_index = location_index
        map:activate_next_phantom()
      else
        --map:phantom_rider_final_stop()
        --Final stop:
        bounty_eye:set_enabled()
      end
    end)
  end

  sol.timer.start(rider, 1000, function()
    if rider:get_distance(hero) <= 80 then
      rider:point()
    else
      return true --repeat timer
    end
  end)
end

--Fireworks from ur gun - I did this for a little "celebrate" gif, might be fun to tie to a hotkey for making gifs
local function get_random_color()
  return {math.random(200,255), math.random(200,255), math.random(200,255)}
end

function firework:on_activated()
  return
--[[
  hero:freeze()
  hero:set_animation("yee_haw_blam")
  local x, y, z = hero:get_position()
  local i = 1
  sol.timer.start(hero, 180, function()
    i = i + 1
    local ex = (x - 16) + (32 * (i % 2))
    local ey = y - math.random(48, 56)
    local em = map:create_particle_emitter(ex, ey, z, "burst")
    em.particle_color = get_random_color()
    em.particle_lifetime = 200
    em:emit()
    return true
  end)
--]]
end



