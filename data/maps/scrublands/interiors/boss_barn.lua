local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_darkness_level("interior_2")
  map:set_doors_open("boss_door")
end)


--Boss
for s in map:get_entities"boss_sensor" do
  function s:on_activated()
    boss_sensor_1:remove()
    boss_sensor_2:remove()
    if game:get_value("scrublands_ranch_cattle_corpse_boss") then return end
    map:close_doors("boss_door")
    cattle_boss:start_boss_bar()
    cattle_boss:start_aggro()
    sol.audio.play_music("boss_fight_test_02")

    --Summon boss's assistants at low health
    sol.timer.start(map, 1000, function()
      if cattle_boss:get_life() <= cattle_boss.max_life * .4 then
        for e in map:get_entities("boss_assistant") do
          sol.timer.start(map, math.random(1000, 3000), function()
            map:summon_assistant(e)
          end)
        end
      else
        return true
      end
    end)
  end
end

function map:on_boss_dead(breed)
  if breed == "tombwater_enemies/boss/cattle_corpse" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music("ominous_wind")
  end
end


function map:summon_assistant(e)
  --map:create_poof(e:get_position())
  sol.timer.stop_all(e)
  local sprite = e:get_sprite()
  sprite:set_animation"head_slam_windup"
  sprite:set_xy(0, -250)
  e:set_enabled()
  sol.timer.start(20, function()
    local step = 10
    local _, y = sprite:get_xy()
    y = y + step
    if y < 0 then
      sprite:set_xy(0, y)
      return true
    else
      sprite:set_xy(0, 0)
      sprite:set_animation("screaming")
      e:start_aggro()
    end
  end)
end

