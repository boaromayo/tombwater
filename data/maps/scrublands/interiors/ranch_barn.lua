local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_darkness_level(1)

  map:set_doors_open"horsehead_trap_door"
end)



for sensor in map:get_entities"horsehead_trap_sensor" do
  function sensor:on_activated()
    sensor:remove()
    if game:get_value("scrublands_ranch_slaughterhouse_horsehead_trap_defeated") then return end --only trap you in here until you've beaten it once
    map:close_doors("horsehead_trap_door")
  end
end


function map:on_enemy_group_dead(group)
  if group == "horsehead_trap" then
    game:set_value("scrublands_ranch_slaughterhouse_horsehead_trap_defeated", true)
    map:open_doors("horsehead_trap_door")
  end
end

