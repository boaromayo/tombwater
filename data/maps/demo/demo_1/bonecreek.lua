local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level("town_day")

  --Close easy escape miniboss door if you've defeated him, so you have to use the hookshot
  if game:get_value("demo_1_graveyard_sentinel") then
    miniboss_door_gate:set_open(false)
  end
end)


--Fogs:
for sensor in map:get_entities("fog_activate_sensor") do
  if not map.area_fogs then map.area_fogs = {} end
  local fog_type = sensor:get_property("fog_type")
  function sensor:on_activated()
    if map.area_fogs[fog_type] then return end
    --map.area_fogs[fog_type] = map:fade_in_fog(fog_type)
  end
end

for sensor in map:get_entities("fog_deactivate_sensor") do
  function sensor:on_activated()
    local fog_type = sensor:get_property("fog_type")
    if map.area_fogs[fog_type] then
      map:fade_out_fog(map.area_fogs[fog_type])
      map.area_fogs[fog_type] = nil
    end
  end
end




--Dude turns into monster:
function ichorcyst_townsfolk_sensor:on_activated()
  self:remove()
  if ichorcyst_townsfolk then
    ichorcyst_townsfolk:start_transformation()
    ichorcyst_townsfolk.aggro = true
  end
end


--Adelaide Encounter:
function adelaide_encounter_sensor:on_activated()
  if game:get_value("demo_1_scrublands_adelaide_encounter") then return end
  game:set_value("demo_1_scrublands_adelaide_encounter", true)

  hero:freeze()
  map:start_coroutine(function()
    --Vines
    for vine in map:get_entities("adelaide_vines") do
      sol.timer.start(map, math.random(0,500), function()
        sol.audio.play_sound"grass_rustling"
        vine:set_enabled()
        local vine_sprite = vine:get_sprite()
        vine_sprite:set_animation("growing", function()
          vine_sprite:set_animation("flowering", function() vine_sprite:set_animation("flowered") end)
        end)
      end)
    end
    wait(2000)
    hero:set_animation("walking")
    local m = sol.movement.create"target"
    m:set_target(adelaide_encounter_target)
    movement(m, hero)
    hero:set_animation"stopped"
    hero:set_direction(1)
    wait(2000)
    --Adelaide appears
    adelaide.light_source_sprite = "entities/effects/light_l"
    adelaide:set_enabled(true)
    run_on_main(function()
      map:check_dynamic_light_sources()
    end)
    --Particle Effects
    local em
    run_on_main(function()
      local x, y, z = adelaide:get_position()
      em = map:create_particle_emitter(x, y - 28, z, "smoke")
      em.particle_sprite = "effects/adelaide_smoke"
      em.particle_scaling = {1, 1.5}
      em.width = 10
      em:emit()
    end)
    --Abberation
    local ab_shader = sol.shader.create("chromatic_aberration")
    ab_shader:set_uniform("aberration_amount", 8)
    local cam_surface = map:get_camera():get_surface()
    cam_surface:set_shader(ab_shader)
    sol.timer.start(map, 100, function()
      cam_surface:set_shader(nil)
    end)
    map:screenshake()
    sol.audio.play_sound("scenes/god_horn")
    sol.audio.play_sound("scenes/silver_flash")
    --Adelaide dialog:
    wait(2000)
    dialog("npcs.demo_1.adelaide.scrubland_encounter")
    wait(400)
   --Aberration again:
    local ab_shader = sol.shader.create("chromatic_aberration")
    ab_shader:set_uniform("aberration_amount", 8)
    local cam_surface = map:get_camera():get_surface()
    cam_surface:set_shader(ab_shader)
    sol.timer.start(map, 100, function()
      cam_surface:set_shader(nil)
    end)
    map:screenshake()
    --sol.audio.play_sound("scenes/god_horn")
    sol.audio.play_sound("scenes/silver_flash")
    adelaide:set_enabled(false)
    em:remove()
    wait(200)
    hero:unfreeze()
  end)
end



--Miniboss:
--Give graveyard sentinel boss death behavior:
graveyard_sentinel:register_event("on_dying", function()
  graveyard_sentinel:boss_death("jumping_crouch")
  graveyard_sentinel:set_life(950)
end)

function miniboss_sensor:on_activated() --close doors when entering arena
  miniboss_sensor:remove()
  if game:get_value("demo_1_graveyard_sentinel") then return end
  map:close_doors("miniboss_door")
  graveyard_sentinel:start_boss_bar()
  graveyard_sentinel:start_aggro()
  sol.audio.play_music("boss_fight_test_03")
end


--Boss
function boss_sensor:on_activated()
  boss_sensor:remove()
  if game:get_value("central_tombwater_deputy_boss") then return end
  map:start_coroutine(function()
    hero:freeze()
    game:set_suspended(true)
    dialog("npcs.demo_1.deputy.1")
    local deputy_sprite = deputy_boss:get_sprite()
    wait(200)
    deputy_sprite:set_direction(3)
    wait(300)
    dialog("npcs.demo_1.deputy.2")
    map:close_doors("boss_door")
    deputy_boss:start_boss_bar()
    deputy_boss:start_aggro()
    sol.audio.play_music("boss_fight_test_02")
    hero:unfreeze()
    game:set_suspended(false)
  end)
end



--Boss Death
function map:on_boss_dead(breed) --open doors after defeat
  --Miniboss
  if breed == "tombwater_enemies/graveyard_sentinel" then
    map:open_doors"miniboss_door"
    miniboss_door_gate:set_open(false)
    sol.audio.play_sound("a_chord")
    sol.audio.play_music(map:get_music())

  --End boss: trigger cutscene
  elseif breed == "tombwater_enemies/boss/deputy_beast" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music("ominous_wind")
    hero:freeze()
    sol.timer.start(map, 1000, function()
      local m = sol.movement.create"straight"
      m:set_max_distance(96)
      m:set_angle(math.pi / 2)
      m:start(map:get_camera())
      game:start_flash({0,0,0}, 80)
      sol.timer.start(map, 3000, function()
        hero:teleport("demo/demo_1/interiors/mine")
      end)
    end)
  end
end




