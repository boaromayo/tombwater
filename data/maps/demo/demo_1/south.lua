local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level("town_day")

  --Close easy escape miniboss door if you've defeated him, so you have to use the hookshot
  if game:has_item("gear/hookcaster") and not game:get_value("demo_1_hookshot_use_proven") then
    miniboss_door_gate:set_open(false)
  end

  miniboss_los_blocker:set_modified_ground"wall"
  if game:has_item("gear/hookcaster") then miniboss_los_blocker:remove() end

  --Give graveyard sentinel boss death behavior:
  if graveyard_sentinel then
    --print("setting up boss death")
    graveyard_sentinel:register_event("on_dying", function()
      graveyard_sentinel:boss_death("jumping_crouch")
    end)
  end
end)


--Fogs:
for sensor in map:get_entities("fog_activate_sensor") do
  if not map.area_fogs then map.area_fogs = {} end
  local fog_type = sensor:get_property("fog_type")
  function sensor:on_activated()
    if map.area_fogs[fog_type] then return end
    map.area_fogs[fog_type] = map:fade_in_fog(fog_type)
  end
end

for sensor in map:get_entities("fog_deactivate_sensor") do
  function sensor:on_activated()
    local fog_type = sensor:get_property("fog_type")
    if map.area_fogs[fog_type] then
      map:fade_out_fog(map.area_fogs[fog_type])
      map.area_fogs[fog_type] = nil
    end
  end
end




--Dude turns into monster:
function ichorcyst_townsfolk_sensor:on_activated()
  self:remove()
  if ichorcyst_townsfolk then
    ichorcyst_townsfolk:start_transformation()
    ichorcyst_townsfolk.aggro = true
  end
end


--Graveyard zombie ambushes:
function mausoleum_ambush_sensor:on_activated()
  self:remove()
  for e in map:get_entities("mausoleum_ambush_zombie") do
    e:start_aggro()
  end
end

function boner_ambush_sensor:on_activated()
  self:remove()
  corpse_boner_1:start_aggro()
end


--Proove you've used the hookshot:
function hookshot_proven_sensor:on_activated()
  game:set_value("demo_1_hookshot_use_proven", true)
  self:remove()
end



--Miniboss:
function miniboss_sensor:on_activated() --close doors when entering arena
  miniboss_los_blocker:remove()
  miniboss_sensor:remove()
  if game:get_value("demo_1_graveyard_sentinel") then return end
  map:close_doors("miniboss_door")
  graveyard_sentinel:start_boss_bar()
  graveyard_sentinel:start_aggro()
  sol.audio.play_music("boss_fight_test_03")
end


--Boss Death
function map:on_boss_dead(breed) --open doors after defeat
  --Miniboss
  if breed == "tombwater_enemies/graveyard_sentinel" then
    map:open_doors"miniboss_door"
    miniboss_door_gate:set_open(false)
    sol.audio.play_sound("a_chord")
    sol.audio.play_music(map:get_music())

  --End boss: trigger cutscene
  elseif breed == "tombwater_enemies/boss/deputy_beast" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music("ominous_wind")
    hero:freeze()
    sol.timer.start(map, 1000, function()
      local m = sol.movement.create"straight"
      m:set_max_distance(96)
      m:set_angle(math.pi / 2)
      m:start(map:get_camera())
      game:start_flash({0,0,0}, 80)
      sol.timer.start(map, 3000, function()
        hero:teleport("demo/demo_1/interiors/mine")
      end)
    end)
  end
end




