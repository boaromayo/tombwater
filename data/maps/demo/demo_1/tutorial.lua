local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_darkness_level("dusk")

  hero:set_animation"sitting_head_down"
  elliot:get_sprite():set_animation"sitting"
  game:set_hud_enabled(false)
  game:start_flash({0,0,0}, 0)
end)


map:register_event("on_opening_transition_finished", function()
  game:stop_flash(70)
  game:refill_respawn_items()
  if not game:get_value("demo_tutorial_setup_complete") then
    game:set_value("demo_tutorial_setup_complete", true)
    game:save_checkpoint(hero:get_position())
  end

  --Waking up animations:
  -- no coroutine method:
  hero:freeze()
  hero:set_animation"sitting_head_down"
  sol.timer.start(hero, 1500, function()
    hero:set_animation"sitting"
    sol.timer.start(hero, 1000, function()
      hero:set_animation("sitting_head_down")
      sol.timer.start(hero, 900, function()
        hero:set_animation("sitting")
        sol.timer.start(hero, 700, function()
          hero:set_animation("standing_up", function()
            hero:set_animation("stunned")
            sol.timer.start(hero, 1200, function()
              hero:set_animation("stopped")
              game:set_hud_enabled(true)
              hero:unfreeze()
            end)
          end)
        end)
      end)
    end)
  end)

--[[
  --Using coroutine:
  hero:freeze()
  local hero_sprite = hero:get_sprite()
  map:start_coroutine(function()
    hero_sprite:set_animation"sitting_head_down"
    wait(1500)
    hero_sprite:set_animation"sitting"
    wait(1000)
    hero_sprite:set_animation"sitting_head_down"
    wait(900)
    hero_sprite:set_animation"sitting"
    wait(700)
    animation(hero_sprite, "standing_up")
    hero_sprite:set_animation"stunned"
    wait(1200)
    hero_sprite:set_animation"stopped"
    wait(400)
    game:set_hud_enabled(true)
    hero:unfreeze()
  end)
--]]
end)


for sensor in map:get_entities_by_type("sensor") do
  if sensor:get_property("tuto_message") then
    function sensor:on_activated()
      self:remove()
      game:start_tutorial(sensor:get_property("tuto_message"))
      if sensor:get_property("tuto_message") == "shoot" then
        game:add_bullets(6)
      end
    end
  end
end


function map:cutscene_1()
  map:start_cutscene()
  sheriff_sprite = elliot:get_sprite()
  hero:set_animation"walking"
  local m = sol.movement.create"target"
  m:set_target(endscene_target)
  m:start(hero, function()
    hero:set_direction(1)
    hero:set_animation"stopped"
    sol.timer.start(map, 500, function()
      game:start_dialog("npcs.demo_1.elliot.1", function()
        sol.timer.start(map, 500, function()
          sheriff_sprite:set_animation("standing_up", function() sheriff_sprite:set_animation"stopped" end)
          sol.timer.start(map, 1200, function()
            game:start_dialog("npcs.demo_1.elliot.2", function()
              sol.timer.start(map, 300, function()
                local m2 = sol.movement.create"path"
                m2:set_path{7,7,7,6,6}
                m2:set_ignore_obstacles(true)
                m2:start(elliot, function()
                  sol.timer.start(map, 400, function()
                    game:start_dialog("npcs.demo_1.elliot.3", function()
                      map:cutscene_2()
                    end)
                  end)
                end)
              end)
            end)
          end)
        end)
      end)
    end)
  end)

end


function map:cutscene_2()
  local m3 = sol.movement.create"path"
  m3:set_path{6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6}
  m3:set_ignore_obstacles(true)
  m3:start(elliot)
  sol.timer.start(map, 3000, function()
    hero:set_direction(3)
    hero:set_animation"walking"
    local m4 = sol.movement.create"path"
    m4:set_path{6,6,6,6,6,6}
    m4:start(hero, function()
      hero:set_animation"stopped"
      sol.timer.start(map, 1000, function()
        for vine in map:get_entities("adelaide_vines") do
          sol.timer.start(map, math.random(100,1400), function()
            sol.audio.play_sound"grass_rustling"
            vine:set_enabled()
            local vine_sprite = vine:get_sprite()
            vine_sprite:set_animation("growing", function()
              vine_sprite:set_animation("flowering", function() vine_sprite:set_animation("flowered") end)
            end)
          end)
        end --end vines loop
        sol.timer.start(map, 500, function()
          hero:set_direction(1)
          hero:set_animation("walking")
          local m5 = sol.movement.create"path"
          m5:set_path{2,2}
          m5:start(hero, function()
            hero:set_animation"stopped"
          end)
          sol.timer.start(map, 3000, function()
            --Adelaide appears
            adelaide.light_source_sprite = "entities/effects/light_l"
            adelaide:set_enabled(true)
            map:check_dynamic_light_sources()
            --Particle Effects
            local x, y, z = adelaide:get_position()
            local em = map:create_particle_emitter(x, y - 28, z, "smoke")
            em.particle_sprite = "effects/adelaide_smoke"
            em.particle_scaling = {1, 1.5}
            em.width = 10
            em:emit()
            --Abberation
            local ab_shader = sol.shader.create("chromatic_aberration")
            ab_shader:set_uniform("aberration_amount", 8)
            local cam_surface = map:get_camera():get_surface()
            cam_surface:set_shader(ab_shader)
            sol.timer.start(map, 100, function()
              cam_surface:set_shader(nil)
            end)
            map:screenshake()
            sol.audio.play_sound("scenes/god_horn")
            sol.audio.play_sound("scenes/silver_flash")
            --Hero aim gun at hero
            sol.timer.start(map, 300, function()
              hero:set_animation("gun_pistol_stopped")
              sol.audio.play_sound("gun_aim_pistol")
            end)
            sol.timer.start(map, 2000, function()
              hero:set_animation("walking")
              local m6 = sol.movement.create("path")
              m6:set_path{2}
              m6:start(hero, function()
                hero:set_animation("stopped")
                sol.timer.start(map, 1500, function()
                 --Aberration again:
                  local ab_shader = sol.shader.create("chromatic_aberration")
                  ab_shader:set_uniform("aberration_amount", 8)
                  local cam_surface = map:get_camera():get_surface()
                  cam_surface:set_shader(ab_shader)
                  sol.timer.start(map, 100, function()
                    cam_surface:set_shader(nil)
                  end)
                  sol.timer.start(map, 100, function()
                    --Wake up on train:
                    game:start_flash({0,0,0}, 0)
                    sol.timer.start(map, 1500, function()
                      hero:unfreeze()
                      hero:teleport("demo/demo_1/interiors/traincar", "destination", "immediate")
                      game:set_hud_enabled(true)
                      game:stop_flash(0)
                    end)
                  end)
                end)
              end)
            end)
          end)
        end)
      end)
    end)
  end)
end


function endscene_sensor:on_activated()
  map:cutscene_1()
end










