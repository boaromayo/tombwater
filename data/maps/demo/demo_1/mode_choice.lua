local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  game:set_hud_enabled(false)
  game:start_flash({0,0,0}, 0)

  game:start_dialog("game.demo.mode_choice", function(answer)
    if answer == 1 then
      hero:teleport("demo/demo_1/tutorial", "destination", "immediate")
    else
      hero:teleport("demo/demo_1/boss_rush/intro", "destination", "immediate")
    end
    game:stop_flash()
    game:set_hud_enabled(true)
  end)

end)