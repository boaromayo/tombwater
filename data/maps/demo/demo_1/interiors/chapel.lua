local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level{210,210,190}

  --remove cultists if already observed:
  if game:get_value("demo_1_chapel_seance_observed") then
    for ent in map:get_entities("cultist") do
      ent:remove()
    end
    seance_sensor:set_enabled(false)
  end
end)

--
function seance_sensor:on_activated()
  if game:get_value("demo_1_chapel_seance_observed") then return end
  game:set_value("demo_1_chapel_seance_observed", true)
  seance_sensor:remove()
  local camera = map:get_camera()
  hero:freeze()
  camera:scroll_to_entity(seance_focus_target)
  for i = 1, 4 do
    local cultist = map:get_entity("cultist_" .. i)
    cultist:set_drawn_in_y_order(true)
    sol.timer.start(map, 600 * i + math.random(0, 200), function()
      map:remove_cultist(cultist)
    end)
  end
  sol.timer.start(map, 6000,function()
    local eye_rune_sprite = seance_focus_target:create_sprite("effects/eye_rune_large")
    sol.audio.play_sound("scenes/seance_eye_summon")
    local leader_sprite = cultist_leader:get_sprite()
    sol.timer.start(map, 300, function()
      leader_sprite:set_animation("walking")
      local m = sol.movement.create"path"
      m:set_path({6,6,6,6})
      m:set_ignore_obstacles(true)
      m:start(cultist_leader, function()
        leader_sprite:set_animation"raised_arms"
        sol.timer.start(map, 1000, function()
          local x, y, z = cultist_leader:get_position()
          local sunbeam = map:create_custom_entity{
            x=x, y=y + 2, layer=z, width = 16, height = 16, direction = 0,
            sprite = "items/silverbeam",
          }
          sunbeam:set_drawn_in_y_order(true)
          sunbeam:get_sprite():set_scale(1, 2)
          sol.audio.play_sound"bot_beam"
          leader_sprite:set_animation("conducting")
          sol.timer.start(map, 1500, function()
            eye_rune_sprite:set_animation("closing")
            sol.timer.start(map, 1000, function()
              cultist_leader:blood_splatter(40)
              sol.audio.play_sound"enemy_killed"
              leader_sprite:set_animation("melting")
              sol.timer.start(map, 2000, function()
                map:stop_camera_tracking()
                hero:unfreeze()
              end)
            end)
          end)

        end)
      end)
    end)
  end)
end--]]

--[[
function seance_sensor:on_activated()
  if game:get_value("demo_1_chapel_seance_observed") then return end
  game:set_value("demo_1_chapel_seance_observed", true)
  seance_sensor:remove()
  --Cutscene:
  map:start_coroutine(function()
    local camera = map:get_camera()
    hero:freeze()
    camera:scroll_to_entity(seance_focus_target)
    wait(300)
    for i = 1, 4 do
      local cultist = map:get_entity("cultist_" .. i)
      cultist:set_drawn_in_y_order(true)
      sol.timer.start(map, 600 * i + math.random(0, 200), function()
        map:remove_cultist(cultist)
      end)
    end
    wait(6000)
    local eye_rune_sprite = seance_focus_target:create_sprite("effects/eye_rune_large")
    sol.audio.play_sound("scenes/seance_eye_summon")
    local leader_sprite = cultist_leader:get_sprite()
    wait(1000)
    leader_sprite:set_animation("walking")
    local m = sol.movement.create"path"
    m:set_path({6,6,6,6})
    m:set_ignore_obstacles(true)
    movement(m, cultist_leader)
    leader_sprite:set_animation("raised_arms")
    wait(1000)
    local x, y, z = cultist_leader:get_position()
    local sunbeam = map:create_custom_entity{
      x=x, y=y + 2, layer=z, width = 16, height = 16, direction = 0,
      sprite = "items/silverbeam",
    }
    sunbeam:set_drawn_in_y_order(true)
    sunbeam:get_sprite():set_scale(1, 2)
    sol.audio.play_sound"scenes/silver_flash"
    leader_sprite:set_animation("conducting")
    wait(1500)
    animation(eye_rune_sprite, "closing")
    wait(100)
    dialog("npcs.demo_1.seance_leader")
    wait(500)
    sol.timer.start(cultist_leader, 0, function()
      cultist_leader:blood_splatter(40)
    end)
    sol.audio.play_sound"enemy_killed"
    leader_sprite:set_animation("melting")
    wait(2000)
    map:stop_camera_tracking()
    hero:unfreeze()
  end)
end
--]]


function map:remove_cultist(cultist, callback)
  local sprite = cultist:get_sprite()
  --create warp circle
  local x, y, z = cultist:get_position()
  local circle = map:create_custom_entity({
    x=x, y= y - 1, layer=z, width=16, height=16, direction=0,
    sprite = "npc/seance_cultists/magic_circle",
  })
  circle:set_drawn_in_y_order(false)
  sol.audio.play_sound("scenes/seance_portal")
  sprite:set_animation("floating")
  sol.timer.start(cultist, 2500, function()
    --cultist:blood_splatter()
    sol.audio.play_sound"fireball_2"
    sol.audio.play_sound"bell_wave"
    sprite:set_animation("glowing", function()
      cultist:create_sprite("npc/seance_cultists/tentacle_burst")
      sprite:set_animation("burning", function()
        cultist:remove()
        circle:get_sprite():set_animation("closing", function()
          circle:remove()
          if callback then callback() end
        end)
      end)
    end)
  end)
end
