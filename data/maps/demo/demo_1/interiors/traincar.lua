local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_darkness_level("interior_1")
  game:refill_respawn_items()

  game:set_hud_enabled(false)

  --Sound loop
  local trustle_timer = sol.timer.start(map, 10, function()
    sol.audio.play_sound("ambiance/train_loop")
    return 4500
  end)
  trustle_timer:set_suspended_with_map(false)
  sol.audio.play_sound"train_steam_whistle_01"

  --Window flash
  local ripple_delay = 70
  local flash_frequency = 2000
  for i = 1, 7 do
    local sprite = map:get_entity("train_window_" .. i):get_sprite()
    sprite:set_animation"stopped"
    local flash_timer = sol.timer.start(map, ripple_delay * i, function()
      sprite:set_animation("flashing", function()
        sprite:set_animation("stopped")
      end)
      local glow_sprite = map:get_entity("window_glow_" .. i):get_sprite()
      glow_sprite:set_animation("off")
      sol.timer.start(map, ripple_delay, function() glow_sprite:set_animation("stopped") end)
      return flash_frequency
    end)
    flash_timer:set_suspended_with_map(false)
  end

  hero:set_animation"traincar_sleeping"
end)


local function timeskip_title()
  --X Years Later title card:
  local menu = {}
  local screen_width, screen_height = sol.video.get_quest_size()
  local txt_surface = sol.text_surface.create{
    font = "OldNewspaperTypes",
    font_size = 32,
    vertical_alignment = "middle",
    horizontal_alignment = "center",
    rendering_mode = "antialiasing",
  }
  txt_surface:set_text_key("demo.ten_years_later")
  function menu:on_draw(dst)
    txt_surface:draw(dst, screen_width / 2, (screen_height / 4) * 3 )
  end

  sol.menu.start(map, menu)
  sol.timer.start(map, 3000, function()
    sol.menu.stop(menu)
  end)
end


map:register_event("on_opening_transition_finished", function()
  local sit_alone_time = 4000
  game:set_hud_enabled(false)
  hero:freeze()
  hero:set_animation"traincar_sleeping"
  sol.timer.start(map, 600, function()
    hero:set_animation"traincar_waking_up"
    sol.audio.play_sound"scenes/wakeup_gasp"
  end)
  sol.timer.start(map, 600, function()
    timeskip_title()
  end)
  sol.timer.start(map, sit_alone_time, function()
    sol.audio.play_sound("fabric_rustle")
    sol.timer.start(map, 800, function() sol.audio.play_sound("scenes/page_turn") end)
    hero:set_animation("traincar_reading_letter")
    sol.timer.start(map, 2000, function()
      game:start_dialog("scenes.demo_1.traincar.letter", function()
        hero:set_animation("sitting")
        sol.timer.start(map, 2000, function()
          hero:teleport("demo/demo_1/interiors/tombwater_station", "from_train")
        end)
      end)
    end)
  end)

end)


