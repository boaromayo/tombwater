local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  if not game:get_value("demo_1_arrived") then
    sol.audio.stop_music()
    game:set_hud_enabled(false)
    game:start_flash({0,0,0}, 0)
    hero:freeze()
  end

  map:set_darkness_level("interior_1")
  map:set_fog("dust")

  arms_guy:setup()

  sol.timer.start(map, 300, function()
    if amos:get_distance(hero) <= 100 then
      amos:get_sprite():set_direction(amos:get_direction4_to(hero))
    else
      amos:get_sprite():set_direction(3)
    end
    return true
  end)
end)


map:register_event("on_opening_transition_finished", function()
    --Arrival at station effect:
    if not game:get_value("demo_1_arrived") then
      game:set_value("demo_1_arrived", true)
      map:show_arrival_cutscene()
    end
end)


function map:show_arrival_cutscene()
  hero:freeze()
  sol.audio.play_sound("station_arrival_short")
  sol.timer.start(map, 3000, function()
    game:stop_flash(70)
    sol.timer.start(map, 3000, function()
      local m = sol.movement.create("straight")
      m:set_speed(40)
      m:set_angle(math.pi/2 * 3)  
      m:set_max_distance(56)
      hero:set_animation("walking")
      m:start(hero, function()
        hero:set_animation("stopped")
        sol.audio.play_music("tombwater_station_test_01")
        sol.timer.start(map, 1000, function()
          game:set_hud_enabled(true)
          game:add_exp(200) --good job on the tutorial, now learn to level up
          game:save_checkpoint(hero:get_position()) --in case they ignore the campfire and die
          game:save()
          hero:unfreeze()
        end)
      end)
    end)
  end)
end


function tutorial_campfire_sensor:on_activated()
  game:start_tutorial("campfire")
end



function amos:on_interaction()
  local counter = game:get_value("demo_1_amos_dialog_counter")
  if counter == nil then
    game:start_dialog("npcs.demo_1.amos.1")
    counter = 2
  elseif game:get_value("demo_1_visited_addled_tracks") and not game:get_value("demo_1_amos_addled_tracks_dialog") then
    game:set_value("demo_1_amos_addled_tracks_dialog", true)
    game:start_dialog("npcs.demo_1.amos.addled_tracks")
  elseif counter == 2 then
    game:start_dialog("npcs.demo_1.amos.2")
    counter = "2b"
  elseif counter == "2b" and not game:has_item("gear/hookcaster") then
    game:start_dialog("npcs.demo_1.amos.2b")
  elseif counter == "2b" and game:has_item("gear/hookcaster") then
    game:start_dialog("npcs.demo_1.amos.3")
    counter = 4
  elseif counter == 4 then
    game:start_dialog("npcs.demo_1.amos.4")
  end
  game:set_value("demo_1_amos_dialog_counter", counter)
end


--Weird Arms Merchant---------------------------------------------------------------------------------------
local function do_shop()
  sol.audio.play_sound("vox_barks/arms_guy_open_shop_0" .. math.random(1, 3))
  local shop_menu = game:open_shop("demo_station_shop", {
    {id = "consumables/remedial_mushroom", price = 15}, --item_id and price are required
    {id = "consumables/cure_poison", price = 8},
    {id = "collectibles/bandolier_strap", price = 100, quantity = 1},
    {id = "inventory/dart", price = 35, quantity = 1},
    {id = "spells/tombstone", price = 45, quantity = 1},
    {id = "charms/flamestep", price = 55, quantity = 1},
    {id = "guns/rifle", price = 50, quantity = 1},
    {id = "collectibles/healing_flask_silverdust", price = 25, quantity = 1},
  })

  shop_menu:register_event("on_finished", function()
    sol.audio.play_sound("vox_barks/arms_guy_goodbye")
  end)
end

function arms_guy:setup()
  --check for player to be nearby:
  sol.timer.start(arms_guy, 100, function()
    arms_guy:check_hero()
    return true
  end)
  --Blink
  local sprite = arms_guy:get_sprite()
  sol.timer.start(arms_guy, 100, function()
    if sprite:get_animation() == "hidden" then
      sprite:set_animation(arms_guy.visible and "blinking" or "hidden_blinking")
      sol.timer.start(arms_guy, 200, function()
        sprite:set_animation(arms_guy.visible and "stopped" or "hidden")
      end)
    end
    return math.random(1000, 7000)
  end)
end

function arms_guy:hide()
  local sprite = arms_guy:get_sprite()
  sprite:set_animation("hiding", function() sprite:set_animation("hidden") end)
end

function arms_guy:show_self()
  local sprite = arms_guy:get_sprite()
  sprite:set_animation("emerging", function()
    sprite:set_animation("stopped")
    --[[sprite:set_animation("waving")
    sol.timer.start(arms_guy, 600, function()
      sprite:set_animation("stopped")
    end) --]]
  end)
  sol.audio.play_sound("vox_barks/arms_guy_greeting_0" .. math.random(1, 2))
end

function arms_guy:check_hero()
  local threshold = 64
  local distance = arms_guy:get_distance(hero)
  if (distance > threshold) and arms_guy.visible then
    arms_guy.visible = false
    arms_guy:hide()
  elseif (distance < threshold) and not arms_guy.visible then
    arms_guy.visible = true
    arms_guy:show_self()
  end
end

function arms_guy:on_interaction()
  game:start_dialog("npcs.demo_1.weird_arms_shopkeeper", function()
    do_shop()
  end)
end
