local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level("interior_1")
  map:set_fog("dust")
end)

function hook_tutorial_sensor:on_activated()
  if game:has_item("gear/hookcaster") then
    self:remove()
    game:start_tutorial("hookshot")
  end
end

