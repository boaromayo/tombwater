local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level("interior_1")
  map:set_fog("dust")
end)


function gravedigger:on_interaction()
  local count = game:get_value("demo_1_gravedigger_dialog_counter")
  if count == nil then
    game:start_dialog("npcs.demo_1.gravedigger.1")
    count = 2
  elseif count == 2 and not game:get_value("demo_1_graveyard_sentinel") then
    game:start_dialog("npcs.demo_1.gravedigger.2")
  elseif count == 2 and game:get_value("demo_1_graveyard_sentinel") then
    game:start_dialog("npcs.demo_1.gravedigger.3")
    count = 4
  elseif count == 4 then
    game:start_dialog("npcs.demo_1.gravedigger.4")
  end
  game:set_value("demo_1_gravedigger_dialog_counter", count)
end

