local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level"mine"
  map:set_fog("dust")

  for beam in map:get_entities("light_beam") do
    beam:pulse_opacity{
      min = 25, max = 200, starting_opacity = math.random(25, 200), step = 4
    }
  end

  game:start_flash({0,0,0}, 0)
  hero:set_visible(false)
  game:set_hud_enabled(false)
end)



map:register_event("on_opening_transition_finished", function()
  local fade_time = 40
  local level_look_duration = 3500
  local presound_delay = 500
  local fade_out_duration = 2000

  hero:freeze()

  local horn_i = 1
  local function show_level(target_entity, callback)
    hero:set_position(target_entity:get_position())
    game:stop_flash(fade_time)
    sol.timer.start(map, presound_delay, function()
      sol.audio.play_sound("scenes/god_horn_far_0" .. horn_i)
      horn_i = horn_i + 1
    end)
    sol.timer.start(map, level_look_duration, function()
      game:start_flash({0,0,0}, fade_time)
      sol.timer.start(map, fade_out_duration, function()
        callback()
      end)
    end)
  end

  show_level(level_1, function()
    show_level(level_2, function()
      hero:set_position(level_3:get_position())
      game:stop_flash(fade_time)
      local m = sol.movement.create"straight"
      m:set_ignore_obstacles(true)
      m:set_speed(120)
      m:set_angle(math.pi / 2)
      m:set_max_distance(216)
      m:start(hero)
      sol.timer.start(map, 1600, function()
        sol.audio.play_sound("scenes/god_horn")
        local eye_sprite = door_eye:get_sprite()
        eye_sprite:set_animation("eye")
        sol.timer.start(map, 1700, function()
          game:start_flash({0,0,0}, 0)
          sol.timer.start(map, 200, function()
            map:end_demo()
          end)
        end)

      end)
    end)
  end)

end)



function map:end_demo()
  local menu = {}
  local screen_width, screen_height = sol.video.get_quest_size()
  local txt_surface = sol.text_surface.create{
    font = "OldNewspaperTypes",
    font_size = 32,
    vertical_alignment = "middle",
    horizontal_alignment = "center",
  }
  txt_surface:set_text_key("demo.to_be_continued")
  function menu:on_draw(dst)
    txt_surface:draw(dst, screen_width / 2, screen_height / 2 )
  end

  sol.menu.start(sol.main, menu)
  sol.timer.start(sol.main, 5000, function()
    sol.main.reset()
  end)

end

