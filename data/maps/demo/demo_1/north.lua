local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level("town_day")
end)


--Fogs:
for sensor in map:get_entities("fog_activate_sensor") do
  if not map.area_fogs then map.area_fogs = {} end
  local fog_type = sensor:get_property("fog_type")
  function sensor:on_activated()
    if map.area_fogs[fog_type] then return end
    map.area_fogs[fog_type] = map:fade_in_fog(fog_type)
  end
end

for sensor in map:get_entities("fog_deactivate_sensor") do
  function sensor:on_activated()
    local fog_type = sensor:get_property("fog_type")
    if map.area_fogs[fog_type] then
      map:fade_out_fog(map.area_fogs[fog_type])
      map.area_fogs[fog_type] = nil
    end
  end
end


--Adelaide Encounter:
function adelaide_encounter_sensor:on_activated()
  if game:get_value("demo_1_scrublands_adelaide_encounter") then return end
  game:set_value("demo_1_scrublands_adelaide_encounter", true)

  map:start_cutscene()
  --Vines
  for vine in map:get_entities("adelaide_vines") do
    sol.timer.start(map, math.random(0,500), function()
      sol.audio.play_sound"grass_rustling"
      vine:set_enabled()
      local vine_sprite = vine:get_sprite()
      vine_sprite:set_animation("growing", function()
        vine_sprite:set_animation("flowering", function() vine_sprite:set_animation("flowered") end)
      end)
    end)
  end
  sol.timer.start(map, 2000, function()
    hero:set_animation("walking")
    local m = sol.movement.create"target"
    m:set_target(adelaide_encounter_target)
    m:start(hero, function()
      hero:set_animation"stopped"
      hero:set_direction(1)
      sol.timer.start(map, 2000, function()
        --Adelaide appears
        adelaide.light_source_sprite = "entities/effects/light_l"
        adelaide:set_enabled(true)
        map:check_dynamic_light_sources()
        --Particle Effects
        local em
        local x, y, z = adelaide:get_position()
        em = map:create_particle_emitter(x, y - 28, z, "smoke")
        em.particle_sprite = "effects/adelaide_smoke"
        em.particle_scaling = {1, 1.5}
        em.width = 10
        em:emit()
        --Abberation
        local ab_shader = sol.shader.create("chromatic_aberration")
        ab_shader:set_uniform("aberration_amount", 8)
        local cam_surface = map:get_camera():get_surface()
        cam_surface:set_shader(ab_shader)
        sol.timer.start(map, 100, function()
          cam_surface:set_shader(nil)
        end)
        map:screenshake()
        sol.audio.play_sound("scenes/god_horn")
        sol.audio.play_sound("scenes/silver_flash")
        sol.timer.start(map, 2000, function()
          game:start_dialog("npcs.demo_1.adelaide.scrubland_encounter", function()
            sol.timer.start(map, 400, function()
             --Aberration again:
              local ab_shader = sol.shader.create("chromatic_aberration")
              ab_shader:set_uniform("aberration_amount", 8)
              local cam_surface = map:get_camera():get_surface()
              cam_surface:set_shader(ab_shader)
              sol.timer.start(map, 100, function()
                cam_surface:set_shader(nil)
              end)
              map:screenshake()
              --sol.audio.play_sound("scenes/god_horn")
              sol.audio.play_sound("scenes/silver_flash")
              adelaide:set_enabled(false)
              em:remove()
              map:stop_cutscene()
            end)
          end)
        end)
      end)
    end)
  end)
end


--Boss
function boss_sensor:on_activated()
  boss_sensor:remove()
  if game:get_value("central_tombwater_deputy_boss") then return end
  local camera = map:get_camera()
  map:start_cutscene()
  game:set_suspended(true)
  local m = sol.movement.create("target")
  m:set_target(camera:get_position_to_track(boss_intro_cutscene_target))
  --m:set_ignore_obstacles(true)
  m:start(camera, function()
    game:start_dialog("npcs.demo_1.deputy.1", function()
      local deputy_sprite = deputy_boss:get_sprite()
      deputy_sprite:set_direction(3)
      sol.timer.start(map, 500, function()
        game:start_dialog("npcs.demo_1.deputy.2", function()
          local m2 = sol.movement.create"straight"
          m2:set_angle(3 * math.pi / 2)
          m2:set_max_distance(32)
          m2:set_ignore_obstacles(true)
          m2:start(camera, function()
            map:close_doors("boss_door")
            deputy_boss:start_boss_bar()
            deputy_boss:start_aggro()
            sol.audio.play_music("boss_fight_test_02")
            map:stop_cutscene()
            game:set_suspended(false)
          end)
        end)
      end)
    end)
  end)

end



--Boss Death
function map:on_boss_dead(breed) --open doors after defeat
  --Miniboss
  if breed == "tombwater_enemies/boss/deputy_beast" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music("ominous_wind")
    hero:freeze()
    sol.timer.start(map, 1000, function()
      local m = sol.movement.create"straight"
      m:set_max_distance(96)
      m:set_angle(math.pi / 2)
      m:set_ignore_obstacles(true)
      m:start(map:get_camera(), function()
        game:start_flash({0,0,0}, 80)
        sol.timer.start(map, 3000, function()
          map:get_camera():start_tracking(hero)
          hero:teleport("demo/demo_1/interiors/mine")
        end)
      end)
    end)
  end
end




