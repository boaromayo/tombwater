local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level("town_day")
end)


map:register_event("on_opening_transition_finished", function()
  sol.audio.play_music("boss_fight_test_02")
  boss:start_boss_bar()
  sol.timer.start(map, 1000, function()
    boss:start_aggro()
  end)
end)

function map:on_boss_dead(breed)
  sol.audio.play_sound("a_chord")
  sol.audio.play_music("ominous_wind")
  game:start_flash({255,255,255}, 30)
  sol.timer.start(map, 2000, function()
    hero:teleport("demo/demo_1/boss_rush/exit", "destination", "immediate")
    game:stop_flash()
  end)
end

