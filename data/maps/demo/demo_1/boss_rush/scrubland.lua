local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level("town_day")

  --Adjust bull health:
  boss:set_life(450)
end)

require("maps/demo/demo_1/boss_rush/rush_functions"):init(map, "demo/demo_1/boss_rush/deputy")


boss:register_event("on_dying", function()
  boss:boss_death("stopped")
end)
