local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level"mine"
  map:set_fog("dust")

  for beam in map:get_entities("light_beam") do
    beam:pulse_opacity{
      min = 25, max = 200, starting_opacity = math.random(25, 200), step = 4
    }
  end

  game:set_value("tutorial_watched_flask", true)
  game:set_value("tutorial_watched_death", true)
  game:set_value("tutorial_watched_charms", true)
  game:set_value("tutorial_watched_weapon", true)
  game:set_value("tutorial_watched_spells", true)

  game:get_item("solforge/pickaxe"):set_variant(1)
  game:get_item("solforge/surgeons_knife"):set_variant(1)
  game:get_item("spells/cinder"):set_variant(1)
  game:get_item("spells/arcane_bolt"):set_variant(1)
  game:get_item("spells/tombstone"):set_variant(1)
  game:get_item("inventory/grenade"):set_variant(1)
  game:get_item("inventory/homeward_talisman"):set_variant(0)

  game:refill_respawn_items()
  game:save_checkpoint(hero:get_position())
end)