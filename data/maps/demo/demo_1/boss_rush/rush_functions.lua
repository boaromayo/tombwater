local manager = {}

function manager:init(map, next_map)
  local game = map:get_game()
  local hero = map:get_hero()
  local boss = map:get_entity"boss"

  map:register_event("on_opening_transition_finished", function()
    boss:start_boss_bar()
    sol.timer.start(map, 1000, function()
      boss:start_aggro()
    end)
    sol.audio.play_music("boss_fight_test_03")
  end)

  function map:on_boss_dead(breed)
    --Warp to next boss in rush:
    game:start_flash({255,255,255}, 30)
    sol.timer.start(map, 2000, function()
      hero:teleport(next_map, "destination", "immediate")
      game:stop_flash()
      game:set_life(game:get_max_life())
    end)
  end

end

return manager
