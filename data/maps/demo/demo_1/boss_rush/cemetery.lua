local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("canyon_mist")
  map:set_darkness_level("town_day")
  boss:set_life(950)
end)


require("maps/demo/demo_1/boss_rush/rush_functions"):init(map, "demo/demo_1/boss_rush/scrubland")


--Make Graveyard Sentinel into boss:
boss:register_event("on_dying", function()
  boss:boss_death("jumping_crouch")
end)


