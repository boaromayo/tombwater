local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level"mine"
  map:set_fog("dust")

  for beam in map:get_entities("light_beam") do
    beam:pulse_opacity{
      min = 25, max = 200, starting_opacity = math.random(25, 200), step = 4
    }
  end
end)

function exit_sensor:on_activated()
  hero:freeze()
  sol.audio.play_sound"scenes/god_horn"
  local eye_sprite = door_eye:get_sprite()
  eye_sprite:set_animation("eye")
  sol.timer.start(map, 1700, function()
    game:start_flash({0,0,0}, 0)
    sol.timer.start(map, 2000, function()
      game:start_dialog("game.demo.boss_rush_complete", function()
        game:stop_flash()
        sol.main.reset()
      end)
    end)
  end)

  
end

