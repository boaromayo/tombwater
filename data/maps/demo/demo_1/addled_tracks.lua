local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level("town_day")

end)


function east_teleporter:on_activated()
  self:set_enabled(false)
  game:set_value("demo_1_visited_addled_tracks", true)
  hero:teleport("demo/demo_1/north", "from_addled_tracks")
end

local function loop_map()
  map.times_looped = (map.times_looped or 0) + 1
end
north_tele.on_activated = loop_map
west_tele.on_activated = loop_map
south_tele.on_activated = loop_map

