local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("fog")
  map:set_fog("canyon_mist")
  map:set_darkness_level({170,220,255})
end)
