local map = ...
local game = map:get_game()

local fog_vignette = require("scripts/fx/vignette_fog")

map:register_event("on_started", function()
  map:set_darkness_level{200,200,240}
  if not sol.menu.is_started(fog_vignette) then sol.menu.start(map, fog_vignette) end
  map:set_fog("heavy_mist")

end)




--Doppelganger
function doppel_show_sensor:on_activated()
  if game:get_value("bleakpool_woods_mirror_shard") then return end --disable this event once you already have this mirror shard
  if doppelganger:exists() then
    doppelganger:set_enabled(true)
  end
end

function doppel_hide_sensor:on_activated()
  if doppelganger:exists() then
    doppelganger:set_enabled(false)
  end
end

function doppel_scene_sensor:on_activated()
  if game:get_value("bleakpool_woods_mirror_shard") then return end --disable this event once you already have this mirror shard
  local m = sol.movement.create"path"
  m:set_path{2,2,2,2,2,2,2,2}
  m:start(doppelganger)
  mirror_shard:set_enabled(true)
end

