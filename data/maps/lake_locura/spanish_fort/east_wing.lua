local map = ...
local game = map:get_game()


map:register_event("on_started", function()

  --Reskin sniper:
  if sniper and sniper:exists() then
    sniper:set_life(140)
    sniper:remove_sprite()
    sniper:create_sprite("enemies/tombwater_enemies/bandit_sniper")
    sniper:get_sprite():set_direction(3)
  end

  map:set_doors_open("cell_door")
end)


function miniboss_sensor:on_activated()
  if miniboss and miniboss:exists() and miniboss:get_life() > 0 then
    map:close_doors("cell_door")
    miniboss.aggro = true
    miniboss:restart()
  end
end


if miniboss then
  miniboss:register_event("on_dead", function()
    map:open_doors("cell_door")
  end)
end
