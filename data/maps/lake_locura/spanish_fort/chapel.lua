local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_darkness_level(2)
  map:set_doors_open("boss_door")
  ichor_root_marker_top:set_visible(false)
end)


for switch in map:get_entities("boss_gate_switch") do
  function switch:on_activated()
    local num_flipped = game:get_value("spanish_fort_boss_gate_switches_activated") or 0
    num_flipped = num_flipped + 1
    game:set_value("spanish_fort_boss_gate_switches_activated", num_flipped)
    if num_flipped >= 2 then
      map:open_doors("boss_gate")
    end
  end
end


--Boss
function boss_sensor:on_activated()
  boss_sensor:remove()
  if game:get_value("spanish_fort_boss_defeated") then return end
  map:close_doors("boss_door")
  stilton_boss:start_boss_bar()
  stilton_boss:start_aggro()
  sol.audio.play_music("boss_fight_test_02")
end

function map:on_boss_dead(breed)
  if breed == "tombwater_enemies/boss/stilton" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music("ominous_wind")
  end
end

