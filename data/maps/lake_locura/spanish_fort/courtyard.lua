local map = ...
local game = map:get_game()


map:register_event("on_started", function(map, destination)
  for zone in map:get_entities("sniper_zone") do
    zone:set_visible(false)
  end
  if destination and destination:get_name() == "from_sniper_tower_r" then
    sniper.is_hiding = true
    sniper:set_enabled(false)
  else
    map:start_sniper()
  end
end)


--Sniper:
function map:start_sniper()
  if game:get_value("spanish_fort_courtyard_sniper_state") then
    sniper:remove()
    return
  end

  local hero = map:get_hero()
  local frequency = 5000
  sol.timer.start(map, frequency, function()
    if not sniper then return end
    if sniper.is_hiding then return frequency end --if he's run up into his room, don't shoot
    if not sniper:exists() then return false end --if he's gone end the timer
    local sniper_sprite = sniper:get_sprite()
    sniper_sprite:set_direction(sniper:get_direction4_to(hero))
    sniper_sprite:set_animation("aiming")
    local overlaps = false
    for zone in map:get_entities("sniper_zone") do
      if hero:overlaps(zone) then overlaps = true end
    end
    if overlaps then
      --Shoot:
      sol.audio.play_sound("gunshot_sniper")
      sol.audio.play_sound("gunshot_pistol-big")
      map:sniper_shoot()
    end
    return frequency
  end)
end

function map:sniper_shoot()
  local damage = 35
  local x, y, z = sniper:get_position()
  local bullet = map:create_custom_entity{
    x=x, y=y, layer=z, width=16, height=16, direction = 0,
    sprite = "entities/enemy_projectiles/bullet",
    model = "enemy_projectiles/generic_projectile",
  }
  bullet:set_projectile_type("bullet")
  bullet.ignore_obstacles = true
  bullet.damage = damage
  bullet.speed = 500
  bullet.max_distance = 1000
  bullet:shoot(sniper:get_angle(hero))
end

function sniper_flee_sensor:on_activated()
  if sniper and not sniper.is_hiding then
    sniper.is_hiding = true
    local sniper_sprite = sniper:get_sprite()
    sniper_sprite:set_animation"walking"
    local m = sol.movement.create"straight"
    m:set_ignore_obstacles(true)
    m:set_speed(90)
    m:set_angle(math.pi / 2)
    m:set_max_distance(16)
    m:start(sniper, function()
      sniper:remove()
    end)
  end
end

