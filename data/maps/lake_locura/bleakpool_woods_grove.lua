local map = ...
local game = map:get_game()

local fog_vignette = require("scripts/fx/vignette_fog")

map:register_event("on_started", function()
  map:set_darkness_level{200,200,240}
  if not sol.menu.is_started(fog_vignette) then sol.menu.start(map, fog_vignette) end
  map:set_fog("heavy_mist")

end)

