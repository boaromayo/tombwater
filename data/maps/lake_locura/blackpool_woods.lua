local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_darkness_level{200,200,255}
  sol.menu.start(map, require("scripts/fx/vignette_fog"))
  map:set_fog("heavy_mist")

end)

