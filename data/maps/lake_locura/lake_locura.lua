local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("light_mist")
end)


local leech_awake_sensors = {}
for sensor in map:get_entities("leech_awake_sensor") do
  table.insert(leech_awake_sensors, sensor)
  function sensor:on_activated()
    for e in map:get_entities("ambush_leech") do
      e:stop_hibernating()
    end
    for _, s in pairs(leech_awake_sensors) do
      s:remove()
    end
  end
end

function church_hunter_aggro_sensor:on_activated()
  if church_hunter_miniboss then church_hunter_miniboss:start_aggro() end
  church_hunter_aggro_sensor:remove()
end
