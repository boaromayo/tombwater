local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level(1)
  map:set_fog"dust"
end)


function spider_door_switch:on_activated()
  map:open_doors"spider_door"
end
