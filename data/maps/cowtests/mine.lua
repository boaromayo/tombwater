local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level"mine"
  map:set_fog("dust")

  for beam in map:get_entities("light_beam") do
    beam:pulse_opacity{
      min = 25, max = 200, starting_opacity = math.random(25, 200), step = 4
    }
  end

end)



map:register_event("on_opening_transition_finished", function()

end)


