local map = ...
local game = map:get_game()



--Fog in the forest:
for sensor in map:get_entities("forest_fog_activate_sensor") do
  function sensor:on_activated()
    if map.forest_fogs then return end
    map.forest_fogs = map:fade_in_fog("forest")
  end
end

for sensor in map:get_entities("forest_fog_deactivate_sensor") do
  function sensor:on_activated()
    if map.forest_fogs then
      map:fade_out_fog(map.forest_fogs)
      map.forest_fogs = nil
    end
  end
end
