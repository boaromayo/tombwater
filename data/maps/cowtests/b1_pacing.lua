local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level{240,230,205}
end)

