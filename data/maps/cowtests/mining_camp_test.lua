local map = ...
local game = map:get_game()


map:register_event("on_started", function()

end)


--Boss
function boss_sensor:on_activated()

  boss_sensor:remove()
  if game:get_value("central_tombwater_deputy_boss") then return end
  map:close_doors("boss_door")
  deputy_boss:start_boss_bar()
  deputy_boss:start_aggro()
  sol.audio.play_music("boss_fight_test_02")
end

function map:on_boss_dead(breed)
  if breed == "tombwater_enemies/boss/deputy_beast" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music(map:get_music())
  end
end


function map:run_test()
  local name
  for enemy in map:get_entities_by_type("enemy") do
    name = enemy:get_name()
  end
  print("Name is:", name)
  local selected_enemy = map:get_entity(name)
  assert(selected_enemy, "How did this happen?")
end
