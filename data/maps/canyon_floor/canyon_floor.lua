local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_fog("clouds")
  map:set_darkness_level{240,230,205}

  if game:get_value("canyon_floor_gunsmith_cellar_visited") then
    gunsmith_vine_door_1:remove()
    gunsmith_vine_door_2:remove()
  end
end)


--Doppelganger
function doppel_scene_sensor:on_activated()
  if game:get_value("canyon_floor_mirror_shard") then return end --disable this event once you already have this mirror shard
  sol.timer.start(map, 10, function()
    local m = sol.movement.create"path"
    m:set_speed(110)
    m:set_path{2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}
    m:set_ignore_obstacles(true)
    m:start(doppelganger, function()
      doppelganger:remove()
    end)
    mirror_shard:set_enabled(true)
  end)
end