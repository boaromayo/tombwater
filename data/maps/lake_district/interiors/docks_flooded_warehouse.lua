local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level("interior_1")
  --Unlock door once you've been inside:
  game:set_value("lake_district_docks_flooded_warehouse_door_state", true)
end)
