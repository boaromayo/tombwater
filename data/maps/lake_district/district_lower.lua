local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")

end)


--Ichorcyst Transformation:
function ichorcyst_townsfolk_sensor:on_activated()
  self:remove()
  if transforming_ichorcyst then
    transforming_ichorcyst:start_transformation()
    transforming_ichorcyst.aggro = true
  end
end

--Ichor Mouth ambush:
function ambush_mouth:ambush_attack()
  
  ambush_mouth:set_enabled(true)
  ambush_mouth:immobilize(5000)
  ambush_mouth:get_sprite():set_animation"falling"
  sol.audio.play_sound"skid"
  sol.audio.play_sound"enemies/deputy_beast_scream_short"
  local m = sol.movement.create"straight"
  m:set_angle(3 * math.pi / 2)
  --m:set_direction8(6)
  m:set_speed(400)
  m:set_max_distance(160)
  m:set_ignore_obstacles(true)
  m:start(ambush_mouth, function()
    local x, y, z = ambush_mouth:get_position()
    local aoe = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=16, height=16,
      model = "enemy_projectiles/general_attack",
      sprite = "items/shockwave_8x7",
    }
    aoe:remove_after_animation()
    aoe:activate_collision()
    sol.audio.play_sound"running_obstacle"
    map:screenshake()
    if ambush_mouth_victim then ambush_mouth_victim:process_hit(999, "physical") end
    ambush_mouth.aggro = true
    ambush_mouth:melee_attack(ambush_mouth.lash_attack)
  end)
end

