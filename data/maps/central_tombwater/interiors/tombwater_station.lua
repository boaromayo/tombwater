local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level{210,210,190}


  --Emmaline Shrubbs:
  if not game:get_value("central_tombwater_rescued_apothecary_woman") then
    emmaline:set_enabled(false)
    for entity in map:get_entities"apothecary_herb" do
      entity:set_enabled(false)
    end
  end

  --Carolina
  if not game:get_value("central_tombwater_rescued_station_merchant") then
    carolina:set_enabled(false)
    for entity in map:get_entities"carolina_" do
      entity:set_enabled(false)
    end
  end

  --Theseus
  if not game:get_value("theseus_dialog_counter") then
    theseus:set_enabled(false)
    theseus_pack:set_enabled(false)
  end

end)




--Merchant
local function do_shop()
  game:open_shop("test_shop", {
    {id = "consumables/remedial_mushroom", price = 25,}, --item_id and price and required
    {id = "consumables/cure_burn", price = 20,},
    {id = "consumables/cure_poison", price = 20,},
    {id = "collectibles/ghost_seed", price = 300, quantity = 1,},
    {id = "materials/crafting/flask", price = 500, quantity = 1,},
  }) 
end

function carolina:on_interaction()
  if not game:get_value("carolina_dialog_counter") then
    game:set_value("carolina_dialog_counter", 1)
    game:start_dialog("npcs.central_tombwater.carolina.3", function()
      do_shop()
    end)
  elseif game:get_value("carolina_dialog_counter") < 2 then
    game:start_dialog("npcs.central_tombwater.carolina.4", function()
      do_shop()
    end)
  end
end


--Emmaline Shrubs
function emmaline:on_interaction()
  if game:get_value("emmaline_dialog_counter") < 2 then
    game:start_dialog("npcs.central_tombwater.emmaline.3", function()
      game:open_upgrades_shop()
    end)
    game:set_value("emmaline_dialog_counter", 2)
  else
    game:start_dialog("npcs.central_tombwater.emmaline.shop", function()
      game:open_upgrades_shop()
    end)
  end
end


--Theseus Watts
local function do_sell_shop()
  game:open_sell_shop()
end

function theseus:on_interaction()
  if game:get_value("theseus_dialog_counter") == 1 then
    game:start_dialog("npcs.central_tombwater.theseus.3", function()
      do_sell_shop()
    end)

  end
end
