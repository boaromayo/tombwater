local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  --map:set_darkness_level{240,230,205}
  map:set_darkness_level{210,210,190}

  if game:get_value("central_tombwater_rescued_apothecary_woman") then
    emmaline:set_enabled(false)
  end
end)


function emmaline:on_interaction()
  if not game:get_value("emmaline_dialog_counter") then
    game:start_dialog"npcs.central_tombwater.emmaline.1"
    game:set_value("emmaline_dialog_counter", 1)
    game:set_value("central_tombwater_rescued_apothecary_woman", true)
  elseif game:get_value("emmaline_dialog_counter") == 1 then
    game:start_dialog"npcs.central_tombwater.emmaline.2"
  end
end

