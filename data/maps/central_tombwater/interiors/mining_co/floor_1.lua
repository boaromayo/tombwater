local map = ...
local game = map:get_game()

map:register_event("on_started", function()

  --Gate contraption
  _, map.contraption_initial_y = gate_contraption:get_position()
  if not game:get_value("mining_co_hq_gate_contraption_floor") then
    game:set_value("mining_co_hq_gate_contraption_floor", 1)
  end
  gate_contraption:set_drawn_in_y_order(true)
  local gcx, gcy, gcz = gate_contraption:get_position()
  gate_contraption:set_position(gcx, gcy + 2, gcz)
  --Set contraption to saved position
  map:set_contraption(game:get_value("mining_co_hq_gate_contraption_floor"))
  --Broken controls panel:
  if not game:get_value("mining_co_hq_gate_controls_repaired") then contraption_control_broken:get_sprite():set_animation"broken" end
  --Crane Load
  if game:get_value("mining_co_hq_crane_load_dropped") then
    crane_load:remove()
  end
end)





--Gate Contraption
local gate_contraption_parts = {
  gate_contraption,
  gate_contraption_wall_1, gate_contraption_wall_2, gate_contraption_wall_3, gate_contraption_wall_4, gate_contraption_wall_5, gate_contraption_wall_6, 
}
local floor_gap = 80

function map:move_contraption(target_floor)
  local camera = map:get_camera()
  local current_floor = game:get_value("mining_co_hq_gate_contraption_floor")
  local x, y, z = gate_contraption:get_position()
  local distance = (target_floor - current_floor) * floor_gap
  local direction = (distance > 0) and "up" or "down"

  if current_floor == target_floor then return end

  map.gate_contraption_moving = true
  for _, e in pairs(gate_contraption_parts) do
    local m = sol.movement.create"straight"
    m:set_angle(direction == "up" and math.pi / 2 or 3 * math.pi / 2)
    m:set_max_distance(math.abs(distance))
    m:set_ignore_obstacles(true)
    m:set_speed(90)
    m:start(e)
    function m:on_finished()
      map.gate_contraption_moving = false
      sol.timer.start(map, 200, function()
        camera:scroll_to(hero)
        hero:unfreeze()
      end)
    end
  end
  hero:freeze()
  camera:scroll_to(gate_contraption)

  game:set_value("mining_co_hq_gate_contraption_floor", target_floor)
end

function map:set_contraption(target_floor)
  local initial_y = map.contraption_initial_y
  local distance = (target_floor - 1) * floor_gap
  for _, e in pairs(gate_contraption_parts) do
    x,  y, z = e:get_position()
    e:set_position(x, y - distance, z)
  end
end

function map:select_contraption_floor()
  if map.gate_contraption_moving then return end
  game:start_dialog("map.central_tombwater.mining_co_hq.security_contraption_question", function(answer)
    map:move_contraption(answer)
  end)
end


--Set up gate contraption controls
for control in map:get_entities"contraption_control" do
  --For broken control
  if control == contraption_control_broken and not game:get_value("mining_co_hq_gate_controls_repaired") then
    function control:on_interaction()
      local gears_item = game:get_item("collectibles/misc/mining_co_hq_contraption_gear")
      --Broken, but enough gears:
      if gears_item:get_amount() >= 4 then
        game:start_dialog("map.central_tombwater.mining_co_hq.repair_contraption_controls", function(answer)
          if answer == 1 then
            contraption_control_broken:get_sprite():set_animation"stopped"
            sol.audio.play_sound"device_set"
            sol.audio.play_sound"switch"
            game:set_value("mining_co_hq_gate_controls_repaired", true)
            control.on_interaction = function() map:select_contraption_floor() end
          end
        end)
      --Broken, not eough gears
      else
        game:start_dialog("map.central_tombwater.mining_co_hq.broken_contraption_controls")
      end
    end

  --For working controls:
  else
    function control:on_interaction()
      map:select_contraption_floor()
    end
  end
end




--Crane switch
function crane_switch:on_activated()
  if not crane_load then return end
  sol.audio.play_sound"finger_snap"
  sol.audio.play_sound"tripwire"
  sol.audio.play_sound"hero_falls"
  local m = sol.movement.create"straight"
  m:set_speed(100)
  m:set_ignore_obstacles()
  m:set_angle(math.pi * 1.5)
  m:set_max_distance(24)
  m:start(crane_load)
  function m:on_finished()
    crane_load:remove()
  end
  sol.timer.start(map, 1000, function()
    sol.audio.play_sound"breaking_crate"
    sol.audio.play_sound"running_obstacle"
    map:screenshake()
  end)
  game:set_value("mining_co_hq_crane_load_dropped", true)
end

