local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  if game:get_value("mining_co_hq_crane_load_dropped") then
    for e in map:get_entities"smash_gate" do
      e:remove()
    end
  end
end)
