local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  --map:set_darkness_level{240,230,205} --outside level
  --map:set_darkness_level{190,190,170} --a bit too dark
  map:set_darkness_level{210,210,190}

  if game:get_value("theseus_dialog_counter") then
    theseus:set_enabled(false)
    theseus_pack:set_enabled(false)
  end
end)


function theseus:on_interaction()
  if game:get_value("theseus_dialog_counter") == nil then
    game:start_dialog("npcs.central_tombwater.theseus.1", function()
      game:set_value("theseus_dialog_counter", 1)
      game:open_sell_shop()
    end)
  elseif game:get_value("theseus_dialog_counter") == 1 then

  end
end

