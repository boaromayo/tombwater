local map = ...
local game = map:get_game()


map:register_event("on_started", function()
  map:set_fog("dust")
  map:set_darkness_level{240,230,205}

  if game:get_value("central_tombwater_rescued_station_merchant") then
    carolina:set_enabled(false)
    carolina_horse:set_enabled(false)
    for enemy in map:get_entities"carolina_enemy" do enemy:set_enabled(false) end
  end

  --We need to manually remove the boss if you've beat him - we can't save his state since he's a two-phase guy
  if game:get_value("central_tombwater_deputy_boss") then
    if not deput_boss then return end --it's possible the boss was removed just because we keep track of enemies killed
    deputy_boss:remove()
  end
end)


--Boss
function boss_sensor:on_activated()
  boss_sensor:remove()
  if game:get_value("central_tombwater_deputy_boss") then return end
  map:close_doors("boss_door")
  deputy_boss:start_boss_bar()
  deputy_boss:start_aggro()
  sol.audio.play_music("boss_fight_test_02")
end

function map:on_boss_dead(breed)
  if breed == "tombwater_enemies/boss/deputy_beast" then
    map:open_doors"boss_door"
    sol.audio.play_sound("a_chord")
    sol.audio.play_music("central_test_02")
  end
end


--Carolina
function carolina_scene_sensor:on_activated()
  carolina_scene_sensor:remove()
  if game:get_value("central_tombwater_rescued_station_merchant") then return end
  game:set_suspended(true)
  map:focus_on(carolina)
  game:start_dialog("npcs.central_tombwater.carolina.1-help")
end

for enemy in map:get_entities("carolina_enemy") do
  function enemy:on_dead()
    if not map:has_entities"carolina_enemy" then
      map:focus_on(carolina)
      game:start_dialog("npcs.central_tombwater.carolina.2-rescue")
      game:set_value("central_tombwater_rescued_station_merchant", true)
    end
  end
end
