local manager = {}

function manager:apply_melee(enemy)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()

  function enemy:melee_attack(props)
    local sprite = enemy:get_sprite()
    local windup_delay = props.windup_delay or 500
    local windup_animation = props.windup_animation
    local attack_animation = props.attack_animation
    local weapon_sprite = props.weapon_sprite
    local weapon_windup_animation = props.weapon_windup_animation
    local weapon_attack_animation = props.weapon_attack_animation
    local attack_sound = props.attack_sound or "sword1"
    local damage = props.damage or 1
    local damage_type = props.damage_type or "physical"
    local recovery_delay = props.recovery_delay or 500
    local next_action = props.next_action or (function() enemy:decide_action() end)

    enemy:stop_movement()
    local x, y, z = enemy:get_position()
    local direction = enemy:get_direction4_to(hero)
    sprite:set_direction(direction)
    sprite:set_animation(windup_animation)
    local attack_entity = map:create_custom_entity{
      x = x, y = y, layer = z, direction = direction,
      width = 16, height = 16,
      name = "enemy_melee_attack",
      sprite = weapon_sprite,
    }
    enemy.attack_entities[attack_entity] = attack_entity
    local attack_sprite = attack_entity:get_sprite()
    attack_sprite:set_animation(weapon_windup_animation)
    attack_sprite:set_direction(direction)

    sol.timer.start(enemy, windup_delay, function()
      sol.audio.play_sound(attack_sound)
      sprite:set_animation(attack_animation, "stopped")
      attack_sprite:set_animation(weapon_attack_animation)
      attack_entity:add_collision_test("sprite", function(attack_entity, other)
        if other:get_type() == "hero" and not hero:is_invincible() then
          attack_entity:clear_collision_tests()
          if hero.process_hit then
            hero:process_hit({damage = damage, enemy = enemy, damage_type = damage_type})
          else
            hero:start_hurt(enemy, damage)
          end
        end
      end)
      local attack_duration = sprite:get_num_frames(attack_animation, direction) * sprite:get_frame_delay(attack_animation)
      sol.timer.start(enemy, attack_duration + recovery_delay, function()
        enemy.attack_entities[attack_entity]:remove()
        next_action()
      end)
    end)
  end



  function enemy:melee_combo(attacks)
    for i = 1, #attacks - 1 do
      attacks[i].next_action = function()
        enemy:melee_attack(attacks[i + 1])
      end
    end
    enemy:melee_attack(attacks[1])
  end


end


return manager