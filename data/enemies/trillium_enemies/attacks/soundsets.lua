--[[
By Max Mraz, licensed MIT but I mean you can't really license a table

Sets of sound effects to be used by attacks
--]]

return {

  default = {
    "weapons/slash_01",
    "weapons/slash_02",
    "weapons/slash_03",
    "weapons/slash_04",
    "weapons/slash_05",
    "weapons/slash_06",
    "weapons/slash_07",
  },

  blade = {
    "weapons/sword_01",
    "weapons/sword_02",
    "weapons/sword_03",
    "weapons/sword_04",
    "weapons/sword_05",
    "weapons/sword_06",
    "weapons/sword_07",
  },

  blunt = {
    "weapons/blunt_01",
    "weapons/blunt_02",
    "weapons/blunt_03",
    "weapons/blunt_04",
    "weapons/blunt_05",
    "weapons/blunt_06",
    "weapons/blunt_07",
    "weapons/blunt_08",
  },

}

