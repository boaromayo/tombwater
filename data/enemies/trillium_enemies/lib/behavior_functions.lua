--[[
By Max Mraz, licensed MIT
Additional functions to be called by the Behavior Applicator script
Adds on useful but optional functions for enemies
For example, some of these are common functions enemies may use in their AI, like retreating, or closing distance before they attack
--]]

local manager = {}

function manager.apply_behavior(enemy)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()

  --Apply all attacks:
  local attack_scripts = {
    "enemies/trillium_enemies/attacks/circle_bursts",
    "enemies/trillium_enemies/attacks/howl",
    "enemies/trillium_enemies/attacks/melee",
    "enemies/trillium_enemies/attacks/ram_attack",
    "enemies/trillium_enemies/attacks/ranged",
    "enemies/trillium_enemies/attacks/spoke_bullet",
    "enemies/trillium_enemies/attacks/warp_away",
  }
  for _, id in pairs(attack_scripts) do
    require(id).apply_behavior(enemy)
  end

  function enemy:approach_then_attack(ata_props)
    local sprite = enemy:get_sprite()
    local speed = ata_props.speed or 50
    local dist_threshold = ata_props.dist_threshold or 32
    local approach_duration = ata_props.approach_duration or nil
    local target = enemy:choose_target()
    enemy.target_entity = target
    local m = sol.movement.create"target"
    m:set_target(target)
    m:set_speed(speed)
    m:start(enemy)
    sprite:set_animation"walking"
    local elapsed_time = 0
    sol.timer.start(enemy, 50, function()
      elapsed_time = elapsed_time + 50
      if enemy:get_distance(target) <= dist_threshold then
        enemy:stop_movement()
        ata_props.attack_function()
      elseif approach_duration and (elapsed_time >= approach_duration) then
        --Give up the chase:
        enemy.target_entity = nil
        enemy:stop_movement()
        enemy:restart()
      else
        return true
      end
    end)
  end


  function enemy:approach_hero(ata_props)
    local sprite = enemy:get_sprite()
    local speed = ata_props.speed or 50
    local dist_threshold = ata_props.dist_threshold or 32
    local approach_duration = ata_props.approach_duration or nil
    local target = enemy:choose_target()
    local m = sol.movement.create"target"
    m:set_target(target)
    m:set_speed(speed)
    m:start(enemy)
    sprite:set_animation"walking"
    local elapsed_time = 0
    sol.timer.start(enemy, 50, function()
      elapsed_time = elapsed_time + 50
      if enemy:get_distance(target) <= dist_threshold then
        enemy:stop_movement()
        enemy:decide_action()
      elseif approach_duration and (elapsed_time >= approach_duration) then
        enemy:stop_movement()
        enemy:restart()
      else
        return true
      end
    end)
  end


  function enemy:retreat(attrs)
    local sprite = enemy:get_sprite()
    attrs = attrs or {}
    local speed = attrs.speed or 70
    local duration = attrs.duration or 1000
    local max_distance = attrs.max_distance or 0
    local animation = attrs.animation or "walking"
    local target = enemy:choose_target()
    local angle = attrs.angle or target:get_angle(enemy)
    local lock_facing = attrs.lock_facing or false
    local callback = attrs.callback or function() enemy:decide_action() end
    local test_dist = 8
    local is_blocked = enemy:test_obstacles(test_dist * math.cos(angle), test_dist * math.sin(angle))
    if is_blocked then angle = angle + (math.pi / 2 * math.random(1, 3)) end
    local m = sol.movement.create("straight")
    m:set_angle(angle)
    m:set_speed(speed)
    m:set_max_distance(max_distance)
    enemy.lock_facing = lock_facing
    m:start(enemy)
    function m:on_finished()
      sprite:set_animation"stopped"
    end
    sprite:set_animation(animation)
    sol.timer.start(enemy, duration, function()
      m:stop()
      sprite:set_animation"stopped"
      enemy.lock_facing = false
      callback()
    end)
  end


  function enemy:stagger(length)
    local sprite = enemy:get_sprite()
    length = length or 500
    sol.timer.stop_all(enemy)
    enemy:stop_movement()

    for _, entity in pairs(enemy.attack_entities) do entity:remove() end
    enemy.aggro = true
    enemy.staggered = true
    if sprite:has_animation("staggered") then
      sprite:set_animation"staggered"
    elseif sprite:has_animation("stunned") then
      sprite:set_animation"stunned"
    elseif sprite:has_animation("stopped") then
      sprite:set_animation"stopped"
    else
      sprite:set_animation"walking"
    end
    sol.timer.start(enemy, length, function()
      enemy.staggered = false
      enemy:restart()
    end)
  end


  function enemy:should_lose_aggro()
    local should_lose = false
    local dist = enemy:get_distance(hero)
    if (dist > (enemy.abandon_hero_distance or 200)) or (not enemy:is_in_same_region(hero)) or (enemy:get_layer() ~= hero:get_layer()) then
      should_lose = true
    end
    return should_lose
  end


end


return manager
