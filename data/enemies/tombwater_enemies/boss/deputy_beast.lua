local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 800
enemy.money = 0
enemy.weight = 50
enemy.base_damage = 25

local max_life = 900


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = max_life,
  width = 32,
  height = 24,
  sprite_direction_style = "horizontal",
  detection_distance = 400,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})


local slash_step_distance = 24
local step_speed = 60

local slash_1_attack = {
  windup_duration = 500,
  windup_animation = "slash_1_windup",
  attack_animation = "slash_1_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/deputy_beast_slash",
  weapon_attack_animation = "slash_1_attack",
  damage = enemy.base_damage,
  step_distance = slash_step_distance,
  step_speed = step_speed,
  step_tracking = true,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_2_attack = {
  windup_duration = 500,
  windup_animation = "slash_2_windup",
  attack_animation = "slash_2_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/deputy_beast_slash",
  weapon_attack_animation = "slash_2_attack",
  damage = enemy.base_damage,
  step_distance = slash_step_distance,
  step_speed = step_speed,
  step_tracking = true,
  damage_type = "physical",
  recovery_duration = 500,
}

--Slash combo
local slash_combo_1_a = table.duplicate(slash_1_attack)
slash_combo_1_a.recovery_duration = 10
local slash_combo_1_b = table.duplicate(slash_2_attack)
slash_combo_1_b.windup_duration = 10
local slash_combo_1 = { slash_combo_1_a, slash_combo_1_b }

--Extended combo
local slash_combo_2_a = table.duplicate(slash_1_attack)
slash_combo_2_a.windup_duration = 300
slash_combo_2_a.recovery_duration = 10
local slash_combo_2_b = table.duplicate(slash_2_attack)
slash_combo_2_b.windup_duration = 10
slash_combo_2_b.recovery_duration = 10
local slash_combo_2_c = table.duplicate(slash_1_attack)
slash_combo_2_c.windup_duration = 10
slash_combo_2_c.recovery_duration = 10
local slash_combo_2_d = table.duplicate(slash_2_attack)
slash_combo_2_d.windup_duration = 10
slash_combo_2_d.recovery_duration = 10
local slash_combo_2_e = table.duplicate(slash_1_attack)
slash_combo_2_e.damage = enemy.base_damage * 1.2
slash_combo_2_e.windup_duration = 200
slash_combo_2_e.recovery_duration = 1500
slash_combo_2_e.recovery_animation = "crouching"
local slash_combo_2 = { slash_combo_2_a, slash_combo_2_b, slash_combo_2_c, slash_combo_2_d, slash_combo_2_c, slash_combo_2_d, slash_combo_2_e }


--Ranged:
local worm_cannon = {
  windup_animation = "shooting",
  windup_duration = 500,
  attack_sound = "enemies/worm_cannon",
  generic_projectile_type = "worms",
  projectile_sprite = "entities/enemy_projectiles/worm_cannon",
  projectile_offset_radius = 16,
  projectile_offset_y = -32,
  damage = enemy.base_damage * .5,
  recovery_duration = 100,
  recovery_animation = "shooting",
}
local worm_cannon_end_combo = table.duplicate(worm_cannon)
worm_cannon_end_combo.recovery_animation = "stopped"
worm_cannon_end_combo.recovery_duration = 1200
local worm_cannon_combo = { table.duplicate(worm_cannon), table.duplicate(worm_cannon), worm_cannon_end_combo, }


--As "AOE attack"
local worm_puddle_attack = {
  damage = enemy.base_damage,
  windup_animation = "screaming",
  windup_duration = 800,
  windup_sound = "enemies/deputy_beast_scream_low",
  attack_animation = "stop_screaming",
  attack_radius = 64,
  num_attacks = 16,
  attack_entity_model = "enemy_projectiles/floor_blast",
  attack_entity_sprite = "entities/enemy_projectiles/eldritch_puddle_burst",
  recovery_duration = 500,
}


--Summon tentacles
function enemy:worm_puddle_attack(props)
  props = props or {}
  local sprite = enemy:get_sprite()
  local num_attacks = props.num_attacks or 20
  local attack_animation = "screaming"
  local attack_duration = props.attack_duration or 1000
  local attack_sprite = "entities/enemy_projectiles/eldritch_puddle_burst"
  local attack_width, attack_height = 16, 16
  local min_radius = props.min_radius or 12
  local max_radius = props.max_radius or 80
  local min_delay, max_delay = props.min_delay or 400, props.max_delay or 1000
  local x, y, z = enemy:get_position()

  sprite:set_animation(attack_animation)
  sol.audio.play_sound"enemies/deputy_beast_scream_low"
  map:screenshake{ shake_count = 25}
  for i = 1, num_attacks do
    sol.timer.start(enemy, math.random(min_delay, max_delay), function()
      local angle = 2 * math.pi / num_attacks * i
      local spawn_x = x + math.cos(angle) * math.random(min_radius, max_radius)
      local spawn_y = y + math.sin(angle) * math.random(min_radius, max_radius)
      if map:get_ground(spawn_x, spawn_y, z) == "traversable" then
        local attack = map:create_custom_entity{
          x = spawn_x, y = spawn_y, layer = z, direction = 0, width = attack_width, height = attack_height,
          model = "enemy_projectiles/floor_blast",
          sprite = attack_sprite,
        }
        local emitter = map:create_particle_emitter(spawn_x, spawn_y, z)
        emitter.target = attack
        emitter.angle = math.pi / 2
        emitter.duration = 500
        emitter.angle_variance = math.rad(20)
        emitter.particle_color = {30, 0, 20}
        emitter.particle_opacity = {100,180}
        emitter.frequency = 100
        emitter:emit()
      end
    end)
  end
  sol.timer.start(enemy, attack_duration, function()
    enemy:decide_action()
  end)
end

local arena_puddle_attack = {
  num_attacks = 75,
  max_radius = 300,
  attack_duration = 1400,
  min_delay = 600,
  max_delay = 7000,
}


--Extension arm punch
function enemy:long_punch()
  local sprite = enemy:get_sprite()
  local windup_duration = 500
  local windup_animation = "long_punch_windup"
  local attack_animation = "long_punch_attack"
  local attack_length = 1200

  local x, y, z = enemy:get_position()
  local hx, hy = hero:get_position()
  sprite:set_animation(windup_animation)
  sol.audio.play_sound"enemies/deputy_beast_growl"
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_direction(enemy:get_facing_direction_to(hero))
    sprite:set_animation(attack_animation)
    enemy.hyper_armor = true
    --Create attack entity
    local y_offset = -38
    local x_offset = 8 * (sprite:get_direction() == 0 and 1 or -1)
    local attack = map:create_custom_entity{
      x = x + x_offset, y = y + y_offset, layer = z, width = 16, height = 16, direction = 0,
      sprite = "enemies/tombwater_enemies/weapons/deputy_beast_tentacle",
      model = "enemy_projectiles/melee_attack",
    }
    attack.damage = enemy.base_damage
    attack.damage_type = "physical"
    attack:enable_collision()
    --add attack to attack entities for cleanup
    enemy.attack_entities = enemy.attack_entities or {}
    enemy.attack_entities[attack] = attack
    --Timer for end of attack
    sol.timer.start(enemy, attack_length, function()
      sprite:set_animation"stopped"
      attack:remove()
      enemy.hyper_armor = nil
      enemy:decide_action()
    end)
    --Manipulate arm
    local angle = attack:get_angle(hx, hy)
    local attack_sprite = attack:get_sprite()
    attack_sprite:set_rotation(angle)
    attack_sprite:set_animation("attacking", function()
      attack_sprite:set_animation("extended")
      sol.timer.start(enemy, 200, function()
        attack_sprite:set_animation("returning")
      end)
    end)
  end)
end


--Gallows summon
function enemy:summon_gallows(num_attacks)
  local sprite = enemy:get_sprite()
  local attack_duration = 1000
  num_attacks = num_attacks or 3

  sprite:set_animation"screaming"
  sol.audio.play_sound("enemies/deputy_beast_scream")
  map:screenshake{ shake_count = 25}
  sol.timer.start(enemy, attack_duration, function()
    sprite:set_animation"crouching"
    sol.timer.start(enemy, 1000, function()
      sprite:set_animation("rising", function() enemy:decide_action() end)
    end)
  end)

  local chosen_gallows = {}
  local num_spots = map:get_entities_count("underground_gallows")
  local overflow_stopper = 1
  local function choose_gallows()
    if overflow_stopper > 100 then return end
    overflow_stopper = overflow_stopper + 1
    local picked_number = math.random(1, num_spots)
    if not chosen_gallows[picked_number] then
      chosen_gallows[picked_number] = true
      local x, y, z = map:get_entity("underground_gallows_" .. picked_number):get_position()
      if not x and y and z then x, y, z = choose_gallows() end
      return x, y, z
    else
      choose_gallows()
    end
  end

  for i = 1, num_attacks do
    sol.timer.start(enemy, math.random(300, 1000), function()
      --choose position
      local x, y, z = choose_gallows()
      map:create_custom_entity{
        x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
        sprite = "enemies/tombwater_enemies/misc/gallows_post",
        model = "enemy_projectiles/gallows",
      }
    end)
  end
end



local close_range = 56
local long_range = 128

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local has_los = enemy:has_los(hero)
  local rand = math.random(1, 100)
  local sprite = enemy:get_sprite()
  sprite:set_direction(enemy:get_facing_direction_to(hero))

  local function percent_chance(percent)
    local is_happen = rand <= percent
    rand = rand - percent
    return is_happen
  end

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif enemy:get_life() < (max_life / 3 * 2) and not enemy.first_gallows_used then
    enemy:summon_gallows(3)
    enemy.first_gallows_used = true

  elseif enemy:get_life() < (max_life / 3) and not enemy.second_gallows_used then
    enemy:summon_gallows(5)
    enemy.second_gallows_used = true

  elseif rand <= 50 and not enemy.scream_cooldown then
    enemy:worm_puddle_attack(arena_puddle_attack)
    enemy.scream_cooldown = true
    sol.timer.start(enemy, 10 * 1000, function() enemy.scream_cooldown = nil end)

  --Long range
  elseif distance >= long_range then
    if percent_chance(12) then
      enemy:worm_puddle_attack(arena_puddle_attack)
    elseif percent_chance(60) and not enemy.worm_cannon_cooldown then
      enemy.worm_cannon_cooldown = true
      sol.timer.start(map, 5000, function() enemy.worm_cannon_cooldown = nil end)
      enemy:ranged_combo(worm_cannon_combo)
    elseif percent_chance(30) then
      enemy:approach_then_attack{
        speed = 80,
        dist_threshold = 64,
        approach_duration = 500,
        attack_function = function()
          enemy:long_punch()
        end,
      }
    else
      enemy:approach_hero{
        speed = 80,
      }
    end

  --Midrange
  elseif distance >= close_range then
    if percent_chance(40) and not enemy.worm_cannon_cooldown then
      enemy.worm_cannon_cooldown = true
      sol.timer.start(map, 5000, function() enemy.worm_cannon_cooldown = nil end)
      enemy:ranged_combo(worm_cannon_combo)
    elseif percent_chance(40) then
      enemy:approach_then_attack{
        speed = 80,
        dist_threshold = 64,
        approach_duration = 500,
        attack_function = function()
          if rand < 30 then
            enemy:long_punch()
          else
            enemy:melee_combo(slash_combo_1)
          end
        end,
      }
    elseif percent_chance(7) then
      sprite:set_animation"screaming"
      sol.audio.play_sound"enemies/deputy_beast_scream_short"
      sol.timer.start(enemy, 800, function()
        enemy:melee_combo(slash_combo_2)
      end)
    else
      enemy:decide_action()
    end

  --Close range
  elseif distance <= close_range then
    enemy:approach_then_attack{
      speed = 80,
      dist_threshold = 40,
      approach_duration = 500,
      attack_function = function()
        if percent_chance(40) then
          enemy:melee_combo(slash_combo_1)

        elseif percent_chance(40) then
          enemy:long_punch()

        elseif percent_chance (15) then
          sprite:set_animation"screaming"
          sol.audio.play_sound("enemies/deputy_beast_scream_short")
          sol.timer.start(enemy, 800, function()
            enemy:melee_combo(slash_combo_2)
          end)

        else
          sol.timer.start(enemy, 200, function() enemy:decide_action() end)
        end

      end,
    }

  else
    enemy:approach_hero{
      speed = 80,
    }

  end

end



local function get_emitter(x, y, z)
  local emitter = map:create_particle_emitter(x, y, z, "burst")
  emitter.target = enemy
  emitter.duration = 700
  emitter.shape = "circle"
  emitter.particle_sprite = "effects/blood_drop"
  emitter.particles_per_loop = 8
  emitter.particle_fade_speed = 8
  emitter.particle_speed = 350
  emitter.particle_acceleration = -14
  emitter.particle_opacity = {200,250}
  return emitter
end

function enemy:intro()
  enemy:restart()
  sol.timer.stop_all(enemy)
  local x, y, z = enemy:get_position()
  --enemy:blood_splatter(70)
  local emitter = get_emitter(x, y, z)
  emitter:emit()
  local emitter_2 = get_emitter(x, y, z)
  emitter_2.angle = math.pi / 2
  emitter_2.angle_variance = math.rad(50)
  emitter_2.shape = "square"
  emitter_2.width, emitter_2.height = 16, 96
  emitter_2.particle_acceleration = -10
  emitter_2.particle_fade_speed = 4
  emitter_2:emit()

  map:create_poof(x, y, z, "enemies/enemy_killed_big")
  local sprite = enemy:get_sprite()
  sprite:set_animation"crouching"
  sol.timer.start(enemy, 500, function()
    enemy:start_boss_bar()
    sprite:set_animation("rising", function()
        enemy:worm_puddle_attack()
        enemy.scream_cooldown = true
        sol.timer.start(enemy, 11 * 1000, function() enemy.scream_cooldown = nil end)
    end)
  end)
end


enemy:register_event("on_dying", function()
  game:set_value("central_tombwater_deputy_boss", true)
  for e in map:get_entities_by_type("enemy") do
    if e:get_breed() == "tombwater_enemies/walking_corpse" then
      e:set_life(0)
    end
  end
  enemy:boss_death("crouching")
end)

