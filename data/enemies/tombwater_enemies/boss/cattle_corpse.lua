local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 120
enemy.weight = 150
enemy.money = 0
enemy.base_damage = 35

enemy.damage_type_mods = {
  magic = 1.3,
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 900,
  width = 48,
  height = 40,
  sprite_direction_style = "diagonal",
  detection_distance = 300,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
})


--Trigger boss death on dying:
enemy:register_event("on_dying", function()
  enemy:boss_death("stopped")
  for e in map:get_entities("boss_assistant") do
    if e:get_life() > 0 then e:hurt(9999) end
  end
end)

enemy:register_event("on_created", function()
  enemy:set_dying_sprite_id("enemies/enemy_killed_big")
end)


local ram_attack = {
  windup_animation = "charging_windup",
  windup_duration = 1000,
  speed = 200,
  damage = enemy.base_damage + 15,
  attack_animation = "walking",
  weapon_sprite = "enemies/tombwater_enemies/war_ox",
  weapon_attack_animation = "walking_weapon",
  max_distance = 240,
  recovery_duration = 1200,
  obstacle_collision_callback = function()
    enemy:stop_movement()
    enemy:get_sprite():set_animation"stopped"
    --enemy:restart()
    sol.audio.play_sound"running_obstacle"
    map:screenshake()
    sol.timer.start(enemy, 1500, function()
      enemy:decide_action()
    end)
  end
}


local stomp_attack = {
  windup_animation = "stomp_windup",
  windup_duration = 1000,
  attack_animation = "stomp_attack",
  attack_sound = "running_obstacle",
  weapon_sprite = "enemies/trillium_enemies/shockwave_8x7",
  weapon_attack_animation = "stopped",
  damage = enemy.base_damage + 15,
  turn_toward_hero = false,
  --has_aoe = true, --this looked awkward since there's two attack sprites...
  aoe_damage = enemy.base_damage - 10,
  aoe_scale = {1.5, 1.5},
  aoe_screenshake = 4,
  recovery_duration = 1000,
}


local swipe_a = {
  windup_animation = "swipe_1_windup",
  windup_duration = 400,
  attack_animation = "swipe_1_attack",
  weapon_sprite = "enemies/tombwater_enemies/war_ox",
  weapon_attack_animation = "swipe_1_blade",
  damage = enemy.base_damage - 10,
  damage_type = "physical",
  recovery_duration = 800,
  step_distance = 16,
}

local swipe_b = {
  windup_animation = "swipe_2_windup",
  windup_duration = 400,
  attack_animation = "swipe_2_attack",
  weapon_sprite = "enemies/tombwater_enemies/war_ox",
  weapon_attack_animation = "swipe_2_blade",
  damage = enemy.base_damage - 10,
  damage_type = "physical",
  recovery_duration = 800,
  step_distance = 16,
}

local combo_a = table.duplicate(swipe_a)
combo_a.recovery_duration = 10

local combo_b = table.duplicate(swipe_b)
combo_b.windup_duration = 10



local swipe_combo = {combo_a, combo_b}


local melee_range = 48
local ram_range = 96

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local sprite = enemy:get_sprite()

  enemy:check_flies()

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance >= ram_range or (math.random(1, 10) > 9) then
    sprite:set_direction(enemy:get_facing_direction_to(hero))
    sol.audio.play_sound("enemies/bull_snort")
    enemy:ram_attack(ram_attack)
  elseif distance <= melee_range and (math.random(1, 5) < 4) then
    if math.random(1, 10) < 6 then
      enemy:melee_combo(swipe_combo)
    else
      enemy:melee_attack(swipe_b)
    end
  else
    if math.random(1, 10) > 8 then
      enemy:approach_then_attack({
        approach_duration = 1000,
        speed = 100,
        dist_threshold = 48,
        attack_function = function()
          enemy:melee_attack(swipe_a)
        end,
      })
    else
      sprite:set_direction(3)
      enemy:melee_attack(stomp_attack)
    end
  end
end


function enemy:check_flies()
  local function make_flies()
    enemy.fly_swarm = enemy:create_enemy{
      breed = "tombwater_enemies/fly_swarm",
    }
    enemy.fly_swarm.host_entity = enemy
    enemy.fly_swarm.min_radius, enemy.fly_swarm.max_radius = 16, 48
    enemy.min_flies, enemy.max_flies = 15, 20
    --enemy.fly_swarm:restart()
  end

  if (not enemy.fly_swarm) or (not enemy.fly_swarm:exists()) then
    make_flies()
  else
    enemy.fly_swarm:check_for_death(6)
  end

end

