local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 0
enemy.money = 0
enemy.weight = 10
enemy.base_damage = 25

enemy.respawn_manager_ignore = true --since the enemy is only really killed if the second phase is killed, if you somehow escape and come back during the second phase we need this enemy to respawn


local punch_step_distance = 24
local punch_step_speed = 80
local base_damage_punch = enemy.base_damage - 10
local base_damage_gun = enemy.base_damage
local base_damage_axe = enemy.base_damage

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 550,
  detection_distance = 250,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

enemy:register_event("on_created", function()
  enemy:set_dying_sprite_id("enemies/enemy_killed_big")
end)


local shotgun_attack = {
  windup_animation = "gun_aiming",
  windup_duration = 500,
  generic_projectile_type = "bullet",
  attack_sound = "gunshot_shotgun",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  projectile_offset_radius = 8,
  --projectile_offset_y = 16,
  projectile_offset_y = -22,
  damage = base_damage_gun,
  num_projectiles = 6,
  projectile_spread = 80,
  projectile_properties = {
    max_distance = 64,
    distance_damage_reduction_rate = .5,
  },
  recovery_duration = 500,
  recovery_animation = "gun_aiming",
}


local punch_l = {
  windup_duration = 500,
  windup_animation = "punching_left_windup",
  windup_sound = "enemies/townsfolk_vox/big_grunt_04",
  attack_animation = "punching_left_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "punching_left",
  damage = base_damage_punch,
  step_distance = punch_step_distance,
  step_speed = step_speed,
  damage_type = "physical",
  recovery_duration = 500,
}

local punch_r = {
  windup_duration = 500,
  windup_animation = "punching_right_windup",
  attack_animation = "punching_right_attack",
  attack_sound = "enemies/townsfolk_vox/big_grunt_04",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "punching_right",
  damage = base_damage_punch,
  step_distance = punch_step_distance,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 500,
}

local swipe = { --start of a combo
  windup_duration = 500,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "slash",
  damage = base_damage_punch + 10,
  step_distance = punch_step_distance + 16,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash = {
  windup_duration = 500,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = base_damage_axe,
  step_distance = punch_step_distance,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 500,
}

local backslash = {
  windup_duration = 500,
  windup_animation = "backslash_windup",
  attack_animation = "backslash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = base_damage_axe,
  step_distance = punch_step_distance,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 500,
}

local slam_attack = {
  windup_duration = 700,
  windup_animation = "overhead_windup",
  attack_animation = "overhead_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_axe",
  weapon_windup_animation = "overhead_windup",
  weapon_attack_animation = "overhead_attack",
  damage = base_damage_axe + 10,
  damage_type = "physical",
  recovery_duration = 500,
}

local grenade_attack = {
  windup_animation = "punching_left_windup",
  attack_animation = "punching_left_attack",
  attack_sound = "enemies/townsfolk_vox/big_grunt_05",
  generic_projectile_type = "bomb",
  projectile_sprite = "hero_projectiles/grenade",
  attack_sound = "throw",
  damage = 35,
  aim_variance = 25,
}

--Multi grenade toss
local multigrenade_attack = table.duplicate(grenade_attack)
multigrenade_attack.recovery_duration = 1300
multigrenade_attack.num_projectiles = 12
multigrenade_attack.projectile_spread = 50
multigrenade_attack.aim_variance = 10
multigrenade_attack.generic_projectile_type = "scatter_grenade"
multigrenade_attack.projectile_properties = {
  explosion_sprite = "items/explosion_small",
  fuse_length = 800, fuse_variance = 500,
  damage = 5,
}


--combo_1
local combo_1_a = table.duplicate(punch_l)
combo_1_a.windup_duration = 800
combo_1_a.recovery_duration = 10
local combo_1_b = table.duplicate(punch_r)
combo_1_b.windup_duration = 10
combo_1_b.recovery_duration = 10
local combo_1_c = table.duplicate(swipe)
combo_1_c.windup_duration = 10
combo_1_c.recovery_duration = 1200
local combo_1 = {combo_1_a, combo_1_b, combo_1_c}

--combo_2
local combo_2_a = table.duplicate(swipe)
combo_2_a.windup_duration = 1000
combo_2_a.recovery_duration = 10
combo_2_a.windup_sound = "enemies/townsfolk_vox/big_grunt_02"
local combo_2_b = table.duplicate(punch_r)
combo_2_b.windup_duration = 300
combo_2_b.recovery_duration = 10
local combo_2_c = table.duplicate(punch_l)
combo_2_c.windup_duration = 10
combo_2_c.recovery_duration = 10
local combo_2_d = table.duplicate(punch_r)
combo_2_d.windup_duration = 10
combo_2_d.recovery_duration = 10
local combo_2_e = table.duplicate(swipe)
combo_2_e.windup_duration = 200
combo_2_e.recovery_duration = 1300
combo_2 = {combo_2_a, combo_2_b, combo_2_c, combo_2_d, combo_2_e}

--combo_3
local combo_3_a = table.duplicate(swipe)
combo_3_a.windup_duration = 800
combo_3_a.recovery_duration = 10
local combo_3_b = table.duplicate(punch_l)
combo_3_b.windup_duration = 10
combo_3_b.recovery_duration = 10
local combo_3_c = table.duplicate(shotgun_attack)
combo_3_c.windup_time = 300
combo_3_c.recovery_animation = "stopped"
combo_3_c.recovery_duration = 900
combo_3_b.next_action = function()
  enemy:ranged_attack(combo_3_c)
end
local combo_3 = {combo_3_a, combo_3_b}

--Shotgun combo
local shot_combo_1_a = table.duplicate(shotgun_attack)
shot_combo_1_a.windup_duration = 900
shot_combo_1_a.recovery_duration = 10
local shot_combo_1_b = table.duplicate(shotgun_attack)
shot_combo_1_b.windup_duration = 400
shot_combo_1_b.recovery_duration = 10
local shot_combo_1_c = table.duplicate(shotgun_attack)
shot_combo_1_c.windup_duration = 600
--shot_combo_1_c.recovery_animation = "stopped"
shot_combo_1_c.recovery_duration = 1000
local shot_combo = {shot_combo_1_a, shot_combo_1_c}

--Grenade combo
local grenade_combo_a = table.duplicate(grenade_attack)
grenade_combo_a.windup_duration = 700
grenade_combo_a.recovery_duration = 100
local grenade_combo_b = table.duplicate(grenade_combo_a)
local grenade_combo_c = table.duplicate(grenade_combo_a)
local grenade_combo = {grenade_combo_a, grenade_combo_b, grenade_combo_c}

--Slash Combo x3
local slash_combo_1_a = table.duplicate(slash)
slash_combo_1_a.windup_duration = 700
slash_combo_1_a.recovery_duration = 10
local slash_combo_1_b = table.duplicate(backslash)
slash_combo_1_b.windup_duration = 10
slash_combo_1_b.recovery_duration = 10
local slash_combo_1_c = table.duplicate(slash)
slash_combo_1_c.windup_duration = 100
slash_combo_1_c.recovery_duration = 800
local slash_combo_1 = { slash_combo_1_a, slash_combo_1_b, slash_combo_1_c }

--Slash Combo x5
local slash_combo_2_a = table.duplicate(backslash)
slash_combo_2_a.windup_duration = 700
slash_combo_2_a.recovery_duration = 10
slash_combo_2_a.windup_sound = "enemies/townsfolk_vox/big_grunt_03"
local slash_combo_2_b = table.duplicate(slash)
slash_combo_2_b.windup_duration = 10
slash_combo_2_b.recovery_duration = 10
local slash_combo_2_c = table.duplicate(slam_attack)
slash_combo_2_c.windup_duration = 200
slash_combo_2_c.recovery_duration = 100
local slash_combo_2_d = table.duplicate(slash)
slash_combo_2_d.windup_duration = 200
slash_combo_2_d.recovery_duration = 10
local slash_combo_2_e = table.duplicate(backslash)
slash_combo_2_e.windup_duration = 10
slash_combo_2_e.recovery_duration = 1600
slash_combo_2_e.recovery_animation = "tired"
local slash_combo_2 = { slash_combo_2_a, slash_combo_2_b, slash_combo_2_c, slash_combo_2_d, slash_combo_2_e, }


local earthquake_line_delay = 1000
function enemy:earthquake_line()
  local range = 400
  local frequency = 100
  local speed = 300

  local x, y, z = enemy:get_position()
  local angle = enemy:get_sprite():get_direction() * math.pi / 2
  local angle = enemy:get_angle(hero)

  sol.audio.play_sound"running_obstacle"
  map:screenshake({shake_count = 2})
  local target = map:create_custom_entity{x=x, y=y, layer=z, direction=0, width = 16, height=16, }
  local m = sol.movement.create"straight"
  m:set_ignore_obstacles(true)
  m:set_speed(speed)
  m:set_angle(angle)
  m:start(target, function() target:remove() end)
  local elapsed_time = 0
  sol.timer.start(target, frequency, function()
    elapsed_time = elapsed_time + frequency
    local x, y, z = target:get_position()
    local attack = map:create_custom_entity{
      x=x, y=y, layer=z, direction = 0, width = 16, height = 16,
      sprite = "entities/enemy_projectiles/dust_attack",
      model = "enemy_projectiles/general_attack",
    }
    attack.damage = enemy.base_damage / 3
    attack:remove_after_animation()
    attack.must_overlap = true
    if (elapsed_time / 1000 * speed) <= range then return true end
  end)
end


function enemy:slam_chain(num_slams)
  num_slams = num_slams or 6
  local slam_combo_attacks = {}

  for i = 1, num_slams do
    local slam_combo = table.duplicate(slam_attack)
    slam_combo.windup_duration = 500
    slam_combo.recovery_duration = 100
    if i == 1 then slam_combo.windup_duration = 1200 end
    if i == num_slams then
      slam_combo.recovery_duration = 3000
      slam_combo.recovery_animation = "tired"
    end
    slam_combo_attacks[i] = slam_combo
  end

  for i = 1, #slam_combo_attacks - 1 do
    slam_combo_attacks[i].next_action = function()
      enemy:melee_attack(slam_combo_attacks[i + 1])
      sol.timer.start(enemy, slam_combo_attacks[i + 1].windup_duration + 100, function() enemy:earthquake_line() end)
    end
  end
  enemy:melee_attack(slam_combo_attacks[1])
  sol.timer.start(enemy, slam_combo_attacks[1].windup_duration + 100, function() enemy:earthquake_line() end)

  sol.audio.play_sound"enemies/townsfolk_vox/big_grunt_01"
end



local shotgun_range = 84
local melee_range = 56

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local has_los = enemy:has_los(hero)
  local rand = math.random(1, 100)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))

  local function percent_chance(percent)
    local is_happen = rand <= percent
    rand = rand - percent
    return is_happen
  end

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --Attacks
  --Long Range
  elseif distance >= shotgun_range then
    --Grenade combo
    if percent_chance(20) then
      enemy:ranged_attack(multigrenade_attack)
    --Shotgun combo
    elseif percent_chance(30) then
      enemy:approach_then_attack{
        speed = 80,
        dist_threshold = shotgun_range + 20,
        approach_duration = 500,
        attack_function = function() enemy:ranged_combo(shot_combo) end
      }
    --Axe Slam Chain
    elseif percent_chance(20) then
      enemy:slam_chain()
    else
      enemy:approach_then_attack{
        speed = 80,
        dist_threshold = melee_range,
        approach_duration = 500,
        attack_function = function() enemy:melee_attack(slash) end
      }
    end

  --Midrange
  elseif distance >- melee_range then
    --Shotgun combo
    if percent_chance(15) then
      enemy:approach_then_attack{
        speed = 80,
        dist_threshold = shotgun_range,
        approach_duration = 500,
        attack_function = function() enemy:ranged_combo(shot_combo) end
      }
    --Grenade combo
    elseif percent_chance(15) then
      enemy:ranged_attack(multigrenade_attack)
    --Slam Chain
    elseif percent_chance(8) then
      enemy:slam_chain()
    --Other combo
    else
      enemy:approach_then_attack{
        speed = 80,
        dist_threshold = melee_range,
        approach_duration = 500,
        attack_function = function()

          if percent_chance(25) then
            enemy:melee_combo(slash_combo_1)
          elseif percent_chance(25) then
            enemy:melee_combo(slash_combo_2)
          else
            sol.timer.start(enemy, 300, function() enemy:decide_action() end)
          end

        end
      }
    end

  --Close range
  else
    if percent_chance(10) then
      enemy_ranged_attack(shotgun_attack)
    elseif percent_chance(30) then
      enemy:melee_combo(slash_combo_1)
    elseif percent_chance(30) then
      enemy:melee_combo(slash_combo_2)
    else
      sol.timer.start(enemy, 300, function() enemy:decide_action() end)
    end

  end

end


--Transformation
local function do_emitter(x, y, z, dummy)
  local emit = map:create_particle_emitter(x, y - 8, z, "reverse")
  emit.target = dummy
  emit.duration = 1000
  emit.particles_per_loop = 5
  emit.particle_sprite = "effects/blood_drop"
  emit.particle_color = {30, 0, 20}
  emit.rotation = "random"
  emit.particle_speed = 200
  emit.particle_opacity = {160,200}
  emit.point_target_dead_zone = 16
  emit.sprite_scale = {2, 1}
  return emit
end

enemy:register_event("on_dying", function()
  if enemy:get_life() > 0 then return end
  local x, y, z = enemy:get_position()
  enemy:remove()
  local dummy = map:create_custom_entity{
    x=x, y=y, layer=z, width=16, height=16, direction=0,
    sprite = "enemies/tombwater_enemies/boss/deputy",
  }
  local sprite = dummy:get_sprite()
  sprite:set_animation"crouching"
  dummy:set_drawn_in_y_order(true)
  sol.timer.start(map, 1000, function()
    local emitter_1 = do_emitter(x, y, z, dummy)
    local emitter_2 = do_emitter(x, y, z, dummy)
    emitter_2.particle_sprite = "effects/particle_small"
    emitter_1:emit()
    emitter_2:emit()
  end)
  sol.timer.start(map, 2500, function()
    local new_enemy = map:create_enemy{
      x=x, y=y, layer=z, direction = 0, breed = "tombwater_enemies/boss/deputy_beast",
    }
    dummy:remove()
    new_enemy:intro()
  end)
end)


