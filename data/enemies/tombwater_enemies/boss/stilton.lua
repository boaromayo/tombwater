local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

--Stats
local max_life = 1800

enemy.exp = 900
enemy.weight = 15
enemy.money = 60
enemy.base_damage = 25
enemy.damage_type_mods = {
  magic = 1.3
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = max_life,
  detection_distance = 200,
  abandon_hero_distance = 1000,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
  sprite_direction_style = "horizontal",
})


--Trigger boss death on dying:
enemy:register_event("on_dying", function()
  enemy:boss_death("stopped")
end)


--Used by a couple attacks:
function enemy:debris_explosion(props)
  local x, y, layer = props.x, props.y, props.layer
  local damage = props.damage or (enemy.base_damage * .8)
  local sprite = props.sprite or "entities/enemy_projectiles/dust_attack_big"
  local collision_delay = props.collision_delay or 0

  sol.audio.play_sound"running_obstacle"
  sol.audio.play_sound"spells/ground_burst_soft_1"
  local aoe = map:create_custom_entity{
    x=x, y=y, layer=layer, direction = 0, width = 16, height = 16,
    sprite = sprite,
    model = "damaging_entity",
  }
  aoe_sprite = aoe:get_sprite()
  aoe.harmless_to_hero = false
  aoe.harmless_to_enemies = true
  aoe.damage = damage
  aoe.damage_cooldown = 2000
  sol.timer.start(aoe, collision_delay, function()
    aoe:activate_collision()
  end)
end


local slash_forehand = {
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  weapon_attack_animation = "slash_forehand_attack",
  step_distance = 24,
  step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
}


local slash_backhand = {
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  weapon_attack_animation = "slash_backhand_attack",
  step_distance = 16,
  step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
}


local slash_overhead = {
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  --weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  --weapon_attack_animation = "slash_backhand_attack",
  step_distance = 16,
  step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
  --aoe:
  has_aoe = true,
  aoe_offset = 32,
  aoe_delay = 20,
  aoe_sound = "running_obstacle",
  aoe_damage = enemy.base_damage * 1.2,
  aoe_sprite = "entities/enemy_projectiles/debris_attack", 
}

local slash_overhead_2 = table.duplicate(slash_overhead)
slash_overhead_2.aoe_sprite = "entities/enemy_projectiles/dust_attack_big"

local function combo(a, w, r)
  local atk = table.duplicate(a)
  atk.windup_duration = w
  atk.recovery_duration = r
  return atk
end


local combo_a = {
  combo(slash_forehand, 500, 10),
  combo(slash_backhand, 100, 10),
  combo(slash_forehand, 100, 400),
}


local combo_b = {
  combo(slash_backhand, 500, 10),
  combo(slash_forehand, 400, 10),
  combo(slash_overhead, 100, 10),
  combo(slash_overhead_2, 700, 800),
}

local combo_c = {
  combo(slash_overhead, 500, 10),
  combo(slash_backhand, 400, 10),
  combo(slash_forehand, 100, 10),

}



local throw_attack = {
  windup_animation = "throwing_windup",
  attack_animation = "throwing_attack",
  projectile_sprite = "entities/enemy_projectiles/pickaxe",
  projectile_width = 16,
  projectile_height = 16,
  damage = enemy.base_damage * .6,
  damage_type = "physicial",
  recovery_duration = 99999,
  projectile_properties = {
    speed = 400,
    obstacle_callback = function(projectile)
      local x, y, z = projectile:get_position()
      projectile:remove()
      enemy:debris_explosion{x=x, y=y, layer=z}
      sol.timer.stop_all(enemy)
      enemy:stop_movement()
      local dummy_axe = map:create_custom_entity{
        x=x, y=y, layer=z, width=16, height=16, direction=0,
        sprite = "entities/enemy_projectiles/pickaxe",
      }
      enemy:dash_to_axe(dummy_axe)
    end,
  },
}

function enemy:dash_to_axe(axe)
  sol.timer.start(enemy, 500, function()
    --local x, y, z = axe:get_position()
    local sprite = enemy:get_sprite()
    sprite:set_animation"dashing"
    local is_dashing = true
    --local m = sol.movement.create"target"
    --m:set_target(axe)
    local m = sol.movement.create"straight"
    m:set_angle(enemy:get_angle(axe))
    m:set_max_distance(enemy:get_distance(axe))
    m:set_speed(300)
    m:set_ignore_obstacles(true) --is he gonna get stuck in walls?
    m:start(enemy, function()
      is_dashing = false
      axe:remove()
      enemy:decide_action()
    end)
    --Explosions as dashing:
    sol.timer.start(enemy, 0, function()
      if is_dashing then
        local x, y, z = enemy:get_position()
        sol.audio.play_sound"spells/ground_burst_soft_2"
        local trail_explosion = map:create_custom_entity{
          x=x, y=y, layer=z, width=16, height=16, direction=0,
          model = "enemy_projectiles/floor_blast",
          sprite = "entities/enemy_projectiles/dust_attack",
        }
        trail_explosion.damage = enemy.base_damage / 2
        trail_explosion:attack()
        return 80
      end
    end)
  end)
end


--AOE Slam
function enemy:aoe_slam()
  local windup_duration = 700
  local recovery_duration = 1200
  local aoe_collision_delay = 400

  enemy:stop_movement()
  local sprite = enemy:get_sprite()
  sprite:set_direction(enemy:get_facing_direction_to(hero))
  sprite:set_animation"slam_windup"
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_direction(enemy:get_facing_direction_to(hero))
    sprite:set_animation("slam_attack", function()
      sprite:set_animation"stopped"
      sol.timer.start(enemy, recovery_duration, function() enemy:decide_action() end)
    end)
    sol.audio.play_sound"running_obstacle"
    local x, y, z = enemy:get_position()
    local aoe = map:create_custom_entity{
      x=x, y=y, layer=z, direction = 0, width = 16, height = 16,
      sprite = "enemies/tombwater_enemies/weapons/triangular_aoe",
      model = "enemy_projectiles/general_attack",
    }
    aoe_sprite = aoe:get_sprite()
    aoe_sprite:set_rotation(enemy:get_angle(hero))
    aoe.delay_collision = true
    aoe.damage = enemy.base_damage
    aoe.damage_frequency = 2000
    aoe.hit_callback = function(hero)
      hero:knock_up()
    end
    aoe:set_drawn_in_y_order(false)
    sol.timer.start(aoe, aoe_collision_delay, function()
      aoe:activate_collision()
      aoe_sprite:set_animation("attack")
    end)
  end)
end


--Create holes
local pits = {}
local p_i = 1
local open_pits = 0
for pit in map:get_entities("boss_hole_obstacle") do
  pits[p_i] = pit
  pit.open = false
  p_i = p_i + 1
end
function enemy:open_random_pit()
  local pit = pits[math.random(1, #pits)]
  if not pit.open then
    pit.open = true
    pit:set_enabled()
    local x, y, z = pit:get_position()
    enemy:debris_explosion{
      x=x, y=y, layer=z,
      damage = 5, sprite = "entities/enemy_projectiles/debris_attack",
    }
  end
end

function enemy:pit_opening_slam()
  local windup_delay = 400
  local num_slams = 3
  local slam_frequency = 500
  local recovery_delay = 500

  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  local done_slams = 0

  local function do_slam()
    sprite:set_animation("slam_windup")
    sol.timer.start(enemy, windup_delay, function()
      sprite:set_animation("slam_attack", function() sprite:set_animation"stopped" end)
      local x, y, z = enemy:get_position()
      enemy:debris_explosion{
        x=x, y=y, layer=z,
        sprite = "items/shockwave_8x7",
      }
      enemy:open_random_pit()
      done_slams = done_slams + 1
      if done_slams < num_slams then
        sol.timer.start(enemy, slam_frequency, function()
          do_slam()
        end)
      else
        sol.timer.start(enemy, recovery_delay, function()
          enemy:decide_action()
        end)
      end
    end)
  end

  sprite:set_animation"roaring"
  sol.audio.play_sound"boss_charge"
  sol.timer.start(enemy, 700, function()
    do_slam()
  end)

end


--Make boulders fall:
function enemy:boulder_rain_slams()
  local windup_duration = 400
  local slam_frequency = 500
  local slam_frequencies = {300, 800, 100, 300}
  local num_slams = #slam_frequencies
  local recovery_duration = 500

  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  local done_slams = 0

  local function do_slam()
    sprite:set_animation("slam_windup")
    sol.timer.start(enemy, windup_duration, function()
      sprite:set_animation("slam_attack", function() sprite:set_animation"stopped" end)
      local x, y, z = enemy:get_position()
      enemy:debris_explosion{
        x=x, y=y, layer=z,
        sprite = "items/shockwave_8x7",
      }
      done_slams = done_slams + 1
      if done_slams < num_slams then
        sol.timer.start(enemy, slam_frequencies[done_slams], function()
          do_slam()
        end)
      else
        sol.timer.start(enemy, recovery_duration, function()
          enemy:decide_action()
        end)
      end
    end)
  end

  sprite:set_animation"roaring"
  sol.audio.play_sound"boss_charge"
  sol.timer.start(enemy, 700, function()
    do_slam()
    enemy:boulder_rain()
  end)
end


function enemy:boulder_rain(duration)
  duration = duration or 9000
  local init_delay = 500
  local min_frequency, max_frequency = 500, 1000
  local hero_radius = 96 --how far away from the hero boulders can fall

  local function make_boulder()
    local x, y, z = hero:get_position()
    local boulder = map:create_custom_entity{
      x = x + math.random(hero_radius * -1, hero_radius),
      y = y + math.random(hero_radius * -1, hero_radius),
      layer = z, direction = 0, width = 16, height = 16,
      sprite = "hazards/falling_boulder",
      model = "hazards/falling_boulder",
    }
    boulder.harmless_to_enemies = true
    boulder.damage = enemy.base_damage * .7
  end

  local elapsed_time = 0
  sol.timer.start(map, init_delay, function()
    make_boulder()
    local fall_delay = math.random(min_frequency, max_frequency)
    elapsed_time = elapsed_time + fall_delay
    if elapsed_time <= duration and enemy:get_life() > 0 then
      return fall_delay
    end
  end)
end



--Axe Orbit:
function enemy:axe_orbit()
  local windup_duration = 300
  local orbit_duration = 2000
  local orbit_radius = 72
  local recovery_duration = 300

  local sprite = enemy:get_sprite()
  sprite:set_animation("throwing_windup")
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_animation("throwing_attack", function() sprite:set_animation("stopped") end)
    local x, y, z = enemy:get_position()
    local axe = map:create_custom_entity{
      x=x, y=y, layer=z, width=16, height=16, direction=0,
      sprite = "entities/enemy_projectiles/pickaxe",
      model = "damaging_entity",
    }
    axe.harmless_to_hero = false
    axe.harmless_to_enemies = true
    axe.damage = enemy.base_damage * .5
    axe.damage_cooldown = 500
    enemy.attack_entities[axe] = axe --so it can be removed when he dies
    local m = sol.movement.create"circle"
    m:set_center(enemy)
    m:set_radius(0)
    m:set_radius_speed(80)
    m:set_angular_speed(4)
    m:set_ignore_obstacles(true)
    m:start(axe)
    m:set_radius(orbit_radius)

    sol.timer.start(enemy, orbit_duration, function()
      m:set_radius_speed(300)
      m:set_radius(0)
      sol.timer.start(axe, 500, function() axe:remove() end)
      sol.timer.start(enemy, recovery_duration, function() enemy:decide_action() end)
    end)
  end)
end


--Phase 2 attacks:
--Create ichor root:
local function create_ichor_root(x, y, z)
  local attack_delay = 500
  --local warning_sound  = "spells/ground_burst_soft_5"
  local attack_sound = "spells/maw_appear"

  if warning_sound then sol.audio.play_sound(warning_sound) end
  local attack = map:create_custom_entity{
    x=x, y=y, layer=z, width=16, height=16, direction=0,
    model = "enemy_projectiles/general_attack",
    sprite = "entities/enemy_projectiles/ichor_root",
  }
  attack.delay_collision = true
  attack.damage = enemy.base_damage * .75
  attack.damage_type = "physical"
  sol.timer.start(attack, attack_delay, function()
    if not map.stilton_ichor_attack_sound_playing then
      sol.audio.play_sound(attack_sound)
      map.stilton_ichor_attack_sound_playing = true
      sol.timer.start(map, 50, function() map.stilton_ichor_attack_sound_playing = nil end)
    end
    attack:get_sprite():set_animation("attacking")
    attack:remove_after_animation()
    attack:activate_collision()
  end)
end


function enemy:ichor_root_attack(attack_pattern)
  local windup_duration = 600
  local recovery_duration = 500
  local sprite = enemy:get_sprite()

  sprite:set_animation("spell_windup")
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_animation("spell_attack", function() sprite:set_animation"stopped" end)
    if attack_pattern == "wave" then
      enemy:ichor_root_wave()
    elseif attack_pattern == "spoke" then
      enemy:ichor_root_spokes()
      recovery_duration = 800
    elseif attack_pattern == "line" then
      enemy:ichor_root_lines()
      recovery_duration = 2000
    end
    sol.timer.start(enemy, recovery_duration, function()
      enemy:decide_action()
    end)
  end)
end


function enemy:ichor_root_wave()
  local num_waves = 5
  local num_nodes = 15
  local ox, oy, z = map:get_entity("ichor_root_marker_top"):get_center_position()
  local wave_width = map:get_entity("ichor_root_marker_top"):get_size()
  local spacing = wave_width / num_nodes
  local vert_spacing = 64
  local wave_num = 0
  local wave_frequency = 1000
  ox = ox - (wave_width / 2)
  
  local function do_wave()
    sol.audio.play_sound("spells/ground_burst_soft_5")
    for i = 1, num_nodes do
      local x = (wave_width / num_nodes * i + 8) + ox
      local y = ((wave_num - 1) * vert_spacing) + oy
      if map:get_ground(x, y, z) == "traversable" then
        create_ichor_root(x, y, z)
      end
    end
  end

  sol.timer.start(map, 0, function()
    wave_num = wave_num + 1
    if wave_num <= num_waves and enemy:get_life() > 0 then
      do_wave()
      return wave_frequency
    end
  end)
end


function enemy:ichor_root_spokes()
  local num_spokes = 5
  local spoke_length = 128
  local num_nodes = 6
  local frequency = 200

  local ox, oy, oz = enemy:get_position()
  local random_offset = math.rad(math.random(1, 360))
  local nodes_done = 0
  sol.timer.start(map, 0, function()
    sol.audio.play_sound("spells/ground_burst_soft_5")
    nodes_done = nodes_done + 1
    for i = 1, num_spokes do
      local angle = (math.pi * 2) / num_spokes * i + random_offset
      local r = (spoke_length / num_nodes) * nodes_done
      local x = math.cos(angle) * r + ox
      local y = math.sin(angle) * r + oy
      create_ichor_root(x, y, oz)
    end
    if nodes_done < num_nodes and enemy:get_life() > 0 then
      return frequency
    end
  end)
end


function enemy:ichor_root_lines()
  local num_lines = 3
  local line_length = 160
  local num_nodes = 9
  local frequency = 800

  local ox, oy, oz = enemy:get_position()
  local lines_done = 0
  sol.timer.start(enemy, 0, function()
    lines_done = lines_done + 1
    local angle = enemy:get_angle(hero)
    for i = 1, num_nodes do
      local r = (line_length / num_nodes) * i
      local x = math.cos(angle) * r + ox
      local y = (math.sin(angle) * r * -1) + oy
      sol.timer.start(map, 100 * i, function()
        create_ichor_root(x, y, oz)
      end)
    end

    if lines_done < num_lines then
      return frequency
    end
  end)
end



--Decision thresholds:
local melee_distance = 128

function enemy:decide_action()
  local life = enemy:get_life()
  if life >= (max_life * .4) then
    enemy:decide_action_phase_1()
  else
    enemy:decide_action_phase_2()
  end
end


function enemy:decide_action_phase_1()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  local sprite = enemy:get_sprite()

  --Long Range
  if (distance >= melee_distance and random <= 70) or (distance >= 64 and random <= 10) then
    enemy:ranged_attack(throw_attack)

  elseif distance >= melee_distance and random <= 85 then
    enemy:boulder_rain_slams()

  elseif distance >= melee_distance and random <= 90 then
    enemy:approach_then_attack{
      dist_threshold = 128,
      speed = 75,
      attack_function = function()
        enemy:aoe_slam()
      end,
    }

  --Close Range:
  elseif distance <= melee_distance and random < 10 then
    enemy:boulder_rain_slams()

  elseif distance <= melee_distance and random < 25 then
    enemy:axe_orbit()

  elseif distance <= melee_distance and random < 50 then
    enemy:aoe_slam()

  elseif distance <= melee_distance then
    enemy:approach_then_attack{
      dist_threshold = 64,
      speed = 75,
      attack_function = function()
        local r = math.random(1, 100)
        if r <= 33 then
          enemy:melee_combo(combo_a)
        elseif r <= 66 then
          enemy:melee_combo(combo_b)
        else
          enemy:melee_combo(combo_c
)
        end
      end,
    }
  else
    sol.timer.start(enemy, 400, function()
      enemy:decide_action()
    end)
  end
end


function enemy:decide_action_phase_2()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  local sprite = enemy:get_sprite()

  --Long Range
  if (distance >= melee_distance and random <= 45) or (distance >= 64 and random <= 10) then
    enemy:ranged_attack(throw_attack)

  elseif distance >= melee_distance and random <= 70 and not enemy.wave_attack_cooldown then
    enemy:ichor_root_attack("wave")
    enemy.wave_attack_cooldown = true
    sol.timer.start(map, 8000, function() enemy.wave_attack_cooldown = nil end)

  elseif distance >= melee_distance and random <= 80 then
    enemy:ichor_root_attack("line")

  elseif distance >= melee_distance and random <= 100 then
    enemy:approach_then_attack{
      dist_threshold = 128,
      speed = 75,
      attack_function = function()
        enemy:aoe_slam()
      end,
    }

  --Close Range:
  elseif distance <= melee_distance and random < 20 and not enemy.wave_attack_cooldown then
    enemy:ichor_root_attack("wave")
    enemy.wave_attack_cooldown = true
    sol.timer.start(map, 8000, function() enemy.wave_attack_cooldown = nil end)

  elseif distance <= melee_distance and random < 40 and not enemy.spoke_cooldown then
    enemy:ichor_root_attack("spoke")
    enemy.spoke_cooldown = true
    sol.timer.start(map, 5000, function() enemy.spoke_cooldown = nil end)

  elseif distance <= melee_distance and random < 60 and not enemy.line_attack_cooldown then
    enemy:ichor_root_attack("line")
    enemy.line_attack_cooldown = true
    sol.timer.start(map, 6000, function() enemy.line_attack_cooldown = nil end)

  elseif distance <= melee_distance and random < 70 then
    enemy:retreat{
      speed = 200,
      max_distance = 48,
      duration = 250,
      animation = "dashing",
      lock_facing = true,
      callback = function()
        enemy:melee_attack(slash_overhead)
      end,
    }

  elseif distance <= melee_distance and random < 90 then
    enemy:aoe_slam()

  elseif distance <= melee_distance then
    enemy:approach_then_attack{
      dist_threshold = 64,
      speed = 75,
      attack_function = function()
        local r = math.random(1, 100)
        if r <= 50 then
          enemy:melee_combo(combo_a)
        else
          enemy:melee_combo(combo_b
)
        end
      end,
    }
  else
    sol.timer.start(enemy, 400, function()
      enemy:decide_action()
    end)
  end
end

