local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

--Stats
local max_life = 1800
local slash_damage = 20


enemy.exp = 200
enemy.weight = 25
enemy.money = 0
enemy.damage_type_mods = {
  --fire = 1.3,
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = max_life,
  detection_distance = 200,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
  sprite_direction_style = "horizontal",
  width = 48,
  height = 32,
})

enemy:register_event("on_created", function()
  enemy:set_dying_sprite_id("enemies/enemy_killed_big")
end)

--------------
--Attacks--
--------------

--Melee
local slash_a = {
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/elk_spirit_attacks",
  weapon_attack_animation = "slash_a_attack",
  step_distance = 32,
  step_tracking = true,
  damage = slash_damage,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_b = {
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/elk_spirit_attacks",
  weapon_attack_animation = "slash_b_attack",
  step_distance = 32,
  step_tracking = true,
  damage = slash_damage,
  damage_type = "physical",
  recovery_duration = 500,
}

local slam_a  = {
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  --weapon_sprite = "enemies/tombwater_enemies/weapons/elk_spirit_attacks",
  --weapon_attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "slam",
  step_distance = 8,
  step_tracking = true,
  damage = slash_damage,
  damage_type = "physical",
  recovery_duration = 500,
  has_aoe = true,
  aoe_delay = 10,
  aoe_offset = 16,
  aoe_sprite = "items/shockwave_3x3",
  aoe_scale = {1.5, 1.5},
  aoe_damage = 30,
  aoe_sound = "running_obstacle",
}

local combo_a = table.duplicate(slash_b)
combo_a.windup_duration, combo_a.recovery_duration = 400, 10
local combo_b = table.duplicate(slash_a)
combo_b.windup_duration, combo_b.recovery_duration = 10, 500
local two_combo = {combo_a, combo_b}

local combo_c = table.duplicate(slash_a)
combo_c.windup_duration, combo_c.recovery_duration = 600, 10
combo_a.windup_sound = "enemies/elk_spirit_growl_short"
local combo_d = table.duplicate(slash_b)
combo_d.windup_duration, combo_d.recovery_duration = 10, 10
local combo_e = table.duplicate(slash_a)
combo_e.windup_duration, combo_e.recovery_duration = 10, 10
local combo_f = table.duplicate(slash_b)
combo_f.windup_duration, combo_f.recovery_duration = 300, 600
local combo_f_slam = table.duplicate(slam_a)
combo_f_slam.windup_duration, combo_f_slam.recovery_duration = 300, 600
local four_combo = {combo_c, combo_d, combo_e, combo_f_slam}

local dash_attack = table.duplicate(slash_a)
dash_attack.windup_duration = 300
dash_attack.step_distance = 64
dash_attack.step_speed = 300



--Howl
function enemy:howl_attack()
  local sprite = enemy:get_sprite()
  sprite:set_animation("howling")
  sol.audio.play_sound("enemies/elk_spirit_howl")
  sol.timer.start(enemy, 700, function()
    --Create effect:
    local x, y, z = enemy:get_position()
    local aoe = map:create_custom_entity{
      x=x, y=y - 32, layer=z, width=16, height = 16, direction=0,
      sprite = "items/screamwave_14x14",
      model = "enemy_projectiles/general_attack",
    }
    aoe.damage= 10
    aoe.damage_frequency = 300
    aoe.damage_type = "magic"
    local aoe_sprite = aoe:get_sprite()
    aoe_sprite:set_animation("starting")
    --Duration:
    sol.timer.start(aoe, 4160, function()
      aoe_sprite:set_animation("ending", function()
        aoe:remove()
      end)
      sprite:set_animation("stopped")
      if math.random(1, 100) <= 75 then --most likely after a howl, do a sapling attack since the player is already at a distance
        enemy:sapling_attack()
      else
        enemy:decide_action()
      end
    end)
  end)
end


--Plant Growth
function enemy:sapling_attack()
  local tree_duration = 1000
  local tree_damage = 20
  local growth_delay = 800
  local sprite = enemy:get_sprite()
  local completed_attacks = 0

  local function make_tree()
    sprite:set_animation("slam_windup")
    sol.audio.play_sound"enemies/elk_spirit_growl"
    sol.timer.start(enemy, 250, function()
      sprite:set_animation("saplings_attack")
      sol.audio.play_sound"running_obstacle"
      completed_attacks = completed_attacks + 1
      sol.timer.start(enemy, 1000, function()
        if completed_attacks < 3 then
          make_tree()
        else
          enemy:decide_action()
        end
      end)
    end)
    local x, y, z = 0,0,0
    sol.timer.start(enemy, growth_delay - 200, function()
      x, y, z = hero:get_position()
    end)
    sol.timer.start(enemy, growth_delay, function()
      local num_trees = math.random(3, 5)
      for i = 1, num_trees do
        local tree = map:create_custom_entity{
          x = x + math.random(-24, 24),
          y = y + math.random(-24, 24),
          layer=z, width=16, height = 16, direction=0,
          sprite = "entities/enemy_projectiles/elk_sapling",
          model = "enemy_projectiles/general_attack",
        }
        --make ragdoll if they hit the hero:
        tree.hit_callback = function(target_hero)
          target_hero:ragdoll(tree:get_angle(target_hero), 64)
        end
        sol.timer.start(tree, 600, function()
          tree.hit_callback = nil
        end)
        --Trees don't do damage for the first few ms:
        tree.harmless = true
        sol.timer.start(tree, 200, function()
          tree.harmless = nil
        end)
        tree.damage = tree_damage
        tree:set_drawn_in_y_order(true)
        sol.timer.start(tree, tree_duration, function()
          tree:get_sprite():set_animation("shrinking", function() tree:remove() end)
        end)
      end
      sol.audio.play_sound"ichor_burst_ground"
    end)

  end

  make_tree()
end


function enemy:back_up()
  enemy:retreat{
    lock_facing = true,
    speed = 120,
    duration = 500,
  }
end


function enemy:dip_attack()
  enemy:retreat{
    lock_facing = true,
    speed = 120,
    duration = 500,
    callback = function()
      local random = math.random(1, 10)
      if random <= 7 then
        enemy:melee_attack(dash_attack)
      else
        enemy:melee_combo(four_combo)
      end
    end
  }
end



--------------
--AI--
--------------
function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  local sprite = enemy:get_sprite()
  local melee_range = 128

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --Long range attacks:
  elseif distance >= melee_range and random <= 35 then
    enemy:sapling_attack()

  elseif distance >= melee_range and random <= 60 then
    enemy.howl_cooldown = true
    sol.timer.start(map, 8000, function() enemy.howl_cooldown = nil end)
    enemy:howl_attack()

  elseif distance >= melee_range and random <= 85 then
    enemy:melee_attack(dash_attack)

  --Melee range attacks:
  elseif random <= 15 and not enemy.howl_cooldown then
    enemy.howl_cooldown = true
    sol.timer.start(map, 8000, function() enemy.howl_cooldown = nil end)
    enemy:howl_attack()

  elseif random <= 35 then
    enemy:dip_attack()

  elseif random <= 60 then
    enemy:approach_then_attack{
      speed = 80,
      attack_function = function() enemy:melee_combo(two_combo) end,
    }

  elseif random <= 70 then
    enemy:sapling_attack()

  else
    sol.timer.start(enemy, 400, function()
      enemy:decide_action()
    end)
  end
end

