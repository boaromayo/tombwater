local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local lifespan_min, lifespan_max = 2500, 5000

function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
end



function enemy:on_restarted()
  enemy:hover()
  --Lifespan:
  sol.timer.start(enemy, math.random(lifespan_min, lifespan_max), function()
    sprite:fade_out()
    sol.timer.start(enemy, 500, function()
      enemy:remove()
    end)
  end)
end


function enemy:hover()
  local m = sol.movement.create"circle"
  m:set_center(enemy:get_position())
  m:set_radius(0)
  m:set_radius_speed(20)
  m:set_clockwise(math.random(1,2) == 1)
  m:set_ignore_obstacles(true)
  m:start(enemy)
  m:set_radius(math.random(4,16))

  sol.timer.start(enemy, math.random(300, 1800), function()
    enemy:dive_hero()
  end)
end


function enemy:dive_hero()
  if enemy:get_distance(hero) <= 180 then
    local m = sol.movement.create"straight"
    m:set_angle(enemy:get_angle(hero))
    m:set_speed(75)
    m:set_ignore_obstacles(true)
    m:start(enemy, function()
      enemy:hover()
    end)
  end
end
