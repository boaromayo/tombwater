local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local hit_damage = 5
local hit_frequency = 700

enemy.exp = 5
enemy.weight = 1
enemy.money = 0


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 5,
  detection_distance = 96,
  abandon_hero_distance = 150,
})


enemy:register_event("on_created", function()

end)


enemy:register_event("on_restarted", function()
  if not enemy.fallen_on_start then
    enemy.fallen_on_start = true
    local sprite = enemy:get_sprite()
    sprite:set_animation("falling", function()
      sprite:set_animation"walking"
      enemy:restart()
    end)
    return
  end
  enemy:set_traversable(true)
  enemy:be_dangerous()
  enemy:decide_action()
end)


function enemy:wander()
  enemy:stop_movement()
--
  local m = sol.movement.create"circle"
  m:set_center(enemy:get_position())
  m:set_radius(0)
  m:set_radius_speed(20)
  m:set_clockwise(math.random(1,2) == 1)
  m:start(enemy)
  m:set_radius(math.random(32,96))

  function m:on_obstacle_reached()
    enemy:restart()
  end
--]]
--  local m = sol.movement.create"random"
--  m:start(enemy)

  sol.timer.start(enemy, math.random(500, 2000), function()
    enemy:decide_action()
  end)
end


function enemy:go_for_hero()
  local m = sol.movement.create"target"
  m:set_speed(65)
  m:start(enemy)
  sol.timer.start(enemy, math.random(1000,3000), function()
    enemy:decide_action()
  end)
end


enemy:register_event("on_position_changed", function(enemy, x, y, z)
  if enemy.attack_mask then
    enemy.attack_mask:set_position(x, y, z)
  end
end)

enemy:register_event("on_dying", function()
  if enemy.attack_mask then
    enemy.attack_mask:remove()
  end
end)


function enemy:be_dangerous()
  local map = enemy:get_map()
  local x, y, z = enemy:get_position()
  if enemy.attack_mask then enemy.attack_mask:remove() end
  local mask = map:create_custom_entity{
    x=x, y=y, layer=z, direction=0, width=16, height=16,
    sprite = "enemies/tombwater_enemies/spiderbug",
  }
  mask:get_sprite():set_animation("attack_mask")
  mask:set_visible(false)
  mask.collided_entities = {}
  mask:add_collision_test("sprite", function(mask, other_entity)
    if other_entity:get_type() == "hero" and not mask.collided_entities[other_entity] and hero:get_can_be_hurt() then
      mask.collided_entities[other_entity] = other_entity
      sol.timer.start(mask, hit_frequency, function() mask.collided_entities[other_entity] = nil end)
      sol.audio.play_sound("hero_hurt_small")
      other_entity:process_hit({
        damage = hit_damage,
        non_staggering = true,
      })
    end
  end)
  enemy.attack_mask = mask
end


function enemy:decide_action()
  enemy:stop_movement()
  local rand = math.random(1, 10)
  if rand <= 5 then
    enemy:go_for_hero()
  else
    enemy:wander()
  end
end

