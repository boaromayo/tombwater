local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.weight = 1
enemy.money = 4
enemy.base_damage = 25
enemy.damage_type_mods = {
  magic = 1.5
}
enemy.alert_all_directions = true

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 75,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local slash_a = {
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/generic_slash",
  weapon_attack_animation = "slash_small_a",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 16,
}

local slash_b = {
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/generic_slash",
  weapon_attack_animation = "slash_small_a",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 16,
}

local combo_a_1 = table.duplicate(slash_a)
combo_a_1.windup_duration, combo_a_1.recovery_duration = 500, 10
local combo_a_2 = table.duplicate(slash_b)
combo_a_2.windup_duration, combo_a_2.recovery_duration = 60, 700
local combo_a = {combo_a_1, combo_a_2}


function enemy:fade_out()
  local fade_time_min, fade_time_max = 3000, 9000
  local sprite = enemy:get_sprite()
  sol.timer.stop_all(enemy)
  enemy:stop_movement()
  sprite:fade_out()
  enemy:make_sound("enemies/ghost_disappear")
  sol.timer.start(enemy, 1000, function()
    enemy:set_visible(false)
    local m = sol.movement.create"circle"
    m:set_center(hero)
    m:set_radius(56)
    m:start(enemy)
    --Invisibly circle the hero for a while:
    sol.timer.start(enemy, math.random(fade_time_min, fade_time_max), function()
      enemy:fade_in()
    end)
  end)
end


function enemy:fade_in()
  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  enemy:set_visible(true)
  sprite:fade_in()
  enemy:make_sound("enemies/ghost_appear")
  enemy:approach_then_attack{
    dist_threshold = 32,
    speed = 85,
    attack_function = function()
      enemy:melee_combo(combo_a)
    end,
  }
end


function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif distance >= 128 or random <= 40 then
    enemy:fade_out()

  elseif random <= 70 then
    enemy:approach_then_attack({
      dist_threshold = jump_distance,
      approach_duration = 1500,
      speed = 80,
      attack_function = function()
        enemy:melee_combo(combo_a)
      end,
    })

  else
    local m = sol.movement.create"random"
    m:start(enemy)
    sol.timer.start(enemy, 2000, function() enemy:decide_action() end)

  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

