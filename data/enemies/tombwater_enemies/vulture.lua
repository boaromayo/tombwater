local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local fly_sprite_offset = 64

enemy.base_damage = 15
enemy.exp = 5
enemy.weight = 3
enemy.money = 3


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 160,
  abandon_hero_distance = 400,
  idle_movement_speed = 80,
  sprite_direction_style = "horizontal"
})

local ram_attack = {
  damage = 20,
  windup_animation = "windup",
  windup_duration = 750,
  windup_sound = "bird_flapping",
  attack_animation = "swooping",
  recovery_animation = "flapping",
  weapon_sprite = "enemies/trillium_enemies/bat",
  weapon_attack_animation = "ramming_slice",
  max_distance = 275,
  speed = 400
}

function enemy:start_aggro()
  local sprite = enemy:get_sprite()
  enemy.aggro = true
  enemy:alert_nearby_enemies()
  sol.timer.stop_all(enemy)
  if enemy:get_property("grounded") then 
    local sprite = enemy:get_sprite()
    --caw or whatever vultures do, or flappy noise
    local m = sol.movement.create"straight"
    m:set_angle(math.rad(90))
    m:set_speed(100)
    m:set_max_distance(math.random(35,45))
    m:start(enemy)
    sprite:set_animation("takeoff", function()
      sprite:set_animation("gliding")
      enemy:move_randomly()
    end)
  else
    enemy:restart()
  end
end

function enemy:move_randomly()
  enemy:set_obstacle_behavior"flying"
  local sprite = enemy:get_sprite()
  sprite:set_animation"gliding"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(100, 125))
  m:set_speed(enemy.idle_movement_speed)

  local directions = {45,135,225,315}
  local other_directions = {0, 180}
  local comeback = {3,4,1,2}
  local rand = math.random(1,4)

  m:set_angle(math.rad(directions[rand]))
  m:start(enemy, function()
    enemy:make_sound"bird_flapping_short"
    sprite:set_animation("flapping", function()
      sprite:set_animation("gliding")
    end)
    local m2 = sol.movement.create"straight"
    m2:set_angle(math.rad(directions[comeback[rand]]))
    m2:set_max_distance(math.random(100, 150))
    m2:set_speed(enemy.idle_movement_speed)
    function m2:on_obstacle_reached()
      enemy:make_sound"bird_flapping_short"
      sprite:set_animation("flapping", function()
        sprite:set_animation("gliding")
      end)
      enemy:decide_action()
    end
    m2:start(enemy,function()
      enemy:make_sound"bird_flapping_short"
      sprite:set_animation("flapping", function()
        enemy:decide_action()
      end)
      
    end)
  end)

  function m:on_obstacle_reached()
    enemy:make_sound"bird_flapping_short"
    sprite:set_animation("flapping", function()
      sprite:set_animation("gliding")
    end)
    enemy:decide_action()
  end
end


function enemy:move_closer_to_hero()
  local sprite = enemy:get_sprite()
  sprite:set_animation"gliding"
  local m = sol.movement.create"straight"
  m:set_angle(enemy:get_angle(hero))
  m:set_speed(enemy.idle_movement_speed + 20)
  m:set_max_distance(math.random(50, 100))
  m:start(enemy,function()
    enemy:decide_action()
  end)
end


function enemy:decide_action()
  local sprite = enemy:get_sprite()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif (distance >= 200) then
    enemy:move_closer_to_hero()
  elseif not enemy.aggro then
      enemy:move_randomly()
  elseif random <= 40 then
    enemy:move_randomly()
  elseif random <=100 and not enemy.ram_cooldown then
    enemy:make_sound"bird_flapping_short"
    sprite:set_animation("flapping", function()
      sprite:set_animation("gliding")
    end)

    local m = sol.movement.create"target"
    local angle = enemy:get_angle(hero)
    enemy:ram_attack(ram_attack)
    enemy.ram_cooldown = true
    sol.timer.start(map, 3000, function()
      enemy.ram_cooldown = false
    end)
  else
    enemy:move_randomly()
  end
end