local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 50
enemy.money = 10
enemy.weight = 10
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 600,
  detection_distance = 200,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
  sprite_direction_style = "diagonal",
})


local gun_attack = {
  windup_animation = "aiming",
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = enemy.base_damage,
  damage_type = "magic",
  recovery_duration = 500,
}

local gun_a = table.duplicate(gun_attack)
gun_a.recovery_duration = 10
local gun_b = table.duplicate(gun_attack)
gun_b.windup_duration, gun_b.recovery_duration = 200, 10
local gun_c = table.duplicate(gun_attack)
gun_c.windup_duration, gun_c.recovery_duration = 200, 1000

local gun_combo = {gun_a, gun_b, gun_c}



local spear_slash = {
  windup_duration = 650,
  windup_sound = "spells/silver_weapon_summon",
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_sound = "weapons/blunt_06",
  weapon_sprite = "enemies/tombwater_enemies/weapons/church_hunter_spear",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage + 5,
  damage_type = "magic",
  step_distance = 32,
  step_tracking = true,
  recovery_duration = 800,
}


local spear_thrust = {
  windup_duration = 650,
  windup_sound = "spells/silver_weapon_summon",
  windup_animation = "slash_windup",
  attack_animation = "thrust_attack",
  attack_sound = "weapons/blunt_07",
  weapon_sprite = "enemies/tombwater_enemies/weapons/church_hunter_spear",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage + 5,
  damage_type = "magic",
  step_distance = 128,
  step_speed = 400,
  step_tracking = true,
  recovery_duration = 800,
}


function enemy:bolts_attack()
  local windup_duration = 1000
  local bolt_frequency = 600
  local num_bolts = 5
  local bolt_damage = enemy.base_damage + 10

  local sprite = enemy:get_sprite()
  sprite:set_animation"kneeling"

  local function create_bolt()
    local x, y, z = hero:get_position()
    local target_circle = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=16, height=16,
      sprite = "items/spell_circle_silver",
      model = "enemy_projectiles/general_attack",
    }
    target_circle.delay_collision = true
    target_circle.damage = enemy.base_damage * 1.5
    target_circle.damage_type = "magic"
    sol.timer.start(target_circle, 400, function()
      sol.audio.play_sound"scenes/silver_flash"
      target_circle:activate_collision()
      local bolt = map:create_custom_entity{
        x=x, y=y, layer=z, direction = 0, width = 16, height = 16,
        sprite = "items/silverbeam",
      }
      bolt:set_drawn_in_y_order(true)
      bolt:get_sprite():set_animation("beam", function() bolt:remove() end)
      target_circle:get_sprite():fade_out()
      sol.timer.start(target_circle, 200, function() target_circle:remove() end)
    end)
  end

  local i = 0
  sol.timer.start(enemy, windup_duration, function()
    create_bolt()
    i = i + 1
    if i >= num_bolts then
      enemy:decide_action()
    else
      return bolt_frequency
    end
  end)
end


function enemy:aoe_prayer()
  local windup_duration = 2000
  local circle_time = 600
  local bolt_frequency = 600
  local num_bolts = 5
  local bolt_damage = enemy.base_damage + 10

  local sprite = enemy:get_sprite()
  sprite:set_animation"kneeling"
  sol.timer.start(enemy, windup_time - 1300, function()
    sol.audio.play_sound"spells/silver_bass_drop"
  end)

  local target_circle
  sol.timer.start(enemy, circle_time, function()
    local x, y, z = enemy:get_position()
    target_circle = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=16, height=16,
      sprite = "effects/eye_rune_large",
    }
  end)

  sol.timer.start(enemy, windup_duration, function()
    local x, y, z = enemy:get_position()
    sol.audio.play_sound"spells/silver_aoe"
    local beam = map:create_custom_entity{
      x=x, y=y, layer=z, direction = 0, width = 16, height = 16,
      sprite = "items/silverbeam_aoe",
      model = "enemy_projectiles/general_attack",
    }
    beam.damage = enemy.base_damage * 2
    beam.damage_type = "magic"
    target_circle:get_sprite():fade_out()
    sol.timer.start(enemy, 500, function()
      target_circle:remove()
      enemy:decide_action()
    end)
  end)

end


local dodge_range = 72
local melee_range = 64
local gun_range = 56
local gun_cooldown_time = 4000
local backstep_cooldown_time = 3000
local bolts_cooldown_time = 7000
local aoe_cooldown_time = 3000

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 100)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --Long range:----------
  --Gun:
  elseif distance >= gun_range and enemy:has_los(hero) and not enemy.gun_cooldown then
    enemy:ranged_combo(gun_combo)
    enemy.gun_cooldown = true
    sol.timer.start(map, gun_cooldown_time, function() enemy.gun_cooldown = false end)

  --Bolts:
  elseif distance >= melee_range and not enemy.bolts_cooldown then
    enemy:bolts_attack()
    enemy.bolts_cooldown = true
    sol.timer.start(map, bolts_cooldown_time, function() enemy.bolts_cooldown = false end)

  --Thrust:
  elseif distance >= melee_range and rand <= 40 then
    enemy:approach_then_attack{
      dist_threshold = 120,
      approach_speed = 80,
      attack_function = function() enemy:melee_attack(spear_thrust) end,
    }

  --Short range:----------
  --Backstep:
  elseif distance <= dodge_range and not enemy.backstep_cooldown then
    enemy:retreat{
      speed = 150,
      max_distance = 48,
      lock_facing = true,
    }
    enemy.backstep_cooldown = true
    sol.timer.start(map, backstep_cooldown_time, function()
      enemy.backstep_cooldown = false
    end)

  --AOE
  elseif distance <= melee_range and rand <= 40 and not enemy.aoe_cooldown then
    enemy:aoe_prayer()
    enemy.aoe_cooldown = true
    sol.timer.start(map, aoe_cooldown_time, function() enemy.aoe_cooldown = false end)

  --Melee
  elseif distance <= melee_range then
    enemy:melee_attack(spear_slash)

  --Any range:----------
  elseif rand <= 40 then
    enemy:approach_then_attack{
      attack_function = function() enemy:melee_attack(spear_slash) end,
    }

  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end


--Add water sprite while in shallow water:
local shallow_water_sprite
enemy:register_event("on_position_changed", function()
  local ground = enemy:get_ground_below()
  if shallow_water_sprite and ground ~= "shallow_water" then
    enemy:remove_sprite(shallow_water_sprite)
    shallow_water_sprite = nil
  elseif not shallow_water_sprite and ground == "shallow_water" then
    shallow_water_sprite = enemy:create_sprite("enemies/tombwater_enemies/church_hunter")
    shallow_water_sprite:set_animation"shallow_water"
  end
end)

