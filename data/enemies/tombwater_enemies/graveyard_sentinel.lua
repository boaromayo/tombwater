local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

--Stats
local max_life = 950
local sword_damage = 20
local shockwave_damage = 35


enemy.exp = 150
enemy.weight = 25
enemy.money = 0
enemy.damage_type_mods = {
  fire = 1.3
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = max_life,
  detection_distance = 200,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
  sprite_direction_style = "horizontal",
})


local slash_forehand = {
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  weapon_attack_animation = "slash_forehand_attack",
  step_distance = 24,
  step_tracking = true,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}


local slash_backhand = {
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  weapon_attack_animation = "slash_backhand_attack",
  step_distance = 16,
  step_tracking = true,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}

local thrust = {
  windup_duration = 300,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  weapon_attack_animation = "thrust_attack",
  step_distance = 48,
  step_speed = 150,
  step_tracking = true,
  damage = sword_damage + 10,
  damage_type = "physical",
  recovery_duration = 800,
}

local combo_a = table.duplicate(slash_backhand)
combo_a.windup_duration, combo_a.recovery_duration = 400, 10
local combo_b = table.duplicate(slash_forehand)
combo_b.windup_duration, combo_b.recovery_duration = 10, 500
local two_combo = {combo_a, combo_b}

local combo_c = table.duplicate(slash_forehand)
combo_c.windup_duration, combo_c.recovery_duration = 10, 10
local combo_d = table.duplicate(slash_backhand)
combo_d.windup_duration, combo_d.recovery_duration = 10, 10
local combo_e = table.duplicate(slash_forehand)
combo_e.windup_duration, combo_e.recovery_duration = 10, 10
local combo_f = table.duplicate(slash_backhand)
combo_f.windup_duration, combo_f.recovery_duration = 10, 600
local four_combo = {combo_c, combo_d, combo_e, thrust}


local shovel_slam = {
  windup_animation = "jumping_hover",
  windup_duration = 10,
  attack_animation = "jumping_slam",
  attack_sound = "running_obstacle",
  weapon_sprite = "enemies/trillium_enemies/shockwave_8x7",
  weapon_attack_animation = "stopped",
  damage = sword_damage + 20,
  turn_toward_hero = false,
  aoe_damage = shockwave_damage,
  aoe_scale = {1.5, 1.5},
  aoe_screenshake = 4,
  recovery_duration = 1000,
  next_action = function()
    enemy.lock_facing = false
    enemy:decide_action()
  end,
}


local spear_dash = {
  windup_animation = "spear_dash_windup",
  windup_duration = 900,
  speed = 400,
  damage = sword_damage,
  attack_animation = "spear_dash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/graveyard_sentinel_shovel",
  weapon_attack_animation = "spear_dash",
  attack_sound = "weapons/slash_05",
  max_distance = 240,
  recovery_duration = 400,
}

--Decision thresholds:
local melee_distance = 160
local jump_distance = 190
local dash_distance = 72
local previous_move = ""
local first_aggro = true

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  local sprite = enemy:get_sprite()

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif first_aggro then
    first_aggro = false
    enemy:stop_movement()
    sprite:set_animation("screaming")
    sol.audio.play_sound("enemies/graveyard_sentinel_scream")
    sprite:set_direction(enemy:get_facing_direction_to(hero))
    --should roar or osmething nazgul screech?, give the player time to get in room, know that a miniboss encounter is happening?      
    sol.timer.start(enemy, 2000, function()
      enemy:decide_action() 
    end)
  
  --Spinning combo
  elseif distance <= melee_distance and random < 40 and previous_move ~= "spinning_combo" then
    previous_move = "spinning_combo"
    enemy:approach_then_attack{
      speed = 55,
      approach_duration = 2000,
      attack_function = function()
        local spin_time = 600
        sprite:set_animation("spinning_shovel")
        --Spinning sound:
        local elapsed_time = 0
        local sound_delay = 150
        sol.timer.start(enemy, 0, function()
          if elapsed_time >= spin_time then return end
          sol.audio.play_sound("weapons/slash_02")
          elapsed_time = elapsed_time + sound_delay
          return sound_delay
        end)
        --Attack
        sol.timer.start(enemy, spin_time, function()
          enemy:melee_combo(four_combo)
        end)
      end,
    }

  --Dive bomb attack:
  elseif distance <= jump_distance and random < 65 and previous_move ~= "dive_bomb_2" then
    if previous_move == "dive_bomb" then
      previous_move = "dive_bomb_2"
    else
      previous_move = "dive_bomb"
    end
    local jump_windup_duration = 800
    sprite:set_direction(enemy:get_facing_direction_to(hero))
    sprite:set_animation("jumping_crouch")
    sol.timer.start(enemy, jump_windup_duration, function()
      sprite:set_animation("jumping")
      sol.audio.play_sound"jump"
      enemy:stop_movement()
      local m = sol.movement.create"straight"
      m:set_angle(enemy:get_angle(hero))
      m:set_speed(300)
      m:set_max_distance(math.min(jump_distance, enemy:get_distance(hero)))
      sprite:set_direction(enemy:get_facing_direction_to(hero))
      enemy.lock_facing = true
      m:start(enemy, function()
        sol.timer.start(enemy, 300, function()
          enemy:melee_attack(shovel_slam)
        end)
      end)
    end)
    
  --Spear dash
  elseif (distance > dash_distance and random < 85) then
    previous_move = "spear_dash"
    enemy:ram_attack(spear_dash)

  elseif distance <= melee_distance then
    enemy:approach_then_attack{
      speed = 55,
      approach_duration = 2000,
      attack_function = function() enemy:melee_combo(two_combo) end,
    }

  else
    sol.timer.start(enemy, 400, function()
      enemy:decide_action()
    end)
  end
end

