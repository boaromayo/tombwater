local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 5
enemy.weight = 2
enemy.money = 0

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 30,
  detection_distance = 96,
  abandon_hero_distance = 150,
  idle_movement_speed = 50,
})

local ram_attack = {
  damage = 15,
  windup_duration = 800,
  windup_animation = "windup",
  attack_animation = "attacking",
  weapon_sprite = "enemies/tombwater_enemies/rat",
  weapon_attack_animation = "slash_attack",
  max_distance = 56,
  speed = 110,
  smooth = true,
  recovery_duration = 600,
}


enemy:register_event("on_restarted", function()
  enemy:set_traversable(true)
end)


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 10)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif not enemy.ram_cooldown and (math.random(1, 3) == 1)then
        enemy:make_sound("enemies/rat_0" .. math.random(1,4))
    enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
    enemy:ram_attack(ram_attack)
    enemy.ram_cooldown = true
    sol.timer.start(map, 2000, function()
      enemy.ram_cooldown = false
    end)
  elseif random <= 2 then
    enemy:stop_movement()
    enemy:get_sprite():set_animation"stopped"
    sol.timer.start(enemy, 1000, function() enemy:decide_action() end)

  else
    enemy:move_randomly()
  end
end


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(24, 56))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end

