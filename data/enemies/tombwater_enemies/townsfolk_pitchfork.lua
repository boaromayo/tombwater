local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 22
enemy.weight = 10
enemy.money = 2

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 80,
  detection_distance = 200,
  abandon_hero_distance = 400,
})

local thrust_attack = {
  windup_duration = 500,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 500,
}

local slam_attack = {
  windup_duration = 1000,
  windup_animation = "overhead_windup",
  attack_animation = "overhead_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = 40,
  step_distance = 16,
  damage_type = "physical",
  has_aoe = true,
  aoe_offset = 48,
  aoe_sprite = "items/shockwave_3x3",
  aoe_damage = 30,
  aoe_sound = "running_obstacle",
  recovery_duration = 500,
}

local combo_1 = {
  windup_duration = 900,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 15,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 10,
}

local combo_2 = {
  windup_duration = 10,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 20,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 10,
}

local combo_3 = {
  windup_duration = 10,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 20,
  step_distance = 28,
  damage_type = "physical",
  recovery_duration = 1100,
}

local combo_a = {combo_1, combo_2, combo_3}
local combo_b = {combo_1, combo_3}


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  else
    enemy:townsfolk_sound_chance()
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:townsfolk_sound_chance()
        local rand = math.random(1, 3)
        if rand == 1 then
          enemy:melee_combo(combo_a)
        elseif rand == 2 then
          enemy:melee_combo(combo_b)
        else
          enemy:melee_attack(slam_attack)
        end
      end
    })
  end
end
