local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 20
enemy.weight = 10
enemy.money = 2
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 50,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local ranged_attack = {
  windup_animation = "aiming",
  attack_sound = "weapons/gun_rifle_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = enemy.base_damage,
}

local rc1 = table.duplicate(ranged_attack)
rc1.windup_duration, rc1.recovery_duration = 700, 10
local rc2 = table.duplicate(ranged_attack)
rc2.windup_duration, rc2.recovery_duration = 200, 500
local two_combo = {rc1, rc2,}

local quick_attack = table.duplicate(ranged_attack)
quick_attack.windup_time = 300


local charge_attack = {
  windup_animation = "aiming_charge",
  windup_duration = 1000,
  attack_sound = "weapons/gun_rifle_01",
  generic_projectile_type = "worms",
  projectile_sprite = "entities/enemy_projectiles/ichor_glob",
  damage = enemy.base_damage * 1.5,
  projectile_properties = {
    --
  },
}


local attack_range = 64
local attack_cooldown_time = 2500

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)

  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif distance >= attack_range and enemy:has_los(hero) and not enemy.attack_cooldown then
    if random <= 40 then
      enemy:ranged_attack(charge_attack)
    else
      enemy:ranged_combo(two_combo)
    end
    enemy.attack_cooldown = true
    sol.timer.start(map, attack_cooldown_time + math.random(-500, 500), function() enemy.attack_cooldown = false end)

  elseif distance < 32 or (distance < attack_range and random <= 40) then
    enemy:retreat{
      speed = 200,
      duration = 200,
      lock_facing = true,
      callback = function() enemy:ranged_attack(ranged_attack) end,
    }

  elseif distance < attack_range then
    enemy:retreat()

  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end
