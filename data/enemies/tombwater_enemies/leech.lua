local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite

local hit_damage = 5
local hit_frequency = 400

enemy.exp = 5
enemy.weight = 1
enemy.money = 0


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 5,
  detection_distance = 96,
  idle_movement_speed = 25,
  abandon_hero_distance = 150,
  sprite_direction_style = "single",
})


enemy:register_event("on_created", function()
  sprite = enemy:get_sprite()
end)

enemy:register_event("on_restarted", function()
  if enemy:get_property("underground") then
    sprite:set_animation"hibernating_water"
  end
end)


function enemy:stop_hibernating()
  enemy:set_property("hibernating", nil)
  enemy:start_aggro()
end

function enemy:start_aggro()
  if enemy:get_property("hibernating") then
    return
  end
  enemy.aggro = true
  enemy:alert_nearby_enemies()
  sol.timer.stop_all(enemy)
  if enemy:get_property("underground") then
    local sprite = enemy:get_sprite()
    enemy:set_property("underground", nil)
    sprite:set_animation("waking_up_water", function()
      sprite:set_animation("stopped")
      enemy:restart()
    end)
  else
    enemy:restart()
  end
end


enemy:register_event("on_restarted", function()
  enemy:set_traversable(true)
  enemy:be_dangerous()
  if enemy.aggro then
    enemy:decide_action()
  end
end)


function enemy:wander()
  enemy:stop_movement()
  local m = sol.movement.create"circle"
  m:set_center(enemy:get_position())
  m:set_radius(0)
  m:set_radius_speed(20)
  m:set_clockwise(math.random(1,2) == 1)
  m:start(enemy)
  m:set_radius(math.random(32,96))

  function m:on_obstacle_reached()
    enemy:restart()
  end

  sol.timer.start(enemy, math.random(500, 2000), function()
    enemy:decide_action()
  end)
end


function enemy:go_for_hero()
  enemy:get_sprite():set_animation("walking")
  local m = sol.movement.create"target"
  m:set_speed(65)
  m:start(enemy)
  sol.timer.start(enemy, math.random(2000,4000), function()
    enemy:decide_action()
  end)
end


function enemy:jump_toward_hero()
  local m = sol.movement.create"straight"
  m:set_angle(enemy:get_angle(hero))
  m:set_speed(110)
  m:set_max_distance(56)
  m:start(enemy)
  sol.timer.start(enemy, 1100, function()
    enemy:decide_action()
  end)
end


enemy:register_event("on_position_changed", function(enemy, x, y, z)
  if enemy.attack_mask then
    enemy.attack_mask:set_position(x, y, z)
  end
  local ground = enemy:get_ground_below()
  local animation = sprite:get_animation()
  if (ground == "shallow_water") and not animation:match("_water") then
    sprite:set_animation(animation .. "_water")
  elseif (ground ~= "shallow_water") and animation:match("_water") then
    sprite:set_animation(animation:gsub("_water", ""))
  end
end)

enemy:register_event("on_dying", function()
  if enemy.attack_mask then
    enemy.attack_mask:remove()
  end
end)


function enemy:be_dangerous()
  local map = enemy:get_map()
  local x, y, z = enemy:get_position()
  if enemy.attack_mask then enemy.attack_mask:remove() end
  local mask = map:create_custom_entity{
    x=x, y=y, layer=z, direction=0, width=16, height=16,
    sprite = "enemies/tombwater_enemies/spiderbug",
  }
  mask:get_sprite():set_animation("attack_mask")
  mask:set_visible(false)
  mask.collided_entities = {}
  mask:add_collision_test("sprite", function(mask, other_entity)
    if other_entity:get_type() == "hero" and not mask.collided_entities[other_entity] and hero:get_can_be_hurt() then
      mask.collided_entities[other_entity] = other_entity
      sol.timer.start(mask, hit_frequency, function() mask.collided_entities[other_entity] = nil end)
      sol.audio.play_sound("hero_hurt_small")
      other_entity:process_hit({
        damage = hit_damage,
        non_staggering = true,
      })
      hero:build_up_status_effect("bleed", 15)
    end
  end)
  enemy.attack_mask = mask
end


function enemy:decide_action()
  local attack_range = 72
  enemy:stop_movement()
  local dist = enemy:get_distance(hero)
  local rand = math.random(1, 10)

  if dist <= 64 then
    enemy:jump_toward_hero()
  elseif rand <= 5 and dist <= attack_range then
    enemy:go_for_hero()
  else
    enemy:wander()
  end
end

