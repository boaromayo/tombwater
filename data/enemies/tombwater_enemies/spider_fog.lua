local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()


enemy.exp = 30
enemy.weight = 15
enemy.money = 5
enemy.base_damage = 30
enemy.damage_type_mods = {
  fire = 1.5
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 150,
  detection_distance = 200,
  abandon_hero_distance = 400,
  width = 32, height = 32,
  origin_x = 16, origin_y = 29,
})


local slash_a = {
  windup_duration = 800,
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/spider_arm",
  weapon_attack_animation = "slash_a_attack",
  step_distance = 32,
  --step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 600,
}

local slash_b = {
  windup_duration = 800,
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/spider_arm",
  weapon_attack_animation = "slash_b_attack",
  step_distance = 32,
  --step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 600,
}

local cmb_atk_1 = table.duplicate(slash_a)
cmb_atk_1.recovery_duration = 10
local cmb_atk_2 = table.duplicate(slash_b)
cmb_atk_1.windup_duration = 100
local combo_1 = {cmb_atk_1, cmb_atk_2}


local ranged_attack = {
  windup_animation = "shooting_windup",
  windup_duration = 500,
  attack_animation = "shooting",
  projectile_offset_radius = 24,
  projectile_model = "enemy_projectiles/generic_projectile",
  generic_projectile_type = "webball",
  aim_type = "any",
  projectile_sprite = "entities/enemy_projectiles/webball",
  projectile_width = 8,
  projectile_height = 8,
  damage = 2,
  damage_type = "physical",
  recovery_duration = 800,
}


local ranged_range = 48

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif hero:is_status_effect_active("webbed") then
    enemy:approach_then_attack({
      approach_duration = 2000,
      speed = 85,
      attack_function = function()
        enemy:melee_combo(combo_1)
      end
    })

  elseif (distance >= ranged_range and enemy:has_los(hero) and random <= 80 ) or ( random <= 20 ) then
    enemy:ranged_attack(ranged_attack)

  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:melee_combo(combo_1)
      end
    })
  end
end


