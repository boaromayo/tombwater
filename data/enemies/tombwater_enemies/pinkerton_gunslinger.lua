local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.money = 3
enemy.weight = 10
enemy.base_damage = 20

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 75,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})


local ranged_attack = {
  windup_animation = "gun_aiming",
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = enemy.base_damage,
  recovery_duration = 0,
}

local ranged_attack_2 = {
  windup_animation = "gun_aiming",
  windup_duration = 200,
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = enemy.base_damage,
  recovery_duration = 0,
}

local ranged_attack_3 = {
  windup_animation = "gun_aiming",
  windup_duration = 200,
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = enemy.base_damage,
  recovery_duration = 1000,
}

local ranged_combo = {ranged_attack, ranged_attack_2, ranged_attack_3}



local slash_attack = {
  windup_animation = "slash_windup",
  windup_sound = "enemies/townsfolk_vox/moan_01",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage + 5,
  damage_type = "physical",
  recovery_duration = 800,
}


local dodge_range = 48
local melee_range = 24
local attack_cooldown_time = 3000

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance >= dodge_range and enemy:has_los(hero) and not enemy.attack_cooldown then
    sol.audio.play_sound("enemies/townsfolk_vox/grunt_high_05")
    enemy:ranged_combo(ranged_combo)
    enemy.attack_cooldown = true
    enemy.backstep_cooldown = false
    sol.timer.start(map, attack_cooldown_time, function() enemy.attack_cooldown = false end)
  elseif distance >= dodge_range and not enemy:has_los(hero) then
    enemy:approach_hero({
      speed = 60,
      dist_threshold = 40,
      approach_duration = 1000,
    })
  elseif distance <= melee_range then
    enemy:melee_attack(slash_attack)

  elseif distance > melee_range and distance < dodge_range and not enemy.backstep_cooldown then
    enemy:backstep()
    enemy.backstep_cooldown = true
  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end


function enemy:backstep(attrs)
  attrs = attrs or {}
  local sprite = enemy:get_sprite()
  local speed = attrs.speed or 140
  local duration = attrs.duration or 300
  local angle = hero:get_angle(enemy)
  local test_dist = 8
  local is_blocked = enemy:test_obstacles(test_dist * math.cos(angle), test_dist * math.sin(angle))
  if is_blocked then angle = angle + (math.pi / 2 * math.random(1, 3)) end

  local m = sol.movement.create("straight")
  m:set_angle(angle)
  m:set_speed(speed)
  m:start(enemy)
  sprite:set_animation"backstepping"
  sol.timer.start(enemy, duration, function()
    m:stop()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
end

