local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 20
enemy.weight = 10
enemy.money = 2
enemy.base_damage = 15

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 40,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 25,
})

local quick_knife_attack = {
  windup_duration = 100,
  windup_animation = "forehand_slash_windup",
  attack_animation = "forehand_slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 500,
  step_distance = 24,
}


local grenade_attack = {
  windup_animation = "throwing_windup",
  attack_animation = "throwing",
  attack_sound = "throw",
  generic_projectile_type = "bomb",
  projectile_sprite = "hero_projectiles/grenade",
  damage = enemy.base_damage,
}


local scatter_grenade_attack = {
  windup_animation = "throwing_windup",
  attack_animation = "throwing",
  attack_sounds = {"throw", "enemies/townsfolk_vox/grunt_high_02"},
  generic_projectile_type = "scatter_grenade",
  num_projectiles = 8,
  projectile_sprite = "hero_projectiles/grenade",
  damage = enemy.base_damage,
}


local cluster_grenade_attack = {
  windup_animation = "overhead_windup",
  attack_animation = "throwing",
  attack_sounds = {"throw", "enemies/townsfolk_vox/grunt_high_04"},
  generic_projectile_type = "bomb",
  projectile_sprite = "hero_projectiles/grenade",
  damage = enemy.base_damage,
  projectile_properties = {
    obstacle_callback = function(proj)
      proj:get_sprite():set_animation("stopped")
      sol.timer.start(map, 1000, function()
        --extra big explosion
        local x, y, z = proj:get_position()
        local scatter = 32
        for i = 1, math.random(5, 8) do
          sol.timer.start(map, math.random(0, 200), function()
            map:create_explosion{
              x = x + math.random(scatter * -1, scatter),
              y = y + math.random(scatter * -1, scatter),
              layer = z,
              sprite = "items/explosion_small",
            }
          end)
        end
        proj:pop_remove()
      end)
    end,
  },
}




local retreat_range = 48
local attack_cooldown_time = 2500

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --dodge back if way close:
  elseif distance <= 32 then
    enemy:retreat{
      speed = 150,
      duration = 200,
      lock_facing = true,
      callback = function()
        enemy:melee_attack(quick_knife_attack)
      end,
    }

  --Retreat
  elseif distance < retreat_range then
    enemy:retreat({
      duration = 350,
    })

  --Bombs:
  elseif enemy:has_los(hero) and not enemy.attack_cooldown then
    local random = math.random(1, 100)
    if random <= 65 then
      enemy:ranged_attack(grenade_attack)
    elseif random <= 85 then
      enemy:ranged_attack(scatter_grenade_attack)
    else
      enemy:ranged_attack(cluster_grenade_attack)
    end
    enemy.attack_cooldown = true
    sol.timer.start(map, attack_cooldown_time + math.random(-500, 500), function() enemy.attack_cooldown = false end)

  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end
