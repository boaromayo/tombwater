local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 15
enemy.exp = 85
enemy.weight = 25
enemy.money = 0

local projectile_range = 250

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 130,
  detection_distance = 96,
  abandon_hero_distance = 300,
  idle_movement_speed = 25,
  height = 32,
  sprite_direction_style = "horizontal",
})


local ranged_attack = {
  windup_animation = "shooting",
  windup_duration = 500,
  windup_sound = "spells/vomit",
  attack_sound = "enemies/worm_cannon",
  generic_projectile_type = "bullet",
  projectile_sprite = "items/worm_missile",
  damage = enemy.base_damage,
  num_projectiles = 3,
  projectile_properties = {
    max_distance = projectile_range,
  },
  recovery_animation = "stopped",
  recovery_duration = 800,
}


enemy:register_event("on_hurt", function()
  enemy:create_minion()
end)


function enemy:create_minion()
  local x, y, z = enemy:get_position()
  local aim_angle = hero:get_angle(enemy) + math.rad(math.random(-15, 15))
  local shot_range = 32
  sol.audio.play_sound("spells/vomit")
  local blob = map:create_custom_entity{
    x=x, y=y, layer=z, direction=0, width=8, height=8,
    sprite = "entities/enemy_projectiles/ichor_blob",
    model = "enemy_projectiles/generic_projectile",
  }
  blob.damage = blob_damage
  blob.speed = 130
  blob.max_distance = shot_range + math.random(-24, 24)
  blob.obstacle_callback = function(projectile)
    local x, y, z = projectile:get_position()
    projectile:remove()
    --Create small eye:
    local minion = map:create_enemy{
      x = x, y = y, layer = z,
      direction = 0,
      breed = "tombwater_enemies/eye_orb_minion",
    }
    minion:set_property("idle_movement_type", "random")
    minion:approach_hero{
      speed = 45,
      approach_duration = 2000,
    }
    local smoke_sprite = minion:create_sprite("entities/enemy_projectiles/eldritch_thread_smoke")
    smoke_sprite:set_animation("burst", function()
      minion:remove_sprite(smoke_sprite)
    end)
  end
  blob:shoot(aim_angle + math.rad(math.random(-45, 45)))
end


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(24, 56))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end


function enemy:decide_action()
  local sprite = enemy:get_sprite()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 10)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif random <= 3 then
    enemy:approach_then_attack{
      speed = 50,
      approach_duration = 2000,
      dist_threshold = 96,
      attack_function = function()
        local em = map:create_particle_emitter(0, 0, 0)
        em.target = enemy
        em.duration = 400
        em.angle_variance = math.rad(20)
        em.particle_speed = 80
        em.particles_per_loop = 2
        em.particle_sprite = "effects/particle_small_2"
        em.particle_color = {100, 0, 10}
        em.particle_opacity = {150,200}
        em.particle_fade_speed = 30
        em:emit()
        enemy:ranged_attack(ranged_attack)
      end,
    }
  elseif random <= 4 then
    enemy:stop_movement()
    sprite:set_animation("waving", "stopped")
    enemy:create_minion()
    sol.timer.start(enemy, 2000, function() enemy:decide_action() end)
  else
    enemy:stop_movement()
    enemy:move_randomly()
    sol.timer.start(enemy, 1500, function() enemy:decide_action() end)
  end
end


