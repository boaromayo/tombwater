local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 15
enemy.exp = 25
enemy.weight = 10
enemy.money = 4

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 80,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_life_percent = 15,
})

local slash_attack = {
  step_distance =15,
  windup_duration = 150,
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 200,
}

local slash_backoff_attack = {
  step_distance =15,
  windup_duration = 150,
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 0,
  next_action = function() enemy:make_distance() end
}

local combo_1 = {
  step_distance = 30,
  windup_duration = 900,
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 0,
}

local combo_2 = {
  step_distance = 30,
  windup_duration = 0,
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 0,
}

local combo_3 = {
  step_distance = 30,
  windup_duration = 0,
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 1200,
}

local combo_a = {combo_1, combo_2, combo_3}
local previous_attack = 0

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 6)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  else
    if math.random(1,10) <= 6 then
      sol.audio.play_sound("enemies/townsfolk_vox/snarl_02")
    end
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 130,
      attack_function = function()
        if rand <=3 then
          enemy:melee_attack(slash_attack)
          previous_attack = 1
        elseif rand <=4 and previous_attack ~= 2 then
          enemy:melee_combo(combo_a)
          previous_attack = 2
        elseif rand <= 6 and previous_attack < 3 then
          enemy:melee_attack(slash_backoff_attack)
          previous_attack = 3
        else
          enemy:decide_action()
        end
      end
    })
  end
end


function enemy:make_distance()
  local angle = enemy:get_angle(hero)
  local sprite = enemy:get_sprite()
  sol.timer.start(enemy, 5, function()
    sol.audio.play_sound("enemies/townsfolk_vox/grunt_high_03")
    sprite:set_direction(enemy:get_direction4_to(hero))
    enemy.lock_facing = true
    local m = sol.movement.create"straight"
    m:set_angle(angle + math.pi)
    m:set_speed(250)
    m:set_max_distance(40)

    local function recover()
      sprite:set_animation"stopped"
      enemy.lock_facing = false
      sol.timer.start(enemy, 200, function()
        enemy:decide_action()
      end)
    end

    m:start(enemy, function()
      recover()
    end)
    function m:on_obstacle_reached()
      recover()
    end
  end)
end