local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.base_damage = 15
enemy.exp = 20
enemy.weight = 15
enemy.money = 4

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 75,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
  sprite_direction_style = "horizontal"
})

local slash_distance = 64
local spit_distance = 25

local slash_attack = {
  windup_duration = 750,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/coyote_infected_weapon",
  --weapon_windup_animation = "windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  step_distance = slash_distance,
  step_speed = 250,
  step_tracking = true,
  recovery_duration = 400,
}

local spit_attack = {
  windup_duration = 450,
  windup_animation = "spit_windup",
  windup_sound = "enemies/coyote_infected_scream_short",
  attack_animation = "spit_attack",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/coyote_infected_weapon",
  weapon_attack_animation = "spit_attack",
  damage = 15,
  damage_type = "physical",
  step_distance = spit_distance,
  step_speed = 200,
  recovery_duration = 300
}

enemy:register_event("on_restarted", function()
  if enemy:get_property("untransformed") then
    local sprite = enemy:get_sprite()
    sprite:set_animation"untransformed"
  end
end)

function enemy:start_aggro()
  local sprite = enemy:get_sprite()
  enemy.aggro = true
  enemy:alert_nearby_enemies()
  sol.timer.stop_all(enemy)
  if enemy:get_property("untransformed") then 
   sprite:set_animation("transforming", function()
    sprite:set_animation("screaming")
    sol.timer.start(enemy, 500, function()
      enemy:restart()
    end)
   end)
   enemy:set_property("untransformed", nil)
  else
    enemy:restart()
  end
end

function enemy:scream()
  local sprite = enemy:get_sprite()
  sprite:set_animation("screaming")
  sol.audio.play_sound("enemies/coyote_infected_scream_2")
  sol.timer.start(enemy, 300, function()
      enemy:decide_action()
  end)
end

function enemy:do_spit()
  enemy:approach_then_attack({
    dist_threshold = spit_distance,
    approach_duration = 2000,
    speed = 120,
    attack_function = function() enemy:melee_attack(spit_attack) end
  })
end

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1,100)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif rand <= 10 then
    enemy:scream()
  elseif rand <=20 then
    enemy:retreat({
      speed = 200,
      max_distance = 48,
      animation = "leap",
      lock_facing = true,
    })
  elseif rand <= 60 then
    enemy:do_spit()
  else
    enemy:approach_then_attack({
      dist_threshold = slash_distance,
      approach_duration = 1500,
      speed = 80,
      attack_function = function()
        enemy:melee_attack(slash_attack)
      end
    })
  end
end
