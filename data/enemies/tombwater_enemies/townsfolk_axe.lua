local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 20
enemy.exp = 25
enemy.weight = 10
enemy.money = 3

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 95,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local ambush_attack = {
  windup_duration = 50,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 32,
  recovery_duration = 200,
}


local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage * 1.2,
  step_distance = 24,
  damage_type = "physical",
  recovery_duration = 500,
}

local slam_attack = {
  windup_animation = "overhead_windup",
  attack_animation = "overhead_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = enemy.base_damage * 1.8,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_attack = {
  windup_animation = "slash_windup",
  windup_duration = 300,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 200,
}

local backslash_attack = {
  windup_animation = "reverse_slash_windup",
  windup_duration = 200,
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 200,
}

local combo_a1 = {
  windup_animation = "slash_windup",
  windup_duration = 500,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage * .8,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 30,
}

local combo_a2 = {
  windup_animation = "reverse_slash_windup",
  windup_duration = 30,
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 30,
}

local combo_a3 = {
  windup_animation = "thrust_windup",
  windup_duration = 300,
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 800,
}

local combo_b3 = {
  windup_animation = "slash_windup",
  windup_duration = 30,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 30,
}

local combo_b4 = {
  windup_animation = "reverse_slash_windup",
  windup_duration = 30,
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 1000,
}

local combo_a = {combo_a1, combo_a2, combo_a3}
local combo_b = {combo_a1, combo_a2, combo_b3, combo_b4}


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  else
    enemy:townsfolk_sound_chance()
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:townsfolk_sound_chance()
        local rand = math.random(1,4)
        if rand == 1 then
          enemy:melee_combo(combo_a)
        elseif rand == 2 then
          enemy:melee_combo(combo_b)
        elseif enemy:is_aligned(hero, 10) then
          enemy:melee_attack(slam_attack)
        else
          enemy:melee_attack(backslash_attack)
        end
      end
    })
  end
end
