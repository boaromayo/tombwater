local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 22
enemy.weight = 10
enemy.money = 2
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 80,
  detection_distance = 200,
  abandon_hero_distance = 400,
})


local function make_puddle(x, y, z)
  map:create_custom_entity{
    x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
    model = "enemy_projectiles/floor_blast",
    sprite = "entities/enemy_projectiles/eldritch_puddle_burst",
  }
end

local thrust_attack = {
  windup_duration = 500,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 16,
  recovery_duration = 500,
}

local slam_attack = {
  windup_duration = 1000,
  windup_animation = "overhead_windup",
  attack_animation = "overhead_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = enemy.base_damage * 1.6,
  step_distance = 16,
  damage_type = "physical",
  has_aoe = true,
  aoe_offset = 48,
  aoe_sprite = "items/shockwave_3x3",
  aoe_damage = 30,
  aoe_sound = "running_obstacle",
  recovery_duration = 500,
  attacking_callback = function()
    sol.timer.start(enemy, 300, function()
      local ox, oy, z = enemy:get_position()
      local dir = enemy:get_sprite():get_direction()
      ox = ox + game:dx(48)[dir]
      oy = oy + game:dy(48)[dir]
      for i = 1, math.random(4, 6) do
        local x = ox + math.random(-24, 24)
        local y = oy + math.random(-24, 24)
        make_puddle(x, y, z)
      end
    end)
  end,
}

local spin_attack = {
  windup_duration = 600,
  windup_animation = "spin_attack_windup",
  attack_animation = "spin_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "spin_attack_windup",
  weapon_attack_animation = "spin_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 800,
  step_tracking = true,
  step_distance = 40,
  attacking_callback = function()
    local sprite = enemy:get_sprite()
    sprite:set_direction(3)
    --sprite:set_animation"stopped"
  end,
}

local function combo(a, w, r, step)
  local atk = table.duplicate(a)
  atk.windup_duration, atk.recovery_duration = w, r
  atk.step_distance = step or atk.step_distance
  return atk
end

local combo_a = {
  combo(thrust_attack, 900, 10),
  combo(thrust_attack, 10, 10),
  combo(thrust_attack, 10, 1100, 28),
}

local combo_b = {
  combo(thrust_attack, 900, 10),
  combo(thrust_attack, 10, 900, 28),
}


local melee_distance = 96

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif distance > melee_distance and random <= 20 then
    enemy:melee_attack(spin_attack)

  else
    enemy:townsfolk_sound_chance()
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:townsfolk_sound_chance()
        local is_aligned = enemy:is_aligned(hero, 10)

        --If aligned:
        if is_aligned and random <= 30 then
          enemy:melee_combo(combo_a)
        elseif is_aligned and random <= 60 then
          enemy:melee_combo(combo_b)
        --Not aligned:
        elseif random <= 20 then
          enemy:melee_combo(combo_a)
        elseif random <= 40 then
          enemy:melee_combo(combo_b)
        elseif random <= 70 then
          enemy:melee_attack(slam_attack)
        else
          enemy:melee_attack(spin_attack)

        end
      end
    })
  end
end
