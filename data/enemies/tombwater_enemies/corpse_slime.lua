local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 5
enemy.base_damage = 20
enemy.alert_all_directions = true

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 110,
  detection_distance = enemy:get_property("ceiling") and 40 or 96,
  abandon_hero_distance = 150,
  idle_movement_speed = 15,
  sprite_direction_style = "single",
})


local spike_attack = {
  windup_duration = 300,
  windup_animation = "spike_windup",
  attack_animation = "spike_attack",
  weapon_sprite = "enemies/tombwater_enemies/corpse_slime",
  weapon_attack_animation = "spike_weapon_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  attack_sound = "enemies/slime_stab",
  recovery_duration = 2500,
  collision_callback = function(hero)
    hero:build_up_status_effect("bleed", 15)
  end,
}


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 10)
  local melee_distance = 128
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif (distance <= melee_distance) and not enemy.melee_cooldown then
    enemy:approach_then_attack{
      speed = 25,
      dist_threshold = 32,
      attack_function = function()
        enemy.melee_cooldown = true
        sol.timer.start(map, 5000, function() enemy.melee_cooldown = false end)
        enemy:melee_attack(spike_attack)
      end,
    }

  else
    enemy:move_randomly()
  end
end


--Slime movement sound:
enemy:register_event("on_restarted", function()
  enemy.slime_move_timer = sol.timer.start(enemy, 0, function()
    local m = enemy:get_movement()
    if m and m:get_speed() > 0 then
      enemy:make_sound("enemies/slime_0" .. math.random(1,7))
    end
    return 800
  end)
end)


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(24, 56))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end


--Hide in ceiling
enemy:register_event("on_created", function()
  if enemy:get_property("ceiling") then
    enemy.is_in_ceiling = true
    enemy:set_visible(false)
  end
end)

enemy:register_event("on_restarted", function()
  --need to hide enemy again?
end)


function enemy:start_aggro()
  if enemy.is_in_ceiling then
    enemy:ceiling_drop()
  else
    enemy.aggro = true
    enemy:restart()
  end
end


function enemy:ceiling_drop()
  local sprite = enemy:get_sprite()
  local offset = -400
  local step = 10
  enemy.is_in_ceiling = false
  sprite:set_xy(0, offset)
  sol.timer.stop_all(enemy)
  sprite:set_animation("falling")
  enemy:set_visible(true)
  sol.timer.start(enemy, 10, function()
    offset = offset + step
    offset = math.min(offset, 0)
    sprite:set_xy(0, offset)
    if offset >= 0 then
      sol.audio.play_sound("ichor_squish_01")
      sprite:set_animation("splatting", function()
        sprite:set_animation"stopped"
        enemy.aggro = true
        enemy:melee_attack(spike_attack)
      end)
    else
      return true
    end
  end)
end


















