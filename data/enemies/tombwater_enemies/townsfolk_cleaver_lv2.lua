local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 1
enemy.base_damage = 20

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage,
  step_distance = 32,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_attack = {
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 500,
  step_distance = 16,
}

local reverse_slash_attack = {
  windup_duration = 300,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 200,
  step_distance = 16,
}


local ambush_attack = {
  windup_duration = 50,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 32,
  recovery_duration = 200,
}


local function combo(a, w, r)
  local atk = table.duplicate(a)
  atk.windup_duration = w
  atk.recovery_duration = r
  return atk
end


local slash_combo = {
  combo(slash_attack, 500, 20),
  combo(reverse_slash_attack, 50, 20),
  combo(slash_attack, 200, 800),
}

local short_combo = {
  combo(reverse_slash_attack, 400, 20),
  combo(slash_attack, 50, 600),
}

local wild_combo_end_attack = combo(thrust_attack, 500, 100)
wild_combo_end_attack.next_action = function()
  local sprite = enemy:get_sprite()
  sprite:set_animation"leaning_back"
  sol.timer.start(enemy, 200, function() --"windup"
    sol.audio.play_sound"spells/maw_chomp"
    local x, y, z = enemy:get_position()
    local aoe_1 = map:create_custom_entity{
      x=x, y=y+1, layer=z, direction=0, width=16, height=16,
      sprite = "entities/enemy_projectiles/ichor_tentacle_floor_burst",
      model = "enemy_projectiles/general_attack",
    }
    aoe_1.damage = enemy.base_damage
    aoe_1:remove_after_animation()
    local aoe_2 = map:create_custom_entity{
      x=x, y=y+1, layer=z, direction=0, width=16, height=16,
      sprite = "items/shockwave_3x3",
      model = "enemy_projectiles/general_attack",
    }
    aoe_2.damage = enemy.base_damage
    aoe_2:remove_after_animation()
    sol.timer.start(enemy, 500, function() sprite:set_animation"tired" end)
    sol.timer.start(enemy, 1500, function()
      enemy:decide_action()
    end)
  end)
end

local wild_combo = {
  combo(slash_attack, 500, 20),
  combo(reverse_slash_attack, 10, 10),
  combo(slash_attack, 10, 10),
  combo(reverse_slash_attack, 200, 10),
  combo(slash_attack, 10, 10),
  combo(thrust_attack, 70, 100),
  wild_combo_end_attack,
}


local silverbolt_bomb_attack = {
  windup_animation = "slash_windup",
  attack_animation = "thrust_attack",
  attack_sound = "throw",
  projectile_sprite = "hero_projectiles/grenade",
  damage = enemy.base_damage,
  projectile_properties = {
    harmless = true,
    max_distance = 96,
    obstacle_callback = function(p)
      local num_projectiles = 3
      local x, y, z = p:get_position()
      local angle = p:get_angle(hero)
      p:pop_remove()
      for i = 1, num_projectiles do
        local projectile = map:create_custom_entity{
          x=x, y=y, layer=z, direction=0, width=8, height=8,
          sprite = "hero_projectiles/bullet_silver",
          model = "enemy_projectiles/generic_projectile",
        }
        projectile.damage = enemy.base_damage * .7
        projectile.speed = 250
        projectile.rotational_sprite = true
        sol.timer.start(projectile, 100 * (i - 1), function()
          sol.audio.play_sound"spells/arcane_bolt"
          projectile:shoot(angle + math.rad(math.random(-10, 10)))
        end)
      end
    end,
  }
}


local ranged_distance = 128

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif ((distance > ranged_distance and random <= 80) or random <= 8) and not enemy.silverbolt_bomb_cooldown then
    enemy:ranged_attack(silverbolt_bomb_attack)
    enemy.silverbolt_bomb_cooldown = true
    sol.timer.start(map, 8000, function()
      enemy.silverbolt_bomb_cooldown = nil
    end)

  else
    enemy:townsfolk_sound_chance()
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:townsfolk_sound_chance()
        local rand = math.random(1, 6)
        if rand == 1 then
          enemy:melee_combo(slash_combo)
        elseif rand == 2 then
          enemy:melee_combo(short_combo)
        elseif rand == 3 then
          enemy:melee_combo(wild_combo)
        elseif enemy:is_aligned(hero, 10) then
          enemy:melee_attack(thrust_attack)
        else
          enemy:melee_attack(reverse_slash_attack)
        end
      end
    })
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

