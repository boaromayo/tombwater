local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 15
enemy.exp = 25
enemy.weight = 10
enemy.money = 4

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 100,
  detection_distance = 200,
  abandon_hero_distance = 400,
})

local slam_attack = {
  step_distance =15,
  windup_duration = 600,
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 500,
}

local thrust_attack = {
  step_distance =15,
  windup_duration = 600,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 400,
}

local combo_1 = {
  step_distance = 100,
  step_speed = 250,
  windup_duration = 1200,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 150,
}

local combo_2 = {
  step_distance = 10,
  windup_duration = 200,
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 0,
  turn_toward_hero = false
}

local combo_3 = {
  step_distance = 10,
  windup_duration = 200,
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 20,
  turn_toward_hero = false
}

local combo_4 = {
  step_distance = 10,
  windup_duration = 200,
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/pitchfork",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 1300,
}

local combo_a = {combo_1, combo_2, combo_3, combo_4}
local combo_distance = {60,120}
local previous_attack = 0

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 6)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance > combo_distance[1] and distance < combo_distance[2] and rand <=1 then
    enemy:melee_combo(combo_a)
    previous_attack = 1
  else
    local rand = math.random(1,14)
    if rand <= 7 then
      sol.audio.play_sound("enemies/townsfolk_vox/wheeze_0" .. rand)
    end
    enemy:approach_then_attack({
      approach_duration = 700,
      speed = 80,
      attack_function = function()
        if rand <=3 then
          enemy:melee_attack(slam_attack)
          previous_attack = 2
        elseif rand <=5 then
          previous_attack = 3
          enemy:melee_attack(thrust_attack)
        else
          enemy:decide_action()
        end
      end
    })
  end
end


function enemy:make_distance()
  local angle = enemy:get_angle(hero)
  sol.timer.start(enemy, 150, function()
    enemy.lock_facing = true
    local m = sol.movement.create"straight"
    m:set_angle(angle + math.pi)
    m:set_speed(200)
    m:set_max_distance(48)

    local function recover()
      enemy.lock_facing = false
      sol.timer.start(enemy, 600, function()
        enemy:decide_action()
      end)
    end

    m:start(enemy, function()
      recover()
    end)
    function m:on_obstacle_reached()
      recover()
    end
  end)
end