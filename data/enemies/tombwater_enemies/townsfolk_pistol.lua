local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.money = 3
enemy.weight = 10

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})


local ranged_attack = {
  windup_animation = "gun_aiming",
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = 20,
  recovery_duration = 0,
}

local ranged_attack_2 = {
  windup_animation = "gun_aiming",
  windup_duration = 200,
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = 20,
  recovery_duration = 0,
}

local ranged_combo = {ranged_attack, ranged_attack_2, ranged_attack_2}



local slash_attack = {
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_sword",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 800,
}


local attack_range = 60
local melee_range = 48
local attack_cooldown_time = 3000

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance >= attack_range and enemy:has_los(hero) and not enemy.attack_cooldown then
    enemy:ranged_combo(ranged_combo)
    enemy.attack_cooldown = true
    sol.timer.start(map, attack_cooldown_time, function() enemy.attack_cooldown = false end)
  elseif distance >= attack_range and not enemy:has_los(hero) then
    enemy:approach_hero({
      speed = 60,
      dist_threshold = 40,
      approach_duration = 1000,
    })
  elseif distance <= melee_range then
    enemy:melee_attack(slash_attack)

  elseif distance > melee_range and distance < attack_range then
    enemy:retreat()
  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end

