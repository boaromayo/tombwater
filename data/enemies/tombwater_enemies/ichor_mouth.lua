local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 25
enemy.money = 9
enemy.base_damage = 25


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 400,
  detection_distance = 200,
  abandon_hero_distance = 300,
  stagger_when_hurt = false,
  stagger_life_percent = 35,
  sprite_direction_style = "horizontal",
})



local lash_attack = {
  windup_animation = "screaming",
  windup_sound = "enemies/deputy_beast_scream_short",
  attack_animation = "screaming",
  attack_duration = 500, --to use looping attack animation
  attack_sound = "spells/tentacle_lash",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichor_mouth_tentacles",
  weapon_attack_animation = "attack",
  damage = enemy.base_damage,
  step_distance = 16,
  step_tracking = true,
  damage_type = "physical",
  recovery_animation = "tentacle_recovery",
  recovery_duration = 500,
}
enemy.lash_attack = lash_attack --so this can be called by map scripts for ambush scenes


local slam_attack = {
  windup_animation = "slam_windup",
  windup_duration = 700,
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichor_mouth_tentacles",
  weapon_attack_animation = "slam_swipe_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  step_tracking = true,
  damage_type = "physical",
  recovery_duration = 500,
  ragdoll_distance = 48,
  --aoe:
  has_aoe = true,
  aoe_offset = 32,
  aoe_delay = 20,
  aoe_sound = "running_obstacle",
  aoe_damage = enemy.base_damage * 1.5,
  aoe_sprite = "entities/enemy_projectiles/debris_attack", 
}


local jump_attack = {
  windup_animation = "jump_windup",
  windup_duration = 400,
  speed = 600,
  damage = enemy.base_damage,
  attack_animation = "jump_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichor_mouth_tentacles",
  weapon_attack_animation = "jumping_teeth",
  max_distance = 240,
  recovery_duration = 0,
  recovery_animation = "jump_windup",
  finished_callback = function()
    sol.audio.play_sound"skid"
    --local dust = enemy:create_sprite("entities/enemy_projectiles/debris_attack")
    --dust:set_animation("attack", function() enemy:remove_sprite(dust) end)
    sol.timer.start(enemy, 500, function()
      enemy:decide_action()
    end)
  end,
  hit_hero_callback = function(hero)
    hero:knock_up()
  end,
}


local ichor_claws_attack = {
  windup_animation = "ichor_claws_windup",
  windup_sound = "ichor_squish_02",
  attack_animation = "ichor_claws_attack",
  attack_sound = "spells/tentacle_lash",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichor_mouth_tentacles",
  weapon_attack_animation = "ichor_claws_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  step_tracking = true,
  damage_type = "physical",
  recovery_duration = 500,
}

function enemy:ichor_claws()
  local num_shots = math.random(4, 6)
  local shot_range = 96
  local blob_damage = enemy.base_damage * .333
  local shot_frequency = 120

  local sprite = enemy:get_sprite()
  local aim_angle = enemy:get_angle(hero)

  local function shoot_blob()
    local x, y, z = enemy:get_position()
    enemy:make_sound("ichor_squish_01")
    local blob = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=8, height=8,
      sprite = "entities/enemy_projectiles/ichor_blob",
      model = "enemy_projectiles/generic_projectile",
    }
    blob.damage = blob_damage
    blob.speed = 160
    blob.max_distance = shot_range + math.random(-16, 16)
    blob:shoot(aim_angle + math.rad(math.random(-45, 45)))
  end


  enemy:melee_attack(ichor_claws_attack)
  local blob_count = 0
  sol.timer.start(enemy, 400, function()
    shoot_blob()
    blob_count = blob_count + 1
    if blob_count < num_shots then
      return shot_frequency + math.random(-50, 50)
    end
  end)
end


function enemy:ambush_attack()
  enemy:ram_attack(jump_attack)
end



function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 10)

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --Long range:
  elseif distance >= 72 and rand < 8 and not enemy.jump_attack_cooldown then
    sol.audio.play_sound("enemies/townsfolk_vox/croak_01")
    sol.audio.play_sound("enemies/townsfolk_vox/screech_low_02")
    enemy:ram_attack(jump_attack)
    enemy.jump_attack_cooldown = true
    sol.timer.start(map, 5000, function() enemy.jump_attack_cooldown = nil end)

  elseif distance > 96 then
    enemy:make_sound("enemies/townsfolk_vox/wheeze_growl_0" .. math.random(1,2))
    enemy:approach_hero{}

  --Close range:
  elseif distance <= 72 and rand < 4  then
    enemy:approach_then_attack{
      dist_threshold = 64,
      speed = 75,
      attack_function = function()
        enemy:melee_attack(lash_attack)
      end,
    }

  elseif distance <= 72 and rand < 8 then
    sol.audio.play_sound("enemies/townsfolk_vox/croak_01")
    enemy:make_sound("enemies/townsfolk_vox/wheeze_growl_0" .. math.random(1,2))
    enemy:approach_then_attack{
      dist_threshold = 48,
      speed = 75,
      attack_function = function()
        enemy:melee_attack(slam_attack)
      end,
    }

  elseif distance <= 72 and rand < 10 then
    enemy:make_sound("enemies/townsfolk_vox/wheeze_growl_0" .. math.random(1,2))
    enemy:approach_then_attack{
      dist_threshold = 64,
      speed = 75,
      attack_function = function()
        enemy:ichor_claws()
      end,
    }

  elseif distance <= 72 then
    enemy:retreat{
      speed = 40,
      duration = 300,
      lock_facing = true,
    }

  else
    sol.timer.start(enemy, 400, function() enemy:decide_action() end)
  end
end

