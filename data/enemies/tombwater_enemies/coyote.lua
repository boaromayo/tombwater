local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 1

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 50,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local jump_distance = 48

local jump_attack = {
  windup_duration = 550,
  windup_animation = "crouching",
  attack_animation = "attacking",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/coyote_teeth",
  weapon_windup_animation = "windup",
  weapon_attack_animation = "attacking",
  damage = 15,
  damage_type = "physical",
  step_distance = jump_distance,
  step_speed = 200,
  step_tracking = true,
  recovery_duration = 200,
  recovery_animation = "crouching",
}

local jump_followup = {
  windup_duration = 250,
  windup_animation = "crouching",
  attack_animation = "attacking",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/coyote_teeth",
  weapon_windup_animation = "windup",
  weapon_attack_animation = "attacking",
  damage = 15,
  damage_type = "physical",
  step_distance = 24,
  step_speed = 200,
  step_tracking = true,
}

local jump_combo = {jump_attack, jump_followup}


function enemy:make_distance()
  local sprite = enemy:get_sprite()
  local angle = enemy:get_angle(hero)
  sprite:set_animation("crouching")
  sprite:set_direction(enemy:get_direction4_to(hero))
  sol.timer.start(enemy, 150, function()
    enemy.lock_facing = true
    local m = sol.movement.create"straight"
    m:set_angle(angle + math.pi)
    m:set_speed(200)
    m:set_max_distance(48)

    local function recover()
      enemy.lock_facing = false
      sol.timer.start(enemy, 400, function()
        enemy:decide_action()
      end)
    end

    m:start(enemy, function()
      recover()
    end)
    function m:on_obstacle_reached()
      recover()
    end
    sol.audio.play_sound("skid")
  end)
end


function enemy:bark()
  local sprite = enemy:get_sprite()
  sprite:set_animation("barking", function()
    sprite:set_animation("stopped")
    sol.timer.start(enemy, 75, function()
      sprite:set_animation("barking", function()
        sprite:set_animation("stopped")
        sol.timer.start(enemy, 500, function()
          enemy:decide_action()
        end)
      end)
    end)
  end)
  enemy:make_sound("enemies/coyote_bark")
end


function enemy:growl()
  local sprite = enemy:get_sprite()
  sprite:set_animation("crouching")
  enemy:make_sound("enemies/coyote_growl")
  sol.timer.start(enemy, 300, function()
    if enemy.just_growled_back then
      enemy:decide_action()
    else
     enemy:make_distance()
    end
  end)
end


function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif enemy.just_jumped then
    enemy.just_jumped = false
    enemy:make_distance()
  elseif random <= 25 then
    enemy:bark()
  elseif random <= 45 then
    enemy.just_growled_back = true
    enemy:growl()
  else
    enemy:approach_then_attack({
      dist_threshold = jump_distance,
      approach_duration = 1500,
      speed = 80,
      attack_function = function()
        enemy.just_growled_back = false
        enemy.just_jumped = true
        enemy:melee_combo(jump_combo)
      end
    })
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

