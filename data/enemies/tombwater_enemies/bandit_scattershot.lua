local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.money = 3
enemy.weight = 10
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
  ally_alert_range = 80,
})


local pistol_attack = {
  windup_animation = "gun_aiming",
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = enemy.base_damage,
  recovery_duration = 1000,
  recovery_animation = "gun_aiming",
}

local r1 = table.duplicate(pistol_attack)
r1.windup_duration, r1.recovery_duration = 1000, 10
local r2 = table.duplicate(pistol_attack)
r2.windup_duration, r2.recovery_duration = 250, 10
local r3 = table.duplicate(pistol_attack)
r3.windup_duration, r3.recovery_duration = 250, 700

local pistol_combo = {r1, r2, r3}


local dynamite_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  attack_sound = "throw",
  generic_projectile_type = "bomb",
  projectile_properties = {
    max_distance = 24,
    fuse_length = 500,
  },
  projectile_sprite = "hero_projectiles/dynamite",
  damage = enemy.base_damage * 2.5,
}



local dynamite_distance = 48

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif distance <= dynamite_distance and random <= 85 then
    sol.audio.play_sound"enemies/townsfolk_vox/grunt_high_04"
    enemy:ranged_attack(dynamite_attack)

  else enemy:retreat{
    angle = math.rad(math.random(0, 360)),
    speed = 75,
    duration = math.random(500, 3000),
    callback = function()
      for _, attack in pairs(pistol_combo) do
        attack.angle_override = enemy:get_angle(hero) + math.rad(math.random(-25, 25))
      end
      enemy:ranged_combo(pistol_combo)
    end,
  }

  end
end

