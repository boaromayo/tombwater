local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local flies = {}
local min_flies, max_flies = 5, 10
local hit_damage = 5
local hit_frequency = 200
local attack_range = 180

function enemy:on_created()
  --This enemy really just exists to orient a bunch of projectiles together
  enemy:set_life(999999)
  enemy:set_damage(0)
  enemy.min_flies, enemy.max_flies = min_flies, max_flies
  enemy:set_can_attack(false)
  enemy:set_invincible(true)
  enemy.min_radius, enemy.max_radius = enemy.min_radius or 8, enemy.max_radius or 32
  enemy.ignore_enemy_traverse_collider = true --allow to overlap other enemies
end


function enemy:on_restarted()
  sol.timer.start(enemy, 10, function()
    enemy:create_flies()
    enemy:check_for_death()
    if enemy.host_entity then
      enemy:follow_host(enemy.host_entity)
    else
      enemy:hover()
    end
  end)
end


function enemy:get_flies()
  return flies
end


function enemy:create_flies()
  if enemy.flies_created then return end
  enemy.flies_created = true
  local x, y, z = enemy:get_position()
  local num_flies = math.random(enemy.min_flies, enemy.max_flies)
  for i = 1, num_flies do
    local fly = map:create_custom_entity{
      x=x, y=y, layer=z, width=16, height=16, direction=0,
      sprite = "enemies/tombwater_enemies/swarm_fly",
    }
    fly.number = i
    fly:get_sprite():set_direction(math.random(0, 1))
    fly.collided_entities = {}
    fly:add_collision_test("sprite", function(fly, other_entity)
      if other_entity:get_type() == "hero" and not fly.collided_entities[other_entity] and hero:get_can_be_hurt() then
        fly.collided_entities[other_entity] = other_entity
        sol.timer.start(fly, hit_frequency, function() fly.collided_entities[other_entity] = nil end)
        sol.audio.play_sound("hero_hurt_small")
        other_entity:process_hit({
          damage = hit_damage,
          non_staggering = true,
        })
      end
    end)

    function fly:react_to_solforge_weapon()
      flies[i] = nil
      fly:clear_collision_tests()
      fly:remove_sprite()
      local death_sprite = fly:create_sprite("enemies/enemy_killed_projectile")
      death_sprite:set_animation("killed", function() fly:remove() end)
    end

    flies[i] = fly

    local m = sol.movement.create"circle"
    m:set_center(enemy)
    m:set_radius(0)
    m:set_radius_speed(20)
    m:set_angular_speed(math.rad(math.random(90,180)))
    m:set_clockwise(math.random(1,2) == 1)
    m:set_ignore_obstacles(true)
    m:start(fly)
    m:set_radius(math.random(enemy.min_radius, enemy.max_radius))

  end
end


function enemy:hover()
  sol.timer.start(enemy, 200, function()
    if enemy:get_distance(hero) <= attack_range then
      enemy:dive_hero()
    else
      return true
    end
  end)
  enemy:check_for_death()
end


function enemy:dive_hero()
  enemy:check_for_death()
  sol.audio.play_sound("spells/flies")
  local m = sol.movement.create"straight"
  m:set_angle(enemy:get_angle(hero))
  m:set_speed(90)
  m:set_ignore_obstacles(true)
  m:set_max_distance(attack_range + 48)
  m:start(enemy, function()
    enemy:hover()
  end)
end


function enemy:follow_host(entity)
  local m = sol.movement.create"target"
  m:set_target(entity)
  m:set_speed(180)
  m:set_ignore_obstacles(true)
  m:start(enemy)
  sol.timer.start(enemy, 1000, function()
    enemy:check_for_death()
    return true
  end)
end


function enemy:remove_flies()
  for _, fly in pairs(flies) do
    if fly:exists() then
      local sprite = fly:get_sprite()
      sprite:fade_out()
      sol.timer.start(fly, 100, function()
        fly:remove()
      end)
    end
  end
end


function enemy:check_for_death(amount)
  amount = amount or 3
  if #flies <= amount then
    sol.timer.stop_all(enemy)
    enemy:remove_flies()
    enemy:remove()
  end
  if enemy.host_entity and enemy.host_entity.get_life and enemy.host_entity:get_life() <= 0 then
    sol.timer.stop_all(enemy)
    enemy:remove_flies()
    enemy:remove()
  end
end


function enemy:set_lifespan(span)
  sol.timer.start(enemy, span, function()
    enemy:remove_flies()
    enemy:remove()
  end)
end

