local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.money = 3
enemy.weight = 10

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})


local ranged_attack = {
  windup_animation = "gun_aiming",
  windup_duration = 1100,
  attack_sound = "weapons/gun_shotgun_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  projectile_offset_radius = 8,
  damage = 50,
  num_projectiles = 6,
  projectile_properties = {
    max_distance = 64,
    distance_damage_reduction_rate = 2.5,
  },
  recovery_duration = 0,
}



local attack_range = 60
local too_close_range = 24
local attack_cooldown_time = 3000

local function attack_function()
  enemy:ranged_attack(ranged_attack)
  enemy.attack_cooldown = true
  enemy.retreat_cooldown = false --cannot retreat until after attacking
  sol.timer.start(map, attack_cooldown_time, function() enemy.attack_cooldown = false end)
end

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local has_los = enemy:has_los(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  --Retreat:
  elseif distance <= too_close_range and not enemy.retreat_cooldown then
    sol.timer.start(enemy, 300, function()
      enemy:retreat{
        duration = 500,
        speed = 90,
      }
    end)
    enemy.retreat_cooldown = true
  --Attack:
  elseif has_los and not enemy.attack_cooldown then
    enemy:approach_then_attack{
      speed = 60,
      dist_threshold = attack_range,
      approach_duration = 500,
      attack_function = attack_function
    }
  --Get a better angle:
  elseif not has_los or enemy.attack_cooldown then
    sol.audio.play_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,5))
    enemy:approach_hero({
      speed = 60,
      dist_threshold = attack_range,
      approach_duration = 500,
    })
  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end

