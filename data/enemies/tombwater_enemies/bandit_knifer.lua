local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 30
enemy.weight = 10
enemy.money = 3
enemy.base_damage = 15

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
  ally_alert_range = 80,
})

local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage,
  step_distance = 32,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_attack = {
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 500,
  step_distance = 16,
  step_tracking = true,
}

local reverse_slash_attack = {
  windup_duration = 300,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 200,
  step_distance = 16,
  step_tracking = true,
}

local function combo_move(atk, wd, rd)
  local mv = table.duplicate(atk)
  mv.windup_duration, mv.recovery_duration = wd, rd
  return mv
end

local combo_a = {
  combo_move(slash_attack, 1200, 10),
  combo_move(reverse_slash_attack, 10, 10),
  combo_move(thrust_attack, 10, 10),
  combo_move(thrust_attack, 10, 1000),
}

local combo_b = {
  combo_move(thrust_attack, 1000, 10),
  combo_move(thrust_attack, 10, 10),
  combo_move(thrust_attack, 10, 10),
  combo_move(reverse_slash_attack, 100, 10),
  combo_move(slash_attack, 10, 10),
  combo_move(thrust_attack, 200, 1000),
}

local c_end = table.duplicate(slash_attack)
c_end.windup_duration, c_end.recovery_duration = 10, 2500
c_end.recovery_animation = "tired"
local combo_c = {
  combo_move(slash_attack, 1400, 10),
  combo_move(reverse_slash_attack, 10, 10),
  combo_move(slash_attack, 10, 10),
  combo_move(reverse_slash_attack, 10, 10),
  combo_move(slash_attack, 10, 10),
  combo_move(reverse_slash_attack, 300, 10),
  combo_move(slash_attack, 10, 10),
  combo_move(reverse_slash_attack, 10, 10),
  c_end,
}



function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  local possible_combos = {
    combo_a, combo_b, combo_c,
  }
  local random_combo = possible_combos[math.random(1, #possible_combos)]

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif random <= 25 then
    sol.audio.play_sound("enemies/townsfolk_vox/grunt_high_0" .. math.random(1,5))
    enemy:melee_combo(random_combo)
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        sol.audio.play_sound("enemies/townsfolk_vox/grunt_high_0" .. math.random(1,5))
        enemy:melee_combo(random_combo)
      end,
    })
  end
end


function enemy:ambush_attack()
  enemy:melee_combo(combo_b)
end

