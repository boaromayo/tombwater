local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local needle_damage = 25
local sword_damage = 25
local melee_range = 64
local smoke_bomb_range = 64
local smoke_bomb_cooldown_time = 4000
local max_warpdives =3
local warpdive_cooldown_time = 8000

enemy.exp = 20
enemy.weight = 10
enemy.money = 10

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 250,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 35,
})


local needle_attack = {
  windup_animation = "needle_windup",
  windup_duration = 1100,
  attack_animation = "needle_attack",
  attack_sound = "throw",
  generic_projectile_type = "bullet",
  projectile_sprite = "hero_projectiles/cactus_needle",
  projectile_offset_radius = 8,
  projectile_spread = 45,
  damage = needle_damage,
  num_projectiles = 3,
}


local fore_attack = {
  windup_animation = "sword_forehand_windup",
  attack_animation = "sword_forehand_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/assassin_sword",
  weapon_windup_animation = "forehand_windup",
  weapon_attack_animation = "forehand_attack",
  step_distance = 24,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}


local back_attack = {
  windup_animation = "sword_backhand_windup",
  attack_animation = "sword_backhand_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/assassin_sword",
  weapon_windup_animation = "backhand_windup",
  weapon_attack_animation = "backhand_attack",
  step_distance = 24,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}

local combo_a = table.duplicate(fore_attack)
combo_a.recovery_duration = 10
local combo_b = table.duplicate(back_attack)
combo_b.windup_duration, combo_b.recovery_duration = 10, 10
local combo_c = table.duplicate(fore_attack)
combo_c.windup_duration, combo_c.recovery_duration = 10, 10
local focus_combo = {combo_a, combo_b, combo_c}


function enemy:focus_combo()
  local focus_windup_duration = 400
  local sprite = enemy:get_sprite()
  sprite:set_animation"sword_focus"
  sol.timer.start(enemy, focus_windup_duration, function()
    enemy:melee_combo(focus_combo)
  end)
end


function enemy:smoke_bomb()
  enemy.smoke_bomb_cooldown = true
  sol.timer.start(map, smoke_bomb_cooldown_time, function() enemy.smoke_bomb_cooldown = nil end)
  local sprite = enemy:get_sprite()
  sprite:set_animation("throwing_down", function()
    map:create_poof(enemy:get_position())
    enemy:warp_away({
      warping_out_animation = "warping",
      warping_in_animation = "warping_in",
      callback = function()
        map:create_poof(enemy:get_position())
        enemy:decide_action()
      end,
    })
  end)
end


local ram_attack = {
  windup_animation = "crouching",
  windup_duration = 250,
  speed = 400,
  damage = sword_damage,
  attack_animation = "dive_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/assassin_sword",
  weapon_attack_animation = "dive_attack",
  max_distance = 240,
  recovery_duration = 10,
  finished_callback = function()
    enemy:get_sprite():set_animation("stopped")
    if enemy.warp_dive_count < max_warpdives then
      enemy:warpdive()
    else
      enemy:decide_action()
    end
  end
}

function enemy:warpdive()
  if not enemy.warp_dive_count then enemy.warp_dive_count = 0 end
  map:create_poof(enemy:get_position())
  enemy:warp_away({
    warping_out_animation = "warping",
    warping_in_animation = "warping_in",
    callback = function()
      map:create_poof(enemy:get_position())
      enemy.warp_dive_count = enemy.warp_dive_count + 1
      enemy:ram_attack(ram_attack)
    end,
  })
end



function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  --Smoke bomb away:
  elseif (distance <= smoke_bomb_range) and not enemy.smoke_bomb_cooldown then
    enemy:smoke_bomb()
  --Needle attack:
  elseif distance >= melee_range and random < 30 then
    enemy:ranged_attack(needle_attack)
  --Warpdive:
  elseif random < 65 and not enemy.warpdive_cooldown then
    enemy:warpdive()
    enemy.warpdive_cooldown = true
    sol.timer.start(map, warpdive_cooldown_time, function() enemy.warpdive_cooldown = nil end)
  --Melee:
  elseif distance < 32 then
    enemy:melee_attack(back_attack)
  --Melee:
  else
    enemy:approach_then_attack{
      speed = 85,
      attack_function = function() enemy:focus_combo() end,
    }
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

