local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 20
enemy.weight = 10
enemy.money = 2

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 50,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local ranged_attack = {
  windup_animation = "aiming",
  windup_sound = "enemies/bone_pull",
  attack_animation = "throwing",
  attack_sound = "weapons/blunt_07",
  generic_projectile_type = "bone",
  projectile_sprite = "entities/enemy_projectiles/bone",
  damage = 25,
  recovery_animation = "tired",
}

local attack_range = 30
local attack_cooldown_time = 2500

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance >= attack_range and enemy:has_los(hero) and not enemy.attack_cooldown then
    enemy:ranged_attack(ranged_attack)
    enemy.attack_cooldown = true
    sol.timer.start(map, attack_cooldown_time + math.random(-500, 500), function() enemy.attack_cooldown = false end)
  elseif distance < attack_range then
    enemy:retreat({
      speed = 50
    })
  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end


--Overwrite default idle
function enemy:start_idle()
  local sprite = enemy:get_sprite()
  if enemy:get_property("underground") then
    sprite:set_animation"underground"
  elseif enemy:get_property"idle_movement_type" == "random" then
    enemy:move_randomly()

  end
  sol.timer.start(enemy, 50, function()
    if enemy:get_property("hibernate") then return end
    enemy:check_for_hero()
    return true
  end)
end


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(50, 100))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.random(0, 3) * math.pi / 2)
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end


function enemy:start_aggro()
  enemy.aggro = true
  enemy:alert_nearby_enemies()
  sol.timer.stop_all(enemy)
  if enemy:get_property("underground") then
    local sprite = enemy:get_sprite()
    sol.audio.play_sound("spells/ground_burst_soft_4")
    sprite:set_animation("digging_out", function()
      sprite:set_animation("stopped")
      enemy:restart()
    end)
  else
    enemy:restart()
  end
end


