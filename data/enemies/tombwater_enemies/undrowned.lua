local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 5
enemy.base_damage = 20

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 100,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
  idle_movement_speed = 25,
})

local slash_a = {
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/undrowned_slash",
  weapon_attack_animation = "slash_small_a",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 16,
}

local slash_b = {
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/undrowned_slash",
  weapon_attack_animation = "slash_small_b",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 16,
}

local combo_a_1 = table.duplicate(slash_a)
combo_a_1.windup_duration, combo_a_1.recovery_duration = 500, 10
local combo_a_2 = table.duplicate(slash_b)
combo_a_2.windup_duration, combo_a_2.recovery_duration = 60, 10
local combo_a_3 = table.duplicate(slash_a)
combo_a_3.windup_duration, combo_a_3.recovery_duration = 450, 500
combo_a_3.step_distance, combo_a_3.step_speed = 40, 130
local combo_a = {combo_a_1, combo_a_2, combo_a_3}

local combo_b_1 = table.duplicate(slash_b)
combo_b_1.windup_duration, combo_b_1.recovery_duration = 500, 10
local combo_b_2 = table.duplicate(slash_a)
combo_b_2.windup_duration, combo_b_2.recovery_duration = 60, 10
local combo_b_3 = table.duplicate(slash_b)
combo_b_3.windup_duration, combo_b_3.recovery_duration = 200, 10
local combo_b_4 = table.duplicate(slash_a)
combo_b_4.windup_duration, combo_b_4.recovery_duration = 10, 10
local combo_b_5 = table.duplicate(slash_b)
combo_b_5.windup_duration, combo_b_5.recovery_duration = 60, 700
local combo_b = {combo_b_1, combo_b_2, combo_b_3, combo_b_4, combo_b_5}


local function make_splash_entity(x, y, z)
  local attack_sprite = "entities/enemy_projectiles/splash_attack"
  local attack_sound = "splash"
  local damage = enemy.base_damage / 2

  local ground = map:get_ground(x, y, z)
  if ground ~= "shallow_water" and ground ~= "deep_water" then return end
  local attack = map:create_custom_entity{
    x=x, y=y, layer=z, direction = 0, width = 16, height = 16,
    sprite = attack_sprite,
    model = "enemy_projectiles/general_attack",
  }
  attack.damage = damage
  attack:remove_after_animation()
  attack.must_overlap = true
  sol.audio.play_sound(attack_sound)
end



function enemy:line_attack()
  local windup_animation = "slam_windup"
  local windup_duration = 1000
  local attack_animation = "slam_attack"
  local recovery_duration = 500
  local recovery_animation = "stopped"
  local range = 200
  local frequency = 100
  local speed = 300
  local start_sound = "splash"
  local screenshake_amount = 0

  local sprite = enemy:get_sprite()
  sprite:set_animation(windup_animation)
  sol.timer.start(enemy, windup_duration, function()
    local x, y, z = enemy:get_position()
    local angle = enemy:get_sprite():get_direction() * math.pi / 2
    local angle = enemy:get_angle(hero)

    sprite:set_animation(attack_animation, function()
      sprite:set_animation(recovery_animation)
      sol.timer.start(enemy, recovery_duration, function()
        enemy:decide_action()
      end)
    end)
    sol.audio.play_sound(start_sound)
    map:screenshake({shake_count = screenshake_amount})
    local target = map:create_custom_entity{x=x, y=y, layer=z, direction=0, width = 16, height=16, }
    local m = sol.movement.create"straight"
    m:set_ignore_obstacles(true)
    m:set_speed(speed)
    m:set_angle(angle)
    m:start(target, function() target:remove() end)
    local elapsed_time = 0
    sol.timer.start(target, frequency, function()
      elapsed_time = elapsed_time + frequency
      local x, y, z = target:get_position()
      make_splash_entity(x, y, z)
      if (elapsed_time/1000 * speed <= range) then return frequency end
    end)

  end)

end


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif (distance >= 128 and random <= 30) or (distance >= 64 and random <= 15) then
    enemy:line_attack()
  else
    enemy:make_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,6))
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 45,
      attack_function = function()
        local combo_choice = math.random(1,10) <= 5 and combo_a or combo_b
        if math.random(1,2) == 2 then sol.audio.play_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,6)) end
        enemy:melee_combo(combo_choice)
      end,
    })
  end
end


--Splash movement sound:
enemy:register_event("on_restarted", function()
  enemy.slime_move_timer = sol.timer.start(enemy, 0, function()
    local m = enemy:get_movement()
    if m and m:get_speed() > 0 and enemy:get_ground_below() == "shallow_water" then
      enemy:make_sound("water_movement_0" .. math.random(1,5))
    end
    return 400
  end)
end)

