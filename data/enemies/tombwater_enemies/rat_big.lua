local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 15
enemy.exp = 5
enemy.weight = 2
enemy.money = 0

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 96,
  abandon_hero_distance = 150,
  idle_movement_speed = 30,
})

local ram_attack = {
  damage = 15,
  windup_duration = 800,
  windup_animation = "windup",
  attack_animation = "attacking",
  weapon_sprite = "enemies/tombwater_enemies/rat",
  weapon_attack_animation = "attacking",
  max_distance = 56,
  speed = 110,
  smoothe = true,
  recovery_duration = 600,
}

local slash_attack_1 = {
  windup_duration = 300,
  windup_animation = "windup",
  attack_animation = "attacking",
  weapon_sprite = "enemies/trillium_enemies/weapons/goblin_claws",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 10,
  step_distance = 16,
}

local slash_attack_2 = {
  windup_duration = 10,
  windup_animation = "windup",
  attack_animation = "attacking",
  weapon_sprite = "enemies/trillium_enemies/weapons/goblin_claws",
  weapon_attack_animation = "slash_attack_2",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 700,
  step_distance = 16,
}

local slash_combo = {slash_attack_1, slash_attack_2}


enemy:register_event("on_restarted", function()
  --enemy:set_traversable(true)
end)


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 10)
  if (distance >= enemy.abandon_hero_distance) then
    enemy:return_to_idle()
  elseif not enemy.ram_cooldown and (math.random(1, 3) == 1)then
    enemy:make_sound("enemies/rat_big_0" .. math.random(1,4))
    enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
    enemy:melee_attack(slash_attack_1)
    enemy.ram_cooldown = true
    sol.timer.start(map, 2000, function()
      enemy.ram_cooldown = false
    end)
  elseif random <= 2 then
    enemy:stop_movement()
    enemy:get_sprite():set_animation"stopped"
    sol.timer.start(enemy, 1000, function() enemy:decide_action() end)

  else
    enemy:move_randomly()
  end
end


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(24, 56))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  else
    enemy:make_sound("enemies/rat_big_0" .. math.random(1,4))
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 85,
      dist_threshold = 16,
      attack_function = function()
        local m = sol.movement.create"straight"
        m:set_max_distance(16)
        m:set_angle(enemy:get_angle(hero))
        m:start(enemy)
        enemy:melee_combo(slash_combo)
      end,
    })
  end
end


