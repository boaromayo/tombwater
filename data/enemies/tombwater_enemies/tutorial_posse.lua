local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

--Stats
enemy.exp = 0
enemy.weight = 10
enemy.money = 0
local knife_damage = 10
local bullet_damage = 10

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 60,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})


local reverse_slash_attack = {
  windup_duration = 500,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = knife_damage,
  damage_type = "physical",
  step_distance = 16,
  recovery_duration = 10,
}

local slash_attack = {
  windup_duration = 10,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = knife_damage,
  damage_type = "physical",
  step_distance = 16,
  recovery_duration = 400,
}

local slash_combo = {reverse_slash_attack, slash_attack}

local ranged_attack = {
  windup_animation = "aiming",
  attack_sound = "weapons/gun_pistol_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = bullet_damage,
  recovery_duration = 300,
}


local melee_range = 72

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif (distance > melee_range) or (random < 20) then
    enemy:ranged_attack(ranged_attack)
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:melee_combo(slash_combo)
      end
    })
  end
end

