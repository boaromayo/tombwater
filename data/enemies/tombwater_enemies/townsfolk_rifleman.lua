local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 20
enemy.weight = 10
enemy.money = 2

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 50,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local ranged_attack = {
  windup_animation = "aiming",
  attack_sound = "weapons/gun_rifle_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  damage = 25,
}

local attack_range = 30
local attack_cooldown_time = 2500

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance >= attack_range and enemy:has_los(hero) and not enemy.attack_cooldown then
    enemy:ranged_attack(ranged_attack)
    enemy.attack_cooldown = true
    sol.timer.start(map, attack_cooldown_time + math.random(-500, 500), function() enemy.attack_cooldown = false end)
  elseif distance < attack_range then
    enemy:retreat()
  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end
