local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy.exp = 80
enemy.weight = 25
enemy.money = 5


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 300,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local punch_step_distance = 24
local punch_step_speed = 120

local punch_left_a = {
  windup_duration = 500,
  windup_animation = "punching_left_windup",
  attack_animation = "punching_left",
  attack_sounds = {"enemies/townsfolk_vox/big_grunt_05", "weapons/blunt_02",},
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "punching_left",
  damage = 25,
  step_distance = punch_step_distance,
  step_speed = step_speed,
  damage_type = "physical",
  recovery_duration = 10,
}

local punch_left_b = {}
for k, v in pairs(punch_left_a) do punch_left_b[k] = v end
punch_left_b.windup_duration = 100

local punch_left_c = {}
for k, v in pairs(punch_left_a) do punch_left_c[k] = v end
punch_left_c.windup_duration = 100
punch_left_c.recovery_duration = 800


local punch_right_a = {
  windup_duration = 500,
  windup_animation = "punching_right_windup",
  attack_animation = "punching_right",
  attack_sounds = {"enemies/townsfolk_vox/big_grunt_04", "weapons/blunt_04",},
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "punching_right",
  damage = 25,
  step_distance = punch_step_distance,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 10,
}

local punch_right_b = {}
for k, v in pairs(punch_right_a) do punch_right_b[k] = v end
punch_right_b.windup_duration = 100

local punch_right_c = {}
for k, v in pairs(punch_right_a) do punch_right_c[k] = v end
punch_right_c.windup_duration = 100
punch_right_c.recovery_duration = 800


local swipe_a = { --start of a combo
  windup_duration = 500,
  windup_animation = "slash_windup",
  attack_animation = "slash",
  attack_sounds = {"enemies/townsfolk_vox/big_grunt_02", "weapons/blunt_07",},
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "slash",
  damage = 25,
  step_distance = punch_step_distance + 8,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 10,
}

local swipe_b = { --end of a combo
  windup_duration = 120,
  windup_animation = "slash_windup",
  attack_animation = "slash",
  attack_sounds = {"enemies/townsfolk_vox/big_grunt_04", "weapons/blunt_04",},
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "slash",
  damage = 25,
  step_distance = punch_step_distance + 8,
  step_speed = punch_step_speed,
  damage_type = "physical",
  recovery_duration = 1000,
}


local slam_attack = {
  windup_animation = "slam_attack_windup",
  attack_animation = "slam_attack",
  attack_sound = "enemies/townsfolk_vox/big_moan_02",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "slam",
  damage = 50,
  damage_type = "physical",
  has_aoe = true,
  aoe_delay = 450,
  aoe_offset = 16,
  aoe_sprite = "items/shockwave_3x3",
  aoe_scale = {1.5, 1.5},
  aoe_damage = 30,
  aoe_sound = "running_obstacle",
  recovery_duration = 1200,
}

local combo_slam = {}
for k, v in pairs(slam_attack) do combo_slam[k] = v end
combo_slam.windup_duration = 100

local punch_combo = {punch_right_a, punch_left_b, punch_right_b, punch_left_c}
local two_combo = {punch_left_a, punch_right_c}
local three_combo = {punch_right_a, punch_left_b, swipe_b}
local slam_combo = {punch_right_a, punch_left_b, combo_slam}


local long_range = 120
local close_range = 48

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 20)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif distance >= long_range then
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 95,
      attack_function = function()
        enemy:melee_attack(punch_right_c)
      end,
    })
  elseif distance <= close_range then
    if rand <= 10 then
      enemy:melee_combo(two_combo)
    else
      enemy:melee_attack(slam_attack)
    end
  else --midrange
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        if rand <= 6 then
          enemy:melee_combo(two_combo)
        elseif rand <= 12 then
          enemy:melee_combo(punch_combo)
        elseif rand <= 18 then
          enemy:melee_combo(three_combo)
        else
          enemy:melee_combo(slam_combo)
        end
      end
    })
  end
end



