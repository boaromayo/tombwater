local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local min_hide_distance = 150

function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(999)
  enemy:set_damage(1)
  enemy:set_obstacle_behavior("flying")
end


function enemy:on_restarted()
  sprite:set_animation"stopped"
  local check_time = 100
  sol.timer.start(enemy, check_time, function()
    if enemy:get_distance(hero) < min_hide_distance then
      enemy:hide()
    else
      return true
    end
  end)
end


function enemy:hide()
  sol.audio.play_sound("scenes/pit_creature_hide")
  sprite:set_animation("hiding", function() enemy:remove() end)
end
