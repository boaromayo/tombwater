local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 5
enemy.base_damage = 15
enemy.alert_all_directions = true

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 125,
  detection_distance = enemy:get_property("ceiling") and 40 or 96,
  abandon_hero_distance = 150,
  idle_movement_speed = 25,
  sprite_direction_style = "single",
})

local spike_attack = {
  windup_duration = 300,
  windup_animation = "spike_windup",
  attack_animation = "spike_attack",
  weapon_sprite = "enemies/tombwater_enemies/corpse_slime",
  weapon_attack_animation = "spike_weapon_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  attack_sound = "weapons/sword_01",
  recovery_duration = 1000,
  collision_callback = function(hero)
    hero:build_up_status_effect("bleed", 15)
  end,
}


function enemy:ichor_spray()
  local num_shots = 8
  local shot_range = 104
  local blob_damage = 10
  local windup_duration = 500
  local shot_frequency = 300
  local recovery_duration = 1000

  local sprite = enemy:get_sprite()
  local aim_angle = enemy:get_angle(hero)

  local function shoot_blob()
    local x, y, z = enemy:get_position()
    sol.audio.play_sound("ichor_splash")
    local blob = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=8, height=8,
      sprite = "entities/enemy_projectiles/ichor_blob",
      model = "enemy_projectiles/generic_projectile",
    }
    blob.damage = blob_damage
    blob.speed = 160
    blob.max_distance = shot_range - math.random(0, 32)
    blob.obstacle_callback = function(projectile)
      local x, y, z = projectile:get_position()
      projectile:remove()
      map:create_custom_entity{
        x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
        model = "enemy_projectiles/floor_blast",
        sprite = "entities/enemy_projectiles/eldritch_puddle_burst",
      }
    end
    blob:shoot(aim_angle + math.rad(math.random(-45, 45)))
  end

  enemy:stop_movement()
  sprite:set_animation("spraying")
  sol.audio.play_sound("bugs_skittering")
  local blob_count = 0
  sol.timer.start(enemy, windup_duration, function()
    shoot_blob()
    blob_count = blob_count + 1
    if blob_count < num_shots then
      return shot_frequency
    else
      sprite:set_animation("closing", function() sprite:set_animation"stopped" end)
      sol.timer.start(enemy, recovery_duration, function() enemy:decide_action() end)
    end
  end)
end




function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  local melee_distance = 128
  local ranged_distance = 180
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif (distance <= melee_distance) and not enemy.melee_cooldown then
    enemy:approach_then_attack{
      speed = 25,
      dist_threshold = 32,
      attack_function = function()
        enemy.melee_cooldown = true
        sol.timer.start(map, 5000, function() enemy.melee_cooldown = false end)
        enemy:melee_attack(spike_attack)
      end,
    }

  elseif (distance <= ranged_distance) and not enemy.ranged_cooldown then
    enemy.ranged_cooldown = true
    sol.timer.start(map, 4000, function() enemy.ranged_cooldown = false end)
    enemy:get_sprite():set_animation("opening", function()
      enemy:ichor_spray()
    end)

  else
    enemy:move_randomly()
  end
end


--Slime movement sound:
enemy:register_event("on_restarted", function()
  enemy.slime_move_timer = sol.timer.start(enemy, 0, function()
    local m = enemy:get_movement()
    if m and m:get_speed() > 0 then
      enemy:make_sound("enemies/slime_0" .. math.random(1,7))
    end
    return 800
  end)
end)


function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(24, 56))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.rad(math.random(0, 360)))
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end


--Hide in ceiling
enemy:register_event("on_created", function()
  if enemy:get_property("ceiling") then
    enemy.is_in_ceiling = true
    enemy:set_visible(false)
  end
end)

enemy:register_event("on_restarted", function()
  --need to hide enemy again?
end)


function enemy:start_aggro()
  if enemy.is_in_ceiling then
    enemy:ceiling_drop()
  else
    enemy.aggro = true
    enemy:restart()
  end
end


function enemy:ceiling_drop()
  local sprite = enemy:get_sprite()
  local offset = -400
  local step = 10
  enemy.is_in_ceiling = false
  sprite:set_xy(0, offset)
  sol.timer.stop_all(enemy)
  sprite:set_animation("falling")
  enemy:set_visible(true)
  sol.timer.start(enemy, 10, function()
    offset = offset + step
    offset = math.min(offset, 0)
    sprite:set_xy(0, offset)
    if offset >= 0 then
      sol.audio.play_sound("ichor_squish_01")
      sprite:set_animation("splatting", function()
        sprite:set_animation"stopped"
        enemy.aggro = true
        enemy:melee_attack(spike_attack)
      end)
    else
      return true
    end
  end)
end

