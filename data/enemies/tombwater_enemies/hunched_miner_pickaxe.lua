local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 75,
  detection_distance = 200,
  abandon_hero_distance = 400,
})

enemy.base_damage = 10
enemy.exp = 20
enemy.weight = 5
enemy.money = 1
enemy.damage_type_mods = {
  bullet = 1.8,
}

local slash_step_distance = 32
local step_speed = 85

local slash_attack_1 = {
  windup_duration = 10,
  windup_animation = "slash_1_windup",
  attack_animation = "slash_1_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/miner_pickaxe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  step_distance = slash_step_distance,
  step_speed = step_speed,
  damage_type = "physical",
  recovery_duration = 10,
}

local slash_attack_2 = {
  windup_duration = 10,
  windup_animation = "slash_2_windup",
  attack_animation = "slash_2_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/miner_pickaxe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  step_distance = slash_step_distance,
  step_speed = step_speed,
  damage_type = "physical",
  recovery_duration = 10,
}

local ranged_attack = {
  windup_animation = "pickaxe_throw_windup",
  attack_animation = "slash_1_attack",
  projectile_sprite = "entities/enemy_projectiles/pickaxe",
  projectile_width = 16,
  projectile_height = 16,
  damage = 25,
  damage_type = "physicial",
  recovery_duration = 300,
}

local slash_combo_1 = table.duplicate(slash_attack_1)
slash_combo_1.windup_duration = 400
local slash_combo_2 = table.duplicate(slash_attack_2)
local slash_combo_3 = table.duplicate(slash_attack_1)
local slash_combo_4 = table.duplicate(slash_attack_2)
slash_combo_4.recovery_duration = 700

local slash_combo = {slash_combo_1, slash_combo_2, slash_combo_3, slash_combo_4}
local short_combo = {slash_combo_1, slash_combo_4}

local nodelay_slash_combo = { slash_attack_1, slash_attack_4}


local dodge_threshold = 50
local dodge_cooldown_time = 5000
local min_throw_dist = 48
local throw_cooldown_time = 2000

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  --Dodge back
  elseif distance <= dodge_threshold and not enemy.dodge_cooldown then
    enemy.dodge_cooldown = true
    sol.timer.start(map, dodge_cooldown_time, function() enemy.dodge_cooldown = false end)
    enemy:jump_back()

  --Shuffle around
  elseif random <= 30 then
    enemy:shuffle_around()

  --Throw Pickaxe
  elseif random <= 75 and distance >= min_throw_dist and not enemy.throw_cooldown then
    enemy.throw_cooldown = true
    sol.timer.start(map, throw_cooldown_time, function() enemy.throw_cooldown = nil end)
    enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
    enemy:ranged_attack(ranged_attack)

  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 85,
      dist_threshold = 48,
      attack_function = function()
        local m = sol.movement.create"straight"
        m:set_max_distance(16)
        m:set_angle(enemy:get_angle(hero))
        m:start(enemy)
        enemy:melee_combo(short_combo)
      end,
    })
  end
end


--Jump away
function enemy:jump_back()
  local max_distance = 56
  local speed = 140
  local windup_time = 100
  local sprite = enemy:get_sprite()

  sprite:set_direction(enemy:get_direction4_to(hero))
  sprite:set_animation"crouching"
  enemy.lock_facing = true
  sol.timer.start(enemy, windup_time, function()
    sprite:set_animation("jumping_back")
    local angle = hero:get_angle(enemy)
    local m = sol.movement.create"straight"
    m:set_angle(angle)
    m:set_max_distance(max_distance)
    m:set_speed(speed)
    local function end_jump()
      enemy.lock_facing = false
      sprite:set_animation("stopped")
      enemy:shuffle_around()
    end
    m:start(enemy, end_jump)
    m.on_obstacle_reached = end_jump
  end)

end


function enemy:shuffle_around()
--enemy:decide_action()
--
  local shuffle_distance = 32
  local min_time = 200 --how long to shuffle around for
  local max_time = 800
  local speed = 60
  local sprite = enemy:get_sprite()
  enemy.lock_facing = true
  local angle = enemy:get_angle(hero)
  local angle_mod = math.pi / 2
  if math.random(1, 2) == 1 then angle_mod = angle_mod * -1 end

  local function shuffle_direction()
    sprite:set_direction(enemy:get_direction4_to(hero))
    angle_mod = angle_mod * -1
    local m = sol.movement.create"straight"
    m:set_angle(angle + angle_mod)
    m:set_speed(speed)
    m:set_max_distance(shuffle_distance)
    m:start(enemy, function()
      shuffle_direction()
    end)
  end
  sprite:set_animation"walking"
  shuffle_direction()
  sol.timer.start(map, math.random(min_time, max_time), function()
    enemy:stop_movement()
    enemy:decide_action()
  end)
--]]
end

