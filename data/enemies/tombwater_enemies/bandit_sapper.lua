local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 30
enemy.weight = 10
enemy.money = 3
enemy.base_damage = 50

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
  ally_alert_range = 80,
})



local dynamite_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  attack_sound = "throw",
  generic_projectile_type = "bomb",
  projectile_properties = {
    --max_distance = 32,
    --fuse_length = 500,
  },
  projectile_sprite = "hero_projectiles/dynamite",
  damage = enemy.base_damage,
}


function enemy:place_mine()
  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  sprite:set_animation("placing_mine")
  sol.audio.play_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,6))
  local x, y, z = enemy:get_position()
  local direction = sprite:get_direction()
  local offset_x = {[0] = 16, [1] = 0, [2] = -16, [3] = 0}
  local offset_y = {[0] = 0, [1] = -16, [2] = 0, [3] = 4}
  local dx = offset_x[direction]
  local dy = offset_y[direction]
  sol.timer.start(enemy, 100, function()
    local mine = map:create_custom_entity{
      x = x + dx, y = y + dy, layer = z,
      direction = 0, width = 16, height = 16,
      model = "world_objects/mine",
      sprite = "items/mine_proximity",
    }
    sol.timer.start(enemy, 900, function()
      enemy:wander()
    end)
  end)
end


function enemy:wander()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"random"
  m:set_speed(20)
  m:start(enemy)

  sol.timer.start(enemy, math.random(1000, 4000), function()
    enemy:decide_action()
  end)
end





function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  local possible_combos = {
    combo_a, combo_b, combo_c,
  }
  local random_combo = possible_combos[math.random(1, #possible_combos)]

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif random <= 25 and not enemy.dynamite_cooldown then
    enemy:ranged_attack(dynamite_attack)
    enemy.dynamite_cooldown = true
    sol.timer.start(map, 7000, function() enemy.dynamite_cooldown = false end)

  elseif random <= 75 and not enemy.mine_cooldown then
    enemy:place_mine()
    enemy.mine_cooldown = true
    sol.timer.start(map, 8000, function() enemy.mine_cooldown = false end)

  else
    enemy:wander()
  end
end


