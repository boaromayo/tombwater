local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 15
enemy.exp = 5
enemy.weight = 2
enemy.money = 2

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 45,
  detection_distance = 96,
  abandon_hero_distance = 300,
  idle_movement_speed = 5,
})


local slash_attack_1 = {
  windup_duration = 300,
  windup_animation = "backslash_windup",
  attack_animation = "backslash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/goblin_claws",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 10,
  step_distance = 8,
  step_speed = 40
}

local slash_attack_2 = {
  windup_duration = 10,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/goblin_claws",
  weapon_attack_animation = "slash_attack_2",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 700,
  step_distance = 8,
  step_speed = 40
}

local slash_combo = {slash_attack_1, slash_attack_2}


enemy:register_event("on_restarted", function()
  --enemy:set_traversable(true)
end)


local close_range = 50
local melee_range = 32
local attacking_speed = 15

function enemy:decide_action()
  
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif not enemy.aggro then
    enemy:make_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,6))
    sol.timer.start(enemy, math.random(1000, 7000), function()
      enemy:move_randomly()
    end)
  elseif random <= 70 then
    enemy:make_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,6))
    enemy:approach_then_attack{
      speed = attacking_speed,
      dist_threshold = melee_range,
      approach_duration = 2000,
      attack_function = function() enemy:melee_combo(slash_combo) end
    }
  else
    enemy:stop_movement()
    enemy:get_sprite():set_animation"stopped"
    sol.timer.start(enemy, math.random(1000, 3000), function() enemy:decide_action() end)
  end
end


--Overwrite default idle
function enemy:start_idle()
  local sprite = enemy:get_sprite()
  if enemy:get_property("underground") then
    sprite:set_animation"underground"
  else
    enemy:move_randomly()
  end
  sol.timer.start(enemy, 50, function()
    if enemy:get_property("hibernate") then return end
    enemy:check_for_hero()
    return true
  end)
end



function enemy:move_randomly()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"straight"
  m:set_max_distance(math.random(50, 100))
  m:set_speed(enemy.idle_movement_speed)
  m:set_angle(math.random(0, 3) * math.pi / 2)
  m:start(enemy, function()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end)
  function m:on_obstacle_reached()
    sprite:set_animation"stopped"
    enemy:decide_action()
  end
end


function enemy:ambush_attack()
  local m = sol.movement.create"straight"
  m:set_angle(enemy:get_angle(hero))
  m:set_speed(120)
  m:set_max_distance(math.max(enemy:get_distance(hero), 40))
  m:start(enemy, function() enemy:melee_combo(slash_combo) end)
  m.on_finished = function() enemy:melee_combo(slash_combo) end
end



function enemy:start_aggro()
  enemy.aggro = true
  enemy:alert_nearby_enemies()
  sol.timer.stop_all(enemy)
  if enemy:get_property("underground") then
    local sprite = enemy:get_sprite()
    sol.audio.play_sound("spells/ground_burst_soft_4")
    sprite:set_animation("digging_out", function()
      sprite:set_animation("stopped")
      enemy:restart()
    end)
  else
    enemy:restart()
  end
end


