local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.money = 3
enemy.weight = 10
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  stagger_when_hurt = false,
  stagger_life_percent = 60,
  ally_alert_range = 96,
})



enemy:register_event("on_restarted", function()
  enemy:sit_or_wander()
end)


function enemy:sit_or_wander()
  if enemy:get_property("sitting") then
    enemy:get_sprite():set_animation("sitting")
  else
    enemy:wander()
  end
end


function enemy:wander()
  local sprite = enemy:get_sprite()
  sprite:set_animation"walking"
  local m = sol.movement.create"random"
  m:set_speed(20)
  m:start(enemy)

  sol.timer.start(enemy, math.random(2000, 8000), function()
    enemy:stop_movement()
    sprite:set_animation("stopped")
    sol.timer.start(enemy, math.random(1000, 4000), function()
      enemy:wander()
    end)
  end)
end


function enemy:decide_action()
  enemy:sit_or_wander()
end

