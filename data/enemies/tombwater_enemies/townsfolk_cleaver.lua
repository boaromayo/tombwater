local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 1

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 70,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 25,
  step_distance = 32,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_attack = {
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 15,
  damage_type = "physical",
  recovery_duration = 500,
}

local reverse_slash_attack = {
  windup_duration = 300,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 15,
  damage_type = "physical",
  recovery_duration = 200,
}

local ambush_attack = {
  windup_duration = 50,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 20,
  damage_type = "physical",
  step_distance = 32,
  recovery_duration = 200,
}

local combo_1a = {
  windup_duration = 700,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 20,
  step_distance = 16,
}

local combo_1b = {
  windup_duration = 50,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 20,
  step_distance = 8,
}

local combo_1c = {
  windup_duration = 200,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 800,
  step_distance = 8,
}

local combo_1d = {
  windup_duration = 50,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 600,
  step_distance = 8,
}


local combo_1c = {
  windup_duration = 200,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 800,
  step_distance = 8,
}

--combo 2
local combo_2a = {
  windup_duration = 500,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 1,
  step_distance = 8,
}

local combo_2b = {
  windup_duration = 1,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 1,
  step_distance = 8,
}

local combo_2c = {
  windup_duration = 1,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 1,
  step_distance = 8,
}

local combo_2d = {
  windup_duration = 1,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 1,
  step_distance = 8,
}

local combo_2e = {
  windup_duration = 1,
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 1,
  step_distance = 14,
}
local combo_2f = {
  windup_duration = 500,
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  recovery_animation = "tired",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 20,
  damage_type = "physical",
  recovery_duration = 1800,
  step_distance = 8,
}


local slash_combo = {combo_1a, combo_1b, combo_1c}
local short_combo = {combo_1a, combo_1d}
local wild_combo = {combo_2a, combo_2b, combo_2c, combo_2d, combo_2e, combo_2f}

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  else
    enemy:townsfolk_sound_chance()
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:townsfolk_sound_chance()
        local rand = math.random(1, 6)
        if rand == 1 then
          enemy:melee_combo(slash_combo)
        elseif rand == 2 then
          enemy:melee_combo(short_combo)
        elseif rand == 3 then
          enemy:melee_combo(wild_combo)
        elseif enemy:is_aligned(hero, 10) then
          enemy:melee_attack(thrust_attack)
        else
          enemy:melee_attack(reverse_slash_attack)
        end
      end
    })
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

