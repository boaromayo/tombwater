local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local sword_damage = 30
local knife_damage = 20
local knife_status_effect = "poison"
local knife_status_amount = 80
local dodge_range = 48
local dodge_distance = 64
local gun_damage = 40
local bullet_reduction_rate = 2.5
local bullet_distance = 80
local gun_range_min = 40
local gun_range_max = 96
local stun_grenade_distance_min = 16
local stun_grenade_distance_max = 80


enemy.exp = 20
enemy.weight = 10
enemy.money = 7

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 250,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 35,
})


local sword_forehand = {
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/assassin_sword",
  weapon_windup_animation = "forehand_windup",
  weapon_attack_animation = "forehand_attack",
  step_distance = 24,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}


local sword_backhand = {
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/assassin_sword",
  weapon_windup_animation = "backhand_windup",
  weapon_attack_animation = "backhand_attack",
  step_distance = 24,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}


local knife_forehand = {
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup_tall",
  weapon_attack_animation = "slash_attack",
  step_distance = 24,
  damage = knife_damage,
  damage_type = "physical",
  recovery_duration = 500,
  collision_callback = function(hero)
    hero:build_up_status_effect(knife_status_effect, knife_status_amount)
  end,
}


local knife_backhand = {
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  attack_soundset = "blade",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup_tall",
  weapon_attack_animation = "backslash_attack",
  step_distance = 24,
  damage = knife_damage,
  damage_type = "physical",
  recovery_duration = 500,
  collision_callback = function(hero)
    hero:build_up_status_effect(knife_status_effect, knife_status_amount)
  end,
}

local combo_a = table.duplicate(knife_forehand)
combo_a.recovery_duration = 10
local combo_b = table.duplicate(knife_backhand)
combo_b.windup_duration, combo_b.recovery_duration = 10, 10
local combo_c = table.duplicate(knife_forehand)
combo_c.windup_duration, combo_c.recovery_duration = 10, 250
local three_combo = {combo_a, combo_b, combo_c}

local combo_b2 = table.duplicate(combo_b)
combo_b2.recovery_duration = 250
local two_combo = {combo_a, combo_b2}


local blunderbuss_attack = {
  windup_animation = "gun_aiming",
  windup_duration = 500,
  attack_sound = "weapons/gun_shotgun_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  projectile_offset_radius = 8,
  damage = gun_damage,
  num_projectiles = 6,
  projectile_properties = {
    max_distance = bullet_distance,
    distance_damage_reduction_rate = bullet_reduction_rate,
  },
  recovery_animation = "gun_aiming",
  recovery_duration = 300,
}


function enemy:stun_gas_attack()
  local windup_time = 350
  local num_clouds = 5
  local spread = math.pi / 3
  local cloud_offset = 16
  local cloud_distance = 48
  local stun_duration = 2000
  local recovery_duration = 700

  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  sprite:set_animation("throwing_windup")
  sol.timer.start(enemy, windup_time, function()
    local direction = sprite:get_direction()
    local x, y, z = enemy:get_position()
    sprite:set_animation("throwing", "stopped")
    sol.audio.play_sound("throw")
    for i = 1, num_clouds do
      local cloud = map:create_custom_entity{
        x = x + game:dx(cloud_offset)[direction],
        y = y + game:dy(cloud_offset)[direction],
        layer = z, width = 16, height = 16, direction = 0,
        sprite = "items/cloud",
      }
      cloud:get_sprite():set_animation("gas", function()
        cloud:remove()
      end)
      cloud:add_collision_test("sprite", function(cloud, other)
        if other:get_type() == "hero" then
          cloud:clear_collision_tests()
          other:stun(stun_duration)
        end
      end)
      local m = sol.movement.create"straight"
      local angle = (direction * math.pi / 2) - spread + (spread / num_clouds * i)
      m:set_angle(angle)
      m:set_max_distance(cloud_distance)
      m:set_ignore_obstacles(true)
      m:start(cloud)
    end
    sol.timer.start(enemy, recovery_duration, function() enemy:decide_action() end)
  end)
end


local dodge_slash = table.duplicate(combo_b)
--dodge_slash.windup_duration = 300
dodge_slash.next_action = function() enemy:dodge_away() end

function enemy:dodge_away()
  local dodge_speed = 200
  local sprite = enemy:get_sprite()
  sprite:set_direction(enemy:get_direction4_to(hero))
  enemy.lock_facing = true
  local direction = sprite:get_direction()
  sprite:set_animation"dodge_back"
  local m = sol.movement.create"straight"
  m:set_speed(dodge_speed)
  m:set_max_distance(dodge_distance)
  m:set_angle(hero:get_angle(enemy))
  m:start(enemy, function() enemy:decide_post_dodge_action() end)
  function m:on_obstacle_reached() enemy:decide_post_dodge_action() end
end

function enemy:decide_post_dodge_action()
  enemy.lock_facing = false
  local sprite = enemy:get_sprite()
  sprite:set_animation("stopped")
  sprite:set_direction(enemy:get_direction4_to(hero))
  local random = math.random(1, 100)
  if random < 35 then
    enemy:stun_gas_attack()
  elseif random < 70 then
    enemy:ranged_attack(blunderbuss_attack)
  else
    sol.timer.start(enemy, 400, function() enemy:decide_action() end)
  end
end



function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  --Dodge away
  elseif distance <= dodge_range and not enemy.dodge_cooldown then
    enemy.dodge_cooldown = true
    sol.timer.start(map, 5000, function() enemy.dodge_cooldown = nil end)
    enemy:melee_attack(dodge_slash)
  --Stun grenade
  elseif random < 20 and distance >= stun_grenade_distance_min and distance <= stun_grenade_distance_max and not enemy.stun_gas_cooldown then
    enemy.stun_gas_cooldown = true
    sol.timer.start(map, 3000, function() enemy.stun_gas_cooldown = nil end)
    enemy:stun_gas_attack()
  --Blunderbuss
  elseif random < 40 and distance >= gun_range_min and distance <= gun_range_max then
    enemy:ranged_attack(blunderbuss_attack)

  else
    enemy:approach_then_attack{
      speed = 85,
      attack_function = function() enemy:melee_combo(two_combo) end,
    }
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

