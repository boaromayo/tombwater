local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 25
enemy.exp = 35
enemy.weight = 25
enemy.money = 6

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 180,
  detection_distance = 200,
  abandon_hero_distance = 400,
  sprite_direction_style = "horizontal",
  width = 24, height = 24,
})

--Slash Attacks:
local slash_1_attack = {
  step_distance = 24,
  step_tracking = true,
  windup_duration = 150,
  windup_animation = "slash_1_windup",
  attack_animation = "slash_1_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/horsehead_slash",
  weapon_attack_animation = "slash_1_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 200,
}

local slash_2_attack = {
  step_distance = 24,
  step_tracking = true,
  windup_duration = 150,
  windup_animation = "slash_2_windup",
  attack_animation = "slash_2_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/horsehead_slash",
  weapon_attack_animation = "slash_2_attack",
  damage = enemy.base_damage,
  damage_type = "physical",
  recovery_duration = 200,
}

local slash_combo = {slash_1_attack, slash_2_attack}


--Head Slam Attacks:
local head_slam = {
  step_distance =15,
  windup_duration = 400,
  windup_animation = "head_slam_windup",
  attack_animation = "head_slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_punch",
  weapon_attack_animation = "slam",
  damage = enemy.base_damage * 2,
  damage_type = "physical",
  has_aoe = true,
  aoe_delay = 150,
  aoe_offset = 40,
  aoe_sprite = "items/shockwave_8x7",
  aoe_scale = {.75, .75},
  aoe_damage = enemy.base_damage + 5,
  aoe_sound = "running_obstacle",
  recovery_duration = 600,
  sprite_direction_style = "horizontal"
}

local head_combo_1 = table.duplicate(head_slam)
head_combo_1.windup_duration, head_combo_1.recovery_duration = 900, 100
head_combo_1.windup_sound = "enemies/horse_scream_short"

local head_combo_2 = table.duplicate(head_slam)
head_combo_2.windup_duration, head_combo_2.recovery_duration = 50, 100

local head_combo_3 = table.duplicate(head_slam)
head_combo_3.windup_duration, head_combo_3.recovery_duration = 400, 1500

local head_combo = {head_combo_1, head_combo_2, head_combo_3}


--Running Slash Attack:
local rs_1 = table.duplicate(slash_1_attack)
rs_1.windup_duration, rs_1.recovery_duration = 400, 10
rs_1.step_distance = 48
local rs_2 = table.duplicate(slash_2_attack)
rs_2.windup_duration, rs_2.recovery_duration = 100, 10
rs_2.step_distance = 48
local rs_3 = table.duplicate(slash_1_attack)
rs_3.windup_duration, rs_3.recovery_duration = 50, 10
rs_3.step_distance = 48
local rs_4 = table.duplicate(slash_2_attack)
rs_4.windup_duration, rs_4.recovery_duration = 200, 10
rs_4.step_distance = 48
local rs_5 = table.duplicate(slash_1_attack)
rs_5.windup_duration, rs_5.recovery_duration = 50, 10
rs_5.step_distance = 48
local rs_6 = table.duplicate(slash_2_attack)
rs_6.windup_duration, rs_6.recovery_duration = 50, 10
rs_6.step_distance = 48
local rs_7 = table.duplicate(slash_1_attack)
rs_7.windup_duration, rs_7.recovery_duration = 100, 10
rs_7.step_distance = 48
rs_7.next_action = function() enemy:vomit() end


local running_slash_combo = {rs_1, rs_2, rs_3, rs_4, rs_5, rs_6, rs_7}



local previous_action = 0
function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1,100)

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  --Vomit
  elseif rand <= 25 and previous_action ~= 1 then
   enemy:vomit()
    previous_action = 1
  --Running Slash scream
  elseif rand >= 50 and distance >= 64 and previous_action ~= 5 then
    enemy:get_sprite():set_animation("screaming")
    sol.audio.play_sound"enemies/horse_scream"
    sol.timer.start(enemy, 800, function()
      enemy:melee_combo(running_slash_combo)
    end)
    previous_action = 5
  --Melee attack:
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 130,
      attack_function = function()
        if rand <= 50 then
          enemy:melee_attack(head_slam)
          previous_action = 2
        elseif rand <= 75 then
          enemy:melee_combo(slash_combo)
          previous_action = 3
        elseif rand <= 100 and previous_action ~= 4 then
          enemy:melee_combo(head_combo)
          previous_action = 4
        else
          enemy:decide_action()
        end
      end
    })  
  end
end

function enemy:vomit()
  local map = enemy:get_map()
  local hero = map:get_hero()
  local enemy_sprite = enemy:get_sprite()
  local direction = enemy_sprite:get_direction()
  local x,y,z = enemy:get_position()
  local offset = 16 

  if direction == 1 then
    offset = -offset
  end

  enemy:stop_movement()
  enemy_sprite:set_animation("vomiting")
  enemy:make_sound"spells/vomit"
  enemy:make_sound"enemies/horse_grunt_01"

  local puke = map:create_custom_entity{
    x = x + offset, y = y, layer = z,
    direction = 0,
    width = 16, height = 16,
    sprite = "enemies/tombwater_enemies/horsehead"
  }
  local puke_sprite = puke:get_sprite()
  puke_sprite:set_animation("vomit_starting")
  sol.timer.start(puke, 500, function()
    local puddle = map:create_custom_entity{
      x = x + offset, y = y, layer = z,
      direction = 0,
      width = 16, height = 16,
      sprite = "enemies/tombwater_enemies/horsehead"
    }
    local puddle_sprite = puddle:get_sprite()
    puddle_sprite:set_animation("vomit_puddle", function()
      puddle_sprite:set_animation("vomit_puddle_idle")
      puke_sprite:set_animation("vomit_ending")
      sol.timer.start(puddle, 10000, function()
        puddle_sprite:set_animation("vomit_puddle_shrinking", function()
          puddle:remove()
        end)
      end)
      sol.timer.start(enemy, 500, function()
        enemy:restart()
      end)
    end)
    puddle:add_collision_test("sprite", function(puddle,other_entity)
      if other_entity:get_type() == "hero" then
        if not puddle.cooldown then
          sol.audio.play_sound("razorgrass")
          sol.audio.play_sound("hero_hurt")
          hero:blood_splatter(self, 8)
          game:remove_life(5)
          puddle.cooldown = true
          sol.timer.start(puddle, 500, function()
            puddle.cooldown = false
          end)
        end
      end
    end)
  end)
end