local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 20
enemy.weight = 10
enemy.money = 1
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 75,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})

local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage,
  step_distance = 32,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_attack = {
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  step_distance = 16,
  damage = 15,
  damage_type = "physical",
  recovery_duration = 500,
}

local backslash_attack = {
  windup_duration = 300,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  step_distance = 16,
  damage = 15,
  damage_type = "physical",
  recovery_duration = 200,
}

local ambush_attack = {
  windup_duration = 50,
  windup_animation = "reverse_slash_windup",
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 20,
  damage_type = "physical",
  step_distance = 32,
  recovery_duration = 200,
}

--Thrust Combo
local thrust_a = table.duplicate(thrust_attack)
thrust_a.recovery_duration = 10
local thrust_b = table.duplicate(thrust_attack)
thrust_b.windup_duration, thrust_b.recovery_duration = 10, 10
local thrust_c = table.duplicate(thrust_attack)
thrust_c.windup_duration = 100
thrust_c.step_distance = 40

local thrust_combo = {thrust_a, thrust_b, thrust_c}

--Slash combo
local slash_a = table.duplicate(backslash_attack)
slash_a.recovery_duration = 10
local slash_b = table.duplicate(slash_attack)
slash_b.windup_duration, slash_b.recovery_duration = 10, 10
local slash_c = table.duplicate(backslash_attack)
slash_c.windup_duration, slash_c.recovery_duration = 80, 10
local slash_d = table.duplicate(slash_attack)
slash_d.windup_duration, slash_d.recovery_duration = 80, 800

local slash_combo = {slash_a, slash_b, slash_c, slash_d}


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local rand = math.random(1, 100)
  if rand <= 25 then sol.audio.play_sound("enemies/townsfolk_vox/snarl_0" .. math.random(1,2)) end
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance >= 40 and distance <= 80 then
    if rand <= 50 then
      enemy:melee_combo(slash_combo)
    else
      enemy:melee_combo(thrust_combo)
    end
  else
    enemy:approach_then_attack({
      approach_duration = 300,
      speed = 60,
      attack_function = function()
        enemy:melee_attack(backslash_attack)
      end
    })
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

