local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local slash_damage = 25

enemy.exp = 20
enemy.weight = 10
enemy.money = 9
enemy.base_damage = 25

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 250,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 35,
  sprite_direction_style = "horizontal",
})


local slash_a = {
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichorcyst_weapon",
  weapon_attack_animation = "slash_forehand_attack",
  step_distance = 40,
  step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
}

local slash_b = {
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichorcyst_weapon",
  weapon_attack_animation = "slash_attack",
  step_distance = 40,
  step_tracking = true,
  damage = enemy.base_damage,
  damage_type = "physical",
}

local function combo_move(atk, wd, rd)
  local mv = table.duplicate(atk)
  mv.windup_duration, mv.recovery_duration = wd, rd
  return mv
end

local combo_a = {
  combo_move(slash_a, 600, 10),
  combo_move(slash_b, 100, 10),
  combo_move(slash_a, 100, 10),
  combo_move(slash_b, 100, 500),
}

local combo_b = {
  combo_move(slash_a, 10, 10),
  combo_move(slash_b, 10, 10),
  combo_move(slash_a, 10, 500),
}



local ichor_throw = {
  windup_animation = "throw_windup",
  windup_duration = 500,
  attack_animation = "throw_attack",
  projectile_sprite = "entities/enemy_projectiles/ichor_glob",
  projectile_offset_radius = 8,
  damage = enemy.base_damage,
  num_projectiles = 1,
  projectile_properties = {
    max_distance = ichor_range,
    speed = 160,
  },
  recovery_duration = 500,
}


local howl_attack = {
  windup_animation = "crying",
  attack_animation = "screaming",
  attack_sound = "enemies/townsfolk_vox/screech_scream_long",
  attack_scale = {.5, .5},
  y_offset = -24,
  damage = enemy.base_damage * .2,
}


function enemy:ichor_root_attack()
  local windup_duration = 400
  local attack_delay = 500
  local num_attacks = 4
  local attack_frequency = 600
  local warning_sound  = "ichor_burst_ground"
  local attack_sound = "spells/maw_appear"
  local target = hero

  local sprite = enemy:get_sprite()

  sprite:set_animation("slam_windup")
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_animation("slam_attack", function()
      sprite:set_animation("slam_hold")
    end)
    local attacks_done = 0
    sol.timer.start(enemy, attack_delay, function()
      attacks_done = attacks_done + 1
      sol.audio.play_sound(warning_sound)
      local x, y, z = target:get_position()
      local attack = map:create_custom_entity{
        x=x, y=y, layer=z, width=16, height=16, direction=0,
        model = "enemy_projectiles/general_attack",
        sprite = "entities/enemy_projectiles/ichor_root",
      }
      attack.delay_collision = true
      attack.damage = enemy.base_damage * .75
      attack.damage_type = "physical"
      sol.timer.start(attack, attack_delay, function()
        sol.audio.play_sound(attack_sound)
        attack:get_sprite():set_animation("attacking")
        attack:remove_after_animation()
        attack:activate_collision()
      end)

      if attacks_done < num_attacks then
        return attack_frequency
      else
        sprite:set_animation"stopped"
        enemy:decide_action()
      end
    end)
  end)
end




local melee_range = 72

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --Long range attacks:
  elseif distance >= melee_range and random <= 40 and not enemy.ichor_root_cooldown then
    sol.audio.play_sound"enemies/townsfolk_vox/screech_low_04"
    enemy:ichor_root_attack()
    enemy.ichor_root_cooldown = true
    sol.timer.start(map, 6000, function() enemy.ichor_root_cooldown = nil end)

  elseif distance >= melee_range and random <= 60 then
    sol.audio.play_sound"enemies/townsfolk_vox/screech_low_03"
    enemy:ranged_attack(ichor_throw)


  --Short range attacks:
  elseif distance <= melee_range and random <= 30 and not enemy.howl_cooldown then
    enemy:howl_attack(howl_attack)
    enemy.howl_cooldown = true
    sol.timer.start(map, 10000, function() enemy.howl_cooldown = nil end)

  elseif (distance <= melee_range) and (random < 25) then
    enemy:ranged_attack(ichor_throw)

  elseif (distance <= 56) and random <= 55 then
    enemy:retreat{
      speed = 200,
      max_distance = 48,
      duration = 250,
      animation = "slash_a_windup",
      lock_facing = true,
      callback = function()
        enemy:melee_combo(combo_b)
      end,
    }

  else
    enemy:approach_then_attack{
      speed = 75,
      attack_function = function() enemy:melee_combo(combo_a) end,
    }
  end
end

