local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(999999)
  enemy:set_damage(0)
  enemy:set_can_attack(false)
  --enemy:set_traversable(false) --this causes problems with the scarecrow's bugs
  local x, y, z = enemy:get_position()
  local blocker = map:create_custom_entity{
    x=x, y=y, layer=z, direction=0, width=16, height=16,
  }
  blocker:set_traversable_by("hero", false)
end


function enemy:on_restarted()
  sprite:set_animation("stopped")
  if enemy:get_property("bugs") then
    enemy:set_bugs()
  end
end

function enemy:process_hit()
  sol.audio.play_sound("enemy_hurt")
  enemy:blood_splatter(amount)
  enemy:make_sound("enemies/townsfolk_vox/moan_0" .. math.random(1,6))
end


function enemy:set_bugs()
  if enemy.bugs_sprung then return end
  sol.timer.start(enemy, 300, function()
    local distance = enemy:get_distance(hero)
    if distance <= 64 then
      enemy:spring_bugs()
      enemy.bugs_sprung = true
    else
      return true
    end
  end)
end


function enemy:spring_bugs()
  local min_bugs, max_bugs = 4, 9
  local breeds = {"tombwater_enemies/swarm_fly", "tombwater_enemies/spiderbug"}
  local x, y, z = enemy:get_position()
  sprite:set_animation("chest_squirm")
  sol.audio.play_sound("bugs_skittering")
  sol.timer.start(enemy, 400, function()
    sol.audio.play_sound("enemy_killed")
    sprite:set_animation("chest_burst", function()
        sprite:set_animation("empty")
    end)
    
    --enemy:create_enemy{ breed = "tombwater_enemies/fly_swarm" }
    --
    for i = 1, math.random(min_bugs, max_bugs) do
      local bug = enemy:create_enemy{
        breed = "tombwater_enemies/spiderbug",
        y = -17,
      }
      sol.timer.start(enemy, 10, function()
        sol.timer.stop_all(bug)
        bug:stop_movement()
        local m = sol.movement.create"straight"
        m:set_ignore_obstacles(true)
        m:set_max_distance(24)
        m:set_speed(200)
        m:set_angle(math.rad(math.random(240, 310)))
        m:start(bug, function()
          bug:restart()
        end)
      end)
    end
    --]]
  end)
end
