local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 15
enemy.weight = 3
enemy.money = 3
enemy.base_damage = 15

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 50,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
  idle_movement_speed = 25,
})

local slash_a = {
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/bone_crab",
  weapon_attack_animation = "slash_a_weapon",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 8,
}

local slash_b = {
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/bone_crab",
  weapon_attack_animation = "slash_b_weapon",
  damage = enemy.base_damage,
  damage_type = "physical",
  step_distance = 8,
}

local combo_a_1 = table.duplicate(slash_a)
combo_a_1.windup_duration, combo_a_1.recovery_duration = 500, 10
local combo_a_2 = table.duplicate(slash_b)
combo_a_2.windup_duration, combo_a_2.recovery_duration = 60, 500
local combo_a = {combo_a_1, combo_a_2}

local combo_b_1 = table.duplicate(slash_b)
combo_b_1.windup_duration, combo_b_1.recovery_duration = 500, 10
local combo_b_2 = table.duplicate(slash_a)
combo_b_2.windup_duration, combo_b_2.recovery_duration = 60, 10
local combo_b_3 = table.duplicate(slash_b)
combo_b_3.windup_duration, combo_b_3.recovery_duration = 60, 10
local combo_b_4 = table.duplicate(slash_a)
combo_b_4.windup_duration, combo_b_4.recovery_duration = 300, 10
local combo_b_5 = table.duplicate(slash_b)
combo_b_5.windup_duration, combo_b_5.recovery_duration = 10, 700
local combo_b = {combo_b_1, combo_b_2, combo_b_3, combo_b_4, combo_b_5}


function enemy:decide_action()
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 65,
      attack_function = function()
        local combo_choice = math.random(1,10) <= 5 and combo_a or combo_b
        enemy:melee_combo(combo_choice)
      end,
    })
  end
end

