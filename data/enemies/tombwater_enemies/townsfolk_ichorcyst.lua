local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local slash_damage = 25

enemy.exp = 20
enemy.weight = 10
enemy.money = 9

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 250,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 35,
  sprite_direction_style = "horizontal",
})


local ichor_shot = {
  windup_animation = "throw_windup",
  windup_duration = 400,
  attack_animation = "throw_attack",
  projectile_sprite = "entities/enemy_projectiles/ichor_blob",
  projectile_offset_radius = 8,
  damage = 20,
  num_projectiles = 3,
  projectile_properties = {
    max_distance = ichor_range,
    speed = 160,
    obstacle_callback = function(projectile)
      local x, y, z = projectile:get_position()
      projectile:remove()
      map:create_custom_entity{
        x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
        model = "enemy_projectiles/floor_blast",
        sprite = "entities/enemy_projectiles/eldritch_puddle_burst",
      }
    end,
  },
  recovery_duration = 700,
}


function enemy:ichor_spray()
  local num_shots = 8
  local shot_range = 104
  local blob_damage = 10
  local windup_duration = 500
  local shot_frequency = 300
  local recovery_duration = 600

  local sprite = enemy:get_sprite()
  local aim_angle = enemy:get_angle(hero)
  sprite:set_direction(enemy:get_facing_direction_to(aim_angle))

  local function shoot_blob()
    local x, y, z = enemy:get_position()
    sol.audio.play_sound("ichor_squish_01")
    local blob = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=8, height=8,
      sprite = "entities/enemy_projectiles/ichor_blob",
      model = "enemy_projectiles/generic_projectile",
    }
    blob.damage = blob_damage
    blob.speed = 160
    blob.max_distance = shot_range - math.random(0, 32)
    blob.obstacle_callback = function(projectile)
      local x, y, z = projectile:get_position()
      projectile:remove()
      map:create_custom_entity{
        x = x, y = y, layer = z, direction = 0, width = 16, height = 16,
        model = "enemy_projectiles/floor_blast",
        sprite = "entities/enemy_projectiles/eldritch_puddle_burst",
      }
    end
    blob:shoot(aim_angle + math.rad(math.random(-45, 45)))
  end

  enemy:stop_movement()
  sprite:set_animation("ichor_spray")
  enemy:make_sound("bugs_skittering")
  enemy:make_sound"ichor_splash"
  local blob_count = 0
  sol.timer.start(enemy, windup_duration, function()
    shoot_blob()
    blob_count = blob_count + 1
    if blob_count < num_shots then
      return shot_frequency
    else
      sprite:set_animation("stopped")
      sol.timer.start(enemy, recovery_duration, function() enemy:decide_action() end)
    end
  end)
end


local slash_attack = {
  windup_animation = "slash_windup",
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichorcyst_weapon",
  weapon_attack_animation = "slash_attack",
  step_distance = 40,
  step_tracking = true,
  damage = slash_damage,
  damage_type = "physical",
}

local slash_forehand_attack = {
  windup_animation = "throw_windup",
  attack_animation = "throw_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/ichorcyst_weapon",
  weapon_attack_animation = "slash_forehand_attack",
  step_distance = 40,
  step_tracking = true,
  damage = slash_damage,
  damage_type = "physical",
}

local combo_a = table.duplicate(slash_attack)
combo_a.recovery_duration = 10
local combo_b = table.duplicate(slash_forehand_attack)
combo_b.windup_duration, combo_b.recovery_duration = 10, 10
local combo_c = table.duplicate(slash_attack)
combo_c.windup_duration = 10
local three_combo = {combo_a, combo_b, combo_c}

local combo_b2 = table.duplicate(slash_forehand_attack)
combo_b2.windup_duration = 10
local two_combo = {combo_a, combo_b2}

local combo_d = table.duplicate(slash_forehand_attack)
combo_d.recovery_duration = 10
local combo_e = table.duplicate(slash_attack)
combo_e.windup_duration, combo_e.recovery_duration = 10, 10
combo_e.next_action = function() enemy:ranged_attack(ichor_shot) end
local shoot_combo = {combo_d, combo_e}




local melee_range = 64

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif ((distance > melee_range and enemy:has_los(hero)) or (distance > melee_range and random < 40) or (random < 20)) and not enemy.spray_cooldown then
    enemy.spray_cooldown = true
    sol.timer.start(map, 4000, function() enemy.spray_cooldown = nil end)
    enemy:ichor_spray()

  elseif (distance <= melee_range) and (random < 50) then
    enemy:make_sound("enemies/townsfolk_vox/wheeze_04")
    enemy:melee_combo(shoot_combo)

  else
    sol.audio.play_sound("enemies/townsfolk_vox/moan_0" .. math.random(3,6))
    enemy:approach_then_attack{
      speed = 75,
      attack_function = function() enemy:melee_combo(two_combo) end,
    }
  end
end


enemy:register_event("on_restarted", function()
  if enemy:get_property("untransformed") then
    local sprite = enemy:get_sprite()
    sprite:set_animation"human_stopped"
  end
end)


function enemy:start_aggro()
  enemy:alert_nearby_enemies()
  sol.timer.stop_all(enemy)
  enemy.aggro = true
  if enemy:get_property("untransformed") then
    enemy:start_transformation(300)
    enemy:set_property("untransformed", nil)
  else
    enemy:restart()
  end
end


function enemy:start_transformation(cough_duration)
  local sprite = enemy:get_sprite()
  sprite:set_animation("human_sick")
  sol.audio.play_sound("spells/vomit")
  sol.timer.start(enemy, cough_duration or 600, function()
    enemy:transform()
  end)
end


function enemy:transform()
  if enemy.transformation_complete then return end
  sol.audio.play_sound("ichor_transforming")
  local sprite = enemy:get_sprite()
  sprite:set_direction(0)
  sprite:set_animation("transforming", function()
    enemy.transformation_complete = true
    enemy:restart()
  end)
end

