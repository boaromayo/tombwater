local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 5
enemy.weight = 1
enemy.money = 0


require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 5,
  detection_distance = 96,
  abandon_hero_distance = 150,
})

local ram_attack = {
  damage = 5,
  windup_animation = "stopped",
  windup_sound = "spells/fly_shoot",
  attack_animation = "walking",
  weapon_sprite = "enemies/tombwater_enemies/fly",
  weapon_attack_animation = "attack_mask",
  max_distance = 240,
  recovery_duration = 10,
}


enemy:register_event("on_created", function()
  enemy:get_sprite():set_xy(0, -8)
  local shadow_sprite = enemy:create_sprite("shadows/shadow_4")
end)


enemy:register_event("on_restarted", function()
  enemy:set_traversable(true)
  enemy:fly_around()
end)


function enemy:fly_around()
--
  local m = sol.movement.create"circle"
  m:set_center(enemy:get_position())
  m:set_radius(0)
  m:set_radius_speed(20)
  m:set_clockwise(math.random(1,2) == 1)
  m:start(enemy)
  m:set_radius(math.random(32,96))

  function m:on_obstacle_reached()
    enemy:restart()
  end
--]]
--  local m = sol.movement.create"random"
--  m:start(enemy)

  sol.timer.start(enemy, math.random(500, 2000), function()
    enemy:decide_action()
  end)
end


function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif not enemy.ram_cooldown then
    enemy:ram_attack(ram_attack)
    enemy.ram_cooldown = true
    sol.timer.start(map, math.random(1500,5000), function()
      enemy.ram_cooldown = false
    end)
  else
    enemy:stop_movement()
    enemy:fly_around()
  end
end


function enemy:be_briefly_unresponsive(duration)
  --A function so I can move the enemy around with it moving itself and causing conflict
  sol.timer.stop_all(enemy)
  
end



