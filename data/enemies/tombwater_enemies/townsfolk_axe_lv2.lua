local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.base_damage = 20
enemy.exp = 25
enemy.weight = 10
enemy.money = 3

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 95,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})



local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage * 1.2,
  step_distance = 24,
  damage_type = "physical",
  recovery_duration = 500,
}

local slam_attack = {
  windup_animation = "overhead_windup",
  attack_animation = "overhead_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = enemy.base_damage * 1.8,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 500,
  --aoe:
  has_aoe = true,
  aoe_offset = 48,
  aoe_delay = 180,
  aoe_sound = "spells/vomit",
  aoe_damage = enemy.base_damage * 1.5,
  aoe_sprite = "entities/enemy_projectiles/ichor_tentacle_floor_burst", 
}

local slash_attack = {
  windup_animation = "slash_windup",
  windup_duration = 300,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 200,
}

local backslash_attack = {
  windup_animation = "reverse_slash_windup",
  windup_duration = 200,
  attack_animation = "reverse_slash_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = enemy.base_damage,
  step_distance = 16,
  damage_type = "physical",
  recovery_duration = 200,
}

local ambush_attack = table.duplicate(backslash_attack)
ambush_attack.windup_duration, ambush_attack.recovery_duration = 50, 200
ambush_attack.step_distance = 32

local function combo(a, w, r)
  local atk = table.duplicate(a)
  atk.windup_duration = w
  atk.recovery_duration = r
  return atk
end

local combo_a = {
  combo(slash_attack, 500, 30),
  combo(backslash_attack, 30, 30),
  combo(thrust_attack, 300, 800),
}

local combo_b = {
  combo(slash_attack, 500, 30),
  combo(backslash_attack, 30, 30),
  combo(slash_attack, 30, 30),
  combo(backslash_attack, 30, 1000),
}


local ranged_thrust = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = enemy.base_damage * 1.2,
  step_distance = 24,
  damage_type = "physical",
  recovery_duration = 500,
  --Also shoot a projectile with the thrust:
  attacking_callback = function()
    local x, y, z = enemy:get_position()
    local projectile_sprite = "entities/enemy_projectiles/ichor_glob"
    local projectile = map:create_custom_entity{
      x=x, y=y, layer=z, width=8, height=8, direction=0,
      sprite = projectile_sprite,
      model = "enemy_projectiles/generic_projectile",
    }
    projectile.damage = enemy.base_damage
    projectile.damage_type = "physical"
    projectile.firing_entity = enemy
    projectile.rotational_sprite = true
    projectile.speed = 250
    aim_angle = projectile:get_angle(hero)
    projectile:shoot(aim_angle)
  end,
}


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end


local ranged_distance = 160

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  local random = math.random(1, 100)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif (distance > ranged_distance and random <= 75) or (random <= 10) then
    enemy:melee_attack(ranged_thrust)

  else
    enemy:townsfolk_sound_chance()
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        enemy:townsfolk_sound_chance()
        local rand = math.random(1,4)
        if random <= 25 then
          enemy:melee_combo(combo_a)
        elseif random <= 50 then
          enemy:melee_combo(combo_b)
        elseif enemy:is_aligned(hero, 10) or random <= 70 then
          enemy:melee_attack(slam_attack)
        else
          enemy:retreat{
            speed = 150,
            duration = 200,
            lock_facing = true,
            callback = function() enemy:melee_attack(backslash_attack) end,
          }

        end
      end
    })
  end
end
