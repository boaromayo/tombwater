local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 80
enemy.weight = 20
enemy.money = 4

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 260,
  width = 32,
  height = 32,
  detection_distance = 300,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
})

local quick_swipe = {
  windup_animation = "swipe_1_windup",
  windup_duration = 300,
  attack_animation = "swipe_1",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/lanky_man_swipe",
  weapon_attack_animation = "swipe_1",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 500,
  step_distance = 32,
}

local combo_1 = {
  windup_animation = "swipe_2_windup",
  windup_duration = 900,
  attack_animation = "swipe_2",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/lanky_man_swipe",
  weapon_attack_animation = "swipe_2",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 10,
  step_distance = 32,
}

local combo_2 = {
  windup_animation = "swipe_1_windup",
  windup_duration = 90,
  attack_animation = "swipe_1",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/lanky_man_swipe",
  weapon_attack_animation = "swipe_1",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 10,
  step_distance = 32,
}

local combo_2b = {}
for k, v in pairs(combo_2) do combo_2b[k] = v end
combo_2b.recovery_duration = 400

local combo_3 = {
  windup_animation = "swipe_2_windup",
  windup_duration = 90,
  attack_animation = "swipe_2",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/lanky_man_swipe",
  weapon_attack_animation = "swipe_2",
  damage = 30,
  damage_type = "physical",
  recovery_duration = 800,
  step_distance = 32,
}

local combo_3b = {}
for k, v in pairs(combo_3) do combo_3b[k] = v end
combo_3b.recovery_duration = 10

local combo_4 = {
  windup_animation = "swipe_1_windup",
  windup_duration = 90,
  attack_animation = "swipe_1",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/lanky_man_swipe",
  weapon_attack_animation = "swipe_1",
  damage = 25,
  damage_type = "physical",
  recovery_duration = 10,
  step_distance = 32,
}

local combo_5 = {
  windup_animation = "swipe_2_windup",
  windup_duration = 90,
  attack_animation = "swipe_2",
  attack_soundset = "blunt",
  weapon_sprite = "enemies/tombwater_enemies/weapons/lanky_man_swipe",
  weapon_attack_animation = "swipe_2",
  damage = 30,
  damage_type = "physical",
  recovery_duration = 1300,
  step_distance = 32,
}


local combo_a = {combo_1, combo_2, combo_3}
local combo_b = {combo_1, combo_2, combo_3b, combo_4, combo_5}
local combo_c = {combo_2, combo_3}


function enemy:jump_attack()
  local windup_duration = 500
  local jump_speed = 200
  local max_jump_distance = 100
  enemy:stop_movement()
  local sprite = enemy:get_sprite()
  sprite:set_animation("jumping_windup")
  sol.audio.play_sound("enemies/townsfolk_vox/growl_01")
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_animation"jumping"
    local m = sol.movement.create"straight"
    m:set_angle(enemy:get_angle(hero))
    m:set_speed(jump_speed)
    m:set_max_distance(math.min(enemy:get_distance(hero), max_jump_distance))
    local function end_jump()
      sprite:set_animation"stopped"
      enemy:melee_combo(combo_c)
    end
    m:start(enemy, function() end_jump() end)
    m.on_obstacle_reached = end_jump
    sol.audio.play_sound("enemies/townsfolk_vox/screech_0" .. math.random(1,4))
  end)
end


local quick_attack_range = 32
local min_jump_range = 96
local max_jump_range = 160
local attack_cooldown_time = 3000

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  elseif distance <= quick_attack_range then
    enemy:melee_attack(quick_swipe)
  elseif distance < min_jump_range then
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 70,
      dist_threshold = 64,
      attack_function = function()
        local rand = math.random(1, 3)
        sol.audio.play_sound("enemies/townsfolk_vox/screech_0" .. math.random(1,4))
        if rand <= 2 then
          enemy:melee_combo(combo_a)
        else
          enemy:melee_combo(combo_b)
        end
      end
    })
  elseif distance >= min_jump_range and distance <= max_jump_range then
    enemy:jump_attack()
  else
    enemy:approach_then_attack{
      approach_duration = 400,
      speed = 70,
    }
  end
end

