local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

enemy.exp = 25
enemy.money = 3
enemy.weight = 10
enemy.base_damage = 15,

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 80,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 30,
})


local ranged_attack = {
  windup_animation = "gun_aiming",
  windup_duration = 1100,
  attack_sound = "fire_light_2",
  generic_projectile_type = "fireball",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  projectile_offset_radius = 16,
  damage = enemy.base_damage,
  num_projectiles = 6,
  projectile_sprite = "elements/fire",
  projectile_properties = {
    max_distance = 96,
  },
  recovery_duration = 600,
  recovery_animation = "gun_aiming",
}



local attack_range = 60
local too_close_range = 24
local attack_cooldown_time = 3000

function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  enemy:get_sprite():set_direction(enemy:get_direction4_to(hero))
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()
  --Retreat:
  elseif distance <= too_close_range and not enemy.retreat_cooldown then
    enemy:retreat{
      duration = 500,
      speed = 90,
    }
    enemy.retreat_cooldown = true
  --Attack:
  elseif enemy:has_los(hero) and not enemy.attack_cooldown then
    enemy:ranged_attack(ranged_attack)
    enemy.attack_cooldown = true
    enemy.retreat_cooldown = false --cannot retreat until after attacking
    sol.timer.start(map, attack_cooldown_time, function() enemy.attack_cooldown = false end)
  --Get a better angle:
  elseif not enemy:has_los(hero) then
    enemy:approach_hero({
      speed = 60,
      dist_threshold = attack_range,
      approach_duration = 500,
    })
  else
    sol.timer.start(enemy, 300, function() enemy:decide_action() end)
  end
end

