local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

--Stats
local max_life = 950
local sword_damage = 25
local shockwave_damage = 25


enemy.exp = 150
enemy.weight = 25
enemy.money = 10
enemy.damage_type_mods = {
  fire = 1.3
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = max_life,
  detection_distance = 200,
  abandon_hero_distance = 800,
  stagger_when_hurt = false,
  stagger_life_percent = 20,
  sprite_direction_style = "horizontal",
})


local slash_a = {
  windup_animation = "slash_a_windup",
  attack_animation = "slash_a_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/conquistador_sword",
  weapon_attack_animation = "slash_a_attack",
  step_distance = 24,
  step_tracking = true,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}

local slash_b = {
  windup_animation = "slash_b_windup",
  attack_animation = "slash_b_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/conquistador_sword",
  weapon_attack_animation = "slash_b_attack",
  step_distance = 24,
  step_tracking = true,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}

local spin_attack = {
  windup_animation = "spin_attack_windup",
  attack_animation = "spin_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/conquistador_sword",
  weapon_attack_animation = "spin_attack",
  step_distance = 40,
  step_tracking = true,
  damage = sword_damage * 1.2,
  damage_type = "physical",
  recovery_duration = 500,
}

local function combo_move(atab, windup, recovery)
  local atk = table.duplicate(atab)
  atk.windup_duration, atk.recovery_duration = windup, recovery
  return atk
end

local two_combo = {
  combo_move(slash_a, 400, 10),
  combo_move(slash_b, 100, 10),
}

local fast_combo = {
  combo_move(slash_a, 50, 10),
  combo_move(slash_b, 70, 10),
}


local scc1 = combo_move(slash_b, 600,10)
scc1.windup_sound = "enemies/townsfolk_vox/growl_02"
local spin_combo = {
  scc1,
  combo_move(slash_a, 50, 10),
  combo_move(spin_attack, 100, 10),
}


local big_slash_attack = {
  windup_animation = "big_slash_windup",
  windup_duration = 1000,
  windup_sound = "enemies/townsfolk_vox/big_moan_04",
  attack_animation = "big_slash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/conquistador_sword",
  weapon_attack_animation = "big_slash_attack",
  step_distance = 24,
  step_tracking = true,
  damage = sword_damage * 2,
  damage_type = "physical",
  recovery_duration = 1000,
  recovery_animation = "big_slash_recovery",
  collision_callback = function(hero)
    hero:ragdoll(enemy:get_angle(hero), 64)
  end,
}


local sword_dash = { --ram attack
  windup_animation = "dash_windup",
  windup_duration = 900,
  speed = 400,
  damage = sword_damage,
  attack_animation = "dash_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/conquistador_sword",
  weapon_attack_animation = "dash_attack",
  attack_sound = "enemies/slide_attack_metal",
  max_distance = 240,
  recovery_duration = 50,
  finished_callback = function()
    if enemy.do_dust then enemy.do_dust = false end
    enemy:decide_action()
  end
}


local slam_attack = { --melee attack component
  windup_animation = "jumping_hover",
  windup_duration = 10,
  attack_animation = "jumping_slam",
  attack_sound = "running_obstacle",
  --weapon_sprite = "enemies/trillium_enemies/shockwave_8x7",
  weapon_sprite = "items/shockwave_3x3",
  weapon_attack_animation = "stopped",
  damage = sword_damage + 20,
  turn_toward_hero = false,
  aoe_damage = shockwave_damage,
  aoe_scale = {1.5, 1.5},
  aoe_screenshake = 4,
  recovery_animation = "jump_recovery",
  recovery_duration = 1000,
  next_action = function()
    enemy.lock_facing = false
    enemy:decide_action()
  end,
}

local slam_attack_2 = { --melee attack with the jumping and stuff included. Doesn't work great lol.
  windup_animation = "jump_windup",
  windup_duration = 400,
  attack_animation = "jump_attack",
  attack_sound = "running_obstacle",
  damage = sword_damage + 20,
  turn_toward_hero = false,
  has_aoe = true,
  aoe_damage = shockwave_damage,
  aoe_scale = {1.5, 1.5},
  aoe_screenshake = 4,
  aoe_delay = 5000,
  recovery_duration = 1000,
  next_action = function()
    enemy.lock_facing = false
    enemy:decide_action()
  end,
}

function enemy:jump_slam()
  local windup_duration = 500
  local max_distance = 64
  local sprite = enemy:get_sprite()
  sprite:set_direction(enemy:get_facing_direction_to(hero))
  enemy:stop_movement()
  sprite:set_animation("jump_windup")
  sol.timer.start(enemy, windup_duration, function()
    sprite:set_animation("jumping")
    sol.audio.play_sound"jump" --TODO: change this
    local m = sol.movement.create"straight"
    m:set_angle(enemy:get_angle(hero))
    m:set_speed(300)
    m:set_max_distance(math.min(max_distance, enemy:get_distance(hero)))
    sprite:set_direction(enemy:get_facing_direction_to(hero))
    enemy.lock_facing = true
    m:start(enemy, function()
      sol.timer.start(enemy, 100, function()
        enemy:melee_attack(slam_attack)
      end)
    end)
  end)
end



--Decision thresholds:
local melee_distance = 160
local jump_distance = 72
local dash_distance = 80

function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  local sprite = enemy:get_sprite()

  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  --jump back if too close, then attack:
  elseif distance <= 24 then
    enemy:retreat({
      speed = 200,
      duration = 400,
      max_distance = 48,
      animation = "dash_recovery",
      lock_facing = true,
      callback = function()
        enemy:melee_combo(fast_combo)
      end,
    })
  
  --Spinning combo
  elseif distance <= melee_distance and random < 40 then
    enemy:approach_then_attack{
      speed = 55,
      approach_duration = 2000,
      attack_function = function()
        enemy:melee_combo(spin_combo)
      end,
    }

  --Jump attack:
  --Big slash attack:
  elseif distance <= jump_distance and random < 65 then
    --enemy:jump_slam()
    enemy:melee_attack(big_slash_attack)
 
  --Sword dash
  elseif (distance > dash_distance and random < 85) then
    --Make dust:
    enemy.do_dust = true
    sol.timer.start(enemy, sword_dash.windup_duration, function()
      local x, y, z = enemy:get_position()
      map:create_custom_entity{
        x=x, y=y, layer=z, width=16, height=16, direction=0,
        sprite = "entities/roll_effect",
        model = "ephemeral_effect",
      }
      if enemy.do_dust then return 60 end
    end)
    --Ram attack:
    enemy:ram_attack(sword_dash)

  --Quick combo
  elseif distance <= melee_distance then
    enemy:approach_then_attack{
      speed = 55,
      approach_duration = 2000,
      attack_function = function() enemy:melee_combo(two_combo) end,
    }

  else
    sol.timer.start(enemy, 400, function()
      enemy:decide_action()
    end)
  end
end

