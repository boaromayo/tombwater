local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local max_life = 350
local sword_damage = 30
local tentacle_damage = 40
local knife_damage = 20
local knife_status_effect = "poison"
local knife_status_amount = 25
local dodge_range = 48
local dodge_distance = 64
local gun_damage = 40
local bullet_reduction_rate = 2.5
local bullet_distance = 80
local gun_range_min = 40
local gun_range_max = 96
local stun_grenade_distance_min = 16
local stun_grenade_distance_max = 80
local long_melee_distance = 96


enemy.exp = 20
enemy.weight = 10
enemy.money = 7
enemy.damage_type_mods = {
  fire = .9
}

require("enemies/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = max_life,
  detection_distance = 200,
  abandon_hero_distance = 400,
  stagger_when_hurt = false,
  stagger_life_percent = 35,
})


local sword_forehand = {
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  step_distance = 24,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}


local sword_backhand = {
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  step_distance = 24,
  damage = sword_damage,
  damage_type = "physical",
  recovery_duration = 500,
}


local sword_strong = {
  windup_animation = "slam_windup",
  windup_duration = 600,
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/brawler_axe",
  weapon_windup_animation = "overhead_windup",
  weapon_attack_animation = "overhead_attack",
  step_distance = 24,
  damage = sword_damage * 2,
  damage_type = "physical",
  ragdoll_distance = 96,
  recovery_duration = 500,
}


local chest_tentacles = {
  windup_animation = "mouth_opening",
  windup_duration = 500,
  attack_animation = "mouth_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/chest_tentacles",
  weapon_attack_animation = "attack",
  damage = tentacle_damage,
  damage_type = "magic",
  recovery_duration = 650,
}


local knife_forehand = {
  windup_animation = "slash_forehand_windup",
  attack_animation = "slash_forehand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "slash_windup_tall",
  weapon_attack_animation = "slash_attack",
  step_distance = 24,
  damage = knife_damage,
  damage_type = "physical",
  recovery_duration = 500,
  collision_callback = function(hero)
    hero:build_up_status_effect(knife_status_effect, knife_status_amount)
  end,
}


local knife_backhand = {
  windup_animation = "slash_backhand_windup",
  attack_animation = "slash_backhand_attack",
  weapon_sprite = "enemies/tombwater_enemies/weapons/dagger",
  weapon_windup_animation = "backslash_windup_tall",
  weapon_attack_animation = "backslash_attack",
  step_distance = 24,
  damage = knife_damage,
  damage_type = "physical",
  recovery_duration = 500,
  collision_callback = function(hero)
    hero:build_up_status_effect(knife_status_effect, knife_status_amount)
  end,
}

local combo_a = table.duplicate(sword_forehand)
combo_a.windup_duration, combo_a.recovery_duration = 350, 10
local combo_b = table.duplicate(sword_backhand)
combo_b.windup_duration, combo_b.recovery_duration = 10, 10
local combo_c = table.duplicate(sword_forehand)
combo_c.windup_duration, combo_c.recovery_duration = 10, 250
local three_combo = {combo_a, combo_b, combo_c}

local combo_b2 = table.duplicate(combo_b)
combo_b2.recovery_duration = 250
local two_combo = {combo_a, combo_b2}


local blunderbuss_attack = {
  windup_animation = "gun_aiming",
  windup_duration = 500,
  attack_sound = "weapons/gun_shotgun_01",
  generic_projectile_type = "bullet",
  projectile_sprite = "entities/enemy_projectiles/bullet",
  projectile_offset_radius = 8,
  damage = gun_damage,
  num_projectiles = 12,
  projectile_properties = {
    max_distance = bullet_distance,
    distance_damage_reduction_rate = bullet_reduction_rate,
  },
  recovery_animation = "gun_aiming",
  recovery_duration = 400,
}

local gun_slash = table.duplicate(sword_backhand)
gun_slash.recovery_duration = 100
gun_slash.next_action = function() enemy:ranged_attack(blunderbuss_attack) end


function enemy:stun_gas_attack()
  local windup_time = 350
  local num_clouds = 5
  local spread = math.pi / 3
  local cloud_offset = 16
  local cloud_distance = 64
  local stun_duration = 1000
  local recovery_duration = 700

  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  sprite:set_direction(enemy:get_direction4_to(hero))
  sprite:set_animation("throwing_windup")
  sol.timer.start(enemy, windup_time, function()
    local direction = sprite:get_direction()
    local x, y, z = enemy:get_position()
    sprite:set_animation("slash_forehand_attack", "stopped")
    sol.audio.play_sound("throw")
    for i = 1, num_clouds do
      local cloud = map:create_custom_entity{
        x = x + game:dx(cloud_offset)[direction],
        y = y + game:dy(cloud_offset)[direction],
        layer = z, width = 16, height = 16, direction = 0,
        sprite = "items/cloud",
      }
      cloud:get_sprite():set_animation("gas", function()
        cloud:remove()
      end)
      cloud:add_collision_test("sprite", function(cloud, other)
        if other:get_type() == "hero" then
          cloud:clear_collision_tests()
          other:stun(stun_duration)
        end
      end)
      local m = sol.movement.create"straight"
      local angle = (direction * math.pi / 2) - spread + (spread / num_clouds * i)
      m:set_angle(angle)
      m:set_max_distance(cloud_distance)
      m:set_ignore_obstacles(true)
      m:start(cloud)
    end
    sol.timer.start(enemy, recovery_duration, function()
      local state, state_ob = hero:get_state()
      if state == "custom" and state_ob:get_description() == "stunned" and (math.random(1,2) == 1) then
        enemy:melee_attack(sword_strong)
      else
        enemy:decide_action()
      end
    end)
  end)
end


function enemy:oil_throw()
  local windup_time = 350
  local recovery_duration = 100
  local num_oil = 15
  local oil_step = 12
  local oil_deviation = 16
  local ignition_delay = 200

  local sprite = enemy:get_sprite()
  enemy:stop_movement()
  local direction = enemy:get_direction4_to(hero)
  sprite:set_direction(direction)
  sprite:set_animation("throwing_windup")
  sol.timer.start(enemy, windup_time, function()
    sprite:set_animation("slash_forehand_attack", "stopped")
    sol.audio.play_sound"throw"
    local x, y, z = enemy:get_position()
    for i = 1, num_oil do
      local dx = math.cos(direction * math.pi / 2) * oil_step * i + math.random(oil_deviation * -1, oil_deviation)
      local dy = math.sin(direction * math.pi / 2) * oil_step * i + math.random(oil_deviation * -1, oil_deviation)
      sol.timer.start(map, 25 * i, function()
        sol.audio.play_sound("vox_blips/bloop_" .. math.random(4, 7))
        local oil = map:create_custom_entity{
          x = x + dx, y = y + dy, layer = z,
          width = 16, height = 16, direction = math.random(1, 4),
          sprite = "entities/enemy_projectiles/oil_blob",
        }
        sol.timer.start(oil, math.random(ignition_delay - 100, ignition_delay + 100), function()
          local ox, oy, oz = oil:get_position()
          map:create_fire{x = ox, y = oy, layer = oz,}
          sol.timer.start(oil, 100, function() oil:remove() end)
        end)
      end)
    end

    sol.timer.start(enemy, recovery_duration, function()
      enemy:decide_action()
    end)
  end)
end




function enemy:decide_action()
  local random = math.random(1, 100)
  local distance = enemy:get_distance(hero)
  if enemy:should_lose_aggro() then
    enemy:return_to_idle()

  elseif random < 18 or (enemy:get_life() < max_life * .35 and random < 30) then
    enemy:approach_then_attack{
      speed = 85,
      attack_function = function() enemy:melee_attack(chest_tentacles) end,
    }

  elseif random < 35 and not enemy.gas_cooldown then
    enemy.gas_cooldown = true
    sol.timer.start(map, 3000, function() enemy.gas_cooldown = nil end)
    enemy:stun_gas_attack()

  elseif random < 65 and distance >= gun_range_min and distance <= gun_range_max and not enemy.gun_cooldown then
    enemy.gun_cooldown = true
    sol.timer.start(map, 3000, function() enemy.gun_cooldown = nil end)
    enemy:ranged_attack(blunderbuss_attack)

  elseif random < 85 and distance < long_melee_distance then
    enemy:approach_then_attack{
      speed = 85,
      attack_function = function() enemy:melee_combo(three_combo) end,
    }

  elseif random < 95 and distance < long_melee_distance then
    enemy:approach_then_attack{
      speed = 85,
      attack_function = function() enemy:melee_attack(sword_strong) end,
    }

  else
    enemy:approach_then_attack{
      speed = 85,
      attack_function = function() enemy:melee_attack(gun_slash) end,
    }
  end
end


function enemy:ambush_attack()
  enemy:melee_attack(ambush_attack)
end

